﻿#PART 1: THE GOLDEN APPLE #

label part1:
    #scene black with fade
    #centered "{size=36}EMERALD FOREST - 11.13 AM{/size}"
    
    scene bg eforest with fade
    rho "Alright!"
    show rhodo arm smile at midleft with dissolve
    rho "That's the fourth Ursa I killed today."
    rho "New record!"
    show anya nor smile at midright with dissolve
    any "Great job, Rhodo."
    rho arm smile "Hehe, thanks."
    hide rhodo with dissolve
    show rhodo nor default at midleft with dissolve
    rho "Anyway, if my counting's right, we should've hit the quota, right?"
    hide anya with dissolve
    show ulm nor default at midright with dissolve
    
    ulm "According to my calculation ... yes, we already hit the quota."
    rho nor confused "I don't really get it how they decide our killing quota for this mi--"
    ulm nor surprised "Err ... I'm sorry. We {i}exceed{/i} the quota."
    rho nor surprised "W-what?! "
    rho "But it said they'd cut our credit if we kill too many!"
    hide rhodo with dissolve
    show anya nor default at midleft with dissolve
    any "..."
    
    any nor sigh "*sigh*"
    any "How many?"
    ulm "{size=15}It's ... 5 Beowolves ...{/size}"
    hide ulm with dissolve
    show rhodo nor surprised at midright with dissolve
    rho "That's not me!"
    rho "I counted each of them and I didn't miss any!"
    rho "I swear!"
    "Both Rhodo and Anya turn around to look at me, who has been keeping quiet for the last minute."
    hide anya with dissolve
    
    show rhodo nor shout at midleft with dissolve
    rho "I thought you stopped showing off!"
    show mei nor angry at midright with dissolve
    mei "I am not showing off! Why would I deliberately eliminated them with the quota rule?"
    mei "They ambushed me!"
    mei "What do you expect me to do? Standing still and let them shredded me to pieces?"
    rho "You can freeze their feet and run!"
    rho "Or block them with that huge ice wall of yours!"
    show mei nor frown2 with cdissolve
    "I open my mouth to answer, but he made valid points there."
    "I don't really want to admit that the surprise attack made me panicked and my first reflex was going all out with those Grimm creatures ...."
    "... I really have to try better in maintaining my composure these days ...."
    rho nor frown "Or you should've skipped from inputting the number into your Scroll."
    rho "The counting isn't automated."
    
    "..."
    mei nor sad "That's ...."
    hide rhodo with dissolve
    "Rhodo walks away, grumbling about me ruining his plan to make up for his grades."
    show anya nor default at midleft with dissolve
    any "So ...?"
    mei "I'm sorry ...."
    mei "I'm really sorry ...."
    any "Just like Rhodo said, you can actually not inputting the extra number into our counting."
    mei nor frown2 "But that's cheating."
    any nor sigh "I appreciate your effort to be a honest person."
    any "But, do understand that 'honesty' doesn't mean you tell {i}everything{/i} to other people."
    any "Only what they need to know."
    "It's not the advice I want to hear now, but since I've seen first hand how my 'honesty' brought trouble to us, I couldn't help not to agree with Anya in this matter."
    
    any nor default "I'll try to talk with Professor Port later."
    show mei nor default2 with dissolve
    any frown "... Going to spend a long time listening to his story, I guess ...."
    any nor default "Oh."
    any nor smirk "Of course I don't have to suffer alone."
    any "You'll come with me."
    mei nor frown "Like I have another choice ...."
    any "And if I failed, I'll send you and Rhodo to Doctor Oobleck. Give him a little souvenir I found at the ruins."
    any "You'll be the one who explains what this piece is to him."
    mei nor surprised "Wha--?!"
    mei "What's that for???"
    any "Convince him to add some bonus points for Rhodo."
    mei "But he wasn't the one who found this artifact!"
    any nor smile "No problem. They don't exclude Rhodo's grades for the ranking."
    any "One for all and all for one."
    any nor default "And Mei?"
    mei nor default2 "Yes?"
    any nor frown "I know you haven't really got over that incident, but please keep yourself from overuse your summon."
    any "Especially that knight."
    any "It drains your Aura too much now."
    mei nor sad "I understand."
    mei "I'm sorry. I would do better next time."
    hide anya with dissolve
    hide mei with dissolve
    
    any "Rho, done with your weeping business?"
    any "We gotta go to rendezvous point."
    "Rhodo replies with a lazy groan, but he approaches Anya and both of them have a conversation in low voices which I couldn't hear."
    "I could only guess that Anya promises him everything is going to be okay, because Rhodo looks relieved after that."
    "Now that I've spent almost two years with Anya as our team leader, I couldn't imagine how would this team hold up without her."
    
    play music forest fadein 10.0
    "Today, as the end of fourth semester draws near, we, second year students are tasked with a mission to prepare and secure the Initiation Ground."
    "The mission is simple: eliminate the Grimms in Emerald Forest."
    "Beacon Academy authorities had bordered a portion of Emerald Forest for new students' initiation purpose."
    "What we need to do is making sure the Initiation Ground isn't too dangerous for the participants, by killing Grimms following a quota assigned for each team."
    "The border system scanned and counted Grimm population inside the Initiation Ground and decide the quota for us."
    "I heard the system had been there for years. Probably since my parents' year in Beacon Academy, which is about 20 years ago."
    
    "Anya's Scroll is beeping."
    "She answers the calling, putting her Scroll to speaker mode so all of us could hear it."
    "It's Professor Port, our supervisor for this mission."
    stop music fadeout 5.0
    show anya nor default at mcenter with dissolve
    any "Yes, Professor?"
    por "Oh, thank goodness! Where's your position now, team?"
    any "Position ... Wait ...."
    "Ulm shows his Scroll to Anya, who proceed to tell Professor Port."
    
    any "Is something wrong, Professor?"
    por "I really hope not, but team SKYE hasn't answered our call and their location signal has stayed in the same place for the last half an hour."
    play music strange fadein 10.0
    any nor frown "We'll check on them. Can we have their location, Professor?"
    por "Yes, I'll send it to you. Hold on."
    hide anya with dissolve
    "There's a notification of incoming position data in Anya's Scroll."
    show rhodo nor surprised at midleft with dissolve
    rho "That's quite far from here."
    show ulm nor default at midright with dissolve
    ulm "If we don't meet any Grimms on our way, we can reach it in--approximately--10 minutes."
    
    hide rhodo with dissolve
    show anya nor default at midleft with dissolve
    any "There's no faster way?"
    ulm nor thinking "Uh, well, there is ...."
    ulm nor default "If Miss Schnee uses her Glyph to bypass everything on the way, it will be 7 minutes or less."
    hide ulm with dissolve
    show mei nor default at midright with dissolve
    mei "Shall I?"
    any "..."
    extend "..."
    
    show anya nor sigh with cdissolve
    extend "..."
    any nor default "Okay."
    any "We'll catch up with you as soon as possible."
    any "Keep in contact and don't do anything reckless."
    mei nor smile "Yes, Ma'am."
    any nor smile "Be careful."
    hide mei with dissolve
    hide anya with dissolve
    
    "With slight hand movement, I activate my Semblance. Glowing Glyph appear before me."
    play sound glyph
    mei "You better not slack off!"
    rho "Just go already, Schnee!"
    "I laugh and jump on the Glyph, gaining some height before make another series of Glyph I use to jump and glide."
    scene cg glyphglide with fade
    "Looking back to my first year, it certainly isn't my style to ask permission for action like what I just did earlier."
    "Spending two years together with my current team taught me to work together and to trust others' decision."
    "I still find it strange."
    "I had hard times trusting anyone outside of my family."
    "Maybe it's because I grew up among peoples who clearly want to get close to me just because I'm a Schnee, the family who owns giant corporation Schnee Dust Company."
    "Or maybe it's because I believe in darker things about my destiny."
    "It isn't important right now. Destiny and things."
    
    "I wonder what happened to team SKYE .... They're a decent team, which stays in top 10 rank. They wouldn't be easily wiped out by the Grimms."
    scene bg eforest with fade
    "I land on a clearing in the middle of the forest. My destination isn't too far from here. I could walk."
    "Yes, I actually hold myself from using my Semblance too often, like Anya told me."
    "And, yes, a Schnee could actually walk on their two feet. Don't be so surprised to know such a thing!"
    "By the way, I don't see any sign of team SKYE around here."
    show mei nor frown2 at mcenter with dissolve
    mei "Anya?"
    any "Yeah? You find something?"
    mei "That's the thing ... I don't find team SKYE anywhere."
    any "Hmm ...."
    
    mei  "I'll go looking around a little bit."
    any "Be careful. We got a little detour here. Beowolves pack on our way."
    mei "Roger that."
    hide mei with dissolve
    "I look around for some more."
    "On closer look, I find signs of a fight. But those signs are strange: deep cuts on the trees, neatly sliced grass ..."
    "It's as if team SKYE fought against something ... 'less Grimm'? "
    "I'm not sure ...."
    "I put my hand on Alpenaster's hilt, just in case."
    "I step on something."
    
    "It's a Scroll."
    show mei nor surprised at mcenter with dissolve
    mei "Hey, Anya?"
    mei "I hope you're not taking any longer. I really have a bad feeling about this."
    any "What happened?"
    mei "I found a Scroll. I believe it's one of team SKYE's."
    mei nor sad "But they're not here."
    mei "And there was signs of fight, but I doubt it's against Grimms."
    any "... That's weird."
    
    any "Stay where you are, Mei. We're close ... I think?"
    ulm "Yes, we're close."
    hide mei with dissolve
    "This mission is supposed to be a walk in a park. I mean, a walk in {i}a forest{/i}."
    "The Grimms in Emerald Forest aren't as many as in other areas in Vale. They're mostly Beowolves and Ursas. In rare occasion, King Taijitu, Deathstalker, and Nevermore."
    "Sounds like a merry party? Believe me, it's nothing compared to Mountain Glenn."
    "Mountain Glenn is {i}infested{/i} by the Grimms."
    "Rumor said, there's also mutated Grimms in the underground portion of Mountain Glenn."
    "Anya and the others show up, just right when I start to think about picnic."
    "Why picnic? "
    extend "I don't know either."
    
    show mei nor frown2 at midright with dissolve
    show anya nor default at mcenter with dissolve
    show rhodo nor surprised at midleft2 with dissolve
    mei "Here."
    "I hand over the Scroll I picked to Anya. Ulm immediately notices the signs of combat all over the place and he examines it. Rhodo follows him closely."
    any nor frown "Rhodo, Ulm, stay close and go check over that side."
    any "Mei and I will go this way."
    rho nor frown "Okay."
    hide rhodo with dissolve
    hide anya with dissolve
    hide mei with dissolve
    "Anya takes out her weapon and walks slowly toward a group of trees on our right side."
    
    
    "Rhodo once commented that Anya's weapon--a cane and a pistol--are ridiculous."
    "But I don't share same opinion with him."
    "I know someone who use walking stick as a weapon and his partner uses umbrella. Both of them are skillful combatants."
    "Besides, I noticed sometime ago Anya implied that she has a more personal reason why she pick a cane as her weapon."
    "Back to our search for any hints for team SKYE ...."
    "We look around for a while, but couldn't find anything that would lead us to the whereabouts of team SKYE."
    "There are bullet shells scattered on the ground, but we couldn't be sure whether they are leftovers from another battle happened long time ago or just today."
    "Most weapons in Remnant utilize the usage of bullet. It's totally common to find a hybrid of firearms and blade being used by Huntsmen and Huntresses."
    mei "I'm not really sure we would find whatever we look for ...."
    any "Well ... maybe not us."
    any "Call Ulm here. I need his help."
    any "And stay with Rhodo in open space. I need you two to be ready for any attacks."
    mei "Got it."
    "I nod and rush to where Ulm is."
    
    show mei nor default2 at midright with dissolve
    show ulm nor default at midleft with dissolve
    mei "Ulm, Anya needs you."
    ulm "Oh? Okay. Be right there."
    hide ulm with dissolve
    show rhodo nor surprised at midleft with dissolve
    rho "What's going on?"
    mei nor frown2 "I think Anya wants Ulm to 'sightjack' team SKYE if they're nearby."
    rho "Uh ...?"
    show mei nor frown with dissolve
    
    "Rhodo is {i}always{/i} the slowest when it comes to thinking."
    mei "Ulm's Semblance, Sightjack, allows him to see from other creature's eyesight as long as the the target is conscious."
    rho nor default "Oh ... I get it now."
    rho nor surprised "We should back them up, shouldn't we? Not staying here like this."
    mei nor default "Anya would call us if--"
    stop music fadeout 5.0
    
    show mei nor angry
    show rhodo nor frown
    with cdissolve
    "There's something coming from behind."
    "I take Alpenaster out."
    "Rhodo unsheathes his spear, Leonidas."
    "And both of us turn around."
    
    hide mei
    hide rhodo
    with qdissolve
    
    # dodge QTE here? #
    # SFX here
    play music intense fadein 10.0
    "I manage to avoid the incoming attack." with hpunch
    play sound hardhit
    "Rhodo takes the chance and slams the assailant with his shield." with hpunch
    "It's Saul, the leader of team SKYE."
    rho "Dude! Saul!"
    rho "Damnit! Why did you do that???"
    "Saul lunges at us again."
    "I parry his attack and activated one of Dust cartridge inside Alpenaster. The dagger's blade glows with icy blue color."
    
    play sound ice
    "I channel my Aura into Alpenaster, take a careful aim, and, with a slight wrist movement, ice forms on the ground, trapping Saul's halberd in it."
    show rhodo arm surprised at midright with dissolve
    rho "What's wrong with him???"
    show mei arm angry at midleft with dissolve
    mei "I don't know!"
    rho arm frown "Oh, damnit! Here he comes!"
    
    hide rhodo
    hide mei
    with qdissolve
    "Saul doesn't say anything. He just grunts and growls and leaves his stuck halberd to attack us again."
    "Rhodo blocks Saul's punch." with hpunch
    "I crouches and slashes his feet, throwing him off balance."
    
    play sound ice
    "I cast another ice, this time trapping Saul's foot so he couldn't move anywhere."
    show mei arm angry at midleft with dissolve
    mei "Hit his head! Knock him out!"
    show rhodo arm surprised at midright with dissolve
    rho "We can't do that! "
    rho "It's Saul! {i}One of our friend{/i}!"
    mei "Drastic situation needs drastic measure! Do it!"
    rho "Urgh ...."
    rho arm frown "Alright."
    rho arm shout "Blame Schnee for this, dude!"
    hide rhodo with qdissolve
    hide mei with qdissolve
    play sound hardhit
    rho "Heyaah!" with hpunch
    
    stop music fadeout 5.0
    scene black with fade
    #centered "{size=36}BEACON ACADEMY - 18.50 PM, the same day{/size}"
    
    
    "Our mission ended up quite ... "
    extend "odd."
    "After we incapacitated Saul, we reported to Professor Port, who proceeded to send another team for back-up."
    "We found the rest of team SKYE. They hid pretty far from where we met Saul."
    "From their statement, Saul suddenly attacked them. "
    extend "Without provocation."
    "They swore that Saul wasn't his usual self. Something was off. He might be not aware of his own action."
    
    scene bg dininghall with fade
    play music casual fadein 10.0
    "The first rumor spread during dinner time is a mutant Grimm that causes hallucination attacked team SKYE."
    
    "Must be the School Journalist Club or the Conspiracy Theory Debunker Group who starts it."
    show rhodo uni confused at mcenter with dissolve
    rho "Oookay ... that's stupid. All those theories about new Grimm species."
    rho "We know Grimms are just Grimms. They're not changing much during these years."
    show rhodo uni confused at midleft with dissolve
    show ulm uni thinking at midright with dissolve
    ulm "Actually, that's not true. Some Grimms species have shown improved intelligence and changes in behaviour."
    rho uni surprised "No. Way."
    rho uni frown "Nuh uh. Nope."
    
    ulm uni smile "That's Professor Oobleck's lecture, Rho ...."
    rho uni surprised "Uh? Really?"
    rho "But we still manage to kill many of them easily."
    rho uni default "Most of them, I mean."
    hide ulm with dissolve
    show mei uni frown at midright with dissolve
    mei "Because human beings have advantage in thinking process. "
    mei "Inventions, innovations, technologies, new discoveries, you name it."
    
    mei uni default "And we do {i}research{/i}."
    mei "It makes difference with the Grimm who learn by doing or mere experience."
    rho uni smile "Ahem."
    rho "{i}In Schnee Dust Company, we do extensive research and development to further improve humanity.{/i}"
    show mei uni frown with cdissolve
    "Rhodo doesn't hate Schnee Dust Company (SDC) like certain groups I know."
    "He just likes to imitate the lady in one of SDC's commercial after I went too far explaining Dust to him and he said I sound like the marketing person of SDC."
    rho "Thank you very much, SDC."
    "Sometimes he adds non-sensical sarcasm, which I doubt he has real reason to do it or even understands the purpose of sarcasm."
    "Rhodo does things without thinking, most of the time."
    show mei uni default with cdissolve
    "And I play along."
    
    mei uni cocky "Well! Thank you!"
    rho uni confused "That's not a compliment!"
    hide rhodo with dissolve
    hide mei with dissolve
    show anya uni sigh at mcenter with dissolve
    any "What if I say that I have a hunch Journalist Club is currently planning to interview us?"
    any "Ten o'clock from me. The girl with twintail."
    ulm "Uh oh ..."
    hide anya with dissolve
    
    "The Journalist Club is harmless."
    "Really."
    "They're just {i}very annoying{/i} when chasing news."
    "Ulm pressed his palm on his right eye which covered with a bandana. His left eye glows and there's a shiny circular shape floating a few centimeters from his eye."
    "It's 'Sightjack', Ulm's Semblance."
    show ulm uni default at mcenter with dissolve
    ulm "They have a list and .... "
    ulm uni surprised "Yeah, our team name is in there."
    hide ulm with dissolve
    show rhodo uni frown at midleft with dissolve
    
    rho "Damnit those gossip group."
    show anya uni smile at midright with dissolve
    any "Now, now, I don't think they're going to be so aggressive this time."
    any "There's rules, after all, about spreading rumor for sensitive case like this."
    hide rhodo with dissolve
    show ulm uni default at midleft with dissolve
    ulm "Ah, yes. Beacon Academy Rules, article number 23, point b."
    hide anya with dissolve
    hide ulm with dissolve
    
    "In my opinion, Ulm wastes his brain to memorize things like detailed school rules."
    "But, he's capable of it. Ulm has amazing brain capacity and he's smart."
    "I could say I {i}am{/i} smart too, but not that smart."
    "By the way, Anya is right about the Journalist Club."
    stop music fadeout 5.0
    
    scene bg corridor with fade
    "One of them catch up with us in our way back to our room and asks questions regarding team SKYE."
    "Anya handles the amateur journalist with few words, mentioning Professor Port as the supervisor of the mission."
    "She even tells the journalist, who is a first year student, some secrets about how to make Professor Port talks more than he should."
    "The first year leaves with very few information from us, but a bunch of satisfaction from learning some 'secrets'."
    "I could even say that the first year looks amazed with Anya."
    
    scene bg dormnight2 with fade
    play sound door
    rho "That's ... clever."
    any "Just some useful trick I learned."
    "I have my own suspicion that Anya's father--who she rarely mentioned--is a gang or mafia leader that has extensive experience in negotiation."
    rho "By the way ... "
    
    play music discussion fadein 10.0
    scene cg dormtalk with fade
    rho "Do you guys ever hear something called 'The Golden Apple'?"
    
    any "Isn't it something from old folklore?"
    mei "Or fairy tales?"
    rho "There's such thing in folklore???"
    "I pinched the bridge of my nose. How could such uninformed human being ever exist?"
    rho "Schnee! Stop doing that! "
    rho "I honestly don't know if there's such thing in old stories!"
    
    mei "Well, {i}there is{/i} golden apple in old stories."
    mei "*sigh* "
    extend "So, what's The Golden Apple you spoke about?"
    rho "I'm not really sure ..."
    rho "From what I heard, it's something ... uhm, what's the word for 'can be consumed'?"
    mei "'Consumable'."
    rho "Ah, right. That's the word."
    
    "Rhodo didn't mean to test our vocabulary, did he?"
    any "The name sounds like drinks brand for me."
    any "A new variant for Crimson Aries line-up?"
    rho "I'll know if it is. No, it isn't."
    rho "But it does sound like drinks name. I overheard someone talking about it sometime ago."
    
    ulm "Maybe it's some kind of vitamin? "
    ulm "You don't usually curious about such things. Why now?"
    mei "Because Rhodo is a freak for energy drinks."
    rho "Hey! Watch it! I'm a {i}loyal fan{/i} of energy drinks."
    rho "And nothing could beat Crimson Aries until I decide so!"
    "We talk about random topics for another hour before go to sleep that night."
    stop music fadeout 5.0
    
    scene bg dormnight with fade
    
    "With the lights off and I'm lying in bed, I check my Scroll."
    "No new message."
    "Mom messaged me in the afternoon, asking about how's the mission going."
    "I wanted to tell her about the case with team SKYE, but I didn't do it in the end. "
    "I don't want to make her worry. We don't even know what's really happened back then."
    "I told her everything's alright and that the initiation for new students is still the same as her initiation years ago."
    "She said, 'I think Beacon being less creative and less challenging by the years'."
    "..."
    "Thinking about it again, being launched to Emerald Forest and returning back after retrieve artifact placed in the ruins doesn't sound as 'challenging' as Haven Academy's initiation."
    "Haven's new students are divided into groups and each group is left behind in a designated forest for several days. They have to retrieve supplies every day from a drop point deeper in the forest."
    "And, yes, no signal for the Scroll to function properly. They have to rely on {i}real map{/i} made from paper and old transmission computer from decades ago that shows coordinates in green color on black surface."
    
    "Rather than 'challenging', 'torturing' might be a more appropriate word to describe that initiation."
    "..."
    "I know someone who went through Haven Academy's ruthless initiation."
    "I'm staring at her name on my Scroll right now, unsure whether to tell her about what happened in today's mission or not."
    "It's been almost half a year since we last met and I really miss her."
    "Really, {i}really{/i} miss her."
    "I already typed on my Scroll, halfway telling her detailed story about today's mission. I erase all of them and type: "
    "'Hows school today, Li?'"
    "It's midday on Remnant part where she lives now and she still has classes, so I don't really bothered with late reply."
    "And with that, I put away my Scroll and curl up under the blanket."
    
    #scene black with fade
    #centered "{size=36}BEACON ACADEMY - 08.29 AM, the next day{/size}"
    scene bg dormday with fade
    "There's an announcement broadcasted throughout the halls and dorm rooms this morning, just before we leave for class."
    "I kinda hope it's about no class in the morning."
    ann "To all students, all morning classes have been canceled."
    "For real?"
    ann "All students please immediately gather in the amphitheater for an urgent matter."
    any "'Urgent matter'?"
    any "I don't like the sound of that."
    "Neither am I."
    "I wonder what has happened ...?"
    
    scene bg amphitheater with fade
    play music casual fadein 10.0
    "The amphitheater has already been filled with about half of Beacon students when we arrive."
    "There are five semi-transparent screens in the amphitheater, hanging ten or twelve feet above the floor."
    "Each screen shows teams' ranking, grouped by year."
    "I take a glance at the second years' screen."
    "Our team's name--team AURM--is still sitting on the first position, like it should."
    "I wouldn't call myself a 'perfectionist', although some people do."
    "I set higher standard for myself, which I believe helps me improve."
    "I have my own reasons why I want to become stronger."
    "... Especially after {i}that incident{/i}."
    
    stop music fadeout 5.0
    "Professor Goodwitch steps up to the podium. Upon seeing her, the hall engulfs in silence."
    "None of us want to anger Glynda Goodwitch. She is a serious and upright woman that you should never argue with."
    "With a single swing of her hunting crop, she'd easily fling around anything on her way."
    gly "Good morning, students."
    gly "We apologize for the inconvenience--"
    show rhodo uni confused at midleft with dissolve
    rho "Really? {i}Really?{/i}"
    rho uni smile "Canceled classes is an inconvenience? It's the most awesome thing you can find at school!"
    
    show mei uni frown at midright with dissolve
    mei "You're so immature if you think it that way."
    rho uni frown "What's your problem with me, Schnee?"
    mei "Nothing."
    rho uni confused "Tch. Fine ..."
    show anya uni sigh at mcenter with dissolve
    any "Be quiet you two ..."
    hide rhodo with dissolve
    hide mei with dissolve
    hide anya with dissolve
    
    "Of course Rhodo doesn't need to know that I also feel delighted whenever I hear class is canceled. "
    "That's called 'dignity'."
    gly "--as you might have known thanks to an active and diligent effort of amateur journalists of Beacon Academy--"
    "... I have to admit that I'm surprised hearing Professor Goodwitch doesn't even hide her sarcasm when she mentioned the School Journalist Club."
    gly "--that there was a problem with one of the team deployed in yesterday's mission."
    gly "Specifically, the mission regarding preparation of the Initiation Ground."
    gly "In response to that, the school authorities have decided to perform examination on each students."
    "Right after the word 'examination', there is curious and perplexed murmur from the students."
    "Professor Goodwitch doesn't seem to be bothered by the crowd and continues:"
    gly "Details on the examination will be sent to each team leader. Please make sure you follow the schedule."
    gly "That's all from me. Thank you very much for your attention."
    
    "And with that, Professor Goodwitch leaves the podium, ending her announcement."
    show anya uni default at midleft with dissolve
    any "I couldn't really relate the accident and this examination."
    show mei uni frown2 at midright with dissolve
    mei "I couldn't either."
    hide anya with dissolve
    show ulm uni default at midleft with dissolve
    ulm "It's a 'substance test'."
    ulm uni thinking "Like, when people need to figure out if someone use a certain chemical substance or not."
    
    mei uni surprised "Where do you know about that, Ulm?"
    mei "Professor didn't mention it."
    ulm uni surprised "Oh! Umm ... I Sightjack-ed her just a few moment ago."
    ulm uni reluctant "I saw her holding a small note and read the word 'substance test' on it."
    show mei uni frown2 with cdissolve
    "I'll be honest."
    "I didn't realize Ulm Sightjack-ed Professor Glynda."
    "I didn't notice the usual glowing 'magic circle' in front of Ulm's left eye."
    "Hmm ...."
    "Maybe I just miss it since I focus on Professor Glynda's announcement."
    mei "Does Beacon suspect some students of abusing drugs?"
    hide ulm with dissolve
    show anya uni sigh at midleft with dissolve
    any "Probably."
    any uni default "The ranking system put pressure for some and give them enough reason to start taking doping."
    any uni frown "...."
    mei uni default2 "What is it?"
    show anya uni default with cdissolve
    "Anya just shrugs."
    mei uni frown2 "Nothing important?"
    hide anya with dissolve
    hide mei with dissolve
    "I know Anya withholds something. There's a small pause and slight expression changes that indicates it."
    "If you've been together with a person for almost two years, you start noticing hints like that."
    "In my case, I actually learned how to read gesture and body language in early ages."
    "It's useful for most situation in my life ... as a Schnee. As the Heiress."
    
    scene bg corridor with fade
    
    "Later on, when Rhodo and Ulm go to different building for the examination, I tell Anya what has been in my mind."
    show mei uni default at midleft with dissolve
    mei "So ...."
    mei "Do you have something to tell me?"
    show anya uni default at midright with dissolve
    any "About what?"
    mei uni frown2 "Something personal that you don't want the three of us to know. Probably related to the doping matter?"
    mei uni frown "Which I insist to know."
    any uni smirk "Ha."
    any "Never know you like to snoop around like the journalism club."
    mei uni angry "It's ... {i}different{/i}!"
    mei "They look into any hot issues from anyone. They don't actually care about the news they've gotten."
    mei "I {i}care{/i}."
    
    "Anya raises her eyebrows."
    any uni default "I'm surprised."
    any uni smile "And, no, this isn't sarcasm."
    any "I'm really surprised to hear you say that."
    mei uni surprised "Say ... what?"
    any "That you're care."
    mei uni frown2 "Well! I do care!"
    mei uni frown "Why shouldn't I care about my partner?"
    show mei uni default2 with cdissolve
    "That makes me thinking ...."
    "Am I really that ignorant about my partner and teammates?"
    mei uni frown2 "At least I'm trying, am I ...?"
    show mei uni surprised with cdissolve
    "Oh, no. "
    extend "I shouldn't say that."
    show mei uni sad with cdissolve
    "Now Anya would think I'm just curious or not trying hard enough."
    "Argh."
    show mei uni frown2 with cdissolve
    "Great, Mei, {i}just great{/i}."
    
    any uni sigh "Well, okay ... I suppose I can tell you about it."
    show mei uni default with cdissolve
    any uni default "I trust you, so you better not blow this up to anyone else."
    "Anya takes a deep breath."
    play music contemplation fadein 10.0
    any uni frown "I tried taking doping in the past. A prescribed one."
    show mei uni surprised with cdissolve
    any "Of course it's illegal. I'm not supposed to take any of those pills, but I bought it from someone."
    show mei uni sad with cdissolve
    "Oh, my God ..."
    "I'm not prepared to hear that. I have to try really hard to maintain my neutral expression. I don't want to look judgmental."
    mei "You ... did?"
    any "Yeah. When I was 12."
    
    any uni default "You knew from the very first time we met that I'm not particularly a 'good girl', didn't you?"
    mei uni default2 "May I ask why?"
    mei uni sad "Why did you take them?"
    any "I was ...."
    any uni sigh "I don't know, I tried to be 'special'."
    any uni frown "My Semblance is copying other people's Semblance--partially--up to 4."
    any "Although I'm the only one in my family that have Semblance, I'm not satisfied."
    any "I envy everyone with {i}original{/i} Semblance. It's their signature."
    any "Something unique they have. Unlike mine."
    any "So, I turned to doping. Hoping my Semblance could become powerful enough and I can be proud of it."
    any "Unlimited copies. Or such."
    mei "But you managed to stop."
    
    any uni default "Yeah. Mom found out."
    mei uni surprised "W-what!?"
    any "Mom figured out I wasn't myself when the pills affected me. I became more passive, almost apathetic, I think."
    any uni frown "Mom didn't say it directly. She just asked me what's wrong and told me how I changed."
    show mei uni default2 with cdissolve
    any "When she said it, it didn't sound like a right thing."
    any "I never want to disappoint her."
    show mei uni sad with cdissolve
    "I could understand it. I don't want to disappoint my mother either."
    mei "So you stopped?"
    any uni default "So I stopped."
    any uni frown "That's why I kinda understand why people taking doping and such. It's the easiest way to overcome your weakness."
    mei uni angry "But nothing good come up easy."
    mei "Easy come, easy go."
    show mei uni frown2 with cdissolve
    any uni default "True, just like what you've said."
    any uni smirk "Satisfied with my explanation?"
    
    mei uni smile "Yeah."
    mei uni sad "And I'm really sorry for being nosy."
    any "I'm cool with it, don't worry."
    any uni sigh "I'm just not sure how you guys will get it. It's pretty serious thing, right? With what happened now."
    mei uni default "Right."
    mei uni smile "Thank you for trusting me too."
    any uni smile "You're welcome."
    stop music fadeout 5.0
    hide anya with dissolve
    hide mei with dissolve
    
    scene black with fade
    "The test doesn't take any longer than 10 minutes."
    "They take our blood sample and run it through a machine. Then, the machine would come up with a result."
    "My test result is negative, by the way. I'm clear of anything they look for."
    "Anya's result also negative. She shrugs in an almost snobbish manner when we meet again in the hallway."
    scene bg corridor with fade
    play music casual fadein 10.0
    any "No class until lunch."
    any "We have 2 free hours. What should we do until then?"
    
    menu:
        "How about hanging around the library?":
            jump library
        "Do you mind doing combat training with me?":
            jump spar

label library:
    any "Cool with me. Let's go."
    jump endofchoice1
    
label spar:
    $ anya_pt += 1
    any "Sure, with pleasure."
    jump endofchoice1
    
label endofchoice1:
    scene bg dininghall with fade
    "Rhodo shows up in front of us at the dining hall."
    "He looks pale and terrified, as if he just saw a giant crocodile Grimm broke through the school yard ground."
    show rhodo uni surprised at midleft with dissolve
    rho "There you are!"
    "He slams the table with both hands."
    rho "You won't believe me!"
    show mei uni frown at midright with dissolve
    mei "Speak up first and then we can decide whether we could believe you or not ...."
    rho uni default2 "Okay ... okay ... hold on ...."
    "Rhodo looks around. There isn't many students at the dining hall right now. They're either still doing the test or they don't realize the time because of cancelled classes."
    rho "Ulm's result ...."
    stop music fadeout 5.0
    rho uni surprised "Ulm's result is {i}positive{/i}."
    show mei uni surprised with cdissolve
    "WHAT?"
    hide rhodo with dissolve
    hide mei with dissolve
    scene black with fade
    
    call chapter("End of Part 1", "The Golden Apple") from _call_chapter_7
    
    #centered "End of Part 1 - The Golden Apple"
    
    jump part2