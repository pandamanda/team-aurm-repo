﻿# PART 2: BREWING TROUBLE #

label part2:
    
    scene bg dormnight2 with fade
    "We have so many questions for Ulm. But he doesn't show up until evening, after dinner time."
    "I've seen Anya worn out after midterm exams, I've seen Rhodo frustrated while trying to memorize the dates and years for History test."
    play music contemplation fadein 10.0
    show ulm uni reluctant at mcenter with dissolve
    "But, not even once I've seen Ulm stressed out like this."
    "He refuses to look at us. He just stands at the room door, saying nothing, not even trying to go out again and avoid us."
    "Ulm just being there. Waiting to be judged."
    "And it's painful to see him like that."
    show ulm uni reluctant at midright with dissolve
    show rhodo uni surprised at midleft with dissolve
    rho "It's just an error, right? "
    rho "They fixed the machine ... right?"
    rho uni frown "It's a misunderstanding, isn't it?"
    rho uni default2 "..."
    hide rhodo with dissolve
    
    "I could share Rhodo's denial attempt right now. It couldn't be true. "
    "Ulm would never do something like abusing doping or narcotics. "
    extend "{i}Never{/i}."
    ulm "... Will you believe me if I say that I don't do it?"
    ulm "Will you ...?"
    "Oh, Ulm ..."
    ulm "You won't, right?"
    ulm "I'm sorry ..."
    ulm "{size=15}I'm sorry, I'm sorry, I'm so--{/size}"
    show anya uni frown at midleft with dissolve
    any "Ulm."
    any "Let me ask you a question."
    ulm uni surprised "Yes?"
    any "Answer honestly. Did you take it or not?"
    ulm uni reluctant "I'm sorry ..."
    any "I don't need your sorry right now, Ulm."
    any "I need you to answer honestly. Yes or no?"
    mei "Anya, I don't think--"
    any uni default "I'm asking Ulm. Not you. Or Rhodo."
    "Some of Anya's actions are questionable for me. I really don't see the point of asking a question to Ulm right now."
    "The result said Ulm uses the doping. Ulm said he doesn't."
    "It's a contradiction as clear as summer sky. Why Anya need to ask Ulm again whether he does or doesn't take it?"
    ulm "I don't use any drugs."
    any uni sigh "Alright."
    any uni default "Let's hold into that statement as the truth, shall we?"
    
    menu:
        "Say yes.":
            jump okay
        "Say nothing.":
            jump silent
            
label okay:
    $ ulm_pt += 1
    mei "Okay ...."
    "I'm still not sure where is this going."
    jump endofchoice2
    
label silent:
    "I decide it's wise to not agree nor disagree at this point."
    "So I keep quiet."
    jump endofchoice2
    
label endofchoice2:
    any uni smile "Okay. Cool."
    "It doesn't sound 'cool' for me--"
    rho "I ... don't think it's 'cool' ...."
    "Rhodo has said it, exactly my thoughts now."
    any uni sigh "Don't take it literally, come on ...."
    any uni default "By the way, Ulm, are they calling me regarding this too?"
    ulm uni thinking "I don't think so ...."
    ulm uni reluctant "But I guess you will eventually be called too."
    any "I'll try to talk with Professor Goodwitch now."
    any "Let's go, Ulm."
    stop music fadeout 5.0
    hide anya with dissolve
    hide ulm with dissolve
    play sound door
    
    "Anya and Ulm leave the room."
    show rhodo uni surprised at midleft with dissolve
    show mei uni sad at midright with dissolve
    rho "I ... don't get it."
    rho "I really don't get it."
    rho "I mean, Anya looks ... {i}chill{/i} about all of this?"
    mei "I suppose she already has an idea about how to handle this matter."
    show mei uni default2 with cdissolve
    rho uni default2 "I'm thinking to ask her again. To clear things up ... I guess?"
    rho "I know you despise me every now and then, Schnee, but are you with me this time?"
    
    menu:
        "I'm with you.":
            jump wyou
        "I'm not with you.":
            jump nyou
            
label wyou:
    $ rhodo_pt += 1
    rho uni surprised "Whoa!"
    rho uni smile "So we could actually agree about something!"
    mei uni angry "Oh, shut up. You're exaggerating."
    jump endofchoice3
    
label nyou:
    rho uni confused "Nah, why I'm even asking you?"
    rho "I'll ask Anya myself."
    jump endofchoice3
    
label endofchoice3:
    hide rhodo with dissolve
    hide mei with dissolve
    scene bg dininghall with fade
    play music casual fadein 10.0
    "Apparently, Ulm's test result made him got detention for a whole week and he isn't allowed to join us in a mission by the end of this week."
    "He doesn't talk much with the rest of us and detention has made him arrives late at lunch hour, which make Ulm sits separately from us."
    show rhodo uni default2 at midleft with dissolve
    rho "So ... I think this is the right time to ask you about something."
    show mei uni frown2 at midright with dissolve
    show anya uni default at mcenter with dissolve
    any "What is it?"
    rho uni surprised "You look calm about Ulm. If you already have something in mind, explain it to me, please."
    any uni sigh "Calm?"
    show mei uni sad with cdissolve
    "Anya chuckles and in that exact moment, I realize she doesn't feel calm at all."
    "She's nervous. Probably afraid and scared too."
    show rhodo uni default2 with cdissolve
    
    any uni default "A huntress I know told me about similar case earlier."
    any "A case where everyone including yourself and the rest of the team said one of your teammates did something horrible but she insisted that she didn't do it."
    show rhodo uni surprised with cdissolve
    any uni sigh "It's terrible. I said to her that I hoped it never happened to me."
    mei uni surprised "It happened."
    any uni default "Yeah."
    any uni frown "Anyway, the huntress told me that I have to choose the truth I want to believe and uncover what's hidden from your sight, proving the truth you hold into."
    
    show mei uni frown2 with cdissolve
    "Uhm ...."
    "I take a glance at Rhodo who scratches his head. He doesn't get it, of course."
    show rhodo uni default2 with cdissolve
    mei "You {i}can't{/i} choose the truth. That's ... silly."
    mei uni sad "I mean, it isn't wise to blindly believe in one thing without considering other perspectives."
    any uni default "That's what I told her too."
    any "Here, let me explain. It's quite difficult too for me to understand it in the first place."
    show mei uni default2 with cdissolve
    "Anya draws two imaginary circles on the table's surface with her fingertip."
    any "First, Ulm's result is positive. Second, Ulm said he didn't take any drugs."
    
    "Both Rhodo and I nod."
    any "I took Ulm's statement as the truth. So, what to do next is to figure out why he got this result."
    any uni frown "Is it technical error? False alarm? Wrong substance?"
    any uni default "The goal is to get solid proof to prove that Ulm is innocent or to explain the whole thing."
    any "A complete version of what really happened to end up like this."
    mei uni default "Oh!"
    mei "Like defense attorney in court!"
    rho uni smile "Ah, now I get it!"
    rho uni default "Basically, we gotta prove the defendant is not guilty, right?"
    rho uni surprised "But why we have to choose one? I think we can believe in the result too."
    
    mei uni frown2 "It's like picking one start point."
    mei "It determines what should we question first. If we pick Ulm's side, then we start the questions from the result."
    mei uni default2 "Even if you choose to believe both Ulm and the result, you will tend to pick one side eventually."
    mei uni frown2 "But, there'll be more doubt."
    mei "When you believe the result and you start questioning its validity, you might doubt yourself."
    mei "Because it's a contradiction, to believe and to question at the same time."
    show mei uni default2 with cdissolve
    "Whoever the huntress who gave such advice is great, for sure."
    "I wonder if the huntress told Anya her experience dealing with this kind of problem ...."
    mei "Did she tell you about it? About what happened to her teammate?"
    
    $ anya_pt += 1
    $ rhodo_pt += 1
    $ ulm_pt += 1
    any "Yeah, she told me about it."
    any "It happened during Vytal Tournament in her year."
    any "Her team made it to the final and one of her teammate was the one who fought in that round."
    any "She defeated her first opponent."
    any uni frown "But then ... everyone in the arena saw her attacking the opponent when he approached her."
    show mei uni surprised with cdissolve
    rho uni surprised "Wha--?"
    rho "Why did she do that???"
    any "That's the thing. She insisted that her opponent was the one who attacked first and what she did was self-defense."
    any "The spectators and the recording said otherwise."
    
    mei uni frown2 "That's really strange ...."
    rho "Yeah ...."
    rho "What happened then?"
    any uni default "Their team was disqualified instantly, that's for sure."
    any "She didn't tell me the rest of the story. She just said that another friend told her about 'seeing things during battle'."
    show rhodo uni default2
    show mei uni default2
    with cdissolve
    any "Eventhough that was very unlikely to happen, she considered it as one possible explanation of what her teammate did."
    any uni sigh "Aaand, guess what? "
    any uni frown "That huntress met with the guy who fought her teammate just a few hours after the fight."
    
    show rhodo uni surprised
    show mei uni surprised
    with cdissolve
    any "He was alright and walked around normally."
    mei uni angry "He faked it?!"
    mei "He made it looked like he was attacked???"
    any uni default "Hey, remember, everyone in the arena saw her hit him. Not the other way around. Also, the accident was recorded."
    rho "Then how the heck he made it like that?"
    any "She didn't tell me further about it."
    any uni frown "But, one fact remained: the guy wasn't injured."
    any "He deliberately made it looked as if his opponent was the one who break the rules and made her disqualified."
    show anya uni default with cdissolve
    rho "Damnit ... that's cruel ...."
    rho "I don't want to meet anyone like him."
    mei uni frown2 "Me too."
    
    rho uni default2 "By the way ...."
    rho "I think I'm going to look for Ulm now."
    rho "I'm worried about him."
    any "Ah, right ...."
    any "The more he tries to stay away from us, the more worry I am."
    mei uni surprised "Hey! Wait for me!"
    mei "I haven't finished eating!"
    rho uni confused "Why do you always eat so slow???"
    rho "You chew your food 40 times???"
    
    show mei uni angry with cdissolve
    "Rude."
    "I glare at Rhodo."
    "I chew my food {i}properly{/i}! And I use {i}manner{/i} even when I'm eating!"
    "There's nothing wrong with that!"
    show mei uni frown2 with cdissolve
    "That's what I want to say to Rhodo, but I cease to do that and concentrate on finishing my meal as soon as possible before Anya and Rhodo leave me."
    "And it's 30 times, not 40 times, although I don't count. Nobody told me to do that."
    any "I'm going to look for him around the yard and the dorm. Rho, can you handle the classrooms and main hall?"
    rho uni default "Sure."
    mei uni default2 "How about me?"
    any uni smirk "You can go with me or with Rhodo. Your choice."
    
    menu:
        "Go with Anya.":
            jump asearch
        "Go with Rhodo.":
            jump rsearch
    
label asearch:
    $ anya_pt += 1
    mei uni default "I'll go with you."
    any "Okay."
    any uni smile "That's better, actually. I don't have to hear Rhodo's complaint about you if we're together."
    mei uni frown "You antagonize me ..."
    mei "I'm not that {i}villainous{/i}."
    "Anya laughs."
    any uni smirk "You never failed me with your choice of word. 'Villainous', huh?"
    any "Now, don't pout like that! You better eat faster or I'm gonna leave you."
    hide anya with dissolve
    hide mei with dissolve
    hide rhodo with dissolve
    "I managed to finish up my lunch within the next 5 minutes."
    "We leave the dining hall and go a different way: Rhodo goes to the classroom hall, Anya and I go to the courtyard."
    
    scene bg walkway with fade
    show mei uni default2 at midleft with dissolve
    mei "Where should we start looking for Ulm?"
    show anya uni default at midright with dissolve
    any "Any places where people rarely passing by."
    mei uni default "Ah. "
    extend "Ulm just wants to go somewhere quiet, right?"
    "Anya imitates a buzzer sound from the quiz show."
    any uni smile "Try again, Mei."
    any uni default "Ulm will go to the library or our room if he wants to avoid people."
    
    show mei uni sad with cdissolve
    "... That's right."
    "..."
    show mei uni frown2 with cdissolve
    extend "..."
    extend "..."
    show mei uni surprised with cdissolve
    "Oh no ... I get it now."
    mei "You think someone might bully Ulm?"
    any uni frown "Not exactly 'bully'."
    any "Ulm is well known for his vast knowledge."
    
    any "I have suspicion that some people want to know where he got the drugs, assuming Ulm really used it."
    mei uni angry "But we believe that he didn't use it!"
    any uni default "We do, but not with the others."
    mei "And why they want to know where to get it? It's illegal, isn't it?"
    mei uni sad "Consuming it will only get them in trouble, right?"
    any "You know what's more tempting than power?"
    any uni sigh "Money."
    any "They can get some extra Lien by selling it."
    
    show mei uni angry with cdissolve
    "I shake my head in disbelief."
    mei "This is ridiculous ...."
    show anya uni smile with cdissolve
    "Anya just smiles and shrugs."
    any "You learn new things every day, believe me."
    hide anya with dissolve
    hide mei with dissolve
    "I really appreciate Anya for not mentioning how clueless I am when it comes to money."
    "Rhodo, on the other hand, would immediately scold me for it."
    "Since money is not a problem in my family, I grow ... 'numb' about financial problem which most people has to deal with."
    "Or about how money is also a motivation to do unthinkable action."
    "Those are things I would never know if I don't attend Beacon Academy, I guess. I have my mother to thank for sending me to Beacon."
   
    any "I don't see him anywhere ...."
    unk "Looking for someone?"
    "Anya and I look up."
    scene cg jillia with fade
    "A girl is sitting on the tree branch above us. I recognize her as a member of team DJJT, Jillia."
    "Short and quick explanation about Jillia: she is an oddball, but in a good way."
    "From what I heard, Professor Oobleck found her in Emerald Forest several years ago. A rare case of stray child that survived in the wild."
    "Jillia stays at Beacon from there on. She learned the basic of human interaction, but could never really socialize with her peer."
    "Everyone who met her for the first time is being told the same thing: don't mess up with Glynda's Winged Monkey aka Jillia."
    "Jillia is Professor Goodwitch's 'spy'."
    "And a good ally for certain situation."
    
    show bg walkway with fade
    show mei uni default at mcenter with dissolve
    mei "We're looking for Ulm. The one with hat and eyepatch."
    jil "Oh, him? Jill not seeing him."
    mei "Could you tell us if you find him?"
    mei "And, please, save him from any trouble he gets in."
    "I take out a pack of choco chip biscuit I keep in one of my pocket and toss it to Jillia."
    
    $ bribe_jill = True
    
    "She catches it and grins."
    jil "Jill will do."
    mei uni smile "Much obliged for your assistance."
    "Jillia jumps to the next tree, swings on the first branch she grabs, and jumps down with a somersault."
    show mei uni default at midleft with dissolve
    show anya uni default at midright with dissolve
    any "You bribe her."
    mei "Let's call it a {i}compensation{/i} for the work she need to do."
    any uni smirk "I doubt she understands the way you say thank you, by the way."
    mei uni angry "Oh, shush!"
    hide mei with dissolve
    hide anya with dissolve
    
    jump endofchoice4
    
label rsearch:
    $ rhodo_pt += 1
    mei uni default "I'll go with Rhodo."
    show anya uni default with cdissolve
    rho uni confused "*sigh* Does this mean I get to be lectured all the way?"
    mei uni frown "Rude. I'm not obliged to do that."
    rho "Rrrright ..."
    rho uni frown "Now, be quick with your food!"
    rho "Damnit, Schnee, what's so difficult with chewing and swallowing?"
    "Now who's lecturing who?"
    hide anya with dissolve
    hide mei with dissolve
    hide rhodo with dissolve
    "I managed to finish up my lunch within the next 5 minutes."
    "We leave the dining hall and go a different way: Anya goes to the front yard, Rhodo and I go to the classroom hall."
    
    scene bg corridor with fade
    show mei uni default2 at midleft with dissolve
    mei "Where should we start looking for him?"
    show rhodo uni default at midright with dissolve
    rho "Let's start with the farthest classroom."
    rho uni frown "If I'm to take on someone, I will do it somewhere far from crowds."
    mei uni surprised "Wait, what?"
    mei "Someone bullies Ulm?"
    rho uni surprised "I don't know. It could happen, I think."
    rho "I'm worried he is bullied, okay? Not I {i}know{/i} that he is bullied."
    
    rho "...."
    rho uni frown "Now that you mentioned it, Schnee, I think it's not exactly a 'bullying'."
    rho "More like ... I dunno. If I want to get my hand on whatever Ulm took--"
    mei uni angry "But we believe that he didn't use it!"
    rho uni shout "Hey! I'm not finished!"
    rho "Besides, it's an assumption!"
    show mei uni sad with cdissolve
    rho uni default2 "Damnit, chill, Schnee ...."
    mei "I'm sorry ...."
    mei uni frown2 "So, what did you want to say?"
    rho "If I want to get my hand on whatever Ulm took, I'd do it secretly. For sure."
    
    rho "I will go all friendly with him ... lead him to one of the empty classroom like ... this."
    "Rhodo tries to open the classroom door."
    rho uni frown "Damnit. It's locked."
    "If only the classroom door isn't locked, Rhodo's chain of deduction would go smoothly like an investigator."
    show mei uni frown with cdissolve
    "{i}Amateur{/i} investigator, I must say."
    rho "Okay, this is annoying. All classrooms are locked."
    mei uni default "Be a little grateful about it. At least we know that school authorities do try to avoid any kind of bullying in the campus."
    
    rho uni confused "Tsk! But it ruins the scenario!"
    hide rhodo with dissolve
    hide mei with dissolve
    "We walk around the classroom hall, keep trying to open each doors just in case one of them is unlocked."
    "It gets on Rhodo's nerves and after five or six trials, he starts grumbling."
    mei "Let's try the main hall, shall we?"
    rho "*sigh* Okay."
    
    jump endofchoice4
    
label endofchoice4:
    scene bg amphitheater2 with fade
    "The three of us meet again at the amphitheater."
    show anya uni default at mcenter with dissolve
    any "Find any clue?"
    show rhodo uni default2 at midleft with dissolve
    rho "Nope."
    rho uni surprised "Are we getting too paranoid ...?"
    show mei uni frown2 at midright with dissolve
    mei "We haven't checked on the locker room."
    any "The locker room?"
    show rhodo uni default2 with cdissolve
    mei uni default2 "Yes. I just remembered that my mom told me she had a friend that was bullied a lot. The bully once pushed him into the locker and launched the locker away."
    
    stop music fadeout 5.0
    show mei uni surprised
    show rhodo uni surprised
    with cdissolve
    "There is a silence for a few seconds."
    any uni frown "That's ... plausible. "
    any "Let's go."
    hide anya with dissolve
    hide rhodo with dissolve
    hide mei with dissolve
    "Without second thought, we rush to the locker room."
    "Locker room is quiet and empty most of the time, only cramped with students during preparation before outdoor classes or missions."
    "Unlike ordinary one, each lockers in there store students' weapons and if we ever need it quickly, we can use our Scroll device to 'summon' the locker to where we are."
    "The lockers are equipped with Dust-powered booster to launch them to where the coordinate is inputted."
    
    scene bg locker with fade
    "Just when we arrive at the locker room, a loud crash could be heard from inside the room. It doesn't take us more than a second to locate where the noise comes from."
    "Ulm is there, surrounded by four people who I don't recognize."
    "They're probably the 3rd year, but I don't care about being rude to someone older right now."
    play music intense fadein 10.0

    mei "You!"
    # QTE
    scene cg lockerfight with fade
    play sound glyph
    "Merely a reflex, I unleash one of my summon."
    "A white Boarbatusk jumps out from the glyph I create on the air. It rolls and rush toward the 3rd years."
    "It isn't an unavoidable attack, but it's enough to surprise them and force them to move out."
    st4 "Whoa!"
    st2 "Geez, what was that?"
    scene bg locker with fade
    show anya uni default at midleft with dissolve
    any "Mei, call it off."
    "I mumble a disagreement, but I remove my glyph from the air. The white Boarbatusk disappeared along with the glyph."
    show st2 at midright with dissolve
    st2 "Well, aren't you team AURM?"
    st2 "The ace team from 2nd year ...."
    "Standing next to me, Rhodo lets out a low annoyed growl. Both of us are more than ready to fight the 3rd years, but Anya always prefers a non-violence way."
    any "Yes, we are."
    any "Is that the reason why you cornered one of us here?"
    hide st2 with dissolve
    show st3 at midright with dissolve
    st3 "Well, haha ... Of course not ...."
    hide st3 with dissolve
    show st2 at midright with dissolve
    st2 "We just need to talk to him."
    st4 "Private matter, yeah ... Private--ouch!"
    hide st2 with dissolve
    "One of them stomps on the foot of the student who talks about 'private matter'."
    "Another one steps closer to Anya. He's taller than our team leader."
    show st1 at midright with dissolve
    st1 "If we may have a little time to finish our conversation with your boy here ...."
    any "How private is your 'private matter'?"
    any "Because I don't mind waiting here until you're finished talking."
    
    if bribe_jill:
        $ anya_pt += 1
        $ rhodo_pt += 1
        $ ulm_pt += 1
        any uni smile "It'd be look better if, by any chance, Glynda's Winged Monkey is around here."
        "I take a glance at the windows in the room."
        "I don't know whether Anya said that because she knows that the window in the farthest side of the room has been opened or not"
        st1 "You're just bluffing."
        st1 "Get outta here."
        hide anya with dissolve
        hide st1 with dissolve
        
        "I notices a movement from row of lockers on my left."
        "Couldn't think any way to notify Anya about this, I nudge Rhodo with my elbow and whisper:"
        show rhodo uni frown at midleft2 with dissolve
        show mei uni surprised at midleft with dissolve
        mei "{size=15}She's here.{/size}"
        show st3 at midright with dissolve
        st3 "Hey, you two! I see you! What did you just say?!"
        show mei uni angry with cdissolve
        "I shrug and step back. Rhodo lifts both hands, making 'no offense' gesture."
        rho uni surprised "Chill, man! We're not trying to pull anything funny here."
        st4 "Like we're going to believe a punk like you."
        rho uni shout "I'm not a punk!"
        rho "Which part of me looks like a punk?!"
        
        show mei uni frown2 with cdissolve
        "Uh oh."
        show st3 at mcenter with dissolve
        show st2 at midright with dissolve
        "Now two of them approach us."
        "Anya, on the other hand, still stands in front of the senior."
        show rhodo uni surprised
        show mei uni surprised
        with cdissolve
        "Come on, Anya, you have to do something."
        "This isn't good!"
        hide mei
        hide rhodo
        hide st2
        hide st3
        with qdissolve
        
        stop music fadeout 5.0
        any "You know? This won't look good in camera--"
        play sound camera
        "And then, there's a loud camera shutter noise."
        show st2 at midleft with dissolve
        st2 "Glynda's Winged Monkey!"
        show st3 at midright with dissolve
        st3 "What?!"
        st4 "No way! It's impo--"
        "Jillia doesn't even hide herself nor sneak her way out of here. She jumps on top the nearest lockers from the 3rd years, landing with loud sound."
        
        "With a wide grin, she flashes her Scroll device where there's a nice photo of the scene a few seconds ago."
        show st1 at mcenter with dissolve
        st1 "Goddamnit! Get her!"
        hide st1
        hide st2
        hide st3
        with dissolve
        "Jillia makes a run to the opened window. She moves so swiftly, it makes her almost inhumane."
        "(Rumors do say about the possibility that Jillia is a Faunus, but she has no visible trait of one.)"
        "While the 3rd years rush to the other side of the room, trying to catch Jillia, Anya pulls Ulm back to his feet and the four of us leave the room as quickly as possible."
        
    else:
        $ ulm_pt += 2
        st1 "Get outta here."
        any "We stay if Ulm stay."
        "Anya put one of her hand behind her back. She starts making signs:"
        "Number 1 followed by scissors."
        "Number 3 followed by rock."
        "Number 4 followed by paper."
        "Ah, okay. Got it."
        any uni smirk "Well ... I think ... we can agree to disagree then--"
        any uni frown "--Seniors."
        hide anya with qdissolve
        hide st1 with qdissolve
        "Anya tackles the 3rd year in front of her. Surprised, the senior tumbles behind." with vpunch
        stop music fadeout 5.0
        "Rhodo stretches both hands. There's 'black aura' on his hands and, by the next second, identical 'black aura' covers several lockers near the 3rd years."
        "The lockers move by themselves, smashing one or two seniors and proceeding to block their way."
        st2 "What the heck?!"
        st1 "Goddamnit!"
        "I pull Ulm back to his feet and the four of us leave the room as quickly as possible."
    
    # running sfx
    scene bg corridor with fade
    "I'm the last one to go through the door."
    play sound ice
    "I slam it shut and cast a nice ice wall on it, complete with roses sculpting on its edge."
    show mei uni cocky at mcenter with dissolve
    mei "Perfect."
    hide mei with dissolve
    scene bg walkway with fade
    "We stop at the school's courtyard where there are many students around. Those 3rd years would think twice if they're going to make a scene with us."
    
    play music casual fadein 10.0
    show rhodo uni surprised at midleft with dissolve
    rho "That was close!"
    rho uni frown "Anya, listen here, I'm not gonna go with your plan anymore if you decide to keep that {i}pacifist{/i} attitude of yours!"
    rho "I thought I'm gonna having a heart attack back there!"
    show anya uni smile at midright with dissolve
    any "Well, but you weren't having any heart attack."
    rho "So, you're going to pull another stunt until I have one???"
    "Anya laughs."
    any uni smirk "Probably."
    rho uni shout "I'm quit, Anya, damnit. I'm quit."
    hide anya with dissolve
    hide rhodo with dissolve
    
    "Of course Rhodo doesn't mean it when he said 'quit'. Until we're graduated from Beacon Academy, we're obliged to work together as a whole team."
    "After the graduation, it's up to each of us what we want to do. Most fresh graduates continue pursuing their career as Huntsman or Huntress, either solo or with their teammates."
    "Anyway. Back to current matter at hands now."
    show mei uni default at midleft with dissolve
    mei "Ulm, are you okay?"
    show ulm uni surprised at mcenter with dissolve
    ulm "Y-yeah. Pretty much."
    ulm uni smile "Thank you for helping me out."
    ulm uni reluctant "{size=15}And ... sorry for making you going out of way to save me ...{/size}"
    show rhodo uni frown at midright with dissolve
    rho "What're you talking about?"
    rho "Next time, don't act all 'lone ranger' and avoid us, alright?"
    rho uni surprised "Except if you change overnight into some brute like Schnee here, I'm going to be very afraid of you, pal ...."
    mei uni angry "Hey!"
    mei "How dare you!"
    mei "A brute would never add a nice ornamental flowers on his or her ice wall!"
    rho uni confused "... It's just you wanting to show off ...."
    rho uni smile "But, okay. Good point."
    "I kick Rhodo's shin." with hpunch
    rho uni surprised "Yeow!"
    hide rhodo with qdissolve
    
    show anya uni default at midright with dissolve
    any "What those guys want with you, by the way?"
    ulm uni thinking "Ah ... yes, they asked me about the 'brewery'."
    mei uni frown2 "'Brewery'?"
    ulm uni reluctant "I supposed they mean where to get the doping. They asked me about how much Lien for each tablet. Tablet doesn't made in a brewery."
    ulm uni thinking "And, apparently, the doping is nicknamed 'The Golden Apple'."
    "It's the one Rhodo mentioned yesterday."
    mei "Why did they ask you about the brewery?"
    mei uni sad "I find it hard to believe if someone insist on taking them after Saul's incident and the inspection ..."
    "Ulm's face somehow brightens up when we get into that part."
    ulm uni smile "I asked them back about it too."
    ulm "And, guess what, they said The Golden Apple is an exceptional doping."
    ulm "If you take one tablet this morning, you won't get caught if you go through examination by tomorrow morning."
    any uni frown "What?"
    ulm "So, there's a trick how to take them."
    mei uni surprised "Wait. "
    mei "If it goes that way, then the question is {i}\"what did you take the night before the examination\"{/i}?"
    show ulm uni surprised with cdissolve
    
    stop music fadeout 5.0
    "Wave of sudden realization hit us. We turn to Ulm, waiting for his response."
    show mei uni frown2 with cdissolve
    ulm "You're right ...."
    ulm "... "
    ulm uni thinking "Right ...."
    ulm "I took two cod liver oil tablets the night after the mission."
    ulm "I always take them. It's my habit to do so."
    hide anya with dissolve
    
    show rhodo uni surprised at midright with dissolve
    rho "So, you probably mixed up with someone's tablet?"
    show mei uni default2
    show ulm uni surprised
    with cdissolve
    "Our conversation is interrupted by a ringtone from Ulm's Scroll. He answers the call with worried expression. The call doesn't take too long."
    ulm uni reluctant "It's Professor Goodwitch. I have to go. Detention."
    rho uni default2 "I'll go with you then. Just in case."
    ulm uni smile "Thanks."
    ulm "Oh, yes, I put the tablets in the dorm kitchen's cabinet."
    ulm "You might want to check them out."
    ulm "I'll see you later."
    hide ulm with dissolve
    hide rhodo with dissolve
    
    "Ulm and Rhodo leave for Professor Goodwitch's office."
    show anya uni default at midright with dissolve
    mei uni frown2 "I ... I'm surprised it turns out this way."
    any "It's really unexpected."
    any uni smile "So, there's actually a merit from Ulm being interrogated by the seniors."
    mei uni sad "I hate to agree, but, yes."
    hide mei with dissolve
    hide anya with dissolve
    
    scene bg corridor with fade
    play music casual fadein 10.0
    "We rush back to the dorm, straight into the kitchen in the same floor as our room."
    "Each floor in Beacon's dorm is equipped with a small kitchen. Students are allowed to keep their foods and snacks in the cabinet and refrigerator."
    "Although there is a serious issue of ignorance about which item belongs to whom in case of snacks and beverages that make a lot of students prefer to keep their own stock in room rather than the kitchen ...."
    
    "Students also make use of the kitchen's cabinet to store their daily supplement--I'm talking about the legal ones here--and personal medication. (Unlike the issue with snacks, medicine and daily supplement is safer in the kitchen.)"
    "Ulm is no exception."
    show mei uni default at mcenter with dissolve
    mei "There it is--"
    mei uni surprised "Oh? There are two bottles?"
    any "Take both of them."
    hide mei with dissolve
    # medicine bottle image
    
    "One bottle has Ulm's name written on its side with a black marker. The other one is also marked with a name."
    stop music fadeout 5.0
    "Saul's name."
    "Anya takes out some from Saul's bottle. I take some from Ulm's bottle. We compare them side by side."
    show anya uni frown at midleft with dissolve
    any "No wonder Ulm mistook them. They're practically identical."
    show mei uni angry at midright with dissolve
    mei "This will prove that Saul is taking them."
    
    mei uni frown2 "But ... how will we prove that Ulm mistook them?"
    any uni default "Look at the bright side, Mei."
    any "With this we can at least present a probability that Ulm took the wrong tablet as consideration."
    mei uni sad "Yeah. You're right."
    hide mei with dissolve
    hide anya with dissolve
    "But ... we wouldn't be able to free Ulm from all charge by tomorrow."
    "We still leave for the next mission without him."
    scene black with fade
    
    call chapter("End of Part 2", "Brewing Trouble") from _call_chapter_3
    
    #centered "End of Part 2 - Brewing Trouble"
    
    jump part3