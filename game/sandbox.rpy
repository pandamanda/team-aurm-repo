﻿# SANDBOX #

label sandbox:
    scene bg locker
    #$ anya_expression = "smirk"
    #show anya default at mcenter
    #any "Thank you for the compliment."
    
    #"Say with Image Attributes test"
    #$ rhodo_outfit = "uniform"
    # "Test."
    # show rhodo uni2 default at mcenter with dissolve
    # rho "Testing 1"
    # rho uni2 smile "Testing 2"
    # "Test in between."
    # rho uni2 default "Testing 3"
    # hide rhodo with dissolve
    
    # call chapter("Chapter Testing 1") from _call_chapter_1
    
    # #$ rhodo_outfit = "armed"
    # show rhodo arm shout at midleft with dissolve
    # rho "Testing 4"
    # rho smile "Testing 4.1"
    # show rhodo confused with dissolve
    # "Interstitial"
    # rho surprised "Testing 5"
    # rho frown "Testing 6"
    # "End of testing"
    # hide rhodo with dissolve
    
    "Start sepia testing"
    show bg tunnelsepia with fade
    show rhodo nor default s at mcenter with dissolve
    "Sepia testing 1"
    show bg storagesepia with fade
    show rhodo arm surprised s at mcenter with dissolve
    "Sepia testing 2"
    
    "End of sepia testing"
    
    call chapter("End of Part 2", "Brewing Trouble") from _call_chapter_2
    
    
    return