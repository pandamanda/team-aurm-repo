# PART 7: TRAINWRECK #

label part7A:
    "Beacon Academy's library main purpose is to provide sufficient books and reference sources for hunters education such as Grimm creatures, technical detail about various weapons and tools, history, and geographical knowledge."
    "We find a thick document about infrastructure and transportation routes, in which Trans Vale-Mistral railway is mentioned."
    "Unfortunately, only {i}being mentioned{/i}. There's no further detail such as the exact route on the map."

    scene bg library with fade
    play music discussion fadein 10.0
    show rhodo uni surprised at midleft with dissolve
    rho "So ... what should we do now?"
    rho "Looking it up on the net? Maybe we don't find books because everything already posted online now."
    show anya uni default at midright with dissolve
    any "Sure. Go ahead. It's worth trying."
    any "I'll go outside for a bit. Be right back."
    hide rhodo with dissolve
    show mei uni frown at midleft with dissolve
    mei "Don't make any trouble."
    any uni sigh "I'm just going to send message and make some phone call if needed, not exploding the hallway."
    any uni default "I'll let you guys know if I plan to."
    mei "Plan to ... what?"
    any uni smirk "Exploding the hallway."
    any "I'll see you later."
    hide anya with dissolve
    hide mei with dissolve

    "I flip the document pages once again as Rhodo and Ulm browsing the net with one of the computer unit."
    "If it's a Schnee Dust Company facility, I'm quite certain we have them in the company archive."
    "However, I don't have the authority to access it or request an access to the archive. It should be done by higher-ups, like my mother."
    "I could ask her, sure, to give me detailed information regarding Trans Vale-Mistral railway, but I have to provide a very good reason for it."
    "And, no, saying I'm investigating a suspicious 'ghost train' wouldn't get me what I want. My mom would dispatch SDC's agents instead."
    "... That would solve the whole problem, wouldn't it?"
    "As far as I know, SDC has a bunch of reliable agents. They have the skills and experience needed to handle various difficult things."
    "They are more capable than us, students at Beacon Academy."
    "...."
    "... Right."
    "This affair has grown dangerous, in my opinion, with the 'ghost train' we're currently looking for is using a camouflage module. {i}Stolen camouflage module.{/i}"
    "No matter how much I want to take part solving this case, if it endangers me, or my team, we should let the more capable ones taking care of it."
    "I should never make the same mistake again. Ever."
    "Not like when I think I did the right thing pursuing Cinder instead of leaving it to Roman ...."

    show rhodo uni default2 at midleft with dissolve
    rho "Schnee? You okay?"
    show mei uni frown2 at midright with dissolve
    mei "Huh?"
    mei "I'm okay .... Why?"
    rho "You look ... what-is-it-called? Daydreaming?"
    ulm "Lost in thought."
    rho uni smile "Oh, yeah! That's it!"
    rho uni frown "You look lost in thought, Schnee."
    rho "I bet you didn't even catch my question before."
    mei "I'm sorry. What was it?"
    rho uni default2 "I asked about shouldn't you call your parents or something. There has been a bad storm around your home."
    rho "It said it disrupted the flight schedule and such. The CTC is okay, though."
    mei uni surprised "I ... didn't hear about any storm happening around Mantle."
    rho "Well, no wonder, I guess. This news was from this morning. We found it just now, when we're looking up 'SDC transportation'."
    hide rhodo with dissolve
    hide mei with dissolve

    "That means if I tell someone in SDC--possibly Roman, because I got the info about break out from him--they wouldn't be able to take action until the storm cleared."
    "Oh, well. If we're careful enough, I think we could scout this 'ghost train' safely."
    "... But they have armed guards on train, right?"
    "Ugh. I couldn't help not thinking this is a bad idea, but on the other hand, suggesting to abandon this mission for our own safety sounds so selfish."
    "I really hate feeling like this."
    show rhodo uni confused at midleft with dissolve
    rho "... Schnee, you've been making faces in the last few minutes."
    rho "What's in your mind? Are you worried about something?"
    show mei uni frown2 at midright with dissolve
    mei "I am."
    show rhodo uni default2 with dissolve
    "Rhodo stares at me for a few seconds before realizing that I wouldn't clearly state what has been bugging me."
    rho "Well ... just tell us about it if you think we should know."
    "That's a surprise."
    "I thought Rhodo is going to push me to tell him what am I thinking."
    "He was the one who got upset when knowing I keep something from him and Anya back in Forever Fall mission."
    mei uni smile "Thanks."
    mei "I'll tell you guys later, I think."
    rho uni default "Cool."
    rho "Now back to searching."
    show mei uni default with dissolve

    show ulm uni thinking at mcenter with dissolve
    ulm "{size=15}I think the information regarding the train is confidential. Cannot find anything useful so far.{/size}"
    rho uni confused "Ulm, stop mumbling. I can't hear ya."
    ulm uni surprised "{size=15}I'm sorry.{/size}"
    ulm uni reluctant "I can't find anything useful so far. It's probably a confidential data or people simply feel it's not important to include details about long abandoned structure."
    show rhodo uni default2
    show mei uni default2
    with cdissolve
    "Ulm pulls his notebook and opens it on the table."
    ulm uni default "But ... I think I have an idea."
    ulm "If I show you Forever Fall map, can you point where did your last mission take place?"
    mei uni default "I suppose so."
    rho "Maybe I can. Let's see the map first."
    "Almost without any delay, Forever Fall map appears on the computer screen after Ulm typed the keywords on search engine."
    "Rhodo zooms the map and put his finger on one point at the map."
    rho uni frown "I caught some fish from around this water area. Our base camp shouldn't be too far away from there."
    ulm uni thinking "And you told me about the rain and the old structure you went to take shelter to .... Is it ... here?"
    mei "Yes. Around that rocky hill."

    ulm "This is only a wild guess, but since Miss Schnee met ... {i}someone{/i} in that tunnel, the person might be waiting for something."
    ulm "The train has to stop somewhere, unload the goods, but not in the old station. Because the stations, both in Vale and Mistral, are located near the city."
    ulm "It would be too suspicious. They have to bring it with other method."
    show mei uni frown2 with dissolve
    "I look at the map again, recalling more about Rhodo's fishing spot."
    mei "There's ... an abandoned port too around there."
    mei "But, now I think it doesn't make sense. Why is there a port if there's already a railway?"
    mei "Nobody lives in Forever Fall and that port is equipped with a crane. Certainly, it's not something from the distant past."
    rho uni surprised "But I didn't see anything suspicious on that place. Like, it's empty. Nothing happened."
    mei uni frown "We're not talking about Dust transportation that takes time almost every hour around Remnant, blockhead."
    mei "We're talking about something that doesn't even happen everyday."
    rho "So ... we were lucky that we didn't meet with anyone that time?"
    mei uni frown2 "Or they've heard and changed the schedule. Someone from school might've dropped them some ... 'news'."
    rho uni frown "Oh. Yeah. Forgot about that."
    hide mei with dissolve
    hide rhodo with dissolve
    hide ulm with dissolve

    stop music fadeout 5.0
    "I look around the library, feeling nervous that someone might overhear our conversation."
    "The library is quiet and not many students are around. However, as I've said earlier, someone in school give away some info to the culprit behind the Golden Apple smuggling."
    "They even have the ability to grant fake approval using Professor Scrooge's name."
    show rhodo uni surprised at midleft with dissolve
    rho "I feel like ... we shouldn't talk about it here."
    show mei uni frown2 at midright with dissolve
    mei "Agree. We should continue our discussion somewhere else."
    rho "The usual rooftop?"
    "That might a good idea. Students rarely visit the roof and there are no open windows on the floor below it."
    "There's only one door to access the rooftop too."
    mei uni default "Sounds good."
    mei "And ... maybe Anya is already there. She always goes to the roof whenever she has the chance."

    rho uni smile "Oh. Yeah. Now that you mention it, she's probably there."
    hide rhodo with dissolve
    hide mei with dissolve
    "We leave the library after returning the book to the shelf and making sure we clear the search history in the computer."

    
    #hallway bg
    scene bg corridor with fade
    play music casual fadein 10.0
    show rhodo uni confused at midleft with dissolve
    rho "Anya goes to the rooftop a lot, right?"
    rho "I don't understand what's fun hanging out there."
    rho "I mean, I can understand if she practices or something, but ... no, it's not always the case."
    rho "She's just staring to the distance."
    show mei uni default at midright with dissolve
    mei "Well ... maybe it's quiet there."
    mei "I don't want to admit we're too noisy or annoying, but if it were me, sometimes I want some quiet time for myself."

    rho "Hmmm ...."
    rho uni default2 "I like it if nobody annoyed me, but at the same, if things go 'too quiet', you have to be suspicious. High chance something bad gonna happen next."
    rho uni surprised "Uh ... I mean, that's how things are in the orphanage."
    mei uni surprised "Are you serious???"
    mei "How come 'quiet' is associated with something bad???"
    hide mei with dissolve
    show ulm uni smile at midright with dissolve
    ulm "I know what Rhodo means, actually."
    show rhodo uni default with dissolve

    ulm "I live with my cousins, some of them are younger than me. Whenever the house goes quiet while they are at home, we have to check them out."
    ulm "A few times, we caught them in the middle of something they shouldn't do, like ... putting lipstick on the cat."
    show rhodo uni smile with cdissolve
    "Rhodo laughs out loud."
    "I suppose, he could totally picture how the poor cat looked like with lipstick on."
    "On the other hand, I'm torn apart between pitying the cat and wanting to laugh too."
    "That's kinda ... horrible."

    ulm "It's quite common in household with small children in it."
    rho "Haha! I know, right? Can't leave those brats alone for too long or there's gonna be disaster."
    ulm uni default "{size=15}To be fair ... I think we were like them too when we were at their age ....{/size}"
    hide ulm with dissolve
    hide rhodo with dissolve
    "Were I like that too? Gotten really quiet when I was doing something I'm not supposed to do?"
    "... Oh, right. That one time when I was bored with my toy piano color and decided to change its color using watercolor."
    "I was upset the watercolor wouldn't stay and ended up applying the paint directly on the toy piano."
    "Mom teases me about it once in a while, saying it turned up like some sort of artwork and we could totally put the toy piano on top of a pedestal, with glass case and put a little description like the one we could find in museum exhibition."
    "A description, like, quoting Mom: 'Color Retouch, by Mei Schnee; watercolor paint on plastic medium'."
    "It's really, {i}really{/i}, embarrassing."
    "The watercolor paint persistently stayed over the years on that toy piano: yellow and blue on the top and bottom, glaring red on the side, and green on the white notes of the piano."

    "We arrive at the stairs leading to the rooftop."
    "Unexpectedly, when we almost reach the door on top of the stairs, we see someone is standing behind the door."
    "He's startled when turning around and seeing us."
    unk "O-oh?"
    show rhodo uni default at mcenter with dissolve
    rho "You're going to the roof too?"
    "I think Rhodo doesn't notice this person's suspicious behavior."
    "But, well, that's better than jumping into conclusion and end up beating someone innocent."
    unk "Um ... yeah. Y-yeah."
    rho "So why are you standing there? You look like you've been behind the door for sometime."
    unk "It's ... the door."
    hide rhodo with dissolve

    stop music fadeout 5.0
    "The lighting in this stairway is dim. I need a few seconds before realizing that this person is wearing an academy staff uniform, not student uniform."
    "He's ... not a student."
    "I take another glance at his face. He looks at me at the same time."
    "?!"
    "This guy is ...!"
    show rhodo uni surprised at mcenter with dissolve
    rho "What's with the door?"
    "Staff?" "It's, uhm, it's locked! Locked, yeah, right!"
    hide rhodo with dissolve

    "A split second after the guy in academy staff uniform said about the door being locked, the door swings open."
    "The door {i}isn't{/i} locked."
    "Sunlight floods the stairway, blinding us for a moment."
    unk "Gotcha."
    "A silhouette grabs and pulls the staff guy away from us."
    "It's Anya."
    "Both Rhodo and Ulm look confused and unsure of what to do. I nod, signaling them to follow our leader to the rooftop."
    "We have a few questionings to do."

    show mei uni angry at midleft with dissolve
    mei "Keep yourself on guard."
    mei "We're not letting him go before we get some answers."
    show rhodo uni surprised at midright with dissolve
    rho "But we don't know who is he--"
    mei "He's one of the administration staffs, in charge of screening and accepting delivered packages for students."
    mei "He was the one who saw me when I checked for packages at the administration office."
    rho "W-wha!"
    hide rhodo with dissolve
    hide mei with dissolve

    scene bg rooftop with fade
    play music brawl fadein 10.0
    show anya uni frown at mcenter with dissolve
    any "What a coincidence, huh, Mr. Administration Office Staff?"
    staff "I ... I believe there's a misunderstanding here ...."
    "Anya, hasn't let her grasp on the staff's shirt go, grabs even tighter."
    any "What I understand is my teammates caught you standing behind that door, claiming you're going to the rooftop, but the door is closed."
    any "When in fact, the door is open and everyone can go here as easily as I were."
    any "I believe you have no reason to wait behind the door if you really need to go to the rooftop."
    any uni default "So, what were you doing?"
    staff "I ... I heard something from the roof ...."
    staff "So, I went to check it out ...."

    any "But the door is locked?"
    staff "Yes! Exactly!"
    any "Fun fact: the door's lock has been broken before I started visiting this place."
    any "Ask every students ever going here, they'll say the same thing."
    staff "Uhh ...."
    "Well ... who knows Anya's habitual visit to this place proves to be useful?"
    any "I will ask you again, Sir, what were you doing?"
    staff "I don't have to answer your question."

    any uni smirk "That's a better answer than a lie. Cool."
    any uni default "Ulm?"
    show anya uni default at midleft with dissolve
    show ulm uni surprised at midright with dissolve
    ulm "Y-yes?"
    any "Get Professor Goodwitch and ask her to check on the security footage to the rooftop."
    any "Tell her we have caught someone suspicious and will wait for her further instruction."
    ulm uni default "Okay. Be right back."
    hide ulm with dissolve
    hide anya with dissolve
    "The administration staff's face goes pale as he see Ulm leaving. Anya uses this chance to bind the staff's hand on the back with her ribbon tie."
    "She just bound both thumbs together, but I know it's enough."
    "I have firsthand experience with the same thing. Just recently too."

    stop music fadeout 5.0
    staff "S-security footage ...?"
    any "Yeah. Why do you think Beacon doesn't have some security cams? That's ridiculous."
    "People often forget that they are being monitored by surveillance cameras all over the school grounds."
    "As far as I know, beside Professor Scrooge and Professor Goodwitch, Professor Scarlatina also has access to those cameras."
    "Most of the time, when a fight broke or someone causing troubles, one between the three of them would arrive at the scene before the culprit get away."
    "Professor Goodwitch was the one who caught me red-handedly when I brought my cousin to Beacon via ... 'unconventional route'."
    "There are also times when she caught me and Rhodo in the middle of fight against a team who picked on someone."
    "Not to mention, Professor Goodwitch has Jillia on her side. That girl appear without notice in the most inconvenient time and unpredictable places."
    "Rumor said that has something to do with Jillia's Semblance, even though nobody could clearly say what her Semblance is."

    #time passing fade
    scene bg rooftop with fade
    "Ulm return to us with Professor Goodwitch following him closely."
    show glynda default at mcenter with dissolve
    "Professor Goodwitch looks at the staff from head to toe, squints her eyes a little, and turns to Anya."
    gly "Ms. Lyre, I believe you would be more than interested to join me having a conversation with him."
    show glynda default at midleft with dissolve
    show anya uni smile at midright with dissolve
    any "With pleasure, Professor."
    any "But I'd like to speak with my team really quick, if I'm allowed to."
    gly "Go ahead. I will wait at the tower."
    hide glynda with dissolve
    "With that, Professor Goodwitch leaves, bringing the staff guy with her."

    play music cocky fadein 10.0
    show mei uni default2 at midleft with dissolve
    mei "So, what is it?"
    any uni smirk "I've gotten a useful map of the railway."
    any "It's something the old White Fang used to plan their ambush on SDC train."
    hide mei with dissolve

    show rhodo uni surprised at midleft with dissolve
    rho "WHAT?!"
    rho "Where did you find such a thing?!"
    any uni default "From ... someone I know."
    rho uni shout "Someone from the White Fang???"
    any uni sigh "{i}Used to{/i} be a White Fang. Not anymore."
    hide rhodo with dissolve
    show mei uni surprised at midleft with dissolve
    mei "Do you think he or she could be trusted?"
    any uni default "I can guarantee it."
    any "I've been working with them for quite some time. I know them, even their family and they're good people now."
    show mei uni frown2 with dissolve
    "Still shady, in my opinion."
    "I can see Rhodo and Ulm have their doubt too. It doesn't feel right for students like us to have such information."
    "But, well .... Couldn't deny it is a more useful information than we have found so far."

    mei "As long as you don't suggest any weird and dangerous ideas, I guess it's okay."
    any uni smile "Cool. I'll forward it for you to look at."
    any uni default "Don't check it outside of our room. We're lucky with the staff, but who knows if there are more of them."
    hide mei with dissolve
    show rhodo uni surprised at midleft with dissolve
    rho "That guy just now. How much did he overhear from you?"
    any uni sigh "I hope not much. I noticed him coming and ended my phone call, pretending to call someone else after that so he wasn't suspicious."
    any "He didn't leave or opened the door, that was when I knew he listened to me."

    rho uni frown "And then we arrived."
    any uni smile "Thanks to you, we could catch him."
    any uni default "Back to the map topic, anyway."
    hide rhodo with dissolve
    show anya uni default at mcenter with dissolve
    any "I'm going to show it to Professor Goodwitch--when we have taken care of the suspicious guy, of course--and ask for her opinion about it."

    any "Hopefully, I can persuade her to give us a mission to sabotage the railway so the 'ghost train' cannot use the route."
    any "If any of you have better ideas after seeing the map and I haven't returned from meeting Professor Goodwitch, send me a message."
    any uni frown "And stay on guard. Don't say unnecessary things where everyone can hear them. We're not finished yet with the case."
    any uni default "I'll see you again later."
    hide anya with dissolve
    "Anya leaves."

    stop music fadeout 5.0
    "I check my Scroll and find her forwarded map file."
    show rhodo uni surprised at midleft with dissolve
    rho "I should be happy that we get some progress with the train track, but ... I don't."
    show mei uni sad at midright with dissolve
    mei "Same."
    show mei uni sad at mcenter with dissolve
    show ulm uni reluctant at midright with dissolve
    ulm "{size=15}Well, me too ....{/size}"

    show rhodo uni default2
    show ulm uni default
    with cdissolve
    mei uni frown2 "It feels like if we rely too much to information from disclosed source like this, we would eventually find ourselves unable not to repeat it again and again."
    mei "I couldn't shake the feeling that any informations obtained this way are somewhat 'not right'."
    rho uni surprised "Whoa. Schnee. I totally agree."
    rho "You said what really bothers me when I don't even know that's the thing."
    ulm uni thinking "{size=12}Like an addiction ....{/size}"

    mei "?"
    mei "Come again, Ulm?"
    ulm uni surprised "N-no. It's nothing. Really."
    ulm uni thinking "If I may suggest, we can take a closer look at the map in our room."
    ulm "Let's think of something while waiting."
    show rhodo uni default with dissolve
    mei uni default "Good idea. It's better than doing nothing."
    hide ulm with dissolve
    hide rhodo with dissolve
    hide mei with dissolve

    scene black with fade
    #centered "End of Demo Version 0.7.5\nThank you for playing!"
    #return

    "Anya's map, upon closer inspection, hold various spots marked with 'X', complete with small notes how to reach several spots."
    "Ulm looks disturbed by this. He scribbled, doing a few calculations, and said all those spots drawn on the map mark stand-by place for an ambush."
    "Ambushing visible trains is one thing. Ambushing invisible train is another thing."
    "Without seeing it clearly, it would be difficult to estimate its speed, its size, and its shape."
    "I really doubt this 'ghost train' has the shape of normal train."

    "When I think about sabotaging the railway, however, I grow concerned of any crews that might be on the train. They might get killed when the train goes derailed."
    "However, I don't have any better suggestion and end up sending a message to Anya, telling my concern about the crew on the train."
    "We meet with Anya around dinnertime, in our way to the dining hall."

    scene bg corridor with fade
    play music casual fadein 10.0
    show mei uni default at midleft with dissolve
    mei "How is it going?"
    show anya uni default at midright with dissolve
    any "Pretty much okay."
    any "Professor told us to wait. She needs some time to prepare."
    mei "Did you tell her about what I said in the message?"
    any "I did, don't worry."

    hide mei with dissolve
    show rhodo uni frown at midleft with dissolve
    rho "And she told us to wait?"
    rho "How long? We're going to have final exam soon."
    rho "There's no way she send us out during that weeks, right?"
    "Instead of answering Rhodo's question, Anya signals us to cut our conversation here."

    any "Later, Rho."
    hide anya with dissolve
    hide rhodo with dissolve

    scene bg dininghall with fade
    "There is awkward silence after that, during dinner. Each of us want to know more about Professor Goodwitch's decision and possibly her plan, but we couldn't talk about it here."
    "I decide to start another serious topic that has been avoided, almost forgotten, in the past few days."
    show mei uni smile at mcenter with dissolve
    mei "So, speaking of final exam weeks."
    mei uni frown "Do I need to remind both of you to not scoring too low in the test?"
    "Almost in sync, Rhodo and Anya suddenly put more attention to their food, acting as if I'm saying something unimportant."

    mei uni angry "Hey! Don't ignore me!"
    mei "We're doing good in practical test, but it doesn't mean we could underestimate the written test!"
    show mei uni angry at midleft with dissolve
    show rhodo uni surprised at midright with dissolve
    rho "I-I know that!"
    rho "I got some good mark if it isn't multiple choice question. But ... History is always multiple choice ...."

    hide mei with dissolve
    hide rhodo with dissolve
    show anya uni smirk at midright with dissolve
    any "Multiple choice is just some probability. You can get your answer right by guessing. It's 1 out of 4."
    stop music fadeout 5.0
    show ulm uni surprised at midleft with dissolve
    ulm "{size=15}Anya, it doesn't mean you can pick the answer by rolling your pencil and see what number comes up ....{/size}"

    show anya uni default with dissolve
    hide ulm with dissolve
    show rhodo uni surprised at midleft with dissolve
    play music vidgame fadein 10.0
    rho "You--{w}WHAT?"
    hide rhodo with dissolve
    show mei uni frown at midleft with dissolve
    mei "That explains the number you taped around your pencil ...."

    any uni frown "I only did it if I drew a blank. {i}Total{/i} blank. I swear."
    mei "...."
    any "... {size=15}It's only for History.{/size} And it's half of the test, not the whole test."
    hide anya with dissolve
    hide mei with dissolve
    "I could appreciate Rhodo's effort in History. He always tries his best to pay attention during History class, even though sometimes he ends up dozing off."
    "On the other hand, Anya doesn't even bother to try staying awake during History class."
    "At first, Rhodo and I poked her awake, but then we stopped doing it altogether and let Professor Oobleck caught Anya sleeping."
    "Professor assigned her to do a long essay twice and called her to his office once for a {i}very{/i} long lecture."
    "I thought Anya finally learns her lesson, but ... {i}no{/i}. She still dozes off in class. Quite often."
    stop music fadeout 5.0

    scene bg dormnight2 with fade
    "Back to our room, we continue our conversation."
    #play music discussion fadein 10.0
    show anya uni default at mcenter with dissolve
    any "Alright, about how long we should wait for."
    any "It'll be two days from now, when we have to take one last mission before exam weeks."
    any "Professor Goodwitch said she is going to assign us to a certain mission, so noone will see us leaving in odd times."

    show anya uni default at midleft with dissolve
    show mei uni frown2 at midright with dissolve
    mei "Did she mention what mission would it be?"
    any "Not yet. There are a few things to discuss with the headmaster."
    mei "So, we have no idea what we should prepare before hand?"
    any uni frown "I'll push her a bit about it. She should at least let us know one day before the mission."
    mei "Hmm .... Alright."

    any uni default "That's all for now. Any question?"
    hide anya with dissolve
    show rhodo uni smile at midleft with dissolve
    rho "I'm actually excited about it."
    rho "I mean, most of our missions were taking care of Grimms or patrolling or checking security systems."
    rho "This one feels more ... important."
    show mei uni frown2 at midright with dissolve
    mei "And dangerous."
    #stop music fadeout 2.0

    rho uni surprised "...."
    rho "Well ... yes, but ...."
    show ulm uni reluctant at mcenter with dissolve
    ulm "{size=15}I ... I believe if we do it carefully, we can do it.{/size}"
    ulm "...."

    hide rhodo
    hide mei
    hide ulm
    with dissolve

    "Uneasiness fills the room in a mere second after I reminded them this important mission could be dangerous."
    "I didn't mean to make everyone down and anxious, but I feel like I need to say it."
    "Even Rhodo, who usually challenges me, feels the same about it."
    "Anya breaks the silence by huffing loudly and walking toward our self-made refrigerator to take a pack of milk."
    "She doesn't look anxious or worry, you could easily tell from her face."

    "The moment Anya sitting on top of low shelf under our room window, we know she has something to say."
    "Not as our teammate."
    "But as our {i}leader{/i}."
    show anya uni default at mcenter with dissolve
    any "First, I want you guys to know that I admit the potential danger of this upcoming mission. We can't, obviously, just shrug it off."
    any uni smile "We have concern about the danger of this mission and I'm glad to see that we acknowledge it."
    any uni default "One thing for sure, this mission is {i}not{/i} impossible."
    any "And another thing, we are not weak or helpless."

    any "We had been through a lot and we survived. We know what 'danger' is. We don't underestimate it and we don't deny it."
    any "When we can't avoid or run away from it, when we have to face it ...."
    any "We'll find a way. Or we'll {i}make{/i} one."
    hide anya with dissolve

    "{i}This mission is not impossible.{/i}"
    "That's ... true."
    "It is dangerous. And, yet, it has to be done."
    "I was worried and ... scared. I don't want to see my friends got hurt."
    "But, if fear makes us do nothing, it isn't right either."

    show anya uni smile at mcenter with dissolve
    any "With that said, I will ask you a question."
    hide anya with dissolve
    team "Will you help me?"
    "Anya looks dumbfounded, clearly not expecting the four of us said the exact same question she asked a long time ago to each of us."
    "Rhodo grins really wide, seemingly proud that he said it together with us."
    "Even Ulm shows a brighter smile than his usual one."

    show mei uni cocky at mcenter with dissolve
    mei "Of course we will."
    mei uni smile "What kind of silly question is that?"
    hide mei with dissolve
    show anya uni default at mcenter with dissolve
    any "Oh ....{w} Right."
    any uni smirk "That won't be the last time you hear me asking the silly question, though."
    any uni smile "Thank you."
    hide anya with dissolve

    scene black with fade

    "Two days later, the 2nd years gather in the amphitheater for our last mission before final exam weeks."

    scene bg amphitheater2 with fade
    play music casual fadein 10.0

    "The place is humming with the noise of students talking about what mission should they take."
    "From the sounds of it, there isn't many exciting missions for this week."
    "Taking the exams into consideration, I would say it's logical for the academy to put up 'easy missions' so the students wouldn't be too exhausted or gotten hurt."
    "On the surface, our designated mission also looks 'easy', but it is concealed in a certain way so nobody could access and accept it beside us."

    "Our mission isn't listed together with other missions, apparently."
    "Anya has to input a certain mission ID into the search bar to make it appear."
    "She taps the buttons really quick, I don't have the chance to read everything in the mission detail."
    "I'm quite sure this mission is categorized as 'field work', together with checking security systems around Emerald Forest and surveying a certain area."
    

    scene bg locker with fade

    "We don't talk much while preparing for our mission in the locker room."
    "I double-check the requirement listed in mission brief in my Scroll."
    "There's nothing else beside weapon, ammunitions, Dust supplies, armor or any kind of protection deemed suitable by students, and first aid kit."
    "The mission description is 'team has to go to designated spots (see attached map) and check/fix/assemble security system'."
    "With a 'be prepared for potential Grimm encounter' added at the end of description, with all capital, bold, red text."
    "A warning about Grimm encounter could be found in every mission description, as far as I remember."
    
    "... Well, I've came to believe that the greatest threat for mankind isn't just Grimm creatures anymore."
    "But from mankind themselves too."
    "These thoughts make me thinking of Emerald Sustrai."
    "What should we do if we met her again?"
    "What should we do if she pulled the same trick like before?"

    $ rand_pt = renpy.random.randint(1, 3)

    if rand_pt == 1:
        "I look at Anya, feeling bad of what I've done to her in our previous mission."
        show anya nor default at midleft with dissolve
        any "?"
        any "What is it?"
        "As usual, it's either I'm being too obvious or Anya already know me, she guessed right that I have something in mind at the moment."
        show mei nor sad at midright with dissolve
        mei "I couldn't help not thinking we might run into Emerald again this time."
        mei "I haven't put much thought about what should we do if that happened."
        mei "I don't want any of us got hurt like last time."
        show anya nor smile with dissolve
        "In response, Anya smiles and nudges me gently."
        "It isn't the smile she put when teasing me."
        "It's her assuring smile."

        any "Thank you."
        mei nor frown2 "For ... what?"
        any "For being concerned, being honest with your thought."
        any nor sigh "I don't have answer for it now, though."
        any nor frown "She can affect any of us with her Semblance, right?"
        any "I have to think of something along the way."
        any nor default "One thing you can be sure, I will personally punch her on the face for making use of my partner."
        "That's not the answer I want to hear. Nevertheless, it put a smile on my face."
        mei nor smile "I'll help you with it then."
        "Anya holds out her hand, giving me a fist bump."
        any nor smirk "Deal."
        hide anya with dissolve
        hide mei with dissolve
        


    elif rand_pt == 2:
        "Behind me, Rhodo closes his locker door with a slam, making me turn around toward him."
        show rhodo nor confused at midleft with dissolve
        rho "Long face again, Schnee?"
        rho nor default2 "What is it this time?"
        show mei nor sad at midright with dissolve
        mei "Well ...."
        "I'm reluctant to share my thoughts with Rhodo."
        "But, apparently, being together in a team really make us know each other better every day."
        rho "Worried this could go like last time in Forever Fall?"
        mei nor surprised "Is it really obvious or you just guessed?"

        rho nor surprised "Eh, both."
        rho "I've learned that you're upset or worried if you're getting into something ... uhh ...."
        rho nor frown "Unfinished? Unsolved? Unpredictable?"
        mei nor sad "You could say all of them are right."
        mei "Our last mission in Forever Fall didn't go well. That's why ...."

        rho nor confused "Urgh, right. What's her name? That woman."
        rho nor frown "We might run into her again."
        rho nor shout "But! This time won't be the same like before!"
        rho "We've seen her trick! Not gonna work for the second time!"
        mei nor default2 "Do you figure out a way to handle her Semblance?"
        rho nor surprised ".... "
        extend "Uh ...."
        mei nor frown2 "I suppose that's a 'no'."
        rho "Uh ... no."
        rho "Wait. "
        extend "Listen. "
        extend "Schnee."
        rho "At least now we know that she's out there. She won't be unexpected anymore, right?"
        rho "We can anticipate her. Somehow."

        "I know Rhodo isn't the most dependable teammate when it comes to planning and such."
        "However, I would give him a credit for being positive and for trying to grasp the smallest good thing we have at the moment."
        "If you have a teammate like Rhodo, it's difficult to keep being down and pessimistic."
        "More than once, he makes me cringe at his silly, naive statement, but at the same time he also makes me think that there has to be something we could do."
        "There's {i}always{/i} something we could do. No matter how small it is."
        mei nor default "Yeah."
        mei "She wouldn't be unexpected anymore."
        rho nor smile "I know, right!"

        hide rhodo with dissolve
        hide mei with dissolve

    elif rand_pt == 3:
        "I remember Ulm seemed interested when I mentioned Emerald's Semblance sometime ago."
        "It's kind of Ulm's nature to analyze everything, including Semblances. I wouldn't be surprised if I found out he has a complete notes of all Beacon 2nd years' Semblances."
        "I'm curious what does he have to say about Emerald's Semblance."
        show mei nor default2 at midleft with dissolve
        mei "Ulm?"
        mei "Do you remember I told you a bit about Emerald Sustrai's Semblance?"
        mei "What do you think of it so far?"
        show ulm nor thinking at midright with dissolve
        ulm "Oh. Yes. I remember."
        ulm nor reluctant "Unfortunately, without seeing it myself and doing a few testing, all I have right now are assumptions."

        mei nor default "It's still better than nothing at all."
        ulm nor surprised "{size=15}Is it ...?{/size}"
        ulm "{size=15}Well ....{/size}"
        "Ulm clears his throat before I reprimand him about speaking with such low volume."
        ulm nor smile "Thank you."
        "Oohh! This is a rare chance to hear Ulm said something with confidence!"
        mei "I'd like to hear more about it."
        mei "But for now, let's move out first. We could talk about it in the ship."

        hide mei with dissolve
        hide ulm with dissolve

    stop music fadeout 5.0

    scene bg airship with fade
    "As always, onboard the airship, Anya gives us a quick briefing about the mission."
    show anya nor default at mcenter with dissolve
    any "Alright. Let's go through the briefing again really quick."
    any "Our task is to install several security systems on the marked spot on the map."
    any "The installing instruction is attached on the mission briefing we got and we're going to be dropped at one of the installation point."
    any "The devices had already set up on designated places by earlier airship. How are we going to proceed to those points and making sure the machines work, is up to us."
    any "Once we finish, we're going to rendezvous point and contact the academy to pick us up."
    hide anya with dissolve

    "Even though the mission description is quite convincing, I think a more observant team might question about the security system if they were assigned to this mission."
    "Unlike in Emerald Forest, there's no security system installed in Forever Falls at least for the last ten years."
    "As far as I know, it's because both places has different environment and Grimm activity."
    "Other than Beacon Academy students and licensed hunters, civilians aren't allowed to enter Forever Fall."
    "Except if they sneaked their way there."

    "Which is, according to Anya and Ulm, not an impossible thing to do."
    "But since Forever Fall doesn't hold anything interesting or valuable, people would think twice before sneaking in."
    show rhodo nor default at midright with dissolve
    rho "Are they the same machines like the ones in Emerald Forest?"
    show anya nor default at midleft with dissolve
    any "Probably. There are pictures of it in our briefing, but I'm not good with machine."
    hide rhodo with dissolve
    show ulm nor thinking at midright with dissolve
    ulm "No, these ones are older models."
    ulm nor default "If you look at the attached picture files in our mission briefing, you can see these ones are more bulky than the one in Emerald Forest."
    "... I didn't notice. They look similar for me."
    ulm "These old models were withdrawn from the market after several cases of performance disruptions in Dust-powered machineries, especially the ones using lightning Dust."
    ulm nor thinking "I read somewhere that this model can also jammed Scroll signal in close range."
    any nor sigh "I bet Professor Scrooge got them from a junk shop."
    any nor frown "Cheap stuffs, functional. You know our headmaster. Stingy old man."

    hide anya with dissolve
    hide ulm with dissolve

    "I couldn't help not laugh at the statement."
    "Professor Ebonezer Scrooge, Beacon Academy headmaster, is famous for being really, {i}really{/i} strict when it comes to money, fund, and budget."
    "Sending a funding proposal for academy activities could turn into a fierce battle against him."
    "I mean, {i}real{/i} combat. With weapon and Dust."
    "Professor Scrooge isn't just a good negotiator and accountant, he's also a good fighter. He's more than ready to accept a challenge for students."
    show rhodo nor smile at mcenter with dissolve
    rho "I can picture him at the junk shop. Somehow."
    rho nor frown "But, this kind of machine can only detect Grimm, right?"
    rho "How can we use it to spot a tr--"
    rho nor surprised "OW!"
    with hpunch
    hide rhodo with dissolve

    "I kick Rhodo before he finishes his sentence."
    "We're not supposed to say anything about the train here! Even with the airship engine noise making it difficult to hear our conversation, who knows if the pilot could hear us?"
    "And who knows whether the pilot would share what he heard during the flight?"
    
    show rhodo nor shout at midleft with dissolve
    rho "Schnee! Why did you kick me?!"
    show mei nor angry at midright with dissolve
    mei "Because you're talking too much!"
    rho "And you really need to kick me for that?"
    mei "Well! It would already too late if I try to warn you with another way!"
    rho "Grrr. "
    extend "Brute."
    mei "I beg your pardon?"

    any "Guys."
    show mei nor frown2
    show rhodo nor surprised
    with cdissolve
    show anya nor sigh at mcenter with dissolve
    "Both Rhodo and I shut our mouth the moment we notice Anya's tone."
    any nor default "I'm going to take a nap. Wake me up when we arrive, okay?"
    mei "Alright ...."
    hide anya with dissolve
    hide mei
    hide rhodo
    with dissolve

    "With that, we end our fight with a truce."

    scene bg ffall1 with fade
    play music forest fadein 10.0
    "Around the afternoon, we arrive in Forever Fall."
    "There's no clearance big enough for the airship to land, so the pilot fly as close as he could to the ground and we jump off."
    "It gives me a nostalgic feeling of my first time in Beacon Academy, during our initiation test."
    "That reminds me ... Ulm admitted he wasn't prepared at all to be launched from the top of the cliff. The only thing that saved him from breaking his back in an awful landing was his Aura."
    "I could say Ulm has improved a lot when I see him jump off from the airship just now."
    show rhodo nor default at midleft with dissolve
    rho "I was about to catch you, you know? In case you fall head-first like in our initiation."
    "Ulm's face reddened."
    show ulm nor reluctant at mcenter with dissolve
    ulm "{size=15}Please don't .... That's embarrassing.{/size}"
    show anya nor smile at midright behind ulm with dissolve
    any "I'll take a pic if that happened."
    any nor smirk "Oh! You can princess-carry him, Rho. That's going to make a good pic."
    rho nor smile "Oohh! Okay! I'll do that if I got the chance!"
    ulm "{size=15}Please ... don't. Just let me fall head-first to the ground ....{/size}"
    hide rhodo
    hide ulm
    hide anya
    with dissolve

    show mei nor frown at mcenter with dissolve
    mei "Stop teasing Ulm, won't you two? We have some work to do."
    mei nor default "How should we proceed, leader?"
    hide mei with dissolve
    show anya nor default at mcenter with dissolve
    any "We'll set-up the first one together to see how to properly do it and split into two groups. Each group picks the next spots they're going to go."
    any "Keep the communication line open. Immediately report for any activities beside Grimm creatures."
    any "We will decide what to do according the situation. I prefer regrouping first than acting alone, but that depends on the situation."
    any "Any question?"
    "Ulm raises his hand."
    show anya nor default at midleft with dissolve
    show ulm nor default at midright with dissolve
    ulm "As I've said previously about these security systems might cause communication error, what should we do if it happened?"
    
    any nor frown "Good question."
    any nor default "Let's test it with the first machine we set-up then. I think there should be a certain radius where we can expect our Scroll to work."
    ulm nor reluctant "Well ... {size=15}there is. But it'll be far away from the device ....{/size}"
    hide anya with dissolve
    hide ulm with dissolve
    "It doesn't sound good."
    "I can only hope there's nothing wrong."

    scene bg ffall1 with fade
    "The four of us head toward our first stop."
    "There, one of the security machine has been placed."
    "From the distance, the thing looks in good shape despite being old model."
    "Close by, I could smell dust and find the nuts and bolts are rusty. There are dents and few scratches on the machine body too."
    "Really something one could find in a junk shop."
    "We watch Ulm booting up and do initial checking on the system through the embedded touch screen."

    show ulm nor default at mcenter with dissolve
    ulm "Everything looks good."
    ulm "It's just like what written in the instruction: boot the system up, make sure the checklist all green, and lock the access with a password."
    hide ulm with dissolve
    "I look at my Scroll, checking on its network signal."
    "Looks good too. I think we don't need to worry about our communication being disrupted by the security system."
    show anya nor smile at mcenter with dissolve
    any "Cool. I see no problem."
    any "Let's split into two groups and get things done. Report your progress to the team as we usually do."


    menu:
        "Group with Anya":
            jump agroup
        "Group with Rhodo":
            jump rgroup
        "Group with Ulm":
            jump ugroup


label agroup:
    $ anya_pt += 5

    show anya nor smile at midright with dissolve
    show mei nor default at midleft with dissolve
    mei "Let's pair up like the usual."
    any nor smirk "Gotten used to partnering with me, huh?"
    mei nor frown2 "...."
    mei "What do you mean by that?"
    any "Nothing. Just kidding."
    any nor default "Alright. Mei and I will go to number 2 and 4. You guys take care number 3 and 5."
    any "Good luck and be careful."
    rho "Will do! You be careful too!"
    hide anya with dissolve
    hide mei with dissolve

    "I check the map in my Scroll to make sure where number 2 and 4 are located."
    "Number 2 is nearby, but if I'm not mistaken, we have to cross the railway to reach number 4's location."
    "We don't talk much while walking to our first destination."
    "And, as always happened when she walks with someone else and not having any conversation, Anya starts whistling a song."
    show mei nor default at midright with dissolve
    mei "You really like that song, don't you?"
    mei "But I don't think I've heard it in television or radio."
    show anya nor default at midleft with dissolve
    any "It's an old song. I found it from some audio tapes in the house."
    mei "What is it about?"
    any nor frown "Hmm ...."
    "It takes Anya a few seconds before she comes up with answer."
    "It's as if she doesn't know or totally forget the lyrics."

    any nor default "A man wandering in outer space."
    mei nor surprised "Huh ...?"
    mei nor frown "... You made that up, right?"
    any "I'm telling you what the musician said about it."
    mei nor frown2 "Tell me the lyrics and I'll decide what is it about myself then."
    any nor frown "I'm ... not a good singer ...."
    hide mei with dissolve
    hide anya with dissolve

    "But she does it anyway, singing the song with a too low voice."
    "After a couple verses, I could understand why did she go with what the musician said about the song, rather than come up with her own interpretation: I don't quite understand the lyrics myself."
    "At first, I thought it's about someone sent to death sentence by hanging."
    "... No wonder Anya thought about it for seconds back then. If she think the same as me, this song is grim."
    "There's something different I notice in the refrain part, anyway."
    "The verses saying 'when the thunder and lightning come / I know that you'll be by my side'."

    "It's something I can relate to. Somehow."
    "Anya stops halfway, looks nervous with her own singing."
    show anya nor default at midleft with dissolve
    any "That's it. More or less."
    show mei nor default at midright with dissolve
    mei "With some practice you could sing better, I believe."
    any nor frown "I don't want to."
    "I nudge her, teasing."
    mei nor smile "What's with the immediate refusal? Embarrassed with your own voice?"
    any "Kind of."
    mei "Don't you think it's interesting if you could become both Huntress and artist or musician?"
    "Anya draws a sudden sharp breath, indicating ... annoyance?"
    show mei nor default2 with dissolve
    "I stop talking right away. There's something wrong with what I said earlier, even though I'm not sure which part of it and why."

    any nor default "You shouldn't suggest it."
    any nor smirk "I heard at least half of the Schnee family have natural talent for music and singing. If I actually practiced and turned out to be better than you, your reputation is going to be in danger."
    mei nor default "A witty comeback you have there."
    "Anya replies with her usual smirk."
    hide anya with dissolve
    hide mei with dissolve
    "I'm curious with her seemingly irritated response, but I think it's not wise bringing it up again after her reply that saved me from awkward situation."
    "Is that another reason she whistles the song instead of singing it?"

    scene bg ffall1 with fade
    "We arrive at our destination, security system number 2."
    "Without any difficulties, we start and set-up the system. Everything looks good so far."
    
    any "System number 2 is up and good."
    rho "Number 3 up and good too! We're on our way to number 5 now."
    rho "We can return to school before evening if we keep going like this."
    any "Sure, if things going well."
    any "Going to number 4 now."

    "I catch a glimpse of Grimm appearances during our walk to security system number 4, but they aren't attacking."
    "Still, I load Dust cartridge into Alpenaster just in case we end up in a fight against the Grimms."

    scene bg ffall2 with fade
    "Crossing the railway, I couldn't help not wondering how come a train could go through this railway without anyone noticing?"
    "And considering how long has passed since this railway was abandoned, I think they need to repair it."
    "How did they do it?"
    "Also, repairing a railway wouldn't be cheap! Where do they get their fund from?"

    show anya nor frown at midleft with dissolve
    any "Using a train to send goods .... We're dealing with an organized crime here, huh?"
    show mei nor frown2 at midright with dissolve
    mei "I think so."
    mei "I could understand now why Professor Scrooge arranged this instead of preparing an ambush or such, beside he probably hasn't sorted out who could he trust to carry the mission."
    any nor default "For a man that said to bring Vale back to order after The Fall of Beacon, Old Man Scrooge is really something else."
    mei nor smile "You're not supposed to call him 'Old Man'. That's rude."
    any nor smile "Eh, half of Vale called him that."
    hide mei with dissolve
    hide anya with dissolve

    scene bg ffall2 with fade
    "We reach security system number 4 and set it up without delay."
    "Everything seems alright until Anya open the communication line in her Scroll."
    show anya nor smile at mcenter with dissolve
    any "We're finished with number 4. Going to rendezvous point now and we can go back to school."
    any "Just as you said, Rho, we can make it back before evening."
    "There is an answer from Rhodo: a distorted voice barely recognizable with a lot of static."
    any nor default "Rho?"
    "Another distorted voice and static. I'm not even sure is it Rhodo or Ulm who is speaking."

    show anya nor default at midleft with dissolve
    show mei nor surprised at midright with dissolve
    mei "The communication disruption ...."
    any nor frown "But it's normal until we started up this last one."
    mei "!"
    mei "Is it wave interference?"
    mei "We can communicate normally when it was only two or three security systems online, but with the systems as close as this one and all five are online, it mess up with our Scrolls."
    any nor default "So if we turn off at least two of these systems, our Scrolls will be back to normal?"
    mei nor frown2 "Assuming it's really because of wave interference, yes."
    mei "But that's risky. We're dealing with old machines. What if we couldn't get it online again?"

    any "I know, I know."
    "And Anya shut the security system down without asking."
    mei nor angry "Anya!"
    any nor sigh "Ulm might be able to fix it if it's broken. It's more important to contact them."
    show mei nor frown2 with dissolve
    "Well ... good point."
    any nor frown "Rho? Can you hear me now?"
    show mei nor default2 with dissolve
    rho "Not crystal clear, but, yeah, I can hear you."

    any "It has to do with the security systems, so I shut down this one. Temporarily."
    any nor default "Listen. I assume we can't communicate with all the systems up, so let's meet up in the middle between security system number 4 and 5."
    rho "Alright. And then ... what should we do?"
    any "We'll move to the rendezvous point together."

    rho "Oh, okay. That's a good idea."
    any "I'll turn the security system on again after this. Expect heavily disrupted communication then."
    hide anya with dissolve
    hide mei with dissolve
    "I still think turning off the system is a bit reckless, but ...."
    "But, well, it's {i}necessary{/i}. We wouldn't be able to reach Rhodo and Ulm and tell them what to do."
    "As our mentor, Professor Port, keep mentioning to us, communication is important for teamwork."

    "... That should include communicating that you're going to turn off security system for a while, right?"
    "Anya seems to notice my objection."
    show anya nor sigh at mcenter with dissolve
    any "Well ... sorry if it surprised you."
    hide anya with dissolve
    "I cross my arms and say nothing. Anya turns the machine back online."
    "Hearing the machine's low humming relieves me."
    "At least, even though this machine is old, it isn't that fragile."
    show anya nor frown at midleft with dissolve
    show mei nor frown at midright with dissolve
    any "C'mon. Stop pouting like that. Your face looks funny."
    any nor smirk "Oh. Should take a picture and send it to your mom."
    mei nor surprised "!!!"
    mei nor angry "Anya! Don't you dare!!!"
    hide mei with dissolve
    hide anya with dissolve

    "From the way she laughs and put her Scroll back into her pocket, Anya is just teasing me."
    "I still punch her arm for that before we walk toward our next destination."


    jump endofchoice12

label rgroup:
    $ rhodo_pt += 5
    show mei nor default at midleft with dissolve
    show anya nor default
    mei "I think I'll group with blockhead here."
    show rhodo nor confused at midright with dissolve
    rho "...."
    rho "Is it really necessary for you to call me that?"
    mei nor cocky "It's a nice and suitable nickname for you."
    any nor frown "I always have doubt about you two work together ...."
    mei nor default "We could. I know when to take things seriously."
    rho nor default "So am I."

    any nor default "...."
    any "Well, okay then. I'm not gonna ask why you pick Rho."
    any nor smile "Alright, Ulm and I will go to number 2 and 4. You guys take care number 3 and 5."
    any "Good luck and be careful."
    rho "Take care of Ulm and, yeah, you be careful too."

    any "Sure thing."
    hide anya with dissolve
    "I pull out my Scroll, checking security system number 3 and number 5 locations."
    "It's quite some walk, but the distance between them isn't too far, even though we have to cross the railway to reach security system number 5."
    rho "You wanna lead the way, Schnee?"
    mei "Why not? Let's go."
    hide rhodo with dissolve
    hide mei with dissolve
    "There's no conversation between us during the first five minutes we're walking."
    "Both Rhodo and I watch our surroundings, looking for any sign of immediate danger such as Grimm attacks."

    "We spotted an Ursa, but it doesn't seem to be interested in us and just walk away."
    "I think it's safe if I want to say something to Rhodo. There's something in my mind for a while now."
    show mei nor default2 at midright with dissolve
    show rhodo nor default2 at midleft with dissolve
    mei "Hey, blockhead."
    rho "Hey, Schnee."
    "We called each other in sync."
    mei nor frown2 "You can go first."
    rho nor surprised "You first."

    "... Again, we said it in sync. Why is this happening?"
    mei "You go first."
    rho "Oh, okay."
    rho nor default2 "So, I've been thinking about why you kicked me back in the airship."
    "Actually, that's what I want to talk about with him too. I feel like I'm too harsh with him and want to apologize."

    mei nor sad "I apologize. I shouldn't have done it."
    mei "I was ... a bit panicked, hearing you talking about everything without worrying someone might've heard it. Someone that isn't supposed to know them."
    rho nor surprised "Aah ... that. Yeah."
    rho "I didn't realize I should keep my mouth shut that time. I'm a bit too excited. "
    extend "I guess."

    rho "Sorry I shouted at you."
    rho nor confused "{size=17}Since you've kicked and stomped my foot for so many times without reason, I don't really know what's actually my fault and not.{/size}"
    mei nor frown "I hear that."
    rho "It's not like I'm trying to hide anything from you."

    "We glare at each other before slowly grinning."
    show rhodo nor smile
    show mei nor smile
    with cdissolve
    rho "That settles it then."
    mei "Yes."
    rho nor surprised "I'm not paying attention how long we've been walking. Are we close yet?"
    hide rhodo with dissolve
    hide mei with dissolve
    "I stop, looking around the place and checking my Scroll."

    mei "There it is."
    "Apparently, we miss our destination for a few meters. It isn't a big deal, though."
    "Security system number 3 start up in no time. It shows all green status too."
    show rhodo nor surprised at midleft with dissolve
    rho "That's it?"
    rho "So easy!"
    show mei nor default at midright with dissolve
    mei "It's logical for machines and tools to be designed as easy to use as possible."

    rho nor smile "I'm pretty sure we can return to school before evening."
    mei "That would be very convenient."
    rho "I know, right?"
    hide rhodo with dissolve
    hide mei with dissolve
    "While I opening up the communication line on my Scroll, I notice there's nothing wrong with the signal."
    "Maybe we shouldn't be worried that much about the potential communication disruption."

    rho "Security system number 3 up and good."
    any "That's great!"
    rho "It's a quick and easy work, we can wrap things up and return to Beacon before evening."

    any "Only if things go smoothly."
    any "We're going to the next one."
    any "Keep us updated."
    rho "Okay."

    "Rhodo and I continue walking again."
    "I catch a glimpse of another Grimm and decide to load Dust cartridge into Alpenaster."
    "It never hurts to be more careful."
    show rhodo nor frown at midleft with dissolve
    rho "I've been seeing Grimm, but none of them attacking us."
    rho "Not that I want them to attack us, though."
    rho nor surprised "Is something scaring them?"
    show mei nor default2 at midright with dissolve
    mei "I believe Grimm creatures have no reason to be scared of anything beside alpha Grimms."
    mei "Unless they learned to fear something that is dangerous for them."
    rho "Huntsmen and huntresses should be on their fear list."
    rho nor shout "How come they keep attacking hunters after all these years? Don't they learn it's dangerous for them?"
    mei nor frown2 "I would say they couldn't determine which humans are hunters and which are not hunters."
    mei "All they know is whether it's human or not. Whether they are hunters or not is another story."

    rho nor surprised "So ... Grimm attacks are like gambling?"
    rho "They attack as long as it's human, not knowing are they hunters or not?"
    mei nor default "Yes, you could put it that way. Like gambling."
    rho "Gambling, huh?"
    rho nor default "Speaking of gambling, do you ever try one?"
    rho "I mean, I heard stories about huge gambling place in Atlas where only crazy rich people can get in."

    rho "So, yeah, I thought you can go in there."
    mei nor surprised "...."
    mei "That's a really random question."
    mei nor frown "The answer is 'no'. Not to mention that I'm underage and not illegible to enter any gambling places."
    mei "Why the question, though? Don't say you want to try it ...."
    rho nor default "I just don't understand where's the fun of gambling. How can people become addicted to it?"

    mei nor frown2 "I read an article about 'uncertainty triggers addiction for human brain'. It's based on a scientific research, apparently."
    mei "Scientist put lab rats in separated cage, each with a single button linked to a specific mechanism."
    mei "In the first cage, when the rat pressed the button, it will certainly get food."
    mei "In the second cage, when the rat pressed the button, there's 50:50 chance to get food."
    mei "In the third cage, of course, when the rat pressed the button, nothing happened."
    mei "Apparently, the rat in the second cage pressed buttons more often than the one in the first cage."

    show rhodo nor surprised with dissolve
    mei "It's because the thought of the next press might give them food that makes them pressing the button again and again."
    mei "The excitement of trying your luck, I guess? That's why people keep spending money in a casino. Because they think they might win in the next game."
    rho "Wow. "
    extend "Interesting."
    hide rhodo with dissolve
    hide mei with dissolve
    "It's also interesting how our conversation went from Grimm creatures to gambling and scientific experiment."

    scene bg ffall2 with fade
    "We arrive at security system number 5 and boot it up without delay."
    "Rhodo checks his Scroll first before I do, contacting Anya."
    show rhodo nor smile at mcenter with dissolve
    rho "Number 5 done!"
    rho "It feels good when things going smoothly like this."
    "However, the reply is a distorted, crackling noise."

    rho nor surprised "Huh? Anya?"
    "More noise, but this time I hear a bit of Anya's voice, even though I couldn't make of what she said."
    show rhodo nor surprised at midleft with dissolve
    show mei nor surprised at midright with dissolve
    mei "It's the disruption Ulm mentioned earlier."
    rho "But it wasn't like this before! What happened?"
    mei nor frown2 "Is it wave interference?"
    mei "We can communicate normally when it was only two or three security systems online, but with the systems as close as this one and all five are online, it mess our Scrolls up."

    rho "What should we do then?"
    mei "We should try contacting Anya from somewhere less affected by the wave interference, assuming it is what really happening."
    "However, before we move anywhere further, another call gets in."
    any "Mei? Rho? Can you hear me now?"
    "I could still hear a little static noise, but it is better than before."
    show mei nor default2 with dissolve
    rho nor default "Oh, it's clear now!"
    rho "Yep, we can hear you."
    mei "What did you do? I mean ... did you do something to get the communication back?"
    any "Just shutting down the machine here. Temporarily."
    show rhodo nor surprised with dissolve
    mei nor frown "... Anya. It's old machine. What if you couldn't get it starting up again?"

    any "Nah. I'll think what to do if that happened. It's more important to get in contact now."
    any "Alright. Apparently, if all five security systems are up, it'll disrupt our communication signal."
    any "Let's meet up somewhere in the middle of security system number 4 and 5, then we go together to the rendezvous point."
    rho nor default2 "Okay, got it."
    any "I'll turn the system up again. See you later."
    hide rhodo with dissolve
    hide mei with dissolve

    "Anya ends the call."
    "I check my Scroll and see the device already lose its signal again. Anya must have turned the system back online again."
    show rhodo nor frown at midleft with dissolve
    rho "I wonder, what's Anya gonna do if she broke the machine. She's with Ulm, but Ulm isn't someone that can fix everything. There are things he doesn't know too."
    show mei nor frown at midright with dissolve
    mei "I'm just glad it isn't broken."
    rho nor default "Heh, right."
    rho "Let's move then."
    hide rhodo with dissolve
    hide mei with dissolve

    "We both walk to our next destination, not talking much."
    

    jump endofchoice12

label ugroup:
    $ ulm_pt += 5

    show mei nor default at midleft with dissolve
    mei "Can I go with Ulm this time?"
    any nor default "If Ulm said okay, why not?"
    show ulm nor surprised at midright with dissolve
    ulm "O-of course you can."
    ulm "{size=15}I ... I mean ....{/size}"
    ulm "{size=15}Sure. I'm okay with it.{/size}"

    any nor smile "It settled then."
    any "Rhodo and I will go to number 2 and 4 then. You guys take care number 3 and 5."
    any "Good luck and be careful."
    mei "We will."
    hide anya with dissolve

    "Ulm looks nervous, fiddling with his Scroll and then stealing a glance at me back and forth."
    mei nor frown2 "Is something wrong?"
    ulm "N-no!"
    ulm nor "{size=15}Uh, sorry.{/size} I mean. No. Nothing's wrong."
    mei nor default "Do you mind leading the way?"

    ulm "I-I don't mind. But, are you sure, Miss Schnee?"
    mei "Of course! Come on, let's go."
    mei nor frown "Let's not give any chance for Rhodo to call me 'slowpoke' and such for being ... well, slow."
    show ulm nor smile with dissolve
    "Ulm gives a little smile and nods before walking to our first destination."
    hide mei with dissolve
    hide ulm with dissolve

    "Since we don't really have anything in common and I used to Ulm being quiet, I don't start any conversation while walking."

    "Ulm surprises me when he mentions the Grimm after we passed by one."
    show ulm nor thinking at midright with dissolve
    ulm "The Grimms are being wary."
    show mei nor frown2 at midleft with dissolve
    mei "?"
    mei "What do you mean?"
    ulm nor surprised "Uh-uhm ...."

    ulm "{size=15}You might've noticed--{/size}"
    show mei nor frown with dissolve
    "I raise my eyebrows, reprimanding Ulm for speaking too soft. He looks startled and then nervous, but eventually regains his composure and repeats what he said. This time he speaks louder."
    ulm "You might've noticed there was an Ursa watching us a while ago."
    show mei nor default2 with cdissolve
    ulm "It was close enough to attack us, which is normal Grimm behaviour when they meet human beings."
    ulm "But it didn't attack."

    mei nor frown2 "Yes. You're right."
    mei "Do you think they're ... afraid of something?"
    hide mei with dissolve
    hide ulm with dissolve
    "The idea that Grimm creatures are afraid of something is odd."
    "Grimm creatures attack humans on sight. They only wait a little bit to review the situation."
    "The Ursa we passed by just now wasn't waiting to learn its circumstances."

    "The creature watched as we go, as if it was meeting a stronger opponent that they shouldn't mess with."
    "A stronger opponent? Could that be ...?"
    show ulm nor default at midright with dissolve
    ulm "Miss Schnee, have you ever heard a theory about Grimm creatures evolve to have a stronger memory and be able to think better strategy?"
    show mei nor default at midleft with dissolve
    mei "Yes, I heard it."
    mei nor sad "It's quite unsettling, honestly."
    ulm "The theory itself is still disputable. There's no proof to validate it so far."

    ulm nor thinking "But ... sometimes we find a Grimm or two that act more careful and seems more intelligent than others."
    mei nor surprised "Do you suggest that the Ursa watching us earlier is one of them? One of the exceptions?"
    ulm "Probably."
    ulm "As far as I know, behaviors suggested by the theory often applies to solitary Grimms, like Ursas and Boarbatusks."

    mei nor frown2 "What about larger ones, like Deathstalkers and King Taijitus?"
    ulm nor default "Those two are already considered 'smart' from the start. They act more strategically than Ursas and Boarbatusks, so I suppose it's difficult to determine whether they evolve too or not."
    mei "Hmm ... true."
    hide mei with dissolve
    hide ulm with dissolve

    "Our conversation reaches its end as we arrive at our first machine."
    "Ulm gets to work immediately, opening the machine side hatch and accessing the control panel."
    "I always find it amusing to watch Ulm working."
    "He knows a lot of things we don't. Whenever he is charged to do something, Ulm would do it without asking or complaining."
    "Sometimes we are worried we assign him with something too big, but Ulm always managed to do his job well."
    "Ulm is resourceful, both in practice and theory. The only thing holds him back is his timid and reluctant attitude."

    "What made him to develop such personality, I wonder?"
    ulm "It's done."
    "I check my Scroll, making sure the signal is still there before opening our communication line."
    show mei nor default at mcenter with dissolve
    mei "Unit number 3 is online."
    any "Number 2 is {i}almost{/i} online."
    mei nor surprised "What do you mean 'almost'???"
    any "I set up the wrong password and had to change it."
    any "It's good now. Don't worry."

    show mei nor frown2 with dissolve
    "For a second there, I regret my choice to pair up with Ulm."
    "I mean, we all know Anya isn't the brightest when she has to handle tech things. She's great at {i}breaking{/i} them. If she dozed off and did some critical mistake with the security system device, Ulm would be our only choice to fix it."
    mei nor frown "Could I ask you not to break anything today?"
    any "You don't even need to ask."

    rho "I'll take care the next one, Schnee. I'm worried too."
    any "... You guys really make me feeling down."
    rho "How can you be feeling down when you grin like that???"
    rho "Oh, well. I wanna go back to school real quick once we're done with this, so I'll take care the next one."
    rho "Talk again later, Schnee."
    mei nor default "Yeah. Later."
    hide mei with dissolve

    "There's no Grimm watching us from far away this time. Despite of that, we still hold our weapons ready at hand."
    "Who knows what kind of surprise we could encounter? Better to be prepared for any incoming threat."
    show ulm nor reluctant at midright with dissolve
    ulm "{size=15}Umm ... Miss Schnee?{/size}"
    ulm "{size=15}Do you mind if I ask something?{/size}"

    "This time, surprised by Ulm's initiative to start a conversation, I don't even think to reprimand him for speaking too softly."
    show mei nor default at midleft with dissolve
    mei "Well ... I don't mind."
    ulm "{size=15}It's a bit personal ....{/size}"
    mei "Let me hear the question first before I decide to answer it or not, then, agree?"

    ulm "{size=15}O-okay.{/size}"
    mei nor frown "And could you please raise your volume a bit? I barely could hear you."
    ulm nor surprised "{size=15}Oh. Right.{/size}"
    ulm "I'm sorry."

    mei nor default "So, what's the question?"
    ulm nor thinking "I've been wondering why did you choose your current weapon."
    ulm "I believe everyone has their own reasons when they chose their weapons."
    ulm "I already knew about Rhodo and Anya's weapons since I helped them tweaking and fixing theirs, but I never touched yours."

    ulm nor surprised "{size=15}I'm sorry beforehand, Miss Schnee ....{/size}"
    ulm "{size=15}Knowing you're a weapon enthusiast, I was assuming you would try your hand on a more complex, bigger weapon than your current one.{/size}"
    ulm "{size=15}I ... I'm really sorry if this is too much ....{/size}"
    mei nor smile "That's totally okay."
    show ulm nor default with dissolve

    mei nor frown2 "I've tried a few weapons before and, unfortunately, because physical strength isn't my best suit, I could only handle weapons that are smaller than my mother's."
    mei "And with my Semblance, I prefer one-handed weapons than two-handed ones."
    mei nor default "Mom made Alpenaster as a simplified, smaller version of similar weapon."
    mei "Before you ask, Mom did try to add some sort of gun on Alpenaster, but it made the weapon heavier."
    mei "Besides, I think adding a gun wouldn't improved the weapon in terms of range since Alpenaster is designed to convert its user's Aura into a projectile."

    mei "Mom made another version of Alpenaster too. It resembles a collapsible baton rather than a dagger like mine."
    mei nor frown2 "It's even more lighter than the current one, but I couldn't use it properly. I feel bad for Mom about that."
    mei nor default "That's all about it, I guess?"
    mei nor frown2 "Oh, and about complexity. That depends, I think."

    mei "In terms of technical, Alpenaster isn't that complicated, but in terms of usability, it is complicated."
    ulm "How so?"
    "I offer my dagger to Ulm."
    mei nor default "Try holding it."
    ulm nor surprised "{size=15}A-are you sure???{/size}"

    "I nod and hand over Alpenaster to Ulm."
    ulm nor default "???"
    ulm "The handle isn't supposed to be this cold ...."
    ulm "And ... what is this feeling?"

    mei "Like something rippling on your palm?"
    ulm "Yes. Oh--wait. I think I know."
    ulm nor smile "It's resonating with the handler's Aura?"
    ulm "Not 'resonating', maybe. It's more like 'synchronizing'."
    ulm "Channeling your Aura to this weapon will trigger the Dust cartridge loaded in the handle and with a little 'push', you can shoot a projectile imbued with Dust."
    mei nor smile "That is correct."

    ulm "This is the first time I see such weapon."
    mei nor default "Anything you'd like to say about it?"
    ulm nor surprised "Huh? Um ...."
    ulm "{size=15}Y-you mean my comment about the weapon?{/size}"

    ulm "{size=15}I ... I believe it isn't necessary ....{/size}"
    ulm nor default "?"
    ulm "A moment, Miss Schnee."
    hide ulm with dissolve
    hide mei with dissolve

    "We already walked down hillside and now we are crossing a valley where a single railway stretches from north to south."
    "It's the old Schnee Dust Company railway that we believed is used by the 'Ghost Train' to transport their illegal goods."
    "Something on the rail draws Ulm's attention. He inspect it closely, pulling out a dry grass from the ground near the railway."
    show mei nor frown2 at midleft with dissolve
    mei "What is it?"
    show ulm nor default at midright with dissolve
    ulm "As you can see, Miss Schnee, the rail is a bit rusty. If a train run on it, it will most definitely make a loud noise."
    ulm "Not to mention, it's risky. The rail must have been worn down in several spot and the train might get side-tracked."
    mei nor surprised "Now that you mention it ... yes."
    ulm nor thinking "But there are dry grass around the railway, so I think they don't use normal train."
    ulm "On the other hand, I'm not quite sure they could afford something like that ...."
    mei nor frown2 "Like ... what?"

    ulm "The Angrav-Train. Train that slightly levitates from railway, powered by gravity Dust."
    ulm "I believe it was introduced and used for the first time in Atlas, developed by Atlas Military Force for a better transportation in an area with heavy snowfall."
    mei nor surprised "The Angrav ...."
    hide ulm with dissolve
    hide mei with dissolve
    "Ulm steals a glance at me and quickly averts his eyes again."
    "It's no secret that a lot of Atlas Military Force technology are developed with assistance from Schnee Dust Company. Most of them are defensive and transportation technology."
    "SDC had declared that they don't want to take part in developing weapons or anything that deems destructive."

    "The Angrav-Train Ulm mentioned is one of the latest transportation product from SDC and Atlas Military."
    "Let's say if the train that passes through here is one of the Angravs, the next question would be: how could they get their hand on it?"
    show ulm nor surprised at midright with dissolve
    ulm "{size=15}Uhm ... Miss Schnee?{/size}"
    ulm "{size=15}I'm really sorry, but I think we need to go to the next machine now.{/size}"
    show mei nor default at midleft with dissolve
    mei "Oh. Right."
    mei nor frown "Rhodo would say whatever he likes if we're being the last to activate the device. He already said he want to go back to Beacon as soon as we finished too."
    hide ulm with dissolve
    hide mei with dissolve

    scene bg ffall2 with fade
    "The moment we both see the security device, Ulm jogs toward it and starts working."
    "I'm a little disappointed with that, actually wanting to try activating the machine. But, well, saying it to Ulm would make him feels bad about it, followed by a stream of apologies."
    "I think it's not even a minute since we arrived when Ulm closed the control panel hatch and nods at me, indicating that he's done."
    show mei nor surprised at midleft with dissolve
    mei "That's really fast!"
    show ulm nor smile at midright with dissolve
    ulm "As long as I've done it right for the first time, I could do it right the next time too."
    hide mei with dissolve
    hide ulm with dissolve

    "Surprisingly, Ulm sounds proud when he said it. It's very rare to hear that kind of tone from him."
    "I open my Scroll and turn on the communication line."
    show mei nor default at mcenter with dissolve
    mei "Number 5 is online."
    show mei nor default2 with dissolve
    "What I hear from the other end of the line is a crazy static noise."
    "The only thing I could make of is a noise that vaguely sounds like someone is speaking, but I couldn't make any sense of what that noise trying to say."
    mei nor frown2 "Anya? Blockhead?"
    "No good. Still static noise."

    show mei nor frown2 at midleft with dissolve
    show ulm nor surprised at midright with dissolve
    ulm "W-what's wrong?"
    mei "Something disrupt the communication line."
    ulm "OH! Wave interference! From the security systems!"
    ulm "Their locations are closer than the usual ones. No wonder if the radius from each machine overlapping each other, causing wave interference."
    mei "And everything was alright when it was only three online because the interference isn't as strong as when all five online?"

    ulm "Yes. Exactly."
    ulm nor thinking "We have to do something about--"
    any "Do something about what?"
    show ulm nor default with dissolve
    mei nor surprised "Anya? Could you hear me?"
    any "Yeah. There's still a bit noise, but it's clear enough."

    mei "Why it's--"
    mei nor frown "...."
    mei "What did {i}you{/i} do?"
    any "Shutting the machine here down for a moment so I can contact you."
    show ulm nor surprised with dissolve
    "Ulm looks horrified at the answer."

    "These machines are old and there's no guarantee about their condition. We were even worried about whether these devices could be turned on or not."
    "And Anya simply shut it down."
    "Now we need to worry about whether the machine could be turned on again or not."

    any "Anyway."
    show mei nor default2 with dissolve
    any "Something about all five devices going online disturbs our communication line. So, let's regroup on the area around the railway. We should be able to see each other on that open place."
    any "Don't stray anywhere else. Regrouping is our priority now."
    mei "Understood."

    any "Great."
    any "I'll turn the machine on again."
    "The static noise returns. I check my Scroll and see the device already lose its signal again."
    
    mei nor frown2 "Oh, well. Let's move and regroup then."
    ulm nor reluctant "{size=15}I-I'm sorry ....{/size}"
    mei "What are you apologize for???"
    ulm "{size=15}I should have noticed about the interference earlier ....{/size}"

    mei "... Ulm. Come on. It's not your fault."
    hide mei with dissolve
    hide ulm with dissolve
    "But from the looks on his face, Ulm doesn't seem to hear me."
    "I say nothing and we walk in silence."


    jump endofchoice12

# STORY BRANCH NOTES:
# Make the team meet up first and then split the task of taking care the locomotive based on the point
# Going to the loco with Anya/Rhodo/Ulm will lead to who's getting captured as hostage by Emerald
# The split decision will be based by point
    
label endofchoice12:

    "From the corner of my eyes, I see a few more Grimms watching us from far away, between the trees."
    "It doesn't attack. Just ... {i}watching{/i}."
    "Somehow it makes me uneasy."
    "It feels odd. Something doesn't feel right about them."
    "But well ... maybe I'm just worried too much."

    "We meet with the others somewhere on higher ground where we could oversee the railway."

    scene bg ffall2 with fade
    
    show ulm nor reluctant at midleft with dissolve
    ulm "{size=15}I-I'm really sorry I didn't know about the interference earlier ....{/size}"
    ulm "{size=15}I forgot to take distances between each machines into account ....{/size}"
    show mei nor frown2 at midright with dissolve
    mei "Ulm, that's not your fault. {i}At all.{/i}"
    mei "You know a lot of things, but it doesn't mean everything, right?"
    ulm "{size=15}Um ... well ... yes. I guess ....{/size}"

    hide mei with dissolve
    show rhodo nor frown at midright with dissolve
    rho "Schnee said it better than me."
    rho "Really, dude, stop blaming yourself whenever things go wrong!"
    ulm "{size=15}I'm sorry!{/size}"
    rho "And stop saying sorry too!"
    hide rhodo
    hide ulm
    with dissolve

    stop music fadeout 5.0
    show anya nor shout at mcenter with dissolve
    "Anya raises her hand, interrupting us."
    any "Don't you guys feel the ground is ... shaking?"
    "Rhodo and Ulm fall silent. I pay more attention to the ground I stand on."
    "Anya's right. I could feel the ground shaking, like something big is coming closer to us."
    hide anya with dissolve

    "And then ...."
    "There's a distortion in the air."
    "The scenery below twists and dissolves. Color disperses and ripples as it moves along the train track."
    "Then, a train is unraveled before my eyes."
    "The train's appearances is unlike any passenger trains or cargo trains I have knowledge of."
    "It also moves with so little noise. I barely hear anything until the train got really close to us."

    show rhodo nor surprised at midright
    show mei nor surprised at midleft
    with dissolve
    rho "T-that's the Ghost Train! How--?!"
    mei "It lost its camouflage barrier? Why?"
    hide rhodo
    hide mei
    with dissolve
    "Ulm has already used his Semblance to Sightjack anyone that might be on the train when we're still surprised at this unexpected appearances."
    "The train slows down and stops further away. With our current position, if anyone leave the train to look around, they wouldn't immediately notice us."
    
    show rhodo nor frown at midright
    show mei nor angry at midleft
    with dissolve
    mei "I would say this is a good opportunity to ambush and capture them."
    "Rhodo nods in agreement."
    rho "Schnee's right!"
    hide rhodo
    hide mei
    with dissolve

    show anya nor default at midright
    show ulm nor thinking at midleft
    with dissolve
    any "Ulm? How's the situation?"
    ulm "From what I've seen so far, they can't get the camouflage to work again ...."
    ulm "They look like White Fang members from their mask, but I'm not sure."
    any "How many of them?"
    
    "The glow on Ulm's left eye blinks several times. He bites his lower lip."
    ulm nor reluctant "Can't get exact number. Too many movement."
    "Ulm stops using his Semblance. For a moment, he looks like in pain, pressing his thumb on forehead and shuts his eye."
    any nor frown "More than ten?"
    ulm "More than ten. Less than twenty."
    any nor default "Weapons?"
    ulm "Standard rifle. Standard close-ranged weapon."
    hide anya
    hide ulm
    with dissolve
    show rhodo nor frown at mcenter with dissolve
    rho "And they're getting off the train. Some of them."
    rho "What should we do now? I think Schnee got good point about ambushing them."
    hide rhodo with dissolve
    
    "Anya takes another look at the train, seemingly doing a quick calculation about the situation."
    show anya nor default at mcenter with dissolve
    any "We split into two groups. One to the front part, one to the tail. Make sure you avoid anyone that left the train."
    any "Take care of everyone aboard as quickly as possible and as quietly as possible. We don't want to notify the ones left the train."

    play music brawl fadein 10.0

    $ most_pt = max(anya_pt, rhodo_pt, ulm_pt)

    if most_pt == anya_pt:
        jump aambush
    elif most_pt == rhodo_pt:
        jump rambush
    elif most_pt == ulm_pt:
        jump uambush

label aambush:
    show anya nor default at midleft with dissolve
    show rhodo nor frown at midright with dissolve
    any "Mei and I will go to the front. Rhodo and Ulm, you two take care the back."
    rho "Got it!"
    hide anya
    hide rhodo
    with dissolve

    #
    #
    #
    "There are a total of four guards near the locomotive."
    play sound glyph
    "I summon a Boarbatusk, aiming it toward one of the guards."
    "The creature spins and hits its target, successfully render him unconscious with a single hit."
    "Guard" "W-what?"

    "Anya leaps toward one startled guard, grabbing his masked face and slamming him to the ground as she lands."
    play sound hardhit
    "Following Anya closely, I cast a Glyph below another guard. Unseen force tosses the guard up into the air."
    "I finish the last guard with a single Aura slash."

    scene bg train with fade
    "Anya already get into the front train, disabling another guard and holding a man captive."
    "From his outfit, I suppose he is an engineer."
    show anya arm default at mcenter with dissolve
    any "You'll run the train when I told you to, {i}capiche{/i}?"
    "Engineer" "I--I can't do that!"
    "Anya bring the engineer to the seat, unsheathe hidden blade from her cane, and place it on the engineer's neck."
    any arm smirk "Of course you can. You're the engineer of this train after all."
    any "Now, kindly put your hands up and don't do anything suspicious, Sir."
    any arm default "Mei, could you take care of the guards on the next car? As quickly as possible."

    show anya arm default at midleft with dissolve
    show mei arm default at midright with dissolve
    mei "Consider it done."
    hide mei with dissolve
    hide anya with dissolve

    scene bg train with fade
    "I run toward the next train car."
    "There are four people in there, all of them are armed, but only two readied their weapon when seeing me."
    "I take care the nearest one first, incapacitating him with three moves, while casting Glyph behind the next one."
    play sound glyph
    "The Glyph flashes and send the guard smashing to the wall on another side."
    "The other two awkwardly aim their weapon, clearly not used to handle them."
    "I swing Alpenaster, releasing a blazing curve which cause small explosion the moment it hit the two guards."

    "Easy."
    "Now, onto the last car."
    "The moment I open the next door, I see Rhodo slamming a guard to the ceiling."
    show mei arm frown2 at midright with dissolve
    mei "That looks hurt."
    show rhodo arm default at midleft with dissolve
    rho "Of course it is."
    "Rhodo takes out his Scroll and calls."
    rho "Anya, you hear me? We're in position."
    unk "Good. Now be good kids and stay there."

    stop music fadeout 5.0
    show rhodo arm surprised
    show mei arm surprised
    with cdissolve
    mei "!!!"
    mei arm angry "You--!"
    hide rhodo
    hide mei
    with dissolve
    "The door in both sides of the car train slam shuts, followed with a soft click. On the other end of the train car, Ulm try to open the door to no avail."
    emr "Hello again, Heiress. I really don't expect to meet you again this soon."
    emr "Just for your information, I have your friend with me here."
    emr "Do anything funny and you can say good bye to her."
    show mei arm angry at mcenter with dissolve
    mei "Don't touch her or--"

    emr "Or what? I'm the one in control of this situation as you can see."
    show mei arm sad with cdissolve
    emr "I have clear view of you back there, so do not try your luck."
    emr "Now, enjoy the ride."
    hide mei with dissolve

    scene black with fade
    "I could feel the train starts moving. It's not shaking too much nor making noise like normal train."
    "It's eerily quiet."
    "We shouldn't have rushed to the train."
    "We should have waited and seen what happened next."
    "{i}I shouldn't have {/i}...."
    "I grit my teeth."
    "It shouldn't go like this ...."
    
    call chapter("End of Part 7", "Trainwreck") from _call_chapter_18
    jump enddemo7a

label rambush:
    show anya nor default at midleft with dissolve
    show mei nor default at midright with dissolve
    any "Mei, group up with Rhodo and take care the front car. Ulm and I will go to the back."
    mei "Understood."
    hide anya
    hide mei
    with dissolve
    #
    #
    #

    "The very first thing Rhodo decides to do is throwing his spear toward one of the four guards near the locomotive."
    "It hits the mark, surprising the other guards."
    "When they look around and notice us, I already cast a series of Aura projectiles, hitting the rest of them."
    "Rhodo picks up his weapon and follows me into the train."

    scene bg train with fade
    "There's another guard inside, standing near someone that looks like an engineer."
    "He aims his rifle, but I manage to grab and lock his wrist. Rhodo--has shifted his lance into its rifle form--hits the guard with his rifle stock."
    "Rhodo lowers his rifle, blocking the engineer who tries to get away."
    show rhodo arm shout at mcenter with dissolve
    rho "Where do you think you're going, huh?"
    rho "Seems like you're the one who can operate this train."

    "Rhodo turns at me, looking for confirmation."
    show rhodo arm frown at midleft with dissolve
    show mei arm frown2 at midright with dissolve
    mei "We need him to run the train. Don't let him get away."
    "Engineer" "W-wait! I can't do that!"
    rho "Nah, man. You hear her. You'll run the train when we told you to."
    mei "One of us should guard him."
    rho "I'll do it. Go catch up with Anya and Ulm, Schnee."
    mei arm default "Okay."
    hide mei with dissolve
    hide rhodo with dissolve

    scene bg train with fade
    "I run toward the next train car."
    "There are four people in there, all of them are armed, but only two readied their weapon when seeing me."
    "I take care the nearest one first, incapacitating him with three moves, while casting Glyph behind the next one."
    play sound glyph
    "The Glyph flashes and send the guard smashing to the wall on another side."
    "The other two awkwardly aim their weapon, clearly not used to handle them."
    "I swing Alpenaster, releasing a blazing curve which cause small explosion the moment it hit the two guards."

    "Easy."
    "Now, onto the last car."
    "I arrive at the sight of Anya pushing a guard toward Ulm."
    any "Your turn, Ulm!"
    "Ulm planting the back of his crossbow right on the guard's face."
    "The guard falls unconscious with a loud thud. Instead of proud, Ulm looks horrified with his action."
    "Hitting people on the face is too much for him."
    show mei arm default at midright with dissolve
    mei "We got the engineer on the front car. Rhodo guards him."
    show anya arm smile at midleft with dissolve
    any "Great work. Now we--"

    stop music fadeout 5.0
    show mei arm surprised
    show anya arm shout
    with cdissolve
    "The door in both sides of the car train slam shuts, followed with a soft click."
    any "Wha?"
    hide anya
    hide mei
    with dissolve
    "Anya try to open the door to no avail."
    "My Scroll rings. The caller ID shown on the screen is 'Rhodo Manthus'."
    "I answer the call without second thoughts, but before I say anything, a familiar voice greets me."

    emr "Hello again, Heiress. I really don't expect to meet you again this soon."
    show mei arm surprised at mcenter with dissolve
    mei "!!!"
    mei arm angry "You--!"
    emr "Just for your information, I have your friend with me here."
    emr "Do anything funny and you can say good bye to him."
    mei "Don't touch him or--"

    emr "Or what? I'm the one in control of this situation as you can see."
    show mei arm sad with cdissolve
    emr "I have clear view of you back there, so do not try your luck."
    emr "Now, enjoy the ride."
    hide mei with dissolve

    scene black with fade
    "I could feel the train starts moving. It's not shaking too much nor making noise like normal train."
    "It's eerily quiet."
    "We shouldn't have rushed to the train."
    "We should have waited and seen what happened next."
    "{i}I shouldn't have {/i}...."
    "I grit my teeth."
    "It shouldn't go like this ...."
    
    call chapter("End of Part 7", "Trainwreck") from _call_chapter_19
    jump enddemo7a

label uambush:
    show anya nor default at midleft with dissolve
    show ulm nor default at midright with dissolve
    any "Mei, go with Ulm to the front car. Rhodo and I will go to the back."
    ulm "O-okay."
    hide anya
    hide ulm
    with dissolve

    "For Ulm and I, we don't need to get too close to our opponents to take them down."
    "There are a total of four guards around the locomotive and we simultaneously hit two of them from a distance: one with Ulm's crossbow bolt, one with an Aura blast from Alpenaster."
    "When the other guards realize two of their friends are down, I already close enough to land several hits on them."
    "From the corner of my eyes, I notice another guard leaving the front car, trying to help his comrades."

    "Ulm shoots him down before I do anything."
    scene bg train with fade
    "Getting inside the train, I find someone on the seat who doesn't look like any guards."
    "From his uniform, I guess he's an engineer."
    show mei arm angry at mcenter with dissolve
    mei "Stay on your seat and don't move."
    "Engineer" "P-please don't hurt me!"

    mei "Well, do as I say and nobody needs to get hurt, understand?"
    show mei arm angry at midright with dissolve
    show ulm arm default at midleft with dissolve
    ulm "Miss Schnee, there are more guards in the next car."
    mei "I would take care of them. Could you make sure he isn't going anywhere?"
    "Ulm nods and stand on guard with his crossbow reloaded."
    hide mei with dissolve
    hide ulm with dissolve

    scene bg train with fade
    "I run toward the next train car."
    "There are four people in there, all of them are armed, but only two readied their weapon when seeing me."
    "I take care the nearest one first, incapacitating him with three moves, while casting Glyph behind the next one."
    play sound glyph
    "The Glyph flashes and send the guard smashing to the wall on another side."
    "The other two awkwardly aim their weapon, clearly not used to handle them."
    "I swing Alpenaster, releasing a blazing curve which cause small explosion the moment it hit the two guards."

    "Easy."
    "Now, onto the last car."
    "The last guard must have been taken care a second before I arrive, since the first thing I see is Anya and Rhodo exchanges a fist bump."
    show mei arm cocky at midright with dissolve
    mei "Seems like I don't need to lend you a hand."
    show rhodo arm smile at midleft with dissolve
    rho "Heh, that should be my line, Schnee."

    rho arm default2 "Where's Ulm, by the way? You're not leaving him hiding somewhere, right?"
    mei arm angry "Of course not!"
    mei "He's guarding an engineer on the front car. We would need him to run the train."
    rho arm default "I think we're good to--"

    stop music fadeout 5.0

    show mei arm surprised
    show rhodo arm surprised
    with cdissolve
    "The door in both sides of the car train slam shuts, followed with a soft click."
    rho "W-what's happening???"
    hide mei
    hide rhodo
    with dissolve
    "Anya who stands the closest to the door, try to open it to no avail."
    "She cusses and reaches for her Scroll."

    show anya arm shout at mcenter with dissolve
    any "Ulm! What's your situation?"
    unk "Everything is under control, leader."
    hide anya with dissolve
    "That voice ...."

    show mei arm angry at mcenter with dissolve
    mei "Emerald."
    emr "Oh, hello again, Heiress."
    emr "Just for your information, I have your friend with me here."
    emr "Do anything funny and you can say good bye to him."
    mei "Don't touch him or--"

    emr "Or what? I'm the one in control of this situation as you can see."
    show mei arm sad with cdissolve
    emr "I have a clear view of you back there, so do not try your luck."
    emr "Now, enjoy the ride."
    hide mei with dissolve

    scene black with fade
    "I could feel the train starts moving. It's not shaking too much nor making noise like normal train."
    "It's eerily quiet."
    "We shouldn't have rushed to the train."
    "We should have waited and seen what happened next."
    "{i}I shouldn't have {/i}...."
    "I grit my teeth."
    "It shouldn't go like this ...."
    
    call chapter("End of Part 7", "Trainwreck") from _call_chapter_20
    jump enddemo7a

label enddemo7a:
    centered "End of Demo Version 0.8\nThank you for playing!"
    return