image locked_thumb:
    Solid("c00")
    size(266, 144)
    
image unlocked_thumb:
    Solid("00c")
    size(266, 144)
    
image my_hover_border:
    Solid("c0c")
    size(256, 134)
    align(0.5, 0.5)
    
### CG Resized  as thumbnail preview
image thumb1 = im.FactorScale("images/CGs/cg glyphglide.jpg", 0.25)
image thumb2 = im.FactorScale("images/CGs/cg dormtalk.jpg", 0.25)
image thumb3 = im.FactorScale("images/CGs/cg lockerfight.jpg", 0.25)
image thumb4 = im.FactorScale("images/CGs/cg messaging.jpg", 0.25)
image thumb5 = im.FactorScale("images/CGs/cg ursafight.jpg", 0.25)

image thumb6 = im.FactorScale("images/CGs/cg android.jpg", 0.25)
image thumb7 = im.FactorScale("images/CGs/cg anyawound.jpg", 0.25)
image thumb8 = im.FactorScale("images/CGs/cg rubycall.jpg", 0.25)
image thumb9 = im.FactorScale("images/CGs/cg anyatalk.jpg", 0.25)
image thumb10 = im.FactorScale("images/CGs/cg rhodogame.jpg", 0.25)

image thumb11 = im.FactorScale("images/CGs/cg ulmlibrary.jpg", 0.25)
image thumb12 = im.FactorScale("images/CGs/cg fight7a.jpg", 0.25)
image thumb13 = im.FactorScale("images/CGs/cg fight7r.jpg", 0.25)
image thumb14 = im.FactorScale("images/CGs/cg fight7u.jpg", 0.25)


    
transform gallery_tr_right:
    on appear:
        alpha 1.0
    on show:
        xpos 1.0 xanchor 0.0
        linear .25 xpos 0.5 xanchor 0.5
    on hide:
        linear .25 xpos 0.0 xanchor 1.0
    
transform gallery_tr_left:
    on appear:
        alpha 1.0
    on show:
        xpos 0.0 xanchor 1.0
        linear .25 xpos 0.5 xanchor 0.5
    on hide:
        linear .25 xpos 1.0 xanchor 0.0

init python:
    
    g = Gallery()
    
    g.button("glyphglide")
    g.image("cg glyphglide")
    g.unlock("cg glyphglide")
    
    g.button("dormtalk")
    g.unlock_image("cg dormtalk")
    
    g.button("lockerfight")
    g.unlock_image("cg lockerfight")
    
    g.button("messaging")
    g.unlock_image("cg messaging")
    
    g.button("ursafight")
    g.unlock_image("cg ursafight")
    
    g.button("android")
    g.unlock_image("cg android")
    
    g.button("anyawound")
    g.unlock_image("cg anyawound")
    
    g.button("rubycall")
    g.unlock_image("cg rubycall")
    
    g.button("anyatalk")
    g.unlock_image("cg anyatalk")
    
    g.button("rhodogame")
    g.unlock_image("cg rhodogame")
    
    g.button("ulmlibrary")
    g.unlock_image("cg ulmlibrary")
    
    g.button("fight7a")
    g.unlock_image("cg fight7a")
    
    g.button("fight7r")
    g.unlock_image("cg fight7r")
    
    g.button("fight7u")
    g.unlock_image("cg fight7u")
    
    g.transition = dissolve
    
screen gallery():

    tag menu
    
    default page = 1
    default max_page = 2
    default gallery_tr = gallery_tr_right
    
    add "main_menu.png"
    
    #Navigation frame, for next, previous, return, etc
    frame:
        align(0.5, 0.05)
        
        hbox:
            textbutton "Prev" action [SetScreenVariable("gallery_tr", gallery_tr_left), If(page<2, SetScreenVariable("page", max_page), SetScreenVariable("page", page-1))]
            null width 50
            textbutton "Return" action Return()
            null width 50
            textbutton "Next" action [SetScreenVariable("gallery_tr", gallery_tr_right), If(page<max_page, SetScreenVariable("page", page+1), SetScreenVariable("page", 1))]
            
    #PAGINATION
    
    showif page == 1:
        frame:
            align (0.5, 0.6)
            xysize (980, 560)
            grid 3 3 spacing 10:
                
                xfill True
                yfill True
                
                add g.make_button("glyphglide", unlocked="thumb1", locked="locked_thumb", xsize=320, ysize= 180, xalign=0.5, yalign=0.5, xpadding=0, ypadding=0)
                add g.make_button("dormtalk", unlocked="thumb2", locked="locked_thumb",  xsize=320, ysize= 180, xalign=0.5, yalign=0.5, xpadding=0, ypadding=0)
                add g.make_button("lockerfight", unlocked="thumb3", locked="locked_thumb", xsize=320, ysize= 180, xalign=0.5, yalign=0.5, xpadding=0, ypadding=0)
                
                add g.make_button("messaging", unlocked="thumb4", locked="locked_thumb", xsize=320, ysize= 180, xalign=0.5, yalign=0.5, xpadding=0, ypadding=0)
                add g.make_button("ursafight", unlocked="thumb5", locked="locked_thumb",  xsize=320, ysize= 180, xalign=0.5, yalign=0.5, xpadding=0, ypadding=0)
                add g.make_button("android", unlocked="thumb6", locked="locked_thumb", xsize=320, ysize= 180, xalign=0.5, yalign=0.5, xpadding=0, ypadding=0)
                
                add g.make_button("anyawound", unlocked="thumb7", locked="locked_thumb", xsize=320, ysize= 180, xalign=0.5, yalign=0.5, xpadding=0, ypadding=0)
                add g.make_button("rubycall", unlocked="thumb8", locked="locked_thumb", xsize=320, ysize= 180, xalign=0.5, yalign=0.5, xpadding=0, ypadding=0)
                add g.make_button("anyatalk", unlocked="thumb9", locked="locked_thumb", xsize=320, ysize= 180, xalign=0.5, yalign=0.5, xpadding=0, ypadding=0)
                
    elif page == 2:
        frame:
            align (0.5, 0.6)
            xysize (980, 560)
            grid 3 3 spacing 10:
                
                xfill True
                yfill True
                
                add g.make_button("rhodogame", unlocked="thumb10", locked="locked_thumb", xsize=320, ysize= 180, xalign=0.5, yalign=0.5, xpadding=0, ypadding=0)
                add g.make_button("ulmlibrary", unlocked="thumb11", locked="locked_thumb", xsize=320, ysize= 180, xalign=0.5, yalign=0.5, xpadding=0, ypadding=0)
                add g.make_button("fight7a", unlocked="thumb12", locked="locked_thumb", xsize=320, ysize= 180, xalign=0.5, yalign=0.5, xpadding=0, ypadding=0)
                
                add g.make_button("fight7r", unlocked="thumb13", locked="locked_thumb", xsize=320, ysize= 180, xalign=0.5, yalign=0.5, xpadding=0, ypadding=0)
                add g.make_button("fight7u", unlocked="thumb14", locked="locked_thumb", xsize=320, ysize= 180, xalign=0.5, yalign=0.5, xpadding=0, ypadding=0)
                null
                
                null
                null
                null
                
    
    
            