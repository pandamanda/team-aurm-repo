# PART 8 : STRANDED #

label part8BA:
    
    "I walk around the boat, restless like an animal in a cage, until I feel the boat gradually slows down."
    "I could see the boat heads toward an island in the middle of nowhere."
    "I lay low and wait for a moment when the boat finally stops."
    "There's nothing good about this island, I could feel it."
    "Fortunately, though, no White Fang army ambushes us while we're still in the boat."
    
    scene bg island with fade
    
    show anya nor frown at midright with dissolve
    any "Still no signal for the Scroll."
    show mei nor frown2 at midleft with dissolve
    mei "Very weak signal in mine. Better than in the boat."
    mei "It's either this island is contaminated with Dust radiation or there are more signal jammers around."
    any nor default "Can you call Ulm or Rho?"
    any "I'll watch the surrounding while you contacting them."
    hide anya with dissolve
    "I nod and do as Anya said."
    
    show mei nor default2 with dissolve
    "My call is answered almost immediately."
    "Not to mention, Ulm sounds really frightened and panic."
    ulm "Miss Schnee!!!"
    ulm "Are you alright? Where are you now?"
    ulm "Are you safe?"
    mei nor default "I'm okay! Don't worry."
    mei nor frown2 "We're in an island which I don't know its location."
    
    "I could hear Rhodo's voice near Ulm. He doesn't sound any calmer than Ulm."
    show mei nor default2 with cdissolve
    ulm "We can't pick your location or Anya's."
    ulm "... Wait. You're with Anya right now, right, Miss Schnee?"
    mei nor default "Yes."
    mei nor frown2 "About our location ... our Scroll didn't pick any signal in the boat. My Scroll is the only one working right now."
    ulm "That's ... difficult."
    ulm "I don't know whether you are too far from the CCT Tower or there's some sort of disturbance in your current location."
    
    ulm "For now, if I may suggest, try to survey the area and see if you can find someplace where you can get better signal."
    ulm "Hopefully, we can pick your location."
    ulm "And, please, update us whenever you have the chance."
    mei nor default "I would do that. Don't worry."
    hide mei with dissolve
    "I end the call and approach Anya who seems to have found something a little further from our current position."
    
    show anya nor default at midright with dissolve
    any "Take a look. There's a piece of metal on the grass and there are burn marks on the rocks around here. Something exploded around here in the past."
    show mei nor cocky at midleft with dissolve
    mei "Explosion expert has spoken."
    any "...."
    any nor smirk "I'm just used to clean up the mess after accidents during ... experiments."
    show mei nor frown with cdissolve
    "I wouldn't call what Anya did (everything related to Dust; from her bullets to the self-made fridge in our dorm room) as an 'experiment'."
    "She breaks things more often than actually achieve an improvement."
    hide mei with dissolve
    hide anya with dissolve
    
    "Now, return to our current situation."
    "Anya and I walk further, following the shoreline, before we find a path leading into the deeper part of the island."
    "We find linked fences, surrounding what looks like an abandoned factory."
    "Who built a factory in the middle of nowhere like this?"
    "No defensive system to stop the Grimms from roaming around this place."
    "Most of them are easy opponents. We have no problem to defeat them."
    
    "A bit more walking and we find what looks like a communication tower."
    "It's rusty and old and doesn't look working."
    any "Oh? I get signal here."
    any "Let's see if I can send our location to Ulm."
    "I keep watch for our surrounding, noticing three Beowolves trying to approach us quietly."
    "I wait until they are close enough before summoning the knight's sword and throw it at one of them."
    
    show anya nor shout at midright with dissolve
    any "Hey. Cut it out."
    any "We haven't met anything dangerous so far, but reserve your Aura just in case."
    show mei nor frown at midleft with dissolve
    mei "I know, I know."
    show anya nor default with dissolve
    mei nor default2 "Did you manage to send our location?"
    any "Yeah."
    any nor frown "And I start to wonder what is Emerald's purpose by luring you to the boat and sent it here?"
    mei nor frown2 "She sees me as a nuisance and wants to keep me away as far as possible, perhaps?"
    
    any nor default "Plausible. But, if I were her, I would do more than just sending you to an island like this."
    show mei nor default2 with cdissolve
    any nor frown "...."
    mei nor frown2 "What?"
    any "If I were her, the only reason I send you here would be to eliminate you without leaving any witness ...."
    any "To think they're willing to go as far as hurting someone or taking someone's life ...."
    mei nor surprised "Wait."
    mei "What about Rhodo and Ulm? Aren't they in danger too?"
    
    any nor default "I told them to lay low for now when I sent our location."
    mei nor frown2 "According to our arrangement, it would be Ulm that is in charge now, right?"
    mei "Would Rhodo listen to what he say? He's stubborn and Ulm is more a strategist than a leader."
    any "Let's trust them, okay? We can't do anything about it now."
    "I reluctantly nod."
    hide mei with dissolve
    hide anya with dissolve
    "I know that as a team, we wouldn't be together forever. There might be times when we got separated like this and we could only trust the other party to make the best decision based on their situation."
    
    "By the way ...."
    "I pull Anya's jacket, stopping her from leaving the tower base."
    show mei nor surprised at mcenter with dissolve
    mei "The Beowolves."
    mei "I only killed one. There should be two more left, but I don't see them."
    mei nor frown2 "Are they ... hiding?"
    "Grimm creatures are indeed evolving as time goes by, but this isn't what I know about Beowolf."
    "A pack hunter, Beowolves tend to move in a group of two or more."
    "When one of them died, the rest of the group would try to surround their opponent first rather than immediately withdraw."
    
    show mei nor frown2 at midleft with dissolve
    show anya nor sigh at midright with dissolve
    any "Something is off with this island, huh?"
    any nor default "Any idea what's wrong? Any threat we might face up ahead?"
    any "I'd like any hints so we can prepare up front."
    mei "I don't think it would be so easy to guess ...."
    mei "We wouldn't be sent here if we could determine what's going to happen next."
    any "I hate to agree with that, but ... yeah. We might not realize what's wrong with this island before we see it right in front of us."
    any nor frown "Stay alert."
    
    "Of course. It goes without saying."
    "However, there's another thing I've been thinking about."
    mei nor default2 "Do you think we're the only one being sent here?"
    mei "The boat is specifically directed to this place. I'm pretty sure the culprit know more about the island, not just randomly inputted coordinates."
    any nor smile "Good point."
    any nor default "From the looks of it, there was once a big factory or such in this island."
    mei nor frown2 "Let's search it. We might find a clue or something useful."
    any "Sure."
    hide mei with dissolve
    hide anya with dissolve
    
    scene bg island with fade
    "It isn't difficult to find the entrance, but we have to walk quite far before we reach it."
    "The further we go, the lesser this facility look like a normal factory."
    "... In the first place, it isn't {i}normal{/i} to build a factory here."
    "Whoever built this tried to hide their works."
    show anya nor default at midright with dissolve
    any "You're better than me about refinery or mining, so tell me, are these pipes for mining something from the underground?"
    show mei nor default2 at midleft with dissolve
    mei "It is."
    any "Dust?"
    mei "From the looks of these pipes, they are for liquid or gas. It's very rare to find Dust in liquid form, let alone gas form."
    any nor frown "Illegal oil mine, perhaps?"
    mei nor frown2 "Probably."
    hide anya with dissolve
    hide mei with dissolve
    
    "I find a faded symbol in one of the metal box we pass by."
    "It looks like a cube, but I couldn't recall what symbol it is."
    "Illegal mining wouldn't need to brand themselves with symbol, right?"
    "My initial thoughts about the boxes with the symbol are probably stolen doesn't hold too long."
    "I find another symbol on some part of the pipe network too."
    "Who owned this facility? And what purpose did it serve?"
    play sound ringtone loop fadein 5.0
    "A call from Ulm interrupts my thoughts."
    stop sound
    
    ulm "Miss Schnee!"
    ulm "Are you alright? Is everything okay?"
    show mei nor default at midleft with dissolve
    mei "I'm alright. Thank you for worrying."
    show anya nor sigh at midright with dissolve
    any "Should I be sad because nobody worried about me?"
    show mei nor frown with dissolve
    ulm "I don't mean it that way! Really!"
    show mei nor default2 with dissolve
    show anya nor default with dissolve
    "I hear Rhodo's laugh behind Ulm. He joins the conversation after that."
    rho "Seems like you got yourself a good spot. Your voice all clear from here."
    "I check on the signal bar and find that it's full."
    
    rho "We're going to break a good news and a bad news. Which one do you want first?"
    any "Bad one."
    any nor frown "But, first, how are you guys doing over there?"
    any "You're not getting caught by the cops, right?"
    rho "Oh, nope. We're fine."
    ulm "We got away before the authorities came."
    ulm "They arrested the thugs trapped in the net."
    
    show anya nor smile
    show mei nor frown
    with cdissolve
    "I take a glance at Anya who looks satisfied with what had she done to those thugs."
    rho "Back to bad news."
    show mei nor default2
    show anya nor default
    with cdissolve
    rho "We've contacted Professor Goodwitch, told her what happened, and gave her your coordinates."
    rho "She agreed to send one of the airship to pick you up, but the problem is ... a storm is approaching."
    ulm "If my approximation is right, you will have to wait until tomorrow morning before the airship can pick you."
    any nor shout "Whoa. That's a long time ...."
    mei nor frown "Do you expect a storm to be happened in five minutes or so? It's a storm."
    mei "In worse scenario, it lasts a week or half a month."
    
    show anya nor default
    show mei nor default2
    with cdissolve
    rho "Professor promised us she would do what she could to get you out from there as soon as possible."
    rho "And for the good news ...."
    ulm "We managed to convince the huntsman you interrogated to give us more information about the Golden Apple."
    mei nor smile "Oh, that's great!"
    rho "He's a wimp, y'know? He's so afraid of getting arrested and begged us to free him in exchange of information."
    show mei nor default with cdissolve
    any nor smirk "Make sure he doesn't run away."
    any nor default "Did you contact the huntress I told you earlier, Ulm?"
    
    ulm "Yes. Lucky for us, she was in the city and came immediately."
    ulm "She took care of the huntsman for now, asking a few questions. We're still around, just in case if this huntsman try to run."
    any nor smile "Awesome."
    any "Thank you for the good work, guys."
    "My curiosity kicks in. I turn to Anya."
    mei nor frown2 "Is this huntress the same person with the one you usually calls?"
    any nor default "Nope. Different huntress."
    "Before I inquire further from Anya, Rhodo speaks again."
    
    rho "The more I see her, the more I find her resemblance to your cousin, Schnee."
    rho "Ah, yep. It's just their hair color that differs. They both have similar hair style."
    mei nor surprised "?!"
    mei "Aunt Yang???"
    any nor smirk "Everyone in Patch Island know her. And I live in Patch, in case you forg--"
    "I hear a collective growls from behind."
    show mei nor angry with cdissolve
    any nor default "Rho, we gotta go. Contact you again later."
    hide mei with dissolve
    hide anya with dissolve
    "We both turn around."
    
    "The scene in front of us is terrifying, I must admit."
    "There's a large pack of Beowolves. Some of them are normal, but the others have pale green crystals protruding on their back instead of bony spikes."
    "Doing a really quick calculation, there are at least thirty of them."
    scene cg cerberus with fade
    "And looming at the center of this dreadful pack is a gigantic three-headed creature."
    "The giant creature is less humanoid and more canine-like than Beowolves."
    "More importantly, it's {i}abnormally huge{/i}. I could even say this creature is bigger than a Goliath."
    "Pale green crystals decorates its spine, further indicating its unusual nature."
    
    "The first wave of Beowolves attack."
    scene bg island with fade
    "Anya welcomes the Beowolves with an explosive shot."
    "The fierce explosion takes away several Beowolves, but some of them are still alive."
    "I cast and launch icicles towards them, killing two. Anya dodge another and kicks it, letting me finish the Grimm creature."
    show anya arm shout at mcenter with dissolve
    any "We gotta retreat."
    any "Destroy the walkway so they can't follow us."
    hide anya with dissolve
    "There's a deep, large gap behind us where a branched metal walkway is built."
    "The walkway lead to various part of this mysterious mining facility, some toward control panels and valves, the others toward some sort of post, and one toward another part of the island, probably to the main building if there's any."
    
    "The next wave of Beowolves are more ready to avoid Anya's shot. More of them approach us."
    any "Geez."
    "Before I could cast any Glyph, Anya already pulls me back."
    show anya arm sigh at mcenter with dissolve
    any "No, no, Mei, I know what you're thinking. Just stand back and get ready to run."
    any "Reserve your Aura. We still have that huge monster."
    show anya arm default with cdissolve
    "Anya unsheathes the sword inside her cane and flicks its handle."
    "The sword opens into smaller, segmented parts, each connected by what looks like a metal wire."
    "Oh. "
    extend "Anya's threaded-blade."

    "I've only seen her using it twice and that's enough for me to conclude this threaded-blade is tricky and dangerous."
    "Like, {i}really dangerous{/i}. For the opponents, for the people around, and for the weapon handler."
    hide anya with qdissolve
    "I duck. Anya steps forward and swings her blade with all her might."
    "The blade elongates in the air, slashing the first row of incoming Beowolves."
    "One thing that makes this scene less gruesome is the fact that our opponents are monsters. Not human."
    "Nobody would pity a bunch of mutilated Beowolves."
    "Anya whips her blade once again, killing more."
    
    "The giant Grimm lets out a growl and moves toward us."
    "Anya flicks her blade handle and sheathes her sword."
    any "Now, go! RUN!"
    "I follow her order."
    "I don't need to look behind to see the giant Grimm follows us."
    "Thing goes wrong when we reach branched part of the walkway."
    "I immediately take the left walkway. The giant Grimm's heavy footsteps slow down and I look behind to see whether the creature still following me or not."
    "At that moment, I see Anya has taken the {i}right{/i} walkway. We end up in the opposite part."
    "Who's going to cut which walkway now???"
    
    any "Keep running!"
    "I plan to turn around, summon the knight, and cut down the giant beast--"
    "But before I even stop, a dark shadow rushes above my head."
    "The three-headed Grimm lands in front of me. Even though it struggles with small area for it to step on, it manages to maintain its balance." with vpunch
    "The Beowolves behind me growl and creep closer."
    "Unpleasant chill trails on my spine."
    "If I don't do something .... I would ...."
    "I couldn't let that happen."
    
    "I raise my hand and cast a Glyph. One of the knight's hand make its appearance from the Glyph, holding its signature sword."
    "A single slash is what needed to kill a dozen Beowolves in front of me. The rest of them reluctantly retreat a few steps, still not leaving."
    "I face the three-headed canine Grimm again. What was previously I felt as 'fear' has turned into a cold anger."
    "The knight's sword slashes. With a target as big as this giant Grimm, I wouldn't miss."
    "But those three heads aren't decoration."
    "The head on the right see the incoming attack. The creature moves a little and it stops the sword by biting its blade."
    
    show mei arm surprised at mcenter with dissolve
    mei "W-what?"
    "I channel more Aura to the Glyph, pushing the sword. Failed."
    "I try to pull it from the clutch of those fangs. Still to no avail."
    "A sudden pain on my back breaks my connection with the summon Glyph."
    hide mei with dissolve
    #growl sfx
    "I turn around just in time to avoid my neck from being ripped open by Beowolf jaws."
    "It bites my arm and forces me down, not letting go before I stab its eye with my dagger."
    "The Beowolf yelps and steps back. "
    extend "Anya cuts the Beowolf's head."
    show anya arm pain at mcenter with dissolve
    "Without saying anything, Anya proceeds ahead. She makes a gesture as if she picks something from the thin air."
    
    "A glowing Glyph, pale red, with its rim similar to my Glyph, appears in front of her. Instead of Schnee family's snowflake emblem, the Glyph has a triskelion gear symbol on it."
    "The giant Grimm's claw clashes with the Glyph. Anya counters with a shot from her gun."
    hide anya with dissolve
    "She wouldn't win over this Grimm. Not alone."
    "However, we still have a horde of Beowolves on the other side of the walkway."
    "I load lightning Dust into Alpenaster and use it to strike down three incoming Beowolves."
    "I stun the fourth one with just enough amount of lightning shock, using it to hide myself from the incoming ones while I cast another summon Glyph."
    "This time, I only call forth the sword."
    "The sword pierces through the Beowolf I use as a shield. I cast a black Glyph on the sword's pommel, launching it at high speed towards the rest of the Beowolves."
    "I manage to kill quite a lot using that trick."
    "Three more down with lightning strike. There are only four or five left and they reluctantly withdraw."
    
    "A loud shriek returns my attention to Anya and the three-headed Grimm."
    "Despite of the lack of space, Anya somehow managed to slip behind the Grimm while keep attacking, maintaining the creature's attention to her."
    "Provoked, the giant Grimm do whatever it could to turn around so it could counter Anya's attack."
    "It's the source of the previous shrieking metal: the walkway couldn't withhold the Grimm's weight any longer."
    "The railing has broken. The support beams twist and bend."
    show mei arm surprised at mcenter with dissolve
    mei "Anya!"
    mei "The walkway!"
    hide mei with dissolve
    
    "Anya rushes to the further part of the walkway, where most of them are intact."
    "A faint pale red aura envelops her hands, spreading to part of walkway she touches. The twisted metal straightens, providing a solid step and separating itself from the crumbling part."
    "It's Rhodo's Semblance, which Anya 'copied' from him just like she did to my Semblance."
    "Realizing that it's going to fall, the three-headed Grimm lets out an upset growl and--"
    "--jumps. To my side." with vpunch
    
    "I dodge just in time."
    scene cg bridgefall with fade
    "The Grimm's claw is scratching the platform, a few inches from my feet. The walkway couldn't support his weight and fall apart."
    "In desperation, the giant creature claws the air, trying to grab anything."
    "And then ...."
    "The Grimm hits me. Hard. On the head." with hpunch
    "For a second, there's severe pain."
    "And then, everything goes silent."
    "I remember my back hitting the railings and my body slumps ... sliding ... down ...."
    scene black with fade
    "Everything loses its color. Everything turns pitch black."
    
    "..."
    extend "..."
    extend "..."
    "..."
    "I wake up drenched in cold sweats."
    scene bg outpostnight with fade
    "I'm lying on a solid, cold concrete surface. My back and shoulders are stiff."
    "My head feels light and throbbing in the temple. Rather than painful, it's uncomfortable."
    "I sit down, realizing my cloak has been removed and folded to cushion my head. It is stacked with Anya's folded jacket."
    "The owner of that jacket, however, is nowhere to be seen."
    "It takes time for me to recall what had happened and trying to recognize where am I right now."
    "I'm inside a control panel room or what looks like one."
    "There's no door to this room. In exchange, a big metal plate is placed on the entry way."
    "This post is outside the main facility. It's already dark and it's raining outside."
    
    "Still feeling feeble, I move to the corner of the room where I sit quietly until I hear someone's coming."
    "I couldn't help not smiling when hearing Anya's voice, cursing about whatever kind of barricade or traps she set up around the post."
    "It's either she bumped into the barricade or even triggered her own trap. Anya isn't particularly good with those kind of things."
    "The metal plate slides and Anya enters the room. She is soaked wet from head to toes."
    show mei nor frown2 at midleft with dissolve
    mei "Anya?"
    mei "Where did you go?"
    show anya nor shout at midright with dissolve
    any "Oh--!"
    any "You awake!"
    
    "Before I could say anything, Anya hug me."
    show mei nor surprised with cdissolve
    any nor frown "{size=15}Thank goodness ... thank goodness ....{/size}"
    any "Are you okay? Does your wound still hurt?"
    show mei nor frown2 with cdissolve
    any nor default "Or ... I don't know. Feel anything strange?"
    mei nor frown "I could feel your clothes are soaking wet."
    mei "Forgive me for interrupting your relief, but please let me go."
    any "Ah, yeah. Sorry."
    
    "She reluctantly release me and takes a step back."
    mei nor default "I'm okay."
    mei "Where did you go, by the way?"
    any nor sigh "Trying to find food or first aid kit, but I don't find any."
    
    "Anya sneezes."
    any nor frown "Geez ...."
    hide anya with dissolve
    hide mei with dissolve
    "Anya turns around, facing away from me, and taking off her shirt."
    "There's an old scar on her back."
    "Anya once told us that she got injured in an accident when she was a child, but never said any detail about it."
    "The injury was severe, up to the point where it significantly affected her Aura."
    "That's the reason why between the four of us, Anya is the one who rarely uses her Semblance. She wants to reserve her Aura as much as possible."
    
    show mei nor frown2 at midleft with dissolve
    mei "Are you injured?"
    mei "I mean ... you went through a horde of Grimms to reach me back then."
    show anya nor default at midright with dissolve
    any "I'm okay."
    any "It was chaos, but I didn't get hit that much. My Aura could still manage."
    "I pass her jacket. Anya wears and zips it up."
    any nor frown "More importantly, what about you?"
    show mei nor default2 with cdissolve
    any "You got hit pretty badly."
    any nor sigh "You have your Aura to reduce the impact, but you still unconscious."
    
    "I touch the temple of my head, where the Grimm hit me."
    show mei nor frown2 with dissolve
    show anya nor default with dissolve
    "No open wound, at least."
    "Wound on the forehead or the temple usually bleeds more even if it's only a scratch. It makes people panicked and thought the wound is severe."
    mei nor smile "I'm okay. It's not hurt anymore."
    mei nor frown2 "I ... I thought I fell down from the walkway."
    any nor shout "You were!"
    any nor frown "You're lucky the Grimm didn't attack you again when falling down and you landed on a big pipe not too far below."

    show mei nor sad with cdissolve
    "Oh .... So, that was what happened."
    "I shiver. But not because it's cold."
    "The realization that I almost died, scared me."
    "I ... don't want to think or talk about it. Should change the topic now."
    mei nor default "By the way, it's awesome that you managed to hold off the giant Grimm, you know?"
    any nor sigh "Uh .... I was lucky."
    any nor frown "If I didn't go behind it and the area was bigger, I don't think I could leave the fight without losing an arm or a leg."
    any nor sigh "*sigh* Speaking about that creature worries me ...."
    
    any "It fell into the pit hole, which is quite deep, but I doubt it's a problem for it."
    any nor default "So far, it doesn't chase us, which is a relief."
    mei nor frown2 "But, do you think it would let us go just like that?"
    mei "As far as I know, Grimm creatures don't have a concept of revenge. They might memorize certain people, but not actively seeking revenge."
    mei "However ... I'm certain that three-headed Grimm is a chimaera-type ...."
    mei "Maybe, previously, they were normal Beowolves, but then a posession-type Grimm fused them together."
    
    show mei nor default2 with cdissolve
    "Posession-type Grimm, especially the parasitic one, is well-known for creating a dangerous, bizzare fused Grimm creature, which we dubbed as 'chimaera-type'."
    "There's a few chimaera-type creatures that are already documented, but since there's so many possibilities of which Grimm fused with which Grimm, meeting a whole new kind is to be expected."
    any nor frown "By the way, did you notice some of those Grimms have crystal-like spine rather the usual bone-like?"
    mei nor surprised "Yes. I've never seen anything like that before. {i}What{/i} are they?"
    mei "I want to say they're another chimaera-type, but ... something's off about them."
    
    any "I contacted Ulm when I walked around looking for supplies earlier."
    any nor default "I told him about the Grimm, but he has no idea about them."
    show mei nor default2 with cdissolve
    any "I also asked him about those symbols I found around the place."
    any "He knows about that one."
    
    any "He said it's Merlot Industry symbol. A chemical engineering corporation based on Mountain Glenn."
    mei nor surprised "Oh! So that's their symbol!"
    mei nor frown2 "From what I heard, Merlot Industry was shut down when the city in Mountain Glenn destroyed. There's no single news about Merlot Industry since then."
    mei "But 20 years ago, there's ... rumor, I guess? About a secret research facility ran by the founder and director of the corporation, Dr. Merlot."
    mei nor frown "For sure, it raised suspicion. A {i}secret{/i} research facility? It never gives away any good sign."
    
    any nor frown "This place is probably that facility."
    any "Was there a raid or something to this place?"
    "I shake my head."
    show anya nor default with cdissolve
    mei "I don't know."
    mei nor default2 "Whatever happened here never makes it into our history class."
    any nor frown "A research facility, huh ...?"
    any "That explains those strange Grimms and Cerbie."
    mei nor frown2 "'Cerbie'?"
    any "That huge three-headed Grimm. From 'Cerberus'. You know the story, right?"
    
    mei "Well ... I know the story, but that's not how you pick names for the Grimm creatures."
    any nor smirk "'Cerbie' sounds okay."
    show mei nor default with dissolve
    "I'm really amused with this one-sided name giving, all I could do is shaking my head slowly, but smiling."
    mei "You should rest, Leader."
    mei "You've dragged me here, treated my wound, set up protection around this temporary shelter, and tried to find out supplies."
    mei "That's a lot of work."
    any nor default "It's nothing."
    any "By the way ...."
    hide anya with dissolve
    hide mei with dissolve
    
    scene cg shelter with fade
    "Anya takes her hip flask and offers it to me."
    any "Not liquor, I promise. Though I know you're quite good with one."
    any "A sip or two can warm you up."
    "That makes me remember that we still have something to eat, even though it's only minimal amount."
    "I rummage through my pouch and pockets, pulling out a pack of (very likely, crushed) cookies and ...."
    
    any "Is that the protein bar from Forever Fall mission?"
    mei "You were the one who told me to bring the leftover."
    any "But it has been days since that mission!"
    "Despite of her reaction, Anya takes the protein bar from my hand and inspects it closely."
    any "Still as hard as a rock. I'm pretty sure this thing is designed to endure extreme weather and environment."
    mei "The most important question is whether it's still edible or not."
    any "I don't mind eating this."
    
    scene bg outpostnight with fade
    "Our 'supplies' barely fill our stomach. But, we don't have much choices. Beggars cannot be choosers."
    "Anya's drink is both sweet and spicy. I never drink anything tastes like it before."
    "She's right about the mysterious drink warms me up, nevertheless."
    "Out of curiosity, I start a conversation."
    
    show mei nor frown2 at midleft with dissolve
    mei "Is it hurt? The scar on your back, I mean."
    mei "I heard some scars are hurt when the weather is cold."
    show anya nor default at midright with dissolve
    any "It isn't."
    any nor sigh "Honestly? I can't really feel anything around where that scar is."
    any "Like it's numb or, I don't know? 'Hollow'?"
    any nor default "It wasn't immediately taken care back then. No wonder if it messed up the nervous system or something else."
    
    mei nor surprised "It wasn't immediately taken care?"
    mei "But why?"
    any nor sigh "Uh, well ...."
    show mei nor frown with cdissolve
    "I squint my eyes at Anya, warning her not to make up any ridiculous lies."
    any nor frown "I thought it's going to heal."
    any "Apparently, I almost died of it."
    mei nor frown2 "Wasn't it hurt? A lot? I mean, really, {i}really{/i} hurt?"
    any nor default "I don't really remember about it being hurt. Maybe? I don't know."
    
    "It upsets me to hear how ignorant Anya could be with her own condition. The wound was no joke! And how old was she at the time?"
    any nor frown "It was ... well ...."
    show mei nor default2 with cdissolve
    any "I tried making my very first weapon. A gun. It could shoot bullets loaded with fire Dust which each of them exploded. It was a prototype for my current gun."
    any nor default "I put it in a holster on my back, bringing it wherever I went."
    any nor sigh "One day, it exploded and wounded my back."
    any nor frown "Nobody was around that time, but, luckily, I wasn't that far from home. I found out how bad the wound was when I was in the bathroom, trying to treat it."
    
    mei nor surprised "You didn't ask anyone for help?"
    mei "Your mom?"
    any nor default "Mom is the last person I would tell about it."
    any nor sigh "It was my own fault. I have to fix it myself. Mom already has to struggle dealing with her problems. I don't want to give her more trouble."
    mei nor angry "But you're dying!"
    "Anya stares at me, clearly considering whether to continue her story or not."
    
    show mei nor default2 with cdissolve
    any nor default "I'll skip the detail about the wounds, alright? It's pretty bad. You've seen the scar."
    any nor frown "Hmm ...."
    mei nor frown2 "What is it?"
    any nor frown "I don't remember the detail after that. Odd ...."
    show mei nor default2 with cdissolve
    any "Mom found out. Somehow. I don't know how she did it. She just knew I was badly wounded."
    any "I remember I cried and said sorry to Mom .... But, it's vague."
    any "I can't even remember how did I make it to the hospital ...."
    mei nor frown2 "Maybe your mind unconsciously try to shut it down. Sometimes it happens to bad memories."
    
    any nor default "Yeah, maybe."
    show anya nor smile with cdissolve
    "Anya grins."
    any "This is the first time I talked about that accident with someone else, you know?"
    "I ... forgot to consider whether it is a sensitive issue for Anya or not before raised the topic."
    "I don't usually forget about that. Really."
    mei nor sad "I'm sorry."
    any nor smirk "Nah, that's okay."
    any nor sigh "Though, it's a bit embarrassing to talk about some stupid mistakes we've made in the past."
    mei "I'm sorry."
    
    "Anya lightly taps my shoulder with her knuckles."
    any nor smirk "I said that's okay."
    show mei nor default2 with cdissolve
    any "I don't mind sharing one or two stories like that if it's you."
    any nor default "Now, get some rest and let's hope Cerbie won't tear down this place while we're sleeping."
    mei nor default "I could stay up watching for a few hours."
    any "Three hours, okay? Wake me up after three hours."
    mei "Understood."
    hide mei with dissolve
    hide anya with dissolve
    
    scene black with fade
    call chapter("End of Part 8", "Stranded") from _call_chapter
    
    jump part9BA
    
    #return