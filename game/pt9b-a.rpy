# PART 9: CERBERUS #

label part9BA:
    
    "I think I had a nightmare ...."
    scene bg outpostday with fade
    "But when I woke up this morning, stiff from lying on concrete floor, I couldn't recall any detail of my dream."
    "... And I don't remember sleeping with a folded jacket cushioned my head ...."
    "Anya's shirt was soaked and she wore her jacket last night. When did she exchange again? Did she already wake up?"
    "The room I'm in is empty. Anya's not here."
    
    "After strapping my cloak back, I walk outside. I bring the jacket with me."
    
    scene bg island with fade
    "It's foggy and cold after the rain last night."
    "I squint my eyes and see a couple of Grimms on the other side."
    "The fighting against the giant Grimm yesterday had destroyed most walkways and platforms, making it difficult to pass over the gap."
    
    "Except for Anya humming a song, it's really quiet."
    "Recently, I listen to that song closely, trying to figure out what song is that."
    "I don't remember anything resemble that tune. It's not any classic pieces, for sure."
    "It's probably something from the current trend, which I don't really follow."
    
    mei "Anya?"
    "The humming stops. As I've thought, Anya is nearby."
    any "How're you feeling?"
    "Anya--apparently--climbed on to the roof of control panel room and had been there since ... I don't know when."
    "She's probably watching the surroundings."
    
    "She jumps down."
    "And the first thing I do is grabbing her black shirt."
    "It's still damp and cold, as I suspected."
    show mei nor frown at midleft with dissolve
    mei "It's still wet."
    
    show anya nor sigh at midright with dissolve
    any "It's ... fine."
    mei "No, it isn't."
    "I shove the jacket into her hand."
    any nor default "Oh, right. You seemed like having a bad dream last night."
    
    any "I thought that's because you slept on hard concrete."
    mei nor frown "Did you even {i}sleep{/i} last night?"
    mei "How come you noticed me being uncomfortable and then cushioned my head with your jacket?"
    
    any nor smirk "I'm a light sleeper."
    "Anya shrugs, trying to make it looks like no big deal."
    any "And I take a nap whenever I can, so it's not really a problem if I only managed to get 2-3 hours sleep last night."
    "Oh, well, I guess she's being honest. Anya doesn't look sleepy or exhausted."
    
    any nor default "By the way. Is there any update from Ulm?"
    show mei nor default2 with cdissolve
    "I check my Scroll immediately."
    "There are a few messages, most of them are from Ulm and Rhodo."
    
    show mei nor surprised with dissolve
    "And from Mom."
    mei "Ulm sent me a map, but I need to reply Mom first."
    any nor frown "{size=15}Your mom's gonna kill me for this ....{/size}"
    mei nor angry "Of course she wouldn't. It's not your mistake."
    
    mei nor frown2 "Just check on the map. I've forwarded it to you."
    any nor default "Roger that."
    hide anya with dissolve
    hide mei with dissolve
    "I open Mom's messages. They were sent last night, at around 8 o'clock."
    "I hope she doesn't grow suspicious for my delayed response. She only asked about school, not a really urgent matter."
    
    nvl clear
    m "M: Whoa"
    m "M: Sry for the late reply"
    m "M: I sleep early lst nite"
    
    "I almost type about being a bit tired, but then stop."
    "Let's not make her worried and call around this time."
    
    m "M: Test's almost here"
    m "M: Btw. What's up mom?"
    
    "I wait for a moment."
    "The messages are already sent, but there's no sign that Mom see and read it."
    "At least I already responded her."
    "Now back to our current situation."
    
    show mei nor default2 at midleft with dissolve
    mei "What about the map?"
    show anya nor frown at midright with dissolve
    any "It's ... the map of this island."
    any "Ulm said, it's from Professor Goodwitch herself."
    any nor default "I think Beacon had sent someone to this island in the past."
    
    "Anya shows me the map."
    any "To put it simple, we have to go to this marked spot."
    mei nor frown "You couldn't 'put it simple' for this one."
    mei "We have to clear up the area for the airship, at least making sure the Grimm wouldn't attack it when it get closer to the ground."
    
    mei nor frown2 "The three-headed Grimm is still alive and roaming this place too, right ...?"
    any nor sigh "Yep."
    any nor frown "I'm glad it didn't attack us while we're sleeping."
    "I agree."
    
    "On the other hand, it has been hours since our last fight with the giant Grimm. It should've been recovered from its injuries."
    "And while it isn't common behaviour for a Grimm to target a certain human being, we are the only humans it could find now."
    mei "What time the airship should arrive?"
    any nor default "Two hours from now. Approximately."
    
    "If we were not to worry about that certain Grimm, two hours are plenty of time."
    mei "And, more importantly, have you located our position in the map?"
    any nor frown "Err ...."
    show mei nor frown with cdissolve
    "Not yet. Of course."
    "I wouldn't say I could determine our location immediately. We walked quite a distance after we sent the location data yesterday."
    
    mei nor default2 "I think we should move now. Time is essential."
    any nor default "You sure you're okay?"
    mei nor frown2 "?"
    mei "Why I wouldn't?"
    
    any "You're just wake up and maybe need a little time."
    mei nor frown "I'm not a sleepy head like you."
    mei "You're the one who keep sleeping during classes."
    any nor frown "Ouch."
    
    hide anya with dissolve
    hide mei with dissolve
    
    "I usually have a cup of coffee in the morning, but it's not really an obligation."
    "I skipped having one during the last Forever Fall mission and it didn't affect me that much."
    "Having coffee is more like a habit rather than addiction for me."
    "Speaking of addiction .... What about Anya?"
    
    "Her daily sugar intake is pretty high, considering her strawberry milk consumption, her breakfast which is usually dominated with sweets, and twice amount of dessert she takes during lunch and dinner."
    "Ulm is really concerned about that. It's unhealthy to eat so much sugar everyday."
    "But, apparently, we could never treat human bodies as the same. Anya doesn't seem to have problem at all."
    
    "However ...."
    show mei nor default2 at midleft with dissolve
    mei "I should be the one asking, by the way."
    mei "Are you sure you're okay?"
    show anya nor sigh at midright with dissolve
    any "I'm hungry, but it's not the time to complain."
    
    any nor frown "I'm going to buy myself some shortcakes when we're back to Vale."
    hide anya with dissolve
    hide mei with dissolve
    "She sounds like she's going to avenge the death of her dearest pet."
    "I still have a piece of cookies, in case I need it for a quick energy refill later."
    "Should I let Anya have it? I think she needs it more than me."
    
    "But on the other hand ... it would be a lie if I said that I don't need it."
    "...."
    "Well, Mei, it's not the time to be selfish now."
    "I hand over the last cookies to Anya."
    show anya nor default at midright with dissolve
    any "Hm?"
    show mei nor default at midleft with dissolve
    mei "Just take it."
    mei "I think you need it more than me."
    
    any "I'm not that hungry--"
    "Following the usual comedy cliche, her stomach growls furiously."
    mei nor smile "Yeah?"
    any nor sigh ".... *sigh*"
    
    any "Fine .... Lemme have half."
    mei nor frown2 "Why half? Just take it all."
    any nor smirk "Nah, half is good enough."
    mei "Are you sure?"
    
    any "Yeah. Besides, you bring cookies to keep you from blacked out due to Aura usage, right?"
    any nor smile "Let's not risk it. Keep the rest for yourself."
    hide anya with dissolve
    hide mei with dissolve
    "I don't really like being reminded of my weakness, but Anya has a point."
    "I quietly munch the cookies."
    
    "It's rather difficult to trace our way back, but once I recognized a landmark, it's a lot easier to find our way to the tower where we sent our position data."
    "I wouldn't lie; I'm a bit on the edge."
    "We're starving, didn't get a proper rest, and stranded in a mysterious island filled with dangerous Grimm creatures."
    "No matter how we toughen ourselves up, we took a few wrong turns even though the direction on the map is clear."
    
    "At least for me, taking wrong directions is a glaring hint of our poor condition."
    "However, Anya, as always, doesn't show any worries."
    show anya nor smile at midright with dissolve
    any "It reminds me of the initiation day."
    show mei nor frown at midleft with dissolve
    mei "We were lost, but we weren't exhausted like now. It's really different."
    
    any "A reminiscence doesn't need to be an exact same situation, right?"
    mei nor frown2 "It should be more than 50 percent same."
    any nor smirk "Is it?"
    hide anya with dissolve
    hide mei with dissolve
    "Anya starts humming the song again."
    "I thought she was going to continue our conversation. It feels like it ended abruptly."
    "Well ... I'm not really in the mood of small talk either, so I guess it's for the best."
    
    "We stop to check on the map. According to it, we have to walk on a cliff, near the sea, to reach our destination."
    "The pathway is wide. I think someone whose afraid of height could walk through this pathway without worries."
    "And I have to admit the scenery around here is actually nice."
    "I stop and look to my left, where the sea stretches to horizon."
    "No ships or boats. No tall buildings. No people."
    "No Gri--"
    
    #Grimm low growl
    
    "What is that?"
    "I turn around while unsheathing Alpenaster."
    "No Grimm from behind."
    "From the front?"
    "Just now, I noticed that Anya kept walking when I stopped to see the sea. There's quite a distance between us."
    
    "She clearly heard the growl too. I see it from the way she holds her cane."
    "Still no Grimm. But that sound was really, really close."
    "I hold my breath."
    "{i}Above.{/i}"
    
    "I look up and catch a glimpse of something big moved away from the edge of the cliff."
    "Oh dear .... Was it the three-headed Grimm? Cerberus?"
    "Or was it {i}another{/i} giant, mutated Grimm?"
    "We haven't even dealt with Cerberus properly, we don't need another one."
    
    "I catch up with Anya. Both of us hold our weapon ready."
    show anya arm shout at midright with dissolve
    any "It didn't attack us."
    show mei arm surprised at midleft with dissolve
    mei "And we should be grateful that it didn't."
    any arm frown "I mean, why?"
    show mei arm frown2
    any "We saw it with a bunch of Beowolves and Creepers, meaning it has some sort of control over them."
    
    any "If it wanted to attack us, why not commanding its pack of Beowolves?"
    any "There's enough space for them here."
    hide anya with dissolve
    hide mei with dissolve
    "In sync, we pause and listen."
    "No signs of incoming pack of Grimms whatsoever."
    "I think it is what we might call a calm before the storm."
    
    mei "I'll check the map again for some alternative routes then."
    "I open the map on my Scroll, quickly find our current location, and then draw some new routes to our destination."
    "Anya, on the other hand, start tapping on nearby objects, while mumbling something to herself."
    show mei nor default at midleft with dissolve
    mei "Thinking of how to fight the giant Grimm?"
    show anya nor frown at midright with dissolve
    any "More or less. This place ... used to be some sort of factory, right?"
    mei nor frown2 "I suppose so."
    
    any "There should be some heavy duty machines around, that's what I think."
    mei "Not sure they would be in a prime condition or usable."
    mei nor frown "Except if you plan to use them in other ways."
    hide mei with dissolve
    hide anya with dissolve
    "I'm quite sure Anya doesn't plan to use the machines like how they suppose to."
    "Something I wouldn't comment at the moment, because I don't see why she shouldn't."
    
    "This island and the factory have been left behind for nobody knows how long. The only one who could get hurt is either us or the Grimms."
    "I think Anya could blast a thing or two if she want."
    "... As long as she doesn't hurt herself, that's for sure."
    show mei nor default2 at midleft with dissolve
    mei "I think I've got all the routes. Which one should we take now?"
    show anya nor default at midright with dissolve
    any "Let's carry on with our first plan. You branched the alternatives from our current route, right?"
    mei "Yes."
    any nor smile "Great. We'll see whether we need to take a detour or not when we reach each branch."
    
    hide anya with dissolve
    hide mei with dissolve
    "That sounds like the best idea for now."
    "I nod and put away my Scroll. Alpenaster is ready, loaded with Dust cartridge."
    "We encounter some small Grimms along the way. Weak ones, couldn't even be counted as warm-up."
    "However, we reach a dead end. Rubble blocks the path we're supposed to take."
    show anya nor default at midright with dissolve
    any "Aha, we forgot about how old that map is."
    show mei nor frown at midleft with dissolve
    mei "I'm not going to say it as positively as you did."
    
    mei nor frown2 "Shall we go ... there?"
    mei "We might find another way."
    any "Sure."
    hide anya with dissolve
    hide mei with dissolve

    scene bg island with fade
    "Think of a maze, with rock structure and big pipelines as its wall. That's basically the place we go into."
    "Anya climbs the pipes on our left. Each of these pipes are big enough for a human adult to crawl inside of it."
    "What was going on this island? It needs a lot of resource to build a facility as big as this one."
    
    show mei nor default2 at mcenter with dissolve
    mei "Find something?"
    any "There's a ladder over there. Can you check on the map where that lead to?"
    mei nor frown2 "Hold on ...."
    "Before I take out my Scroll, something big lands on top of the pipelines, followed by loud roar and then ... a yelp."
    mei nor surprised "!!!"
    mei "Anya!"
    hide mei with dissolve
    
    "Cerberus leaves. Fierce growl come closer from behind me and when I turn around, I see at least a dozen of Grimm creatures are heading to my way."
    "Some of them have found a way to the top of pipes and quickly surrounds me. Two Beowolves lunge at me from the top of the pipes."
    play sound glyph
    "My Glyph is faster. Icicles formation formed around me, sharp enough to impale the two Beowolves."
    "I should be afraid and panicked in this situation, right? Even though all those practices I've been through."
    "Well, no, I'm not afraid."
    
    "The cursed Grimm had my partner."
    "I'm not afraid."
    "{i}I'm angry.{/i}"
    play sound ice
    "I cast an ice wall, halting the incoming Grimms. Some of them managed to slip through. I eliminate one--almost two--with a precise icicle shot."
    "One of the Beowolf pull my cloak. In retaliation, I spin around and drive Alpenaster deep into the creature's stomach."
    "Another one comes from the front."
    
    "I pull my dagger and force blast the wounded Grimm with my Glyph. It hurls and hit a couple of Beowolves who haven't got a chance to attack."
    "I dodge the incoming attack--a little too late and got scratched--and counter with a stab on the Beowolf's throat."
    "The next ones aren't too lucky. I finished summoning the knight's gauntlet over my own arm. Beowolves' claw are nothing against it."
    "I grab the nearest one, chuck it to the pipelines, and smash flat the next one."
    
    "My ice wall has started to crack. Three Beowolves managed to climb over it."
    "Before I could do anything to them, I heard a gunshot. The bullet hit the ice wall, explodes, and kill most of the Grimms."
    "The rest of them slowly withdraw."
    unk "Mei, c'mon, we gotta hurry up."
    "It's ... Anya."
    
    "She's definitely not alright, that's why I don't ask the question."
    "Half of Anya's face is covered in blood. Her nose bleeds and when she wipes it, I notice her right arm moves as good as a wooden plank."
    "There are white 'veins' spread from some spot on her arm and except I've got something on my eye, the skin in her arm looks a bit bluish."
    show anya arm pain at midright with dissolve
    any "Do I need to explain?"
    show mei arm frown at midleft with dissolve
    mei "You need to treat your wound."
    any arm sigh "It can wait. Trust me. Let's go."
    
    any arm default "Can you run?"
    "I pinch the bridge of my nose."
    mei arm angry "Anya, the right question is could {i}you{/i} run?"
    "Anya replies with a grin."
    any arm smirk "Not too fast, okay? Go ahead. To the ladder."
    hide anya with dissolve
    hide mei with dissolve
    "Fortunately, this pipeline network isn't a maze like I've said before."
    
    "The direction is pretty clear, with not many turns and dead ends like what a maze is."
    "While we go to the ladder, I ask a few questions to Anya about what happened."
    "Especially, what happened to Cerberus?"
    show anya nor frown at midleft with dissolve
    any "Not sure. I stuffed one of its head with ... grenade."
    any "It was collapsed and I left. I didn't check on it again. Sorry."
    "Unfortunately this isn't the time to lecture her about the importance to make sure a Grimm is properly killed or not."
    "She mentioned something more 'interesting'."
    
    show mei nor surprised at midright with dissolve
    mei "... Grenade?"
    any nor default "Ice Dust Grenade."
    mei nor angry "Now, I demand a full explanation about this. What grenade do you mean?"
    mei "Another gizmo you've made without our knowledge? And I thought we've banned you from touching anything Dust related."
    any nor default "I made it this morning. It's actually a big makeshift Dust cartridge, not a grenade ...."
    mei nor surprised "Where did you get the Dust??? And I'm quite sure it's related to the wound in your right arm."
    any nor pain "Never put a grenade into a Grimm's mouth with bare hand. Lesson learned."
    any "I've got a good amount of ice Dust shards here. {i}In{/i} my arm."
    
    show mei nor frown2 with cdissolve
    "... Oh my God, Anya ...."
    mei "Where in Remnant did you get the Dust ...?"
    any nor default "Someplace here."
    mei nor angry "You didn't tell me."
    any "Yeah. I told you what I didn't find, but not what I found. Didn't find first aid kit or food, find Dust instead."
    mei "You have to stop doing dangerous thing."
    any nor smile "The Dust you told me not to touch saved me from Cerbie."
    hide mei with dissolve
    hide anya with dissolve
    
    "Well ... that's right. I couldn't say otherwise."
    "We reach the ladder."
    "Halfway climbing it, I see the giant Grimm figure. It lies still on the ground, one head is stuck in a space between two set of pipelines."
    "It's still alive."
    "This is a trait usually found on King Taijitsu, two-headed giant snake Grimm creature."
    
    "King Taijitsu could only be killed if both of its heads are taken down."
    "I once read, there's a chimera-type formed from about four King Taijitsus, but instead of taking down its multiple heads, the chimera-type could be defeated by killing the parasitic Grimm which fused them together."
    "... Huh. That's right."
    show mei nor frown2 at mcenter with dissolve
    mei "Anya, did you see anything that looked like a core or a smaller parasitic Grimm on Cerberus?"
    any "Not really .... I would have tried to attack it if I saw one."
    hide mei with dissolve
    
    "Hmm, that's odd."
    "I suppose this Grimm is something else. Especially if we take the crystal-like spikes into consideration."
    any "But I haven't seen its back yet. Maybe there's something there."
    "With me climbing the ladder first, I'm currently in higher position than Anya."
    "I turn around to see the Grimm."
    "It's kinda far away from our position, but hopefully I could see its back and check whether there's a core or another smaller Grimm there."
    
    "The Grimm moves, slowly rises back to its feet."
    show mei nor surprised at mcenter with dissolve
    mei "It wakes up ...."
    any "It's not dead yet?"
    hide mei with dissolve
    "I pace myself up, climbing faster."
    any "Correct me if I'm wrong, Mei."
    any "From our previous fight, it's clear that you could cut the creature with the summoned sword."
    show mei nor frown2 at mcenter with dissolve
    mei "If it didn't bite and hold onto the blade."
    
    any "Then we need to make Cerbie stay still. Or hit it from its blind spot."
    mei "Do you have a plan?"
    any "Not yet."
    hide mei with dissolve

    scene bg island with fade
    "We reach the end of the ladder."
    "However, not giving us enough time to breath, the giant Grimm easily catch up with us. It just need two long jumps to climb."
    "Its left head hangs low, not moving. From the looks of it, that head was the one which Anya stuffed with the Dust."
    "In sync, Anya shoots Cerberus while I summon the sword. Even though it is hit by explosive shots, the Grimm doesn't look affected."
    "It's agitated and withdraws a few steps, but the attack doesn't seem to hurt it."
    "I swing the sword downward, aim for Cerberus' middle head."
    
    "It sees the incoming attack and leaps back."
    "Do I need to tell everyone how annoyed am I to see the Grimm managed to dodge my attack?"
    "However, Anya has something in mind."
    show anya arm shout at mcenter with dissolve
    any "Switch to Flocks!"
    hide anya with dissolve
    "'Flocks' is the name we agreed to call one of my summon variations."
    "It's a trick I learned from Aunt Winter in which we call forth a flock of small Nevermores to distract the opponent."
    
    "I don't ask any question and do as Anya said."
    play sound glyph
    "I dismiss the sword and almost immediately, send a flock of Nevermores to surround Cerberus."
    "It wouldn't be for long, but at least the giant Grimm is now occupied."
    show mei arm default2 at midright with dissolve
    mei "Plan?"
    show anya arm frown at midleft with dissolve
    any "I'm gonna lure it to one of those container--"
    mei arm frown2 "The Grimm is too big to get into one."
    any arm default "I just need it to stick its head inside and avoid it from seeing what strike it from above."
    
    any "{i}You{/i} cut it from above. Lay low and wait for the chance to attack it. {i}Capiche{/i}?"
    any arm shout "Now go!"
    hide anya with dissolve
    hide mei with dissolve
    "I really want to disapprove her idea, thinking it's too risky, but in the end, I say nothing."
    "I don't have any better ideas right now, so even though I disagree with Anya, I couldn't offer anything useful."
    "But before I leave ...."
    show mei arm frown at midright with dissolve
    mei "Anya."
    show anya arm default at midleft with dissolve
    any "Yeah?"
    mei arm cocky "Do your best. Don't you dare disappointing me."
    
    "Anya smirks."
    any arm smirk "That's part of the plan."
    hide mei with dissolve
    hide anya with dissolve
    "I do a really quick survey of my surroundings, looking for a suitable spot to wait."
    "There are quite a lot of containers (and ... cages?) here. None of them are aligned properly. Some are even flipped upside down."
    "The highest spot would be ...."
    "That crane."
    
    "Halfway toward the crane, I hear gunshot and explosion."
    "I'm glad the Nevermore Flocks hold down Cerberus long enough."
    play sound glyph
    "I cast several Glyphs and reach the top of the crane in no time."
    "If there's something Anya really good at for both humans and Grimm creatures, it would be provoking them."
    "Cerberus is at her now."
    
    "The three-headed monster lunges at Anya, only to miss and stumble on row of containers. Anya use this chance to jump on top of nearby container."
    "Furious, Cerberus climbs and Anya greets it with another explosive shot."
    "... I start to worry whether Anya counts her bullets or not."
    "Unlike Rhodo's rifle or Ulm's crossbow, Anya's gun takes more time to reload."
    "Rhodo and Ulm have urged her to tweak her weapon mechanism, but ...."
    
    "But as long as my memory serves, I don't think she already fixed her gun."
    "I hold my breath as I see Anya aim her gun but nothing happened."
    "Is it jammed? Or is it out of bullets?"
    "Whichever it is, Cerberus almost has Anya. It growls as its prey made a narrow escape from its claw."
    "Anya runs inside an open container. The Grimm follows."
    "As planned, Cerberus could only stick its heads--two of them--inside the container. Only one if it try to put its claw inside too."
    
    "I could feel my stomach churning."
    "This is my chance."
    "While Cerberus frantically try to reach Anya inside the container, it wouldn't pay attention to its surrounding."
    "But first, I need to get closer ...."
    play sound [glyph, glyph, glyph]
    "I cast a series of floating Glyphs, which act as stepping platforms."
    "Above the Grimm, I jump."
    
    "Mid-air, the giant sword appears. I motion my hand, swinging the sword downward."
    "The blade cut through the head on the right. Halfway into the middle head, Cerberus step back."
    "With two heads down and the last one severely injured, Cerberus moves in a confused and awkward manner."
    "It's enraged, for sure, but couldn't seem to decide what to do: attack or withdraw."
    "As soon as I reach the ground, I take this chance to strike first."
    
    "The knight's sword cut the Grimm's upper jaw and half of its middle head."
    "Now with all of its heads gone, Cerberus collapses to the ground."
    "{i}Finally{/i}."
    "I stand still, waiting for any sign of life from the creature. Its body should be dispersed soon enough if it's really dead."
    "Or maybe not? Those green crystals on Cerberus' body aren't normal, so I shouldn't be surprised if this Grimm doesn't dispersed like common ones."
    
    "Curious, I poke the Grimm with Alpenaster."
    "Still not moving ...."
    show anya arm default at midleft with dissolve
    any "It's ... not disappear?"
    show mei arm frown2 at midright with dissolve
    mei "That's my question too. Does it have something to do with the crystals?"
    any "There were some Grimms with those kind of crystals too, right? The smaller ones we fought before."
    any "They explode when we defeated them."
    show anya arm shout
    show mei arm surprised
    with cdissolve
    any "...."
    mei "...."
    any "Run! TAKE COVER!!!"
    
    hide mei with qdissolve
    hide anya with qdissolve
    "We scurry to behind one of the nearest container."
    "How in Remnant could I forget about the explosion part???"
    #explosion sound
    "The giant Grimm explodes with muffled, low noise."
    "Oh, whew ...."
    "I peek from the corner of the container."
    "The explosion left behind quite an amount of green particles that falls like ashes before disappear."
    show anya nor smirk at midleft with dissolve
    any "We did it."
    "Anya grins and offers her closed fist."
    "I lightly tap my knuckles on hers."
    show mei nor default at midright with dissolve
    mei "Not bad."
    
    any nor shout "'Not bad'? Only 'not bad'?"
    any "Won't you lower your standard for this time?"
    show mei nor frown with cdissolve
    "I point at Anya's injured arm."
    mei "That's a huge minus in your performance."
    any nor sigh "Aww, c'mon, but it worked!"
    mei "I take no arguments for this one."
    mei nor default "Let's go. The carriage would be here anytime soon."
    
    any nor shout "'Carriage' ...?"
    show anya nor smirk with cdissolve
    "Anya snorts and chuckles."
    any "Then, let me escort you there, milady."
    hide anya with dissolve
    hide mei with dissolve
    
    scene black with fade
    "We catch the airship in time."
    "Fortunately, we don't encounter too many Grimms on our way to the rendezvous point."
    "Exhausted (and famished), I sleep almost immediately the moment the airship takes off from the island."
    "I remember thinking about what happened to the Huntsman who led the attacks at the port."
    "And what about Emerald?"
    "Does Rhodo or Ulm manage to get anything?"
    
    "My heart sinks a bit when I think the outcome of our investigation doesn't turn up good."
    "We might have let Emerald and her affiliates escaped."
    "But ... well ...."
    "Not every story has a good ending."
    "For now, I should be glad that we didn't get killed while stranded in the island."
    
    call chapter("End of Part 9", "Cerberus") from _call_chapter_4
    centered "End of Demo Version 0.8\nThank you for playing!"
    return
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    