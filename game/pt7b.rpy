# PART 7 : BETRAYAL #

label part7B:
    
    "During my first semester in Beacon Academy, my visit to the city could be counted with one hand."
    "Most of the times, I went with Li, not with any of my teammates."
    "I didn't think I would need to go to the city after Li went to attend Haven Academy, but how wrong I was."
    "I got bored after spending almost a month not going outside of the school vicinity and asked if I could come with my teammates when they were planning to go out."
    "Going out with each of them proved to be unique experiences."
    
    "With Rhodo, getting lost is almost a guarantee whenever we tried to go to a new place."
    "He frequently visits weapon shops, buying whetstone, nuts and bolts, or supplying his bullets. Sometimes he also gets his weapon and shield checked up."
    "While he's busy shopping, I browse the weapon catalogue."
    "It's not because I want to exchange Alpenaster with another weapon ...."
    "I just love seeing the intricate detail and specification of various weapons."
    
    "Going with Ulm is like having historical tour around the city."
    "Ulm memorizes so many locations, he almost sounds like a human-map."
    "Ask him where to find something and Ulm would come up with a few options, each places' location, and how to reach them."
    "If you inquire further, Ulm could go on and tell you about the first owner of the place."
    
    "Anya doesn't have vast knowledge like Ulm does, but she knows Vale City better than me or Rhodo."
    "She knows Patch Island really well and more than once indicates she isn't a stranger to Industrial District, which Ulm rarely speaks of."
    "And here we are ... in one part of Industrial District, the port of Vale."
    
    scene bg port with fade
    "Ulm had gone ahead of us to scout the surroundings and made a few 'preparations' around the port."
    "I've reviewed our plan today for about ten times and it doesn't make me feel better when we arrive in our destination."
    "Rhodo walks next to me and looks around warily. Anya, whistling and humming a song, leads the way through large containers."
    "We visit a place with a certainty that we are going to be ambushed."
    
    "The idea sounds really bizarre, indeed."
    "We're gambling with a chance that our opponents don't know that we're ready to counter-attack their plan."
    "None of us want this reckless plan to go wrong, so we put all our best effort when planning all of this."
    "However, as Rhodo and I agree, the more things we add to ensure the success rate of this plan, the more nervous we get."
    "At some point, we stopped trying to perfect our strategy."
    
    "At some point, all I did was making a promise to myself to give the best fight I could to survive."
    show rhodo nor surprised at mcenter with dissolve
    rho "Hey, Anya."
    rho "Why is the port so empty? Where's everyone? The workers?"
    hide rhodo with dissolve
    show mei nor frown2 at midleft with dissolve
    mei "I've been thinking the same thing too ...."
    show anya nor default at midright with dissolve
    any "Oh, that?"
    any "If there's no unloading, this place is empty like this. Vale isn't that busy, you know?"
    mei "Not even any security guards?"
    
    "Anya turns around, glancing to her right and to her left, while keep walking and makes her movement looks playful."
    "Her expression changes and she nods."
    any nor frown "Not even any securities."
    hide anya with dissolve
    hide mei with dissolve
    "She looks at me and points to her own ear, signaling me to put on my wireless earpiece."
    "I was assigned to keep contact with Ulm, who is already taking position somewhere in this place."
    "My task is to communicate with Ulm and coordinating the situation between the four of us without raising suspicion."
    "Easy to say, difficult to practice."
    "Even though the earpiece is hidden under my hair, I have the urge to put on my hood for extra protection."
    "I couldn't do that, nevertheless. Hooded figure always raise suspicion."
    
    "We arrive at the bar in the corner of the port."
    show anya nor smile at midleft with dissolve
    any "Good day, Mr. Starrick."
    "The bartender, a man with thin mustache, is wiping clean a small glass at the moment."
    "He looks at Anya and raises his brows, not greeting her with a smile like I expected from a bartender."
    star "Ms. Frye."
    star "I see you bring some friends here."
    any "My teammates. They're not drinking, unfortunately."
    star "I know. Somehow."
    any nor frown "And it's 'Lyre', not 'Frye'."
    any nor default "Rho, Mei, take a seat."
    
    "Rhodo and I sit at one of the table, while Anya sits on the stool at the bar. It {i}is{/i} part of the arrangement we made before come here."
    any "So, you have something you need to tell me."
    star "...."
    any "Spit it out."
    star "You shouldn't have came here, you know ...?"
    any "Kindly tell me {i}why{/i} I shouldn't have?"
    any nor sigh "Or, nevermind that. Tell me what you know, Mr. Starrick."
    
    "There's a long pause before Mr. Starrick speaks up."
    star "Recently, just like you've said, I saw a few people exchanged goods while drinking here."
    show anya nor default with cdissolve
    star "It's not that hard to notice 'em if you have small place like mine."
    star "Though, I'm not really sure what were them."
    any nor frown "How does the package look like?"
    star "Various packs. I saw them in plastic bottle, small paper bag."
    star "Hey, won't you order anything?"
    any nor default "Oh, right."
    any "Are you still serving The Bitter?"
    
    hide anya with dissolve
    "Anya pulls out a hip flask and slides it together with a few Lien."
    "Now, hold on."
    "I never know Anya brings a flask all this time. And what does she fill it with?"
    "Not alcohol, right ...?"
    "... But we're talking about Anya here. It seems nothing is impossible for her."
    "While Mr. Starrick fills the flask with a mix from three different bottle, Anya continues her questioning."
    show anya nor frown at midleft with dissolve
    any "Won't it be bad for you if the cops caught your customers in the middle of transaction?"
    star "They're clever and watchful. Beside, cops rarely come here just to check on my bar."
    any default "Really?"
    star "...."
    star "I don't look that smart, but I believe they already take that into account and put someone to watch if the cops are coming."
    star "This place has a few sweet spots for that."
    hide anya with dissolve
    
    "I hold my breath."
    show mei nor frown2 at midleft with dissolve
    mei "{size=15}Ulm? Are you there?{/size}"
    ulm "Yes?"
    mei "{size=15}Do you see anyone watching this place?{/size}"
    "Ulm doesn't immediately answer. All I could hope is they change their game and didn't send anyone to watch from higher grounds."
    "They might find Ulm if they sent someone to watch us."
    "Rhodo leans toward me and whispered."
    show rhodo nor surprised at midright with dissolve
    rho "{size=15}I don't see anything yet, but I have a bad feeling.{/size}"
    mei nor sad "{size=15}I'm worried about Ulm.{/size}"
    rho nor frown "{size=15}Shouldn't we worry about ourselves first???{/size}"
    hide mei with dissolve
    hide rhodo with dissolve
    
    star "Ms. Lyre."
    star "I'm sorry. I'm really sorry."
    star "But you have to leave. Now."
    show anya nor default at midleft with dissolve
    any "They made you call me here, didn't they?"
    star "Yes ...."
    star "I'm really, really sorry ...."
    star "They know where I live .... They know I've just had a daughter ...."
    
    any nor frown "You disappoint me, Mr. Starrick."
    any "Have you ever think that {i}we{/i} have family too?"
    any "Is it even worth it that I saved you from the trouble with Mr. Xiong?"
    hide anya with dissolve
    "Rhodo frowns and hisses to me:"
    show rhodo nor surprised at midright with dissolve
    rho "{size=15}Schnee! Someone's coming ....{/size}"
    show mei nor surprised at midleft with dissolve
    mei "{size=15}Ulm, you hear me?{/size}"
    ulm "In my position!"
    
    show rhodo nor frown with cdissolve
    "Rhodo quietly reaches to a bottle he bring along. He opens its cap and stuffs a piece of cloth on the bottle."
    hide rhodo with dissolve
    show mei nor default2 with cdissolve
    "I lean back, look at Anya and do my part:"
    mei "Anya, are we done here?"
    mei "We still have something to do after this."
    hide mei with dissolve
    show anya nor default at midright with dissolve
    any "I'll pay you a visit again, Mr. Starrick."
    any nor frown "Don't expect it to be nice."
    any nor default "But, anyway, thank you for your information and have a good day."
    hide anya with dissolve
    
    ulm "Seven approaching. I see four--uhm, I'm sorry. There are eight more, split into two groups of four."
    ulm "Fifteen in total."
    show mei nor surprised at mcenter with dissolve
    mei "How could they gather so many people?"
    ulm "Some of them are Faunus."
    ulm "Uh, gotta go."
    mei nor frown2 "?"
    hide mei with dissolve
    
    "Before I could inquire further from Ulm, Anya already leaves."
    "Anya doesn't look like paying attention until the goons block her way."
    show gn1 at midright with dissolve
    gn1 "Aren't you too young to drink, girl?"
    show anya nor default at midleft with dissolve
    any "Oh? Are you here to arrest me because of underage drinking?"
    any nor smirk "You don't need to bring so many people just to arrest a bunch of kids, do you?"
    gn1 "Good point."
    hide gn1 with dissolve
    "The guy nods at the goons by his side. Two of them approach us."
    
    "Before they get anywhere closer, Anya takes a step back."
    hide anya with dissolve
    "She flicks the dragon head on her cane, producing a lighter-like small fire from the dragon's mouth."
    "Rhodo walks next to her, lighting up the cloth he attached to the bottle previously with the fire from Anya's cane."
    show rhodo nor shout at mcenter with dissolve
    rho "Have a toast, guys!"
    hide rhodo with dissolve
    "And Rhodo throws the bottle toward the goons."

    play music brawl fadein 10.0
    
    show gn2 at midleft with qdissolve
    show gn3 at midright with qdissolve
    gn2 "Molotov cocktail?!"
    gn3 "Run!!!"
    hide gn2 with dissolve
    hide gn3 with dissolve
    
    "It cause quite a stir among them. Anya doesn't let her chance slipping away."
    "She hit nearby goon on the back of his knee, forcing him to kneel. His head becomes a nice target for her cane."
    "The other guys try to attack Anya, but I act faster."
    play sound glyph
    "A Glyph appears in front of him and blast a powerful force."
    play sound hardhit
    "The goon is blown away, hitting a container with loud thud."
    "Meanwhile, the others now realize the bottle Rhodo threw is nothing as dangerous as molotov cocktail."
    "The bottle broke down on the concrete and the liquid inside splashed. "
    "No explosion, "
    extend "no fire. "
    extend "Nothing."
    
    "But there's Rhodo rams directly into them with his shield up."
    "He manages to take down three people, before his opponents--partially--regroup."
    "I catch him up, shielding Rhodo's back from incoming attacks."
    
    show mei arm angry at midleft with dissolve
    mei "Get ready for ice!"
    show rhodo arm frown at midright with dissolve
    rho "Okay!"
    "I parry an incoming attack and counter with a slash on my opponent's chest."
    mei "Ice them!"
    hide mei with qdissolve
    hide rhodo with qdissolve
    "Both Rhodo and I jump as high as we could."
    "An icy blue shot hits the concrete ground, right in the middle of the puddle from the bottle."
    "The unaware goons below us slip on the sheet of ice formed beneath their feet. A few of them falls down."
    "Some of them are clearly surprised, but managed to hold their position, even though they couldn't move freely like before."
    "And the second shot comes."
    "It's red-colored."
    
    "The ice sheet blasts into mist screen, confusing our opponents further."
    "Before going back to the ground and taking the remaining goons, I see Anya has climbed the containers spread around the port."
    "She gleefully jumps from container to container, while keeping an eye for any prey below. I think I heard her confirming something with Ulm before the mist blast."
    "I, on the other hand, need to focus on cleaning up with my part."
    
    "Rhodo lands on an unsuspecting guy, immediately incapacitates him."
    "More than ready to wreak a havoc, Rhodo doesn't even bother to see where he swings his lance."
    "He almost hit me twice."
    show mei frown at midright with dissolve
    mei "Look at your surroundings, blockhead!"
    show rhodo arm shout at midleft with dissolve
    
    rho "Can you {i}really{/i} see with all of this smoke?"
    mei "It's {i}mist{/i} not smoke."
    rho confused "Aggh, really, Schnee?! We can talk about that later!"
    "I load fire Dust to Alpenaster. Rhodo shifts his lance to its rifle form."
    "In sync, we move to the opposite side."
    hide mei with qdissolve
    hide rhodo with qdissolve
    "I lash Alpenaster, sending a crescent fire blast to a shadow creeping from behind the mist. Rhodo kneel and opens fire to a couple of silhouette approaching."
    
    show rhodo arm frown at midright with dissolve
    rho "I think we should move now. The longer we stay here, the easier they find us."
    show mei arm frown2 at midleft with dissolve
    mei "Yes. Let's go."
    hide rhodo with dissolve
    hide mei with dissolve
    
    "We shift position again. This time I'm the one who lead the way and Rhodo on my back to take down anyone attacking us from behind."
    "A bunch of goons run toward us, but they don't get near enough."
    "At least three or four of them are taken down by Ulm's shot. When they stop and try to locate Ulm's position, another one is down."
    "Their next fault is they are distracted by another defeated goon who is hurled toward them."
    "When they look at us again, Rhodo is already close."
    play sound hit
    "He smashes the nearest goon's nose with the back of his rifle." with hpunch
    
    "I turn around, finding the mist has gone and four more thugs are approaching us fast."
    "I crouch, dodging a wild slash aiming for my neck, and lunge back at my attacker, shoving him away with my shoulder."
    play sound glyph
    "A big Glyph shines beneath the thugs. The next thing they know is a huge hand punches them from below, sending them flying to the sea."
    show mei arm cocky at mcenter with dissolve
    mei "Have a nice flight!"
    "It is a premature celebration."
    show mei arm surprised with cdissolve
    play sound loudbang
    "Something hit me and explodes." with hpunch
    hide mei with qdissolve
    
    show rhodo arm surprised at mcenter with dissolve
    rho "Schnee!"
    mei "I'm--I'm okay!"
    hide rhodo with dissolve
    "It hurts, but doesn't do damage beyond repair."
    "Considering its power, another hit could break open my Aura protection. I have to take extra caution."
    "I move closer to a container box, taking cover. Rhodo stays nearby with his shield ready."
    
    show rhodo arm frown at mcenter with dissolve
    rho "Damnit ... someone shot us from above?"
    mei "!!!"
    mei "Behind you!"
    rho arm surprised "Huh?"
    hide rhodo with qdissolve
    "The previous attack definitely affects my reflex."
    "I didn't cast my Glyph fast enough to repel the goon sneaking behind Rhodo."
    
    "Fortunately, Anya hooks the perpetrator's wrist from behind, hindering him and giving Rhodo enough time to counter-attack."
    play sound hit
    gn3 "ARGH!!!" with vpunch
    show rhodo arm shout at mcenter with qdissolve
    rho "Take that, you goddamn--!"
    "Anya grabs Rhodo and pulls him away."
    hide rhodo with qdissolve
    "Another shot comes."
    "But this time, it misses its target."
    show anya arm default at mcenter with dissolve
    any "Ulm?"
    
    ulm "I find them, but not in my scope ...."
    ulm "This is really strange."
    any arm sigh "Which Sightjack did you use?"
    ulm "Uh ...."
    ulm "{size=15}I'm sorry ... it was the usual one. Let me try the other one ....{/size}"
    ulm "{size=15}I'm sorry ....{/size}"
    hide anya with dissolve
    "Ulm has 3 'version' of Sightjack."
    "The one he often uses is seeing with another person's or creature's point of view as if he 'hijacks' their sight."
    "The other two allow Ulm to see in the dark and to see in a heat-vision-like sight."
    "His Semblance is incredibly useful, but Ulm tends forget the 'night-vision' and the 'heat-vision' every now and then."
    
    ulm "Okay. I've taken care of them."
    ulm "Two groups of four I said before? I can't find them anymore."
    show anya arm smirk at mcenter with dissolve
    any "Oh? Them? You see the net hanging near the port entrance?"
    ulm "It's five of them. There are three more and their leader."
    any arm sigh "*sigh* That's not all of them? Shame."
    any arm default "I want the leader at all cost. He's the guy I talked to in front of Crow Bar."
    any "Split into two. One goes with Ulm from above, the rest approach them from the ground."
    
    menu:
        "Go with Rhodo.":
            jump rgo
        "Go with Ulm.":
            jump ugo
        "Go with Anya.":
            jump ago
    
label rgo:
    $ rhodo_pt += 5
    show mei arm default at midleft with dissolve
    mei "I would go with Rhodo."
    mei "I could trust you to at least protect me until my Aura fully recovered, right?"
    show rhodo arm surprised at midright with dissolve
    rho "Eh? You're going with me?"
    mei arm frown "Yes, blockhead. I don't usually need assistance, but when I do, I know who I could rely on."
    rho arm smile "Well, that's an honor! I won't let you down then, Schnee!"
    any "I'll go with Ulm then."
    any arm smirk "Good luck and be careful."
    hide anya with dissolve
    
    "Anya joins Ulm who has left his hiding spot."
    rho arm surprised "Uh ... where should we go?"
    mei arm default2 "If they haven't ran away and wait to ambush us, I suppose it doesn't matter where are we going?"
    mei arm frown2 "But, I think if we want to capture the leader, we have to reach the port exit first before him."
    mei "In case he decide to run."
    rho arm default "Okay, got it."
    hide rhodo with dissolve
    hide mei with dissolve
    
    "Rhodo leads ahead. I follow closely behind him."
    rho "I think they left some trap for us."
    "I look at the spot Rhodo points at."
    "There's a very thin wire stretches from two containers. At one of its end, there's a small device."
    show rhodo arm frown at midright with dissolve
    rho "Is it something from SDC? If we can somehow disarm and reuse it, it's going to be a surprise for them."
    show mei arm frown2 at midleft with dissolve
    mei "Even if it's one of SDC's lineup for the military, I have no idea how to disarm it."
    rho surprised "Oh ...."
    rho "Well, let's not trip on this one then."
    
    mei arm surprised "You're pretty good at noticing traps, by the way."
    rho arm default2 "That's basic, isn't it?"
    show mei arm default2 with cdissolve
    rho "If you know your enemy doesn't run away, but hiding, be really careful for trap or ambush."
    rho arm confused "I learned it the hard way, though ...."
    hide rhodo with dissolve
    hide mei with dissolve
    "I have a guess that Rhodo was tricked into a trap or two by the older kids at the orphanage."
    "Not really surprising, especially after I've heard so many times how people in Vacuo fight."
    
    "There are more traps to come. Fortunately, we find all of them before we step or trip into any."
    "A little further from our position, I could hear another combat ensues."
    show rhodo arm frown at midright with dissolve
    rho "Is someone attacking Anya and Ulm?"
    rho "Should we help them?"
    show mei arm surprised at midleft with dissolve
    mei "!"
    mei "Watch out!"
    hide rhodo with qdissolve
    hide mei with qdissolve
    
    "My warning came too late."
    "While he was trying to figure out whether we should go and help Anya and Ulm, Rhodo diverted his attention and he triggered a wire."
    play sound sbang
    "The explosion follows almost immediately. The only thing I could think is to shield myself." with hpunch
    mei "Rhodo!"
    "Someone moves and coughs behind the smoke screen."
    "The explosion itself wasn't too big, but why is there so much smoke ...?"
    
    "...!"
    "Because from the first place this trap is supposed to emit smoke and limit our sight!"
    "I load wind Dust into Alpenaster, while keep looking at my surrounding."
    "In this situation, anyone could ambush and disable us without much effort."
    "{i}Not many people look up above their heads.{/i}"
    "I look up."
    "A silhouette falls on me, fast."
    "I dodge it and counter."
    "The wind blows the perpetrator away and clear part of the smoke screen."
    "Another attack comes from behind."
    "I parry the slash, return the attack with several thrusts, and ends it with a blast from my Glyph." with hpunch
    
    "However, more are coming."
    rho "Schnee! Duck! Now!"
    "I crouch."
    scene cg fight7r with fade
    "Something passed my head with a swoosh and hit the container next to me with a loud thud."
    play sound shot
    play sound shot
    play sound shot
    "A few shots from Rhodo force the attacker to back away. I glimpse at him and figure out that he's the leader of the goons whom Anya wants to capture."
    "Unfortunately, I need to replace the Dust cartridge in Alpenaster before I could trap him in ice."
    "Also, two others are coming back at me."
    "I dodge the first attack while replacing the Dust cartridge inside Alpenaster."
    
    "The second attacker is slow. Not only I manage to avoid his attack, I grab his wrist and lock his movement, making him into a shield against his own friend."
    play sound glyph
    "I push him toward the other and while they tumbling, two Glyphs float behind them."
    scene bg port with fade
    "A pair of big armored hand appear, smashing the two thugs to each other." with hpunch
    
    "Back to Rhodo, he seems to gain the upper hand against the goon leader."
    "But then, suddenly, the table turns."
    "Rhodo is about to hit his opponent with the back of his lance. "
    "The leader lunges forward, startling Rhodo with the change of distance between them."
    "It's too close for either party to launch an attack properly, but then--"
    
    rho "!!!"
    mei "Rhodo!"
    play sound hardhit
    "The goon leader lifts Rhodo and slams him to the ground!" with hpunch
    "Rhodo protects his head in the last split second, but his shoulder still hit the ground hard."
    "With Rhodo half-paralyzed by the surprise attack, the man shifts his attention to me."
    
    show mei arm surprised at mcenter with dissolve
    "Honestly? His last move intimidates me."
    "I'm very much aware of the size difference between us and my disadvantage in physical feat against him."
    "I lock my gaze at him, readying Alpenaster. Both of us keep our distance and wait."
    "He grins, clearly noticing my reluctance."
    
    show mei arm angry with cdissolve
    "I reply with a frown "
    extend "and cast a black Glyph below my opponent."
    show mei arm frown2 with cdissolve
    "However, this man is good. {i}Really good.{/i}"
    "He takes the chance before I fully finished casting the Glyph, transforming his one-handed sword into a rifle and barraged me with shots."
    
    "I shuffles, avoiding his counter and canceling my Glyph casting process."
    "Behind him, Rhodo has back to his feet, but not looking too ready to fight."
    show mei arm angry with cdissolve
    "The goon leader leaps on me--"
    "And a wire, with a metal hook at its end, circles around the man's neck."
    "The wire pulls him away, dragging him on top of a container, and keep pulling until the goon leader hangs a few meters from the container below him."
    
    show mei arm surprised at midleft with dissolve
    show rhodo arm surprised at midright with dissolve
    rho "Wh--?"
    rho "What was that???"
    mei "I ... I don't know."
    any "Good work on taking them down."
    "Anya peeks down from top of container next to us."
    hide rhodo with dissolve
    show anya arm smile at midright with dissolve
    any "And thank you for the huge fish."
    mei "?!"
    mei arm frown2 "Are you the one who strangled him???"
    any "I didn't strangle him. Just dragged him away. He's not gonna hurt or anything. Don't worry."
    mei "He might choke himself."
    any arm default "Nah."
    any arm smirk "C'mon. We have an interrogation to do."
    hide anya with dissolve
    hide mei with dissolve
    
    scene black with fade
    
    jump endofchoice11
    
    
    
label ugo:
    $ ulm_pt += 5
    
    show anya arm default at midright with dissolve
    show mei arm default at midleft with dissolve
    mei "I could aid Ulm."
    mei "It takes a bit longer for my Aura to recover, so I guess I could take part in support role."
    mei "Right?"
    "Even though Ulm is usually paired with Rhodo in open envinronment to keep Ulm safe, the strategy is more flexible than that."
    "As long as Ulm has someone to watch over his back, the plan could go."
    any "Sure. Don't forget to watch your own back, though."
    any "I'll go with Rhodo then."
    any arm smirk "Good luck and be careful."
    hide anya with dissolve
    hide mei with dissolve
    
    play sound glyph
    "I cast a Glyph and jump, joining Ulm who has already left his hiding spot."
    "Ulm looks a bit reluctant, but after I nod at him, he smiles and nods back."
    "Our usual arrangement is whoever acts as Ulm's protector, he or she has to follow Ulm's lead."
    "Ulm, however, takes my presence too seriously and tends to hold back, trying to switch his role with mine."
    "Ever since we notice this, I always give him some sort of approval to assure him."
    
    "Ulm hops to the nearest container and views his surroundings through the scope on his crossbow."
    "He keeps his communication line open and updates us with what he finds."
    "Apparently, our attackers are waiting somewhere ... possibly to ambush us."
    "I stand near and watch for any sign of incoming attacks."
    
    show mei arm default2 at midleft with dissolve
    mei "Do you find any of them?"
    show ulm arm reluctant at midright with dissolve
    ulm "Not really ...."
    ulm "I did Sightjack them, but it's difficult to determine where they are."
    ulm arm thinking "Their surroundings look similar with those containers."
    
    show mei arm frown2 with cdissolve
    "That's true .... Especially with many of those containers are grayish blue with SDC emblem on it."
    "Ulm hops again to the next container."
    show mei arm default2 with cdissolve
    ulm "All I can do is trying to see anything out of ordinary."
    ulm arm surprised "Uh ...."
    ulm "Anya? They set up a few traps around ...."
    
    any "Rhodo saw it. We'll be careful."
    mei arm frown2 "Could we somehow remove those traps?"
    ulm arm thinking "By triggering them? It's not the best idea, Miss Schnee. Because we don't know what kind of trap we're dealing with."
    mei "I see ...."
    hide ulm with dissolve
    hide mei with dissolve
    
    "It's a bit strange that we chase opponents we couldn't easily find."
    "We all know they are somewhere waiting to ambush us and we're desperate to capture at least one of them."
    "It's a game where we both the hunter and the hunted."
    "I follow Ulm moving to another stack of containers."
    mei "?"
    
    "I think I heard a noise from behind. Like someone has just landed on the container surface."
    "I turn around and observe the surroundings."
    "Nothing suspicious in sight."
    "When I look at Ulm again, a glowing circle floats in front of his left eye, indicating he's using his Semblance."
    "He turns around and shout:"
    ulm "Miss Schnee! Behind!"
    
    "I don't see anything at first."
    "But then I notice that the air look slightly distorted and moving."
    "Still has no clue what is coming for me, I take a few steps back. Next to me, Ulm aims and shoots."
    "Ulm's bolt doesn't hit anything, but it startles our opponent."
    "The air distort again. This time it also flickers and finally the culprit shows up."
    "Our opponent wears a mask which we could easily relate as part of White Fang's infamous attribute."
    "He doesn't show any visible animal trait--"
    "No, correction. His skin shifted to red and then to green before return to its normal shade."
    "He grins at us and motions his thumb crossing his neck, imitating slicing one's throat open."
    
    "... I don't want to be rude, but this chameleon Faunus is disgusting and dangerous ...."
    "Most definitely."
    "The Faunus steps aside, avoiding Ulm's shot. His body flickers and once again turns invisible again."
    "I've loaded lightning Dust into Alpenaster and cast a strike towards the Faunus."
    "My attack hits nothing. Instead, something bumps on the back of my head, sending me tumble and fall." with hpunch
    mei "Uwah!"
    "The Faunus proceeds to attack Ulm, not giving him any chance to aim and shoot."
    "Every time the Faunus moves, I could see the air slightly disturbed. However, the chameleon Faunus fully awares of this and deliberately moves around so much."
    "I hold my breath and aim Alpenaster carefully. The chameleon Faunus seems to forget about me."
    
    "When I sense the Faunus in my range, I launch a thrust towards him, accelerated by the Glyphs formed below me."
    "In a split second, everything changes."
    "I see movements, but not expecting the Faunus to jump and kick Ulm, sending him toward me."
    "I immediately remove the Glyphs and put down Alpenaster, but it's too late."
    scene cg fight7u with fade
    "We crash at each other. Ulm loses his balance and falls head first from the edge of the container. I catch his ankle. Ulm hangs upside down."
    
    ulm "Miss Schnee--!"
    ulm "Behind--!"
    play sound glyph
    "Without really seeing my target, I cast another Glyph. A strong force blasts from it."
    "Judging from the following yelp, I think I get to hit the Faunus."
    "I yank Ulm and help him climbing back."
    scene bg port with fade
    
    show ulm arm surprised at midleft with dissolve
    ulm "I can Sightjack him and tell you where his next attack will be."
    show mei arm surprised at midright with dissolve
    mei "What if he attacks you instead?"
    ulm arm thinking "It's ...."
    
    ulm arm default "It's something I'm willing to risk."
    show mei arm frown2 with cdissolve
    "Not that I'm going to let him hurt too ...."
    mei arm angry "Alright. Let's do it."
    hide ulm with dissolve
    hide mei with dissolve
    "Ulm kneels close to me. Bright circular sign appears on his eye."
    "I watch my surroundings, readying my weapon."
    
    ulm "Two o'clock."
    "I catch a glimpse of moving air from the side of my eye, but I stand still."
    show ulm arm surprised at midleft with dissolve
    ulm "Miss Schnee ...?"
    show mei arm angry at midright with dissolve
    mei "Is he still coming from the same position?"
    ulm "Yes."
    mei "Tell me when he's at around 3 meters or closer."
    
    hide mei with dissolve
    hide ulm with dissolve
    "I slightly look to my left."
    "I could feel the tension weighing my shoulder, making me breath faster."
    "If I miss him ... if I miss him ...."
    "No, no, no! Don't think that way!"
    
    show ulm arm shout at midleft with qdissolve
    ulm "Miss Schnee!"
    ulm "NOW!"
    hide ulm with qdissolve
    "Thunder crackles from Alpenaster tip. It hits the Faunus, right on his chest. His body flickers before fully turns visible." with hpunch
    "In desperation, the Faunus throws a set of knives."
    play sound glyph
    "I cast a Glyph, shielding both of us. Before the Faunus leaves our field of sight, Ulm shots several bolts."
    "The first bolt explodes on the Faunus with electrical spark." with hpunch
    "The second and the third hit the edge of the container, quickly forming an ice structure."
    "I have no idea how Ulm did it, but the ice trapped the chameleon Faunus inside of it, binding him in a way that he wouldn't be able to move his hands to free himself, but leaving his head free to breath "
    extend "and swearing at us."
    
    "I heard Anya's laugh from my earpiece, not too long after we finished our fight."
    any "Isn't that too complicated just for trapping him?"
    show mei arm frown at midright with dissolve
    mei "It was Ulm. Not me."
    show ulm arm surprised at midleft with dissolve
    ulm "{size=15}I'm sorry .....{/size}"
    any "What're you sorry for? You did a great work."
    any "We have an interrogation to do. Come down here."
    hide ulm with dissolve
    hide mei with dissolve
    
    scene black with fade
    
    jump endofchoice11
    
label ago:
    $ anya_pt += 5
    
    show anya arm default at midright with dissolve
    show mei arm default at midleft with dissolve
    mei "How about the usual?"
    mei "I don't see any reason to deviate from our usual arrangement."
    any arm smirk "Neat. Rho, I'll leave Ulm with you, okay?"
    rho "Don't worry! I'll keep him safe like always!"
    "Rhodo catches up with Ulm."
    
    mei arm default2 "So, what's the plan?"
    any arm default "There's only one way out from the port, so if they try to run away, we'll eventually meet them over there."
    mei arm surprised "Then we need to hurry!"
    "A voice goes through my earpiece."
    
    show mei arm default2 with cdissolve
    ulm "I-I'm sorry for interrupting, Miss Schnee ...."
    ulm "But I find some traps lay around, you need to be careful."
    ulm "And they're hiding, not running away."
    any arm frown "Can you locate them?"
    ulm "Uh ... I'm sorry, but I keep getting similar containers around them ...."
    rho "Do you have any idea how many containers are grey-colored with Schnee Dust Company logo on it?"
    
    show mei arm frown2 with cdissolve
    any arm default "Oh ...."
    "... Why do I feel guilty for a moment there ...?"
    any arm frown "Do you think they'll try to get us if we trigger one of those traps?"
    mei "Do you really want to risk it ...?"
    any "I'll see what kind of traps they set up."
    hide anya with dissolve
    hide mei with dissolve
    "I follow my partner closely, watching the surroundings for any potential danger."
    "I learn the hard way that focusing in one thing could make me pay less attention for a more imminent danger ahead."
    "*click*"
    mei "!!!"
    
    any "Wha--"
    "Anya pushes me away and get down."
    "There is an explosion, a bit smaller than I expected."
    "However, the trap produces a lot of smoke in very short time, veiling our sight."
    "I cough, inhaling too much smoke."
    any "Oh?"
    any "Here they come ...."
    
    "I raise my head a little, trying to get a better look."
    "There is, indeed, a silhouette getting closer to us."
    "Is it just her instinct or Anya has some way to know that someone is coming?"
    "Anya rolls aside. In one smooth movement, she pulls her gun and shoots."
    "The explosion--yes, unlike Rhodo's rifle bullets, each of Anya's bullet is highly explosive--distracts our opponents."
    "I use the chance to load wind Dust into Alpenaster."
    "As always, our coordination is a bit off ...."
    "I have to pull Anya's jacket before she run and fight the goon she blew just now. I unleash a strong wind gust from Alpenaster, clearing the smoke screen and also pushing back our opponents."
    
    show anya arm smirk at midright with dissolve
    any "That's better."
    show mei arm frown at midleft with dissolve
    mei "You're welcome."
    any arm default "Be careful with attack from above."
    hide anya with qdissolve
    hide mei with qdissolve
    "And with that, she proceeds to take care of the goons in front of us."
    "There's suppose to be a total of four people we need to take care of ...."
    "Anya has two for now and would they leave Rhodo and Ulm alone? I don't think so."
    "I think too much again."
    
    "I turn around. A silhouette closes in, real fast."
    "It's the goon leader. His sword is high above his head."
    scene cg fight7a with fade
    play sound clash
    "I manage to block his attack with Alpenaster, holding it for a few seconds to feel the huge difference between our physical power."
    "My opponent's blade slides down when I lower Alpenaster tip. I launch a quick thrust toward him as a counter-attack."
    "I hit his shoulder, but doesn't seem to do much damage."
    "He attacks me with a barrage of slashes."
    "My grip on Alpenaster grows weaker with each hit I blocked."
    "This guy is strong and tough."
    "I finally manage to dodge his attack and push him back with wind burst."
    "However, I don't think I could handle more intensive attacks like before with only my dagger."
    
    scene bg port with fade
    "I summoned the knight's hand when the goon leader leap to hit me again."
    "It's certainly not a good surprise for him."
    "I sweep him aside, slamming him on the nearest container." with hpunch
    "He vaults over the hand when I try to hit him for the second time. I back away, put a distance between us."
    "I put a defensive stance with the hand act as my shield."
    "Both of us wait, judging our own situation."
    
    "But, well, battle doesn't work that way."
    "Anya suddenly jump from the top of the container, swinging her cane as hard as she could and hit the goon leader's head." with hpunch
    "Unfortunate for her, this man is tougher than that."
    "Anya's surprise attack doesn't bring him down."
    "Furious, the goon leader turn his attention to Anya."
    "His next attack is so powerful. The blade of his sword tear the container metal side."
    "To my relief, Anya manages to dodge it."
    
    "Feeling a bit confused why the guy seems to forget about me, I smash the goon leader with the knight's hand." with hpunch
    "For some reason, it still doesn't bring him down ...."
    "He has weakened, but still trying to stand up."
    "Anya taps his shoulder."
    "When he turns around, Anya punches him on the face." with hpunch
    "That last attack finally brings him down."
    
    show anya arm sigh at mcenter with dissolve
    any "Whew .... He's tough."
    any arm smirk "But, anyway, it's time for some interrogation."
    hide anya with dissolve
    
    scene black with fade
    
    jump endofchoice11
    

label endofchoice11:
    
    stop music fadeout 5.0

    "I would swear to anybody that there is no specific subject about interrogation being taught at Beacon Academy."
    "Not even anything similar or close to it. "
    extend "I. "
    extend "Swear."
    "Anya seems like she's having fun to have this chance, by the way."
    
    scene bg port with fade
    "After we knocked out the goon leader, now he is tied and hanging upside down from a crane. Both his eyes are covered in a way that not allow him to see anything."
    "Ulm is tasked to watch if anything come over. Rhodo and I stand near the captured man, in case he manages to get away."
    "Anya took the initiative to be the one asking questions."
    "She woke up the man with a splash of water just now."
    
    show anya nor smile at midleft with dissolve
    any "Good morning."
    any "Mr. Seymour, is it?"
    show gn1 at midright with dissolve
    gn1 "Gah! *cough*"
    gn1 "*cough* *cough*"
    gn1 "What--"
    gn1 "What do you want from me?!"
    any nor default "I just wanna ask a few questions from you, Mr. Seymour."
    
    any nor smirk "Say, you have a pic of a beautiful lady in your wallet. Is that your wife?"
    hide anya
    hide gn1
    with dissolve
    "I saw Anya searched the goon leader's pocket and pouch. She found his wallet, where he kept his ID, bills, a photo, and a few Lien."
    "From his ID card, we know his name and his occupation. I find it really odd that this goon leader's occupation is written as 'huntsman'."
    "Why did he accept this job?"
    "And, if I put myself in his position, taking shady, illegal job, I wouldn't bring my wallet with my ID in it."
    "This man might be skillful in combat, but he doesn't seem to have a 'criminal minds'."
    
    show gn1 at midright with dissolve
    gn1 "!"
    gn1 "Don't you dare touching her!"
    show anya nor smirk at midleft with dissolve
    any "I won't, Mr. Seymour. You can have my word."
    any nor default "And you better not moving too much."
    any nor smirk "The rope is a bit thin. We don't want you to crush your head when you fall and hit the ground."
    "It is a bluffing."
    "This Seymour guy is hanged upside-down, but he isn't that high from the ground."
    "Falling down head-first would certainly painful, but I doubt it would kill him."
    
    "Besides, he is hanged with a chain. He couldn't see it since his eyes are covered."
    any nor default "First question, then."
    any nor frown "Who asked you to attack us?"
    gn1 "I--I forgot!"
    any nor default "Are you a drinker?"
    gn1 "Huh?"
    gn1 "I drink sometimes ... what of it?"
    
    any "You must've realized your clothes are wet."
    any nor smirk "I heard old sailors tested their liquor by burning them."
    any "A good one will make a good fire. I won't start talking about the burn it caused, though."
    hide anya
    hide gn1
    with dissolve
    "The goon leader whimpers."
    "Standing next to me, Rhodo grins really wide."
    "It is another bluffing."
    "Of course, we wouldn't light up a man alive. We are not criminals."
    "It's water. The trick is similar with the 'molotov cocktail' Rhodo used to distract our opponents."
    
    show anya nor default at midleft with dissolve
    any "Do your best to recall your memory, Mr. Seymour."
    any "Who asked you to attack us?"
    show gn1 at midright with dissolve
    gn1 "She didn't give me her name!"
    gn1 "It's true! She didn't give me her name and just told me to lead a few people to the port today!"
    any nor frown "Did she specifically tell you who do you need to take care of?"
    gn1 "Not 'take care of'. She told me to capture you alive. The three of you."
    
    "Three? Not {i}four{/i}?"
    "If my suspicion is right ... it might be Emerald."
    "In Forever Fall tunnel she only saw three of us--Anya, Rhodo, and I."
    any "Tell me what she looks like."
    gn1 "Dark skin. Short hair. Er ... green hair? Yeah, I think it's green. An interesting looking woman."
    any nor default "She does sound interesting."
    any "Did she tell you what's her plan after you capture us?"
    gn1 "No. Nothing."
    
    "Anya pauses, giving the goon leader a very short break."
    any nor frown "Ever heard of Golden Apple?"
    any "Take one tablet and improve your Aura ... or Semblance. I'm not sure."
    gn1 "Uh ...."
    any nor default "Yes?"
    gn1 "I don't know."
    hide anya
    hide gn1
    with dissolve
    
    "I shakes my head."
    "From the way he said it, this man lied."
    "But Anya doesn't chase this one question further."
    any "Alright, then."
    "I don't hear the next question. Rhodo interrupts me."
    show rhodo arm default2 at midleft with dissolve
    rho "Schnee, your turn."
    show mei arm default at midright with dissolve
    mei "Oh? Sure."
    hide rhodo with dissolve
    hide mei with dissolve
    
    "Rhodo and I take turn checking our surroundings, in case there's someone who wait for a chance to free this man."
    "Or if there's someone eavesdropping on us."
    mei "?"
    "Just now ... I think I saw someone went hiding behind those containers ...."
    "I walk a little bit further, but then stop and wait."
    "The figure appears again."
    "It's--"
    "Out of reflex, I run towards her."
    "It's Emerald!"
    
    "She notices me, but instead of immediately run away, she flashes the Scroll in her hand."
    "Please don't say she recorded us interrogating Seymour ...."
    "Our interrogation--at least from my point of view--is nowhere illegal, but I do realize the method is questionable at best."
    "And without fully knowing what had happened, it's easy to misunderstand what we did."
    "Emerald run."
    show mei arm surprised at mcenter with dissolve
    mei "Wait!"
    mei arm angry "You--!"
    hide mei with dissolve
    "I give a chase."
    "Emerald doesn't run that fast. I always see a glimpse of her before disappear behind another container box."
    "When we finally get into a straight lane alongside the furthest part of the port, I could see her run towards a boat."
    
    "It isn't a big boat. I think it's for fishing, not for transporting huge cargo."
    
    $ most_pt = max(anya_pt, rhodo_pt, ulm_pt)
    
    if most_pt == anya_pt:
        any "Mei! Wait!"
        scene dark with fade
        "I already enter the boat when I hear Anya."
        #door slammed shut sfx
        mei "Huh?"
        "To my confusion, I don't see Emerald anywhere in the bridge."
        "And the boat start to move. I immediately turn back to the door."
        "The door is locked ...."
        "I turn the handle once again, push and then pull, repeat, but with more strength."
        "The door won't budge."
        
        "It doesn't mean I don't have any other option to get out of here."
        "I cast a Glyph, summoning the knight who is way more powerful compared to me."
        any "Don't break the door!"
        any "Just stay where you are!"
        
        menu:
            "Cancel the summoning.":
                "I dismiss the knight, curious and a bit upset."
                "If I don't leave this boat as soon as possible, it would be too late!"
                "The boat would have been too far from the port!"
                show mei nor angry at mcenter with dissolve
                mei "What's wrong?"
                any "They set up something on the door ...."
                any "Looks like a self-made trap."
                any "Or bomb."
                
                mei nor surprised "Wait--{i}what{/i}?"
                mei "Did you say 'bomb'?!"
                any "There's a note next to it, by the way."
                any "It said ... 'you need to pick a color from the bridge'."
                any "What bridge ...?"
                
                mei nor frown "That means the room I'm in right now."
                mei nor frown2 "But 'pick a color'? From where?"
                any "No idea."
                any "And don't try to bust any of the windows."
                any "They're barred and lots of wires connected to this bomb."
                
                "I exhale."
                "Now I regret everything I've done which led to this trouble."
                mei nor sad "Anya?"
                any "Hm? Did you find any clue?"
                mei "Not yet."
                mei "I'm sorry .... I drag you into trouble. Again."
                any "Let's get you out of there first, okay?"
                mei "Alright ...."
                hide mei with dissolve
                
                "I look around the room."
                "This boat is set on auto-pilot, heading to a predetermined coordinate."
                "The system is secured with a password. I don't try to put any number, pretty sure the system is also connected to another explosives."
                "I don't find anything useful or looks like a clue."
                "Stepping away from the controls, I turn around."
                "It's a coincidence that I look at the floor."
                "A word is written on there with red paint, but the word isn't 'red'."
                "It's 'yellow' in Mantle's local language."
                
                show mei nor frown2 at mcenter with dissolve
                mei "What you should do once we 'pick the color'?"
                any "I don't see input panel for password or such, so I think I need to cut a wire. Or two."
                "Her description doesn't assure me ...."
                mei "Do you see a yellow one or a red one?"
                any "Yellow."
                any "They really saved us from the confusion, didn't they?"
                any "Anyway, try to put up a shield or something, just in case."
                hide mei with dissolve
                
                "I cast a protection Glyph in front of me, hoping the clue is not another trick and no explosion would happen."
                "I hear metal lock slides and the door opens."
                show anya nor sigh at midright with dissolve
                any "Oh, whew."
                any nor frown "Are you hurt?"
                show mei nor default2 at midleft with dissolve
                mei "I'm okay."
                show anya nor default
                mei nor default "Thank you for getting me out."
                any nor smile "No problem."
                hide mei with dissolve
                hide anya with dissolve
                
                "I take a look around, finding ourselves have been very far from Vale's port."
                "How are we going to go back? Where is this boat heading to?"
                "And ...."
                show mei nor frown2 at midleft with dissolve
                mei "Where's Emerald?"
                mei "I saw her going into this boat, but then I couldn't find her anywhere."
                show anya nor shout at midright with dissolve
                any "You saw her {i}again{/i}?"
                mei nor default2 "When I was patrolling around, I saw and chased her. But then she got into this boat and disappeared."
                
                show anya nor frown with cdissolve
                "Anya looks like she's thinking about something."
                any "I did see someone running to the opposite direction from the boat."
                show mei nor surprised with cdissolve
                any "But I didn't see her really well and I went to the boat instead."
                mei nor angry "Why you didn't chase her?!"
                mei "She's the one behind everything! She sent the thugs and White Fang members to capture us!"
                any nor default "Hey, listen."
                
                "Anya doesn't shout, but there's something in her voice that makes me shut my mouth."
                any nor frown "I didn't come to save you when that crazy woman--who's her name? Cinder?--took you and I'm still regretting it."
                any "I won't let that happen again. I promise it to myself and your mom."
                show mei nor sad with cdissolve
                any nor sigh "Catching Emerald is important, I know. But, at the cost of letting you going someplace we don't know and facing danger?"
                any nor frown "I'm not risking it. Not again."
                mei "I'm sorry ...."
                mei "I didn't mean to blame you ...."
                
                any nor default "That's okay. Really."
                any nor frown "Now where's this boat gonna bring us to?"
                show mei nor default2 with cdissolve
                "I take out my Scroll just to find a big 'NO SIGNAL' on it."
                mei nor frown2 "What's going to happen to Rhodo and Ulm?"
                any nor default "I trust Ulm for making decision. There's nothing we can do for now."
                hide anya with dissolve
                show mei nor default2 with cdissolve
                "Anya circles the bridge once, stopping on the deck and surveying our whereabouts before go back."
                "She sits on the side and I join her."
                
                mei nor frown2 "Nothing we could do?"
                show anya nor frown at midright with dissolve
                any "Let's wait and see where we will end up."
                any nor default "We'll think what to do after that."
                hide anya with dissolve
                hide mei with dissolve
                "It doesn't take long before Anya shuts her eyes and--seemingly--sleeps."
                "There's really nothing we could do now, right? Beside wait and see for what would come up next ...."
                "I personally think it needs an admirable amount of composure (or ignorance) to sleep during this kind of situation."
                "Whichever it is, it's quite fascinating to find such person with such quality."
                
                scene black with fade
                call chapter("End of Part 7", "Betrayal") from _call_chapter_9
                #centered "End of Part 7 - Betrayal"
                jump part8BA
                #return
                
                
            "Break the door open.":
                jump badend1
        
    elif most_pt == rhodo_pt:
        rho "Schnee! No! Stop!"
        scene dark with fade
        "I already enter the boat when I hear Rhodo."
        #door slammed shut sfx
        mei "Huh?"
        "To my confusion, I don't see Emerald anywhere in the bridge."
        "And the boat start to move. I immediately turn back to the door."
        "The door is locked ...."
        "I turn the handle once again, push and then pull, repeat, but with more strength."
        "The door won't budge."
        
        "It doesn't mean I don't have any other option to get out of here."
        "I cast a Glyph, summoning the knight who is way more powerful compared to me."
        rho "Don't mess with the door, Schnee!"
        rho "It's dangerous!"
        
        menu:
            "Cancel the summoning.":
                "I reluctantly dismiss the knight."
                "Judging from Rhodo's voice, he's serious about something is dangerous."
                "But I wonder what is it?"
                show mei nor frown2 at mcenter with dissolve
                mei "What's the matter?"
                rho "They strapped something on the door."
                rho "This thing is gonna explode if you force open the door."
                mei nor surprised "B-bomb?!"
                
                mei "You couldn't be serious!"
                rho "I'm {i}really{/i} serious, Schnee!"
                rho "Huh? There's a note ...."
                rho "Let's see ... 'you need to pick a color from the bridge'."
                rho "What bridge?"
                
                mei nor frown "I'm currently in a room called 'bridge', just for your information."
                mei nor frown2 "But 'pick a color'? From where?"
                rho "I don't know ...."
                rho "Maybe it's written somewhere in there?"
                mei "Let me take a look."
                rho "Oh, and don't try to break the windows."
                rho "I think they're connected to this bomb."
                
                mei "So, my only way to get out is to find this 'color'."
                rho "Yeah."
                hide mei with dissolve
                
                "I look around the room."
                "This boat is set on auto-pilot, heading to a predetermined coordinate."
                "The system is secured with a password. I don't try to put any number, pretty sure the system is also connected to another explosives."
                "I don't find anything useful or looks like a clue."
                "Stepping away from the controls, I turn around."
                "It's a coincidence that I look at the floor."
                "A word is written on there with red paint, but the word isn't 'red'."
                "It's 'yellow' in Mantle's local language."
                
                show mei nor frown2 at mcenter with dissolve
                mei "What should we do once we 'pick the color'?"
                rho "Uh ... I thought I should cut the wire with the same color."
                show mei nor default2
                "Rhodo doesn't sound really sure."
                mei "Do you see anything else beside wires?"
                mei nor frown2 "Any input panel?"
                rho "Nope. No such thing."
                mei "Do you see yellow or red wires?"
                rho "Only yellow."
                rho "Should I cut the yellow one?"
                mei "I don't think you have any other choice, so ... yes."
                rho "Okay."
                rho "Protect yourself, Schnee."
                hide mei with dissolve
                
                "Assuming the strongest blow would come from where the device is attached, I cast a protection Glyph in front of me while facing to the door."
                "When I finally hear the sound of metal slides and the door is slowly opened, I let out a relieved sigh. I dismiss my Glyph."
                show rhodo nor surprised at midright with dissolve
                rho "Schnee? You okay?"
                show mei nor default at midleft with dissolve
                mei "Yes. Thank you."
                hide rhodo with dissolve
                hide mei with dissolve
                "I take a look around, finding ourselves have been very far from Vale's port."
                "How are we going to go back? Where is this boat heading to?"
                
                show rhodo nor default2 at midright with dissolve
                rho "So, what or who did you chase, Schnee?"
                rho "I heard you shouted and followed you here."
                show mei nor frown2 at midleft with dissolve
                mei "You didn't see Emerald?"
                rho "I'm a bit behind and didn't see whatever or whoever you chased, actually."
                rho nor surprised "... Wait. I kinda miss something there ...."
                rho "When the boat started to sail away, I was confused about what to do."
                rho nor frown "I look behind, hoping Anya or Ulm caught up with me and asked what's their thoughts."
                rho nor surprised "Instead of them, I think I see this Emerald woman."
                
                mei nor angry "Why you didn't chase her?!"
                mei "You had the chance to capture her!"
                rho nor shout "Hey, Schnee, that's not how things work for me!"
                rho nor frown "I'm not leaving anyone behind!"
                rho "If any of my brothers or sisters--not that they're real ones, though--were kidnapped or trapped or gotten into any sort of trouble, the right thing to do is to help them."
                mei "And letting the culprit gone?"
                
                rho nor shout "If I got the culprit, but lost my friends, what's good in that?"
                show mei nor frown2 with cdissolve
                "Rhodo looks at me, upset."
                rho nor frown "What's wrong with you, Schnee? Get your priorities right, damnit."
                hide mei with dissolve
                hide rhodo with dissolve
                "Well ... until a moment ago, I think the right priority is to capture Emerald first."
                "When I rethink it again, Rhodo is right."
                "If this kind of event happened to Li, or to my family, or to my teammates, I would choose to save them instead of keep chasing."
                
                show mei nor sad at midleft with dissolve
                mei "I'm sorry."
                show rhodo nor default2 at midright with dissolve
                rho "Nah, nevermind. I'm going to get used to lecturing you eventually."
                rho nor surprised "Now, where will this boat take us?"
                mei nor frown2 "It is set to a coordinate and the system is secured with password."
                "Rhodo and I take out our Scroll device in unison. The word 'NO SIGNAL' sinks my feeling."
                rho nor confused "Gaaahh! Why now?! Are there signal jammers here?"
                rho "How could we know our position now?"
                rho nor frown "Ah, goddamnit."
                
                "Grumbling, Rhodo goes to the side and sits down."
                mei "What are we going to do now?"
                rho nor default2 "Waiting. And see where's this boat stop."
                hide rhodo with dissolve
                hide mei with dissolve
                "Since there's nothing we could do for a moment, I think waiting is the only thing we could do."
                "I sit next to Rhodo. Guilty feeling rises again inside of me."
                
                show mei nor sad at midleft with dissolve
                mei "I'm sorry I dragged you into this."
                show rhodo nor default2 at midright with dissolve
                rho "I know what's Father gonna say in this situation."
                mei nor frown2 "What is it?"
                rho nor default "It's better to be in trouble together with a comrade rather than alone."
                show rhodo nor smile with cdissolve
                "Rhodo puts up a silly grin."
                rho "I guess that's true."
                mei nor smile "Yeah."
                "I couldn't help not smiling in response."
                hide mei with dissolve
                hide rhodo with dissolve
                "It might sound ridiculous, but I would admit that what Rhodo said is right and--somehow--calming."
                "It's fair to say that Rhodo has this certain talent to keep the spirit up."
                
                scene black with fade
                call chapter("End of Part 7", "Betrayal") from _call_chapter_10
                #centered "End of Part 7 - Betrayal"
                jump part8BR
                #return
                
            "Break the door open.":
                jump badend1
        
    elif most_pt == ulm_pt:
        ulm "Miss Schnee! Don't--!"
        scene dark with fade
        "I already enter the boat when I hear Ulm."
        #door slammed shut sfx
        mei "Huh?"
        "To my confusion, I don't see Emerald anywhere in the bridge."
        "And the boat start to move. I immediately turn back to the door."
        "The door is locked ...."
        "I turn the handle once again, push and then pull, repeat, but with more strength."
        "The door won't budge."
        
        "It doesn't mean I don't have any other option to get out of here."
        "I cast a Glyph, summoning the knight who is way more powerful compared to me."
        ulm "Miss Schnee!"
        ulm "Please don't force open the door!"
        
        menu:
            "Cancel the summoning.":
                "If Ulm said we shouldn't do something, the wisest choice to do is obeying it."
                "I dismiss the knight."
                show mei nor surprised at mcenter with dissolve
                mei "What's the matter?"
                ulm "They set up a device that looks like an explosive."
                ulm "Please, stay calm, Miss Schnee."
                ulm "It won't explode as long as you don't break its lock mechanism."
                mei nor frown2 "Could you unlock it?"
                
                ulm "I don't think it will be that easy, Miss Schnee ...."
                ulm "There's a writing here: 'you need to pick a color from the bridge'."
                ulm "Miss Schnee, do you see anything that might be a clue to answer this?"
                mei "Let me take a look."
                ulm "{size=15}Please be careful ....{/size}"
                hide mei with dissolve
                
                "I look around the room."
                "This boat is set on auto-pilot, heading to a predetermined coordinate."
                "The system is secured with a password. I don't try to put any number, pretty sure the system is also connected to another explosives."
                "I don't find anything useful or looks like a clue."
                "Stepping away from the controls, I turn around."
                "It's a coincidence that I look at the floor."
                "A word is written on there with red paint, but the word isn't 'red'."
                "It's 'yellow' in Mantle's local language."
                
                show mei nor frown2 at mcenter with dissolve
                mei "Ulm? Does the color yellow mean anything?"
                ulm "It probably relates to the wires on this device. There's a yellow one."
                ulm "I ... I mean, it {i}might{/i} refer to the wires color ...."
                ulm "...."
                ulm "{size=15}Should we take the risk ....?{/size}"
                mei nor default2 "Or do you see red wire too?"
                ulm "Um, no. There's no red one."
                mei frown2 "Well ... I don't think we have any choice than to take the risk."
                
                ulm "Y-yes."
                ulm "Unfortunately, that's our current situation ...."
                ulm "Please try to shield yourself, Miss Schnee."
                ulm "In case it doesn't work as expected."
                mei nor frown "Ulm?"
                ulm "Yes?"
                mei "I would very much appreciate a more optimistic attitude at this moment."
                
                ulm "I'm sorry!"
                hide mei with dissolve
                "I take a few steps back from the door and cast a Glyph in front of me."
                "I don't hear anything except the sound of metal slides. The door slowly open."
                ulm "Miss Schnee?"
                "I dismiss the Glyph and leave the bridge."
                "The port of Vale is way too far behind us now. I could cast Glyphs as a series of platforms, but I doubt I could cast enough to travel from this boat back to the port."
                "Swimming? Not that I don't have the skill, but this water area is declared dangerous with potential smaller aquatic Grimm creatures attack."
                
                ulm "Miss Schnee?"
                "I turn around to find Ulm is peeking from the bridge room door. It's as if I'm already angry at him and he thinks the wall would protect him from the anger."
                "Ulm is ridiculous in his own way."
                show mei nor default2 at midright with dissolve
                mei "Yes?"
                show ulm nor surprised at midleft with dissolve
                ulm "Do you mind if I'm asking why did you go to this boat?"
                mei nor frown2 "Ah, yes ...."
                mei "I saw Emerald while patrolling around and I chased her."
                mei "She went to this boat."
                
                ulm "Um ...."
                ulm nor reluctant "{size=15}Did you see the {i}real{/i} Emerald?{/size}"
                mei nor frown "Huh? I couldn't hear you."
                ulm "{size=19}Did you see the {i}real{/i} Emerald?{/size}"
                ulm nor thinking "{size=19}I mean ... her Semblance has to do with vision trick, right?{/size}"
                hide mei with dissolve
                hide ulm with dissolve
                "Why didn't I think about it earlier?"
                "Emerald did disappear and I was locked inside the bridge after I didn't find her."
                "I should have remember that Emerald has such ability!"
                
                "To be deceived like this twice ...."
                show mei nor surprised at midright with dissolve
                mei "I ...."
                mei nor angry "Aaarggh! Why did I forget about it?!"
                hide mei with dissolve
                "I kick the bridge room outer wall, embarrassed and upset."
                show ulm nor surprised at midleft with dissolve
                ulm "Miss Schnee, please don't do that!"
                ulm "You might trigger the explosives!"
                show mei nor surprised at midright2 with dissolve
                mei "Oh!"
                "I step away."
                show mei nor sad at midright with dissolve
                mei "I'm ... sorry."
                
                mei "It wouldn't be like this if I wasn't so reckless ...."
                ulm nor reluctant "{size=15}Miss Schnee, I'm glad that you're okay.{/size}"
                ulm "{size=15}That's what's important right now.{/size}"
                mei nor frown2 "Ulm ... please speak a little louder ...."
                mei "I couldn't hear you well with the boat engine noise."
                
                ulm nor surprised "I'm sorry!"
                hide ulm with dissolve
                hide mei with dissolve
                "For a moment, both of us fall silent."
                "I don't say anything because the only thing come up into my mind is how sorry I am to Ulm for dragging him into undesired situation like this."
                "Saying sorry to Ulm, however, would lead to another series of apologizes, which I don't have any idea how to stop it."
                "It's better to stay silent until I could think something better to talk about."
                
                show mei nor default2 at midright with dissolve
                mei "Do you have any idea where this boat lead to?"
                show ulm nor thinking at midleft with dissolve
                ulm "I've checked the coordinate, but I know nothing about it."
                ulm nor reluctant "I'm sorry ...."
                mei nor frown2"We could only wait and see, hm?"
                ulm "I'm sorry you stuck with me instead with Rho or Anya ...."
                mei nor default2 "Ulm."
                
                ulm nor default "Yes, Miss Schnee?"
                mei nor frown "I'm going to feed you to the fish if you keep saying sorry. Please stop."
                ulm nor surprised "I'm sor--!"
                "Ulm clasp both hands over his mouth, frightened."
                mei nor default "Stop saying sorry and stop me from doing anything reckless, okay?"
                mei nor smile "I would count on you, Ulm."
                ulm "O-of course!"
                ulm "I will do my best!"
                hide mei with dissolve
                hide ulm with dissolve
                "Ulm doesn't say 'sorry' for time being, which is a good improvement."
                "Heading toward an unknown destination is bad enough and rather than facing it with too much anxiety and pessimism, I would prefer a more positive approach."
                "Even though, I have no idea how long we could keep our spirit up."
                
                scene black with fade
                call chapter("End of Part 7", "Betrayal") from _call_chapter_11
                
                #centered "End of Part 7 - Betrayal"
                jump part8BU
                #return
                
                
            "Break the door open.":
                jump badend1
        
label badend1:
    "I hear a 'click' and everything turns red and yellow."
    "Heatwave sweeps me before I could do anything to protect myself."
    "Darkness follows after a blinding light."
    "I couldn't feel anything after that."
    
    scene black with fade
    call chapter("BAD END") from _call_chapter_12
    
    #centered "BAD END 1"
    return
    