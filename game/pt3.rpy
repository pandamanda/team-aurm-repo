﻿# PART 3: STORM #

label part3:
    #centered "{size=36}STUDENT DORM - 01.01 AM, 8 hours before mission{/size}"
    
    scene bg dormnight with fade
    "I can't sleep."
    "There's a mix of the excitement and worries about the next mission we're going to take and the confusion and curiousity of this whole Golden Apple affair."
    "We already brought our previous finding to Professor Goodwitch. She agreed to look further into this matter based on what we found and Ulm's additional information regarding the tablet's property."
    
    if bribe_jill:
        "Another good news is Jillia gave a photograph of the 3rd years to Professor. It backs up Ulm's testimony and provides potential suspects in this case."
        "I saw Jillia nodded at me while happily snacking an apple at the back of Professor Goodwitch's office."
        "Of course she's happy. She got double reward that day: biscuit from me and apple from Professor Goodwitch."
    else:
        "We also told Professor Goodwitch about the 3rd years from whom Ulm got his information when they cornered him in the locker room."
        "Professor Goodwitch looked doubted, but she said nothing further."
        "I just hope she look into it too ..."
    
    "I roll on my bed and take out my Scroll."
    scene cg messaging with fade
    "No new message."
    "I intend to scroll through my recent chat, but, instead, I press the wrong tab and open the contact list."
    "My contact list isn't long. It's consisted of my family (not all of them), Anya, Ulm, Rhodo, team DJJT (except Jillia; she refuses, saying \"Glynda not allow.\"), and some acquaintances."
    "Hmm. "
    extend "Acquaintances."
    "I stop at 'Romeo Lucignolo', tap on the name to start a new chat with him."
    "'Romeo Lucignolo' is an alias for Roman Torchwick."
    "He's one of my mother's 'specialist', working on gathering information about Dust resources all around the Remnant and investigating potential foul play in some SDC's branches."
    "When I was in Atlas, Mom occasionally asked Roman to accompany me if I was visiting the lab or other facility with her."
    "Sometimes with Neo--the quiet lady that has dangerous and cunning vibe in her, but actually nice and responsible."
    "Of course, not everyone thinks it's a good decision to let Roman accompany the heiress of Schnee Dust Company."
    "Roman Torchwick and Neo are ex-criminals, involved in many thefts, robbery, and smuggling in the past."
    "A perfect person to consult shady business like The Golden Apple, right?"
    
    nvl clear
    m "M: Roman, u awake?"
    "The reply comes a few seconds later. Apparently, he is awake."
    r "R: yea. Wat do u want?"
    m "M: Do u ever heard something called Golden Apple?"
    r "R: huh???"
    r "R: whatre u tryin to pull here???"
    r "R: i dont wanna playin quiz"
    r "R: ur mom put me in another risky job here"
    r "R: i need time planning things out"
    r "R: u better quit wasting my time"
    m "M: Well thats cos you complain about ur work are boring"
    m "M: So do you know it or not?"
    
    r "R: geez"
    r "R: i heard rumor"
    r "R: its some kind of drugs"
    r "R: its failed product from SDC pharmacy division"
    r "R: not passing test, causing disorientation under heavy usage"
    r "R: dunno how but it got outta there"
    r "R: someone mustve stole the formula"
    r "R: maybe that previous breakout"
    m "M: Theres a breakout?????"
    m "M: Where??????"
    r "R: rnd division"
    
    r "R: geez im not supposed to say that now u shut up and dont tell your mom"
    r "R: shes going to kill me for telling outsider"
    m "M: I'm not going to"
    m "M: You know you can trust me"
    r "R: rrright"
    r "R: now why the heck youre asking"
    r "R: dont u dare involving urself in some weird matter"
    r "R: im gonna beat you before ur mom kill me"
    m "M: Im just curious"
    m "M: Theres been a talk about it in Beacon here'"
    r "R: curiousity kills the cat, kid"
    r "R: stay away from trouble"
    r "R: now im back to work"
    m "M: Lst qestion"
    
    m "M: *last question"
    m "M: If u r the 1 who illegally produce it where will u do so?"
    "I could somehow imagine Roman is groaning when reading my last question."
    "He already told me to stay away from trouble and, yet, I ask something that clearly reads as involving myself in some big things."
    "Big trouble."
    "I put away my Scroll, thinking that Roman wouldn't answer my question."
    "But then there's a short notification."
    "I take my Scroll back and view the new message."
    r "R: forefer fall"
    "'Forever Fall' (not 'Forefer Fall', Roman mistyped it). That's the name of the forest near Beacon Academy."
    "The forest is well known for its red-leafed trees. It isn't exagerrating to say the forest is trapped in an eternal autumn, hence its name."
    "I reply Roman for the last time before trying to sleep again."
    m "M: Thank you! :D :D :D"
    r "R: quit using emoticon, you little brat"
    "I turn off my Scroll and slip it under the pillow."
    
    scene bg dormday with fade
    play music casual fadein 10.0
    
    #$ anya_outfit = "default"
    #$ ulm_outfit = "default"
    #$ rhodo_outfit = "default"
    #$ mei_outfit = "default"
    
    "The third years already went to their weekly mission two days ago, so Ulm has less 'enemy' to worry about."
    "But we're still concerned with his safety."
    "Anya has told him to stay in the crowd even though Ulm dislikes it and doesn't have any close friend beside the three of us."
    "Rhodo is trying to be helpful by telling Ulm to always bring around ... "
    extend "a handful of pepper."
    
    show anya nor sigh at midright with dissolve
    any "Pepper is for keeping snakes and ghosts away ..."
    show mei nor frown at midleft with dissolve
    mei "That's salt."
    any nor frown "Eh? It's not pepper?"
    hide anya with dissolve
    show rhodo nor smile at midright with dissolve
    rho "He can throw the pepper right into their faces!"
    rho "It's one awesome trick! And practical!"
    mei "Are you ... trying to make a substitute pepper spray?"
    rho nor confused "Well! It's pepper! We can skip the 'spray' part!"
    
    "I could make 2 or 3 pages essay explaining about why the 'spray' part of 'pepper spray' is something you shouldn't skip at all."
    "But, no. Why would I write an essay if a more thrilling mission is waiting for us down in the main hall?"
    "Ulm, however, looks grateful."
    show ulm nor smile at mcenter with dissolve
    ulm "I'm going to be okay, guys."
    ulm "Please be careful in the mission."
    hide ulm with dissolve
    show anya nor smirk at mcenter with dissolve
    any "Worry not. I'll do my best babysitting these two big babies."
    show rhodo nor shout at midright with dissolve
    show mei nor angry at midleft with dissolve
    mei "Hey!"
    rho "I'm no 'big baby'!"
    any nor sigh "Whatever, come on you two."
    hide anya with dissolve
    hide rhodo with dissolve
    "Anya, followed by Rhodo, leaves the room."
    show mei nor default with cdissolve
    
    menu:
        "Ask Ulm if he's really okay.":
            jump utalk
        "Follow the others.":
            jump unotalk
            
label utalk:
    $ ulm_pt += 1
    mei "Ulm?"
    mei "Are you sure you're going to be okay?"
    show ulm nor surprised at midright with dissolve
    "Ulm looks a bit surprised by my question."
    ulm "Don't worry about me, Miss Schnee."
    ulm smile "I'm going to be okay."
    ulm "I was just a bit off-guard back then. You know I don't usually get ambushed easily."
    "That is true."
    "Ulm excels in long-range combat. Since he has to aim and sometimes staying in one place for a quite long time, Ulm aware how important it is to not let anything get his back."
    mei nor sad "This mission would be different without you."
    ulm nor surprised "Oh ...."
    ulm nor thinking "Uhm ...."
    ulm nor surprised "Yeah?"
    ulm nor reluctant "{size=15}I'm sorry.{/size}"
    mei nor smile "What are you apologize for? You did nothing wrong."
    mei nor default "Now, take down those detentions and join us really soon!"
    ulm nor smile "I will."
    ulm "Thank you!"
    
    jump endofchoice5
    
label unotalk:
    mei "Take care, Ulm."
    mei "See you later!"
    ulm nor smile "See you later, Miss Schnee."
    
    jump endofchoice5
    
label endofchoice5:
    hide mei with dissolve
    hide ulm with dissolve
    scene bg amphitheater with fade
    "I catch up to Anya and Rhodo. The three of us head to the amphitheater, joining crowds of 2nd years."
    "About a dozen of holographic screens have been placed around the place. Each screen shows up to three missions, complete with location and brief description."
    "We pick the one with less students around it."
    show mei nor default at mcenter with dissolve
    mei "Can we pick a mission located in Forever Fall?"
    show mei nor surprised with cdissolve
    "Uh-oh."
    "I shouldn't do that. I don't usually pick a specific place for a mission."
    show mei nor frown2 at midleft with dissolve
    show anya nor default at mcenter with dissolve
    any "Forever Fall?"
    any "The last time we went there, you made a fuss about the leaves left red stain all over your clothes."
    show mei nor surprised with cdissolve
    "Aaaaaaaaaahhhhh! "
    extend "Noooooo!"
    "Anya {i}remembers{/i}!"
    
    show mei nor frown2 with cdissolve
    show rhodo nor default at midright with dissolve
    rho "I'm cool with Forever Fall."
    rho "I wanna beat some Ursa now. There's plenty of them in that red forest, right?"
    "Unaware of Anya's suspicious stare toward me, Rhodo--unexpectedly--saves me with his approval."
    any "Hmm ... let's see ...."
    any "It's a scouting/patroling mission and it'll be 3 days long."
    rho nor smile "Well! That's great!"
    rho "Three days out in the wild sounds pretty awesome for me!"
    hide rhodo
    hide mei
    hide anya
    with dissolve
    
    "I immediately caught up in a dilemma between worrying about the stain on my clothes if I were to stay there for 3 days and my desire to find out what Roman said."
    "Oh, come on, Mei."
    "Looking for possible doping 'factory' is way more important than clothes."
    "It's the {i}right{/i} thing to do!"
    show mei nor default at midleft with dissolve
    mei "I have no objection."
    mei "Besides, we never take scouting mission up until now."
    mei nor smile "'You learn new things every day'."
    
    show anya nor default at midright with dissolve
    any "...."
    "Anya doesn't look too assured."
    "I know she still holds suspicion, but she seems couldn't figure out what my real intention is."
    any nor sigh "Alright."
    any nor default "Forever Fall then."
    "YES!"
    any nor smirk "You better not complain if I push you to a pile of red leaves."
    show mei nor surprised with cdissolve
    "!!!"
    mei "What?!"
    
    mei "I can promise you I'm not going to say anything about leaves staining my jacket."
    mei nor frown2 "But it doesn't mean you can do that!"
    any "I get it, I get it."
    any "You know, Mei? Your face is funny whenever you pout like that."
    mei nor angry "So that's your real purpose??? Teasing me???"
    "Anya ruffles my hair and picks the mission to Forever Fall with her other free hand."
    hide mei with dissolve
    hide anya with dissolve
    "After a couple confirmation, team AURM is officially registered for the mission."
    "More information regarding the departure time and a checklist for our items and equipment is sent to our Scroll right after that."
    
    show rhodo nor default at midright with dissolve
    "Rhodo nudges me when Anya walks a bit further from us to converse with other team."
    show mei nor frown at midleft with dissolve
    mei "... What?"
    rho "Anya really treats you like a little sister, huh?"
    mei "*sigh*"
    mei "She just takes every chance to tease me ..."
    rho nor smile "That's exactly what older sister does."
    rho nor default "I guess you don't know about it since you're the only child in your family."
    hide rhodo with dissolve
    hide mei with dissolve
    
    "Now that Rhodo mentions it ... I do tease Li every now and then."
    "Making her chasing laser pointer around the room ... "
    extend "pouncing on her and tickling her ... "
    extend "poking on her cheek when she's reading a book."
    "It's fun to watch her reaction, since Li is really quiet."
    "... "
    extend "I guess Anya finds similar fun in poking me, huh?"
    
    show mei nor sad at midleft with dissolve
    mei "It's ... {i}strange{/i}, you know?"
    mei "I'm not the one who is usually treated like a younger sister."
    mei "And it happens quite recently. Not from the beginning."
    show rhodo nor default2 at midright with dissolve
    rho "Yeah, I can tell you why."
    rho "Anya wasn't sure about how to approach you. She once talked to us about that."
    mei nor frown2 "Huh?"
    mei "Pardon me?"
    rho nor surprised "Euh ... How to put it ..."
    rho "You were, like, putting a wall around you. Yeah, you listened to her. You talked with us. We even argued about some stuff."
    rho nor confused "But, nah. We--at least me--couldn't really understand or relate to you."
    rho nor surprised "And you spent more time with your cousin rather than with us."
    rho nor default2 "Well, I kinda understand. That little cat is the kind who's being pushed around a lot. It's difficult to leave someone like her alone."
    show mei nor default2 with cdissolve
    "Oh."
    mei nor sad "Well ..."
    hide mei with dissolve
    hide rhodo with dissolve
    
    "In our first and second semester, I brought my cousin--Li Belladonna--to Beacon."
    "It's against the school rule, of course. But I couldn't leave her alone while her parents going on a mission. My best idea at that time was to sneak her to Beacon."
    "Professor Goodwitch caught us redhandedly back then."
    "I had to give a long explanation to Professor Goodwitch. She finally gave special permission to 'keep' Li in the school as long as she didn't make any trouble."
    "Li is quiet, almost a little asocial, so she kept her promise not to make any trouble. (But she caught in some trouble with seniors that think her Faunus trait--her cat ears--as something fun to play with.)"
    stop music fadeout 5.0
    "And then {i}that incident{/i} happened."
    
    "Li's parents took her back after that. It's understandable if they're afraid their daughter is involved in another danger when she is with me."
    "I let her go. Because I think it's for the best."
    "I don't want to see someone beloved to me got hurt because of me."
    "And, well, I realized that I was wrong about my teammates."
    "I always viewed them as 'someone to work with'. I never saw them as something closer than that."
    "Rhodo was the one who made me realize that my teammates are more than just 'someone to work with'."
    "His bold and insensitive approach has its own merit, I guess."
    "From then on, I tried to 'reconcile' with them. I started putting trust on them. I might have opened up to them, even though it's only a little."
    "But it actually changed our relationship."
    "It changed a lot of thing in me. For the better."
    "When Anya comes back, we proceed with the mission preparation."
    
    scene bg locker with fade
    play music casual fadein 10.0
    "Ammunition. "
    extend "Dust. "
    extend "Food rations and water. "
    extend "Flashlight. "
    extend "Emergency flare. "
    extend "Sleeping bag."
    "For scouting/patrolling mission like this one, we would spend the night in designated campsites and outposts."
    "There are basic supplies like canned food, water, and medkits inside the outposts."
    "But we've also been warned that there's high possibility for those campsites and outposts to be in misshaped condition due to various reasons."
    "That prospect alone makes me worried."
    "Well!"
    "I can't back down now even if I want to, right? So, off we go!"
    stop music fadeout 5.0
    
    scene bg airship with fade
    "Inside the airship that brings us to the site, Anya starts a quick briefing."
    show anya nor default at mcenter with dissolve
    any "They assign us in the smallest area."
    any "The good thing is we can finish patrolling in a day. The bad thing is there's only one outpost and no campsite in the list."
    show mei nor default2 at midright with dissolve
    mei "We have to find a place to spend the night by ourself, then?"
    any "Yeah. Or, depend on what kind of outpost is this, we can make it as our temporary base."
    
    mei nor frown2 "If the outpost is in a good shape ..."
    show rhodo nor confused at midleft with dissolve
    rho "You really kill the mood here, Schnee."
    mei nor frown "There's a saying that 'if something can go wrong, it will'."
    mei "Better expecting the worst than the best, I supposed."
    rho nor default "I prefer a more optimistic approach, to be honest."
    rho "But, yeah, I guess you have a point."
    mei nor frown2 "What should we do if we can't use the outpost?"
    any "If the weather's good, we can camp outside. But, of course, I'll look for a decent shelter first just in case."
    rho nor smile "Or, we can make a shelter ourselves."
    any nor smile "Right."
    
    "I really don't want to admit my incapabilities of building any kind of shelter, but my curiosity get the best of me."
    mei nor surprised "You two know how to build a ... 'shelter'? You mean, 'tent'?"
    any nor default "Only a simple lean-on. Using one of the sleeping bag."
    rho nor default "The sleeping bag is good for additional layer for cold wind."
    rho "I don't think we need more layer, here, in Vale. Branches and leaves are good enough."
    rho nor smile "I have a waterproof canvas if it's raining. We can use that."
    "Anya and Rhodo must have noticed my confusion because of my silence. Both of them turn at me."
    show rhodo nor default with cdissolve
    mei nor surprised "W-well ..."
    mei "You know ..."
    mei nor sad "{size=15}I never camp in the outdoor ...{/size}"
    mei "{size=15}I don't know how to raise a tent or something like that ...{/size}"
    
    "I thought Rhodo will be the first one to laugh at me. "
    "Surprisingly, he doesn't!"
    rho "Eh, that's okay. I just happened to be the one Father taught about living in the outdoor since I told him I want to become a hunter."
    any "Remember the huntress I mentioned earlier?"
    any nor smirk "She said her partner didn't know what a 'sleeping bag' is when they took their first mission."
    show mei nor default2 with cdissolve
    "I feel far more better for I {i}know{/i} what a sleeping bag is."
    mei nor default "I bet her partner can't set up tent too?"
    any nor smile "Obviously."
    any nor default "I'm going to take a quick nap before we arrive."
    any nor sigh "Don't make any childish fight, both of you."
    mei nor angry "We won't!"
    hide anya
    hide mei
    hide rhodo
    with dissolve
    
    scene bg ffall1 with fade
    play music forest fadein 10.0
    "We arrive in Forever Fall in the afternoon."
    "After the airship left, the first thing we do is locating the outpost."
    "It isn't far from our landing spot and, better yet, the place is in a prime condition!"
    "Anya doesn't waste any time and starts a short briefing about our patrol route for today."
    "Since our coverage area isn't too big, it has been decided that we will patrol in individual."
    "Another good news for me because by working alone, Anya--hopefully--wouldn't notice that I'm looking for something."
    "I find an old unused port during my patrol."
    "No ship. But there are signs that this port was once used to transport something. I can see a structure that previously is a crane machine."
    "I don't find any clues in the port, but I take note that this port could be a possible route for transporting goods like The Golden Apple."
    
    "If I take it that way, it opens a possibility that The Golden Apple production is located in {i}another place{/i}."
    "... "
    extend "*sigh*"
    "I currently don't have any idea how should I pursue this matter further if my assumption of the 'brewery' whereabouts is right."
    "Done with my patrol/scout (and the investigation), I walk back to our rendezvous point."
    "I'm the first one to be there. Anya and Rhodo are still away."
    "I check my Scroll "
    extend "and my heart skip a beat."
    "It's a message from Li!"
    
    "Lately, Li has been telling me about 'she found a new teacher'."
    "Li is quiet, both in verbal and written form. She usually replies with short message. So, when she writes a lot about something or someone, I know she's really fond of them."
    "Her 'new teacher', apparently, taught Li some new fighting techniques focused on kicks."
    "Unlike me who use Semblance and Dust in combat, Li is more advance in physical feat. She prefers taking down Grimm with her fists."
    "And I know she has been wanting to learn a more flexible form for a while now."
    "What I'm worried about her is ... she tends to overwork herself in training ..."
    "Let's see what she has this time."
    
    "..."
    extend "..."
    extend "..."
    extend "This is {i}unbelieveable{/i}."
    "Her mentor, her {i}official{/i} mentor pitted her against a Beringel two days ago."
    "Beringel is a gorilla-like Grimm. It's basically as strong as an Ursa, but as quick as a Beowolf."
    "It's a Grimm that could be found in Mistral region, not in Vale. That's why I never meet one."
    "Anyway ..."
    
    nvl clear
    m "M: R u ok?"
    m "M: U're not hurting urself, r u?"
    l "L: Nope but i almost late the next day"
    l "L: Oversleep"
    l "L: The grimm is weakened too so no prob"
    "But still! That Grimm is too dangerous for first year student!"
    
    "Even though Li assures me that she is okay, I couldn't help not feeling upset."
    "Really ... what did they think by bringing a dangerous Grimm to the school vicinity?"
    "What if the Grimm hurts the students? What if it escapes?"
    "Do they even think that far?"
    "..."
    "Well ... I wouldn't change anything by complaining."
    "More importantly, it's a relief Li managed to handle it."
    "I'm still surprised whenever she told me about her achievement, especially in combat training."
    "Despite of her small stature, Li is actually {i}that{/i} strong ..."
    
    #sudden QTE
    stop music fadeout 5.0
    "Sensing something approaching from behind, I turn around."
    play sound ursa
    "An Ursa."
    #sudden QTE again
    "I click my tongue and move my hand, fully forming a Glyph in the air."
    play sound glyph
    "A big sword appear from inside the glowing Glyph."
    "Moving accordingly with my hand, the sword slash the Ursa in front of me."
    "With a weak whine, the Ursa collapses, splitted into two."
    "And I see another one stands further behind the one I've just defeated."
    any "What did I say about limiting your summon usage?"
    mei "It's the first time for today ... I'm not going to collapse because of summoning a single sword."
    "Anya steps next to me. She swirls her cane in one hand."
    
    play music brawl fadein 10.0
    show anya nor smirk at mcenter with dissolve
    any "Let me have that one."
    any "I can use some warm-ups with it."
    mei "Sure. It's all yours."
    any "Thanks."
    hide anya with dissolve
    "I release my Glyph and the giant sword disappear."
    
    play sound ursa
    "Anya walks toward the Ursa. The bear-like creature responds with an intimidating growl, followed by swinging its claw. "
    extend "Anya dodges it--"
    play sound hit
    "--and lands a powerful hit on the Ursa's head." with vpunch
    show cg ursafight with fade
    "Before the Ursa get back into its stance, Anya launches a few quick thrusts. The creature staggers."
    "Anya looks as if she only plays around with the Ursa."
    "I couldn't help not teasing her."
    mei "You have to try harder, Anya!"
    any "Try harder to make it looks like a punching bag?"
    mei "Oh, ha-ha."
    
    "Anya does finish off the Ursa in her next moves."
    "She parry another claw attack and unsheathe the hidden blade inside her cane."
    "With uninterrupted smooth move the blade slice the creature's neck, cut the other claw, and dig deep on the abdomen."
    "Anya sheathe her blade back after poking the creature with the tip of her cane."
    "The Ursa stumbles and crashes to the ground," with vpunch
    extend " not moving."
    scene bg ffall1 with fade
    "When Rhodo comes not long after that, the Ursas we defeated had dispersed into black smoke and disappeared."
    stop music fadeout 5.0
    
    scene bg outpostnight with fade
    play music outnight fadein 10.0
    "The three of us return to the outpost, wrapping our job for today."
    any "Send me your recording for today's patrol, you two."
    "Beside walking around and trying to find what's off, we are required to record the surroundings at certain point in our patrolling area and send them back to the coordinator of this mission."
    show rhodo nor default2 at midleft with dissolve
    rho "Do we need to patrol again at night?"
    show anya nor default at mcenter with dissolve
    any "We'll do that tomorrow. We'll start early, take a rest in the afternoon, and patrol again at night."
    
    show mei nor frown2 at midright with dissolve
    mei "Why we're not doing it tonight?"
    any "We arrived in the afternoon, so I think it's best if we save our energy for today."
    "Makes sense."
    any nor smirk "So, Mei, did you find what you're looking for?"
    mei nor sad "*sigh* No, I didn't. I don't know where to st--"
    show mei nor frown2 with cdissolve
    "Wait."
    show mei nor surprised with cdissolve
    "I cover my mouth with both hands when I realize that I'm not supposed to answer that question."
    "Double failure. My reaction is confirming everything."
    "Anya {i}tricked{/i} me!"
    
    rho nor surprised "What? "
    rho "You're looking for ... something?"
    mei "No, no, no."
    mei "I ... misheard the question."
    any "Seeing how she reacted, yes, she's looking for something."
    any "And she probably doesn't know where to start."
    "Arrgggh! I blew my own secrets!"
    
    rho nor frown "You're going all lone wolf again."
    rho "Why is it so hard for you to trust us?"
    mei nor frown2 "It's not about trusting you or not."
    mei nor sad "I don't want to involve you in such ... uncertainty."
    any nor sigh "We already involved in that 'uncertainty' by coming here."
    any "You were the one who suggested us to take a mission in Forever Fall."
    mei "...."
    
    mei "I'm sorry ...."
    rho "I really hate this part of you, Schnee."
    rho "Why--"
    any nor default "That's how she works. Don't get too worked up, Rhodo."
    any "It's not something you can change overnight. "
    any "Let's just deal with it, okay?"
    rho nor surprised "Oh, well, if you say so ...."
    rho nor frown "But, that doesn't mean I don't hate it."
    mei "I'm sorry. Really."
    mei "{size=15}And thank you ....{/size}"
    any nor smirk "No problem."
    any nor default "Now, about what you're looking for ...."
    
    show rhodo nor default2 with cdissolve
    mei nor frown2 "Ulm said something about 'brewery', remember?"
    mei "Assuming it's located in Vale, someone gave me a tip to look around in Forever Fall."
    rho "'Someone'?"
    mei nor frown "A person you don't want to mess up with."
    rho nor surprised "Oh, alright ...."
    mei nor frown2 "I think it's plausible. Forever Fall has several train tracks, both active and inactive, that go through tunnels."
    show rhodo nor default2 with cdissolve
    mei "I even find a port in my patrol route today. It doesn't seem it has been used in decades."
    "There is silence for a moment after I finish my explanation."
    
    show anya nor frown with cdissolve
    "Anya looks thinking about it. Rhodo is awaiting for Anya."
    "Feeling uneasy with all this sudden silence, I decided to say something:"
    mei nor default "You should consider being a private investigator, Anya, really."
    show anya nor smile with cdissolve
    "Anya chuckles."
    any "It's nice to know that my job opportunity is wider than average person."
    show mei nor sad with cdissolve
    "Which I secretly envy."
    "I don't see myself having so much choice in life, especially for a 'job' ...."
    
    show mei nor default with cdissolve
    any nor frown "But with no clues of what should we look for, it's hard to come up with a plan."
    any nor default "Putting myself in their position, I'd definitely not going to set up my brewery and transportation in plain sight."
    any "And this place is regularly patrolled if I'm not mistaken."
    show mei nor sad with cdissolve
    "I guess I'm still too naive to think this Golden Apple case is going to be easy to figure out ...."
    "But giving up is not an option too!"
    mei nor default2 "Do you think there's a {i}slight{/i} chance we capture something in our recording during the patrol?"
    mei nor frown2 "We have to record our surrounding at certain spot, right?"
    any nor sigh "If you're talking about {i}tiny{/i} possibility, then it is possible."
    any "Keep your eyes open, but don't get your hopes too high. Okay?"
    "It doesn't sound like I have any better choice for now."
    mei "Well ... okay."
    any nor default "We need to take turn for guard tonight, by the way."
    any "Who wants to go for the first shift?"
    show mei nor default with cdissolve
    rho nor default "I'll do it."
    any nor smile "Great. Mei, do you want to take the second shift or the third one?"
    
    menu:
        "The 2nd shift.":
            jump secondshift
        "The 3rd shift.":
            jump thirdshift
    
label secondshift:
    mei "I'll take the 2nd shift."
    any nor default "Let's see ..."
    any "Alright, set your alarm to 12.30."
    any "Rhodo, wake her up when it's time, okay?"
    rho nor smile "Sure. Why won't I?"
    hide rhodo
    hide anya
    hide mei
    with dissolve
    
    scene black with fade
    "Apparently, Rhodo's idea of night guard is by walking around the outpost back and forth. He doesn't sit still and watch his surrounding."
    play sound foot
    "His footsteps keep me awake for a while, until I start to treat the sound as a pattern."
    "Finding a monotonous pattern--just like counting imaginary sheep jumping over a fence--helps me fall asleep quite faster."
    "..."
    extend "..."
    extend "..."
    extend "..."
    "..."
    #alarm SFX
    "I doubt I have slept when the alarm from my Scroll beeping."
    
    scene bg outpostnight with fade
    "With a low groan, I rise from my sleeping bag. I look across the place we're sleeping in and find Anya."
    "I thought she's still awake, because she is in sitting position with one hand clutching her cane that lean on her shoulder. Her breath is in rhythmic. Both eyes are closed."
    
    "... Is she sleeping?"
    "There is no way someone could sleep while sitting like that, right?"
    "I quickly doubt myself because Anya stays still and doesn't open her eyes to catch me staring at her."
    
    show rhodo nor smile at mcenter with dissolve
    rho "Hey, Sch--"
    rho nor default "Oh, you're awake?"
    rho nor surprised "Uh ... something's wrong ...?"
    hide rhodo with dissolve
    "Doesn't want to awake Anya (no, really, she couldn't sleep in that position, right?) I drag Rhodo outside before I answer his question."
    
    show rhodo nor surprised at midleft with dissolve
    rho "Well, never done it myself, but I know people do that."
    rho "Of course you're not gonna totally sleeping in that position."
    show mei nor frown2 at midright with dissolve
    mei "But why?"
    mei "I mean, why she do that?"
    
    rho "So she can wake up anytime there's trouble. Or potential trouble."
    mei nor sad "Does she think we couldn't do our job right ...?"
    rho nor default "Nah, I don't think so. It's Anya we're talking about."
    show mei nor default2 with cdissolve
    rho "Remember our first week together? She asked us to {i}help{/i} her."
    rho nor surprised "It ... surprised me, actually."
    rho nor default "I thought she's going to order us around like what I thought of a team leader will be. But she isn't."
    mei nor smile "You're right."
    mei nor default "So ... what people usually do when on guard duty?"
    rho "A really humble request from big city girl. I feel honored."
    mei nor angry "Hey!"
    rho nor smile "Haha! Just kidding, Schnee!"
    hide rhodo with dissolve
    hide mei with dissolve
    "Rhodo explains his version of how one do a guard duty. He makes it sound easy (\"Just keep watching your surrounding for potential threat.\") and difficult at the same time(\"Don't fall asleep!\")."
    "However, despite of his earlier teasing, Rhodo doesn't make fun of me when he's explaining."
    
    jump endofchoice6
    
label thirdshift:
    mei "I'll take the 3rd shift."
    any nor default "Let's see ..."
    any "Alright, set your alarm to 3.30."
    any "I'll wake you up when it's your turn."
    mei "Got it."
    hide rhodo
    hide anya
    hide mei
    with dissolve
    
    scene black with fade
    "I fall fast asleep after treating Rhodo's constant footsteps outside as something rhythmical."
    "At first, I doubt I could make myself comfortable with the sleeping bag. This is my first time sleeping with this kind of arrangement."
    "Not particularly comfortable compared to dorm's bedding (I exclude the bed in home; it's not a fair comparison if I put it in), but I'll manage. "
    extend "Somehow. "
    extend "Somewhat."
    "..."
    extend "..."
    extend "..."
    extend "..."
    "..."
    any "Mei, wake up."
    any "It's your turn now."
    "I wake up in groggy state."
    
    scene bg outpostnight with fade
    "It takes me a few seconds before I recall where am I and what am I doing here."
    show anya nor smirk at mcenter with dissolve
    any "You're such a sleeping beauty, not awake by your Scroll alarm."
    any "It went off a little while ago."
    mei "Sorry ..."
    mei "*yawn*"
    mei "Sorry again."
    
    "I rise from my bed and walk outside, not fully awake and alert of my surrounding."
    "Anya approaches me and put Alpenaster, my dagger, on my palm."
    show anya nor default at midright with dissolve
    any "Here. You forgot this."
    show mei nor default2 at midleft with dissolve
    mei "Oh. Right. Thanks."
    show mei nor frown with cdissolve
    "Ugh, I really couldn't function properly in this state."
    
    any "Can you, with your current condition, make one little icicle?"
    any "No need to launch it at anyone. Just make one and hold it in your hand."
    "I mumble. Purely doing it by muscle memory, I create an icicle and hold it just as requested."
    "The coldness on my hand starts as a mere cool sensation."
    "The longer I hold onto it, the feeling grows into something unpleasant. "
    extend "My palm is wet."
    show mei nor frown2 with cdissolve
    "Uh ... now it feels numb ..."
    
    "When I loosen my grip, Anya wraps her own hand on mine, forcing me to hold the melting icicle tighter despite of the numb."
    mei nor surprised "Anya, I couldn't feel my finger! Let go!"
    any nor smirk "Count to 10."
    any nor default "One ... "
    extend "Two ... "
    extend "Three ... "
    extend "Four ... "
    extend "Five ..."
    
    show mei nor frown2 with cdissolve
    "I grit my teeth, not counting until Anya reaches 5. Her counting is slow and it's a torture with ice on hand."
    any "Nine ...."
    any "Ten."
    "I flick the ice away. The small chunk is disappear immediately among the bushes."
    mei nor angry "What was that for?!"
    any nor smile "Now, you're awake."
    any "Close your eyes and put your hand on your eyelid."
    any "Yeah, like that."
    "I'm still steaming with what just happened, but I do what I'm told."
    mei "Not cool, Anya."
    any nor default "Really?"
    any nor smirk "That's probably what makes the ice melt so quickly."
    any "You're not making it cool enough."
    mei "Grrrrr."
    any nor smile "More importantly, now you're awake."
    show mei nor frown2 with cdissolve
    "Anya pats me on the shoulder."
    any "Be a good guard, okay? If you feel sleepy anytime soon, try to do the ice trick again."
    any "I'll see you in the morning."
    hide anya with dissolve
    hide mei with dissolve
    "And Anya leaves, return to the outpost."
    "Unsure of what one should do during a guard duty, I watch the surroundings of the outpost and--somehow--manage to keep awake until dawn."
    
    jump endofchoice6
    
label endofchoice6:
    stop music fadeout 5.0
    scene black with fade
    "There is always first time for everything, so it said."
    "First time spending the night in Forever Fall in a sleeping bag."
    "First time having protein bar as breakfast. "
    extend "Which I regret so much."
    
    scene bg outpostday with fade
    "I've seen Li eating this ... 'thing' every now and then."
    "She looks enjoying it, so it's normal for me to conclude that this 'thing' is tasty to a certain extent, right?"
    "No. "
    extend "{b}NO{/b}."
    "Just. "
    extend "No."
    "{b}Absolutely NO.{/b}"
    "It barely has any taste! It only has the aroma of delicious chocolate/strawberry/vanilla/coffee/other, but not the taste of it!"
    "And the worst of all this 'thing' is as chewy as a car tire."
    "How in Remnant Li could eat this???"
    
    "Interestingly, Anya and Rhodo seem to share the same concern regarding our protein bar breakfast."
    show rhodo nor confused at mcenter with dissolve
    rho "Can we, I don't know, crush this thing into powder and drink it with water or something?"
    mei "That doesn't make it more appealing ..."
    rho "At least I don't break my jaw--!"
    rho nor surprised "Ouch! "
    rho "My tongue ... Ouch ..."
    hide rhodo with dissolve
    
    "Anya return to us with a can. She has been ransacking the supply in the outpost for a while."
    show anya nor sigh at mcenter with dissolve
    any "Here. Canned peach."
    any "I'm going to go all stubborn and finish this 'protein rubber' bits. You can leave yours and eat the peach if you want."
    any nor default "Bring the leftovers with you, though. In case you need it. Who knows."
    hide anya with dissolve
    "I reluctantly keep half of the protein bar in my pocket, hoping it would melt because of the heat."
    "... But if it's melting, it would be gross."
    "Oh well ... whatever."
    "Anya assigns us with different patrol route than yesterday."
    
    scene bg ffall2 with fade
    play music forest fadein 10.0
    "My route today presents me with some small Grimm creatures along the path. I finish them without much effort."
    "Again, I find nothing."
    "I start to question Roman's suggestion."
    "Maybe he gave me random answer just to stop me bugging him."
    "... I guess I need to call him again after this mission."
    "Anya doesn't find anything either. I don't have much hope for Rhodo, considering how he often misses details."
    "But, he looks cheerful when come back to our rendezvous point."
    stop music fadeout 5.0
    
    scene bg outpostday with fade
    show rhodo nor smile at mcenter with dissolve
    rho "Look what I've found!"
    rho "Lunch!"
    "It takes me a couple of second before I get the connection between the fish Rhodo brought and what he said."
    show rhodo nor smile at midleft with dissolve
    show mei nor surprised at midright with dissolve
    mei "Where did you get them?"
    rho nor default "The port you said yesterday. I took a little detour and explore the shore nearby. I found a nice spot where there's a lot of fish."
    
    mei nor frown2 "But you don't bring any fishing equipment ..."
    hide mei with dissolve
    show anya nor smile at midright with dissolve
    "Anya chuckles."
    any "Lemme guess. You spear them?"
    rho nor smile "Yep."
    "I'm at loss of words."
    hide rhodo
    hide anya
    with dissolve
    "Never once in my lifetime I know there's a fishing method using {i}spear{/i}. Not fishing rod, not fishing net, but {i}spear{/i}!"
    "There's so many new things I discovered in so little times, it's almost overwhelming."
    
    "The fish, by the way, is a delicious bounty compared to the protein bar, even though I have a fair difficulties adjusting to how I eat it without fork and knife."
    "... That reminds me."
    "I don't remember seeing fish head and tail whenever Li eats fish ... Does she eat them too?"
    "As expected from a cat Faunus, I guess."
    
    scene black with dissolve
    "After peaceful morning and afternoon, our night patrol turns out pretty bad."
    "The three of us go through each patrol route together. Because it's already dark, we need to practice extra caution."
    play sound thunderroll
    "Almost reaching the end of our second patrol route, we hear thunder cracks above our head."
    rho "That doesn't sounds--"
    play sound lightning
    rho "WHOA!"
    play music rain fadein 2.0 fadeout 2.0
    mei "It's pouring down!"
    any "There's old tunnel nearby!"
    rho "No, no, no, no! It's not in that direction! Anya!"
    mei "Shouldn't it over there?"
    play sound lightning
    
    scene bg tunnel with fade
    "After a couple of misdirection, we find the tunnel. There's an old train track go through here and, the more interesting part, this tunnel seems to be built based on older tunnel that led to an ancient structure."
    play sound thunderroll
    "Just like in Emerald Forest, there's derelict ruins from older times in Forever Fall. They are severely damaged, leaving the ruins look a random out of place structure in the middle of the forest."
    "I even found a set of tall pillars with platform-like top where you can stand on it and jump to the next one. With the aid of Aura, of course. It's a little far between one pillar to the next one."
    
    show mei nor frown at midleft with dissolve
    mei "This isn't just heavy rain. This is a {i}storm{/i}."
    show anya nor frown at midright with dissolve
    any "Why at a time like this ..."
    show mei nor default2 with cdissolve
    "We take out our flashlight and turn it on, inspecting the tunnel we're in."
    "One good news, there's no leaking as far as we can see. This place is dry."
    
    play sound lightning
    "One bad news, if the storm goes on through the night, we have to spend our night here without decent food and bed."
    mei nor default "Do you mind if I go checking what ahead?"
    "I point my flashlight to what looks like a corridor. The arch above the corridor entryway is part of the old ruins, but the rest are newer structure."
    any nor default "Make sure your Scroll is on."
    "I check the device: "
    extend "half battery, "
    extend "full signal, "
    extend "a bit soaked wet, but everything's good."
    
    play sound thunderroll
    any "Shout for help or blast something if you need help."
    mei "Okay."
    any "Running away is also permitted."
    show mei nor frown with cdissolve
    "Well! Anya should've known by now that 'running away' hurt my pride badly and I will make it the last choice I pick."
    any "Mei?"
    mei nor frown2 "Okay, okay."
    mei "I'm going to be fine. "
    mei nor cocky "{i}Mom{/i}."
    show anya nor smile with cdissolve
    "I grin at Anya. She snorts and nods, signaling me to go doing whatever I want while she try figuring out our next moves."
    hide mei
    hide anya
    with dissolve
    stop music fadeout 5.0
    
    scene black with fade
    "The corridor leads to a spacious room. Again, it's a mix of old and new structure. Judging from the size of this room, I guess it's some sort of storage for big container."
    "... Most likely one of SDC's property."
    "It isn't written in any history books, but I can tell what happened: this place was shut down due to constant raid from a group called White Fang."
    "Schnee Dust Company and the White Fang group had a history of bad blood. They detest each other."
    "White Fang was a group of Faunus--a race who has certain animal trait on them, like ears, tail, horns, or claws--that claimed to fight for Faunus' equality."
    "Back then, SDC was one of their target because the company employed a lot of Faunus and there was indication of mistreatment."
    
    "... "
    extend "I'm really not in the position to talk about this."
    "People around me has different definition and opinion about the White Fang."
    "My mom sees them as 'a result of anger and disappointment born from discrimination' and SDC also at fault in the past, more or less."
    "Roman said they're 'bunch of thugs hiding behind mask' that lack of good management."
    "On the other hand, my aunt, Li's mother, was an ex-White Fang, who left the group when she thought they strayed from their original purpose."
    "Really ... Being a part of influential family that once involved in a big conflict makes me have to proceed with extra caution in everything."
    "I'm glad my teammates are already in the exception range of that rule."
    "I admit, I haven't entirely opened up to them, but they're great friends."
    "We could exchange playful banter without any worries of raising worldwide affair, which is more than enough for me."
    "I find a lever with electricity signage."
    
    play sound switch
    scene bg storage with fade
    "The lever moves easily when I pull it. There's a humming sound faraway and the whole room is flooded by the light from the lamps above."
    "Another good news: lights!"
    play sound noise
    mei "...?"
    "Uh oh. What is that? "
    "Grimm?"
    "I reach Alpenaster and walk toward the source of the sound."
    "This place is too quiet."
    
    "I could hear my own breath and it's unsettling."
    unk "It's sure pouring down outside, huh?"
    "I turn around and stand face to face with a woman."
    show emerald default at midleft with dissolve
    show mei nor surprised at midright with dissolve
    mei "Uh ... yes. Who--"
    unk "I was out for some treasure hunting in this area when the storm hit."
    mei nor frown2 "Treasure hunting?"
    
    show emerald smile with cdissolve
    unk "Just my hobby."
    mei "Did you find something?"
    "I couldn't shake the feeling that there's something 'wrong' with this woman."
    "On the other hand, I couldn't help not trying to be polite to her."
    show emerald default with cdissolve
    unk "Unfortunately, no."
    unk "Talking about bad luck."
    unk "By the way, you're a huntress, right?"
    mei nor default2 "I'm a student, actually."
    
    show emerald smile with cdissolve
    unk "Oh, that's better! You're together with your friends, aren't you?"
    mei nor frown2 "Yes ... Why?"
    show emerald default with cdissolve
    unk "Mind if I join you? I'm not really confident with my combat skill, so I think I'll be safe with all of you."
    unk "Oh, yeah. I have my rations. Here. I can share it with you guys."
    unk "If you allow me, of course."
    "I {i}really{/i} couldn't read her. Anya might be better at this."
    
    mei "Well ... okay. They're still in the tunnel."
    unk "It's warmer around here. Why don't they get in?"
    mei nor frown "They're not really ... adventurous."
    unk "Unlike you?"
    show mei nor frown2 with cdissolve
    "I feel a little flattered when she said it. I'm not sure why."
    "I already took my Scroll out and typed a message to Anya a moment ago."
    "Even though I could say with confidence that social interaction is one skill I excel at, this woman here is ... out of the league. She's something else."
    "She makes me {i}nervous{/i}."
    
    "Anya's immediate reply relieves me. I take a deep breath and talk again to the woman."
    mei nor default2 "Let's go to the storage room."
    unk "Oh? You know about this place?"
    mei nor frown "No. If you mean have I ever been here before, the answer is 'no'."
    mei "I'm just guessing what was the room purpose before this property is abandoned, that's all."
    show emerald smile with cdissolve
    unk "I see."
    show emerald default with cdissolve
    unk "I heard from my friend that SDC equipped this place with bots."
    mei nor frown2 "You mean androids?"
    
    unk "Yes, androids! I also heard SDC strapped their train with ones too."
    unk "They're paranoid bunch, I must say."
    "This lady talks too much, doesn't she? And about something I disagree too."
    mei nor default2 "It's for security measure. Back when this property is in use, the trains were attacked and robbed many times. It's logical to prepare a security mechanism inside the train."
    unk "I don't particularly agree with the use of androids."
    unk "They can be easily manipulated and malfunctioned. It's ... dangerous ... uh, will you look at over there?"
    "I turn my eye to the direction she points at."
    
    show mei nor frown2 with cdissolve
    "There are two droids approaching us and I've seen them too often to know that these two are hostile toward us."
    unk "You see my point now, right?"
    hide emerald with dissolve
    hide mei with dissolve
    
    #$ mei_outfit = "armed"
    show mei arm angry at mcenter with dissolve
    mei "Stand back. I'll handle them."
    hide mei with qdissolve
    play music rosecombat fadein 10.0
    "I load Alpenaster with ice Dust. Its blade glows in blue."
    "This should be sufficient to finish them off."
    #ice Dust QTE
    play sound glyph
    "White Glyph formed in front of me, circling and launching several icicles--"
    extend "which are blocked with force shield by one of the android."
    show mei arm frown2 at mcenter with dissolve
    mei "Wha--?"
    "This isn't going as I planned. The androids are smarter than I thought."
    "They looks hesitant, but perhaps it's because their system haven't fully kickstarted."
    #Time Dilation QTE
    show mei arm angry with cdissolve
    play sound glyph
    "I cast Time Dilation and leap on the nearest android."
    "This android holds a baton-like weapon, not the most dangerous type, but that baton is loaded with electricity to incapacitate its opponent."
    "I don't see the baton is activated now."
    "Finger crossed, I hope it's also malfunctioning."
    hide mei with qdissolve
    
    #QTE QTE QTE QTE and QTE
    play sound clash
    "It blocks my dagger."
    play sound clash
    "I move next to it and launch a few quick stabs. I could only manage to hit it one or two times out of five." with hpunch
    "The other android approaches. It raises its shield, giving me very few open target."
    "Still under Time Dilation effect, I manage to go around the second android and attack it from behind."
    "Much to my surprise, the android reacts faster and parry my attack."
    "There is a wide opening in my stance that is more than enough for the android to counter-attack. I'm lucky it doesn't take its chance."
    "I take a few steps back."
    
    "This isn't right."
    "They've managed to fend me off for a while now."
    #show mei weapon at mcenter with dissolve
    "I don't think I should use my Semblance more than this, but taking these androids down without it would eventually exhaust me too."
    "Where are Anya and Rhodo? If they come here right at this moment, it would be easy to defeat these androids."
    "But for now ..."
    "I see the indicator on the androids' head turn bright red from yellow."
    "I raise my hand and form a bigger Glyph."
    play sound glyph
    #Summon Knight QTE
    #show mei knight with dissolve
    
    scene cg android with fade
    "A giant hand covered in metal gauntlet emerges from the Glyph I casted. It holds a giant sword."
    play sound hit
    "I move the hand, hitting the android with metal shield attached to its hand. It fly away to the furthest corner of the room." with hpunch
    "The one with baton, however, dodge the next attack."
    #hide mei with qdissolve
    "It buzzes electrical noise while keep moving."
    "This persistent android starts to tick me off."
    "Whoever tampering with their system is an advance practitioner for sure. Atlessian Military would be very interested to get their hand on such talent."
    
    #QTE QTE QTE
    "The android backs off and I chase it. I move the hand again to slash my target--"
    "The giant sword sinks in the storage room wall." with hpunch
    "The android ... {i}tricks{/i} me???"
    "This is an intelligence beyond what it suppose to have!"
    "A sudden chill spread through my spine, sending anxiety throughout my system."
    "Something must be wrong here .... Something isn't right ...."
    "At this rate, I might be ...."
    
    "{i}I might be defeated ...{/i}"
    "No, no, no, that couldn't happened. It {i}wouldn't{/i} happen."
    "With the fear of getting overwhelmed by mere security android clouds my mind, I launch an all-out attack."
    "I move the summoned gigantic hand, leaving its sword stuck in the wall and grabbing the android on its hand." with hpunch
    "I remember throwing my opponent toward the ceiling above."
    "I'm about to hit it again for the last time, making sure I fully defeat it, when something hit the back of my head."
    
    scene bg storage with fade
    stop music fadeout 5.0
    play sound hardhit
    mei "!!!" with hpunch
    
    scene black with fade
    call chapter("End of Part 3", "Storm") from _call_chapter_5
    
    #centered "End of Part 3 - Storm"
    
    jump part4
    
#return