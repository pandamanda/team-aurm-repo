﻿# You can place the script of your game in this file.

# Declare images below this line, using the image statement.
# eg. image eileen happy = "eileen_happy.png"

# Declare characters used by this game.
# define e = Character('Eileen', color="#c8ffc8")

### TRANSITION ###
define qdissolve = Dissolve(0.25)
define cdissolve = Dissolve(0.3)

### IMAGE POSITION ###
### 3 Characters ###
define midright = Position(xpos=.75, ypos=1.35, xanchor='center')
define midright2 = Position(xpos=.875, ypos=1.35, xanchor='center')
define midleft = Position(xpos=.25, ypos=1.35, xanchor='center')
define midleft2 = Position(xpos=.125, ypos=1.35, xanchor='center')
define mcenter = Position(xpos=.5, ypos=1.35, xanchor='center')

### 4 Characters ###
define charleft1 = Position(xpos=.2, ypos=1.35, xanchor='center')
define charleft2 = Position(xpos=.4, ypos=1.35, xanchor='center')
define charright1 = Position(xpos=.6, ypos=1.35, xanchor='center')
define charright2 = Position(xpos=.8, ypos=1.35, xanchor='center')

### BACKGROUNDS ###
##### Background List: amphitheater, dormday, dormnight, classroom, corridor, dininghall, eforest, ffall1, ffall2, locker, tunnel, walkway, vale1, vale2
image dark = Solid("#808080")

image bg outpost = Placeholder("bg")
image bg veloffice = Placeholder("bg")
image bg admoffice = Placeholder("bg")

###Sepia Backgrounds
image bg tunnelsepia = im.Sepia("bg tunnel.jpg")
image bg storagesepia = im.Sepia("bg storage.jpg")

### CGs ###
#image cg glyphglide = "images/CGs/cg glyphglide.jpg"
#image cg dormtalk = "images/CGs/cg dormtalk.jpg"
#image cg lockerfight = "images/CGs/cg lockerfight.jpg"
#image cg messaging = "images/CGs/cg messaging.jpg"

#image cg ursafight = "images/CGs/cg ursafight.jpg"
#image cg android = "images/CGs/cg android.jpg"
#image cg anyawound = "images/CGs/cg anyawound.jpg"
#image cg rubycall = "images/CGs/cg rubycall.jpg"

### CHARACTERS ###
##### MEI
##### Expression List: default, smile, frown, frown2, surprised, angry, sad
define mei = Character('Mei', image = "mei", who_color="#ffffff")
define mei_expression = "default"
define mei_outfit = "default"

##### Tinted Images
image meitint default = im.MatrixColor("images/characters/mei/default/mei default.png", im.matrix.brightness(-0.25))
image meitint default2 = im.MatrixColor("images/characters/mei/default/mei default2.png", im.matrix.brightness(-0.25))
image meitint smile = im.MatrixColor("images/characters/mei/default/mei smile.png", im.matrix.brightness(-0.25))
image meitint frown = im.MatrixColor("images/characters/mei/default/mei frown.png", im.matrix.brightness(-0.25))
image meitint surprised = im.MatrixColor("images/characters/mei/default/mei surprised.png", im.matrix.brightness(-0.25))
image meitint frown2 = im.MatrixColor("images/characters/mei/default/mei frown2.png", im.matrix.brightness(-0.25))
image meitint angry = im.MatrixColor("images/characters/mei/default/mei angry.png", im.matrix.brightness(-0.25))
image meitint sad = im.MatrixColor("images/characters/mei/default/mei sad.png", im.matrix.brightness(-0.25))
image meitint cocky = im.MatrixColor("images/characters/mei/default/mei cocky.png", im.matrix.brightness(-0.25))

image meitint2 default = im.MatrixColor("images/characters/mei/uniform/mei default.png", im.matrix.brightness(-0.25))
image meitint2 default2 = im.MatrixColor("images/characters/mei/uniform/mei default2.png", im.matrix.brightness(-0.25))
image meitint2 smile = im.MatrixColor("images/characters/mei/uniform/mei smile.png", im.matrix.brightness(-0.25))
image meitint2 frown = im.MatrixColor("images/characters/mei/uniform/mei frown.png", im.matrix.brightness(-0.25))
image meitint2 surprised = im.MatrixColor("images/characters/mei/uniform/mei surprised.png", im.matrix.brightness(-0.25))
image meitint2 frown2 = im.MatrixColor("images/characters/mei/uniform/mei frown2.png", im.matrix.brightness(-0.25))
image meitint2 angry = im.MatrixColor("images/characters/mei/uniform/mei angry.png", im.matrix.brightness(-0.25))
image meitint2 sad = im.MatrixColor("images/characters/mei/uniform/mei sad.png", im.matrix.brightness(-0.25))
image meitint2 cocky = im.MatrixColor("images/characters/mei/uniform/mei cocky.png", im.matrix.brightness(-0.25))

image meitint3 default = im.MatrixColor("images/characters/mei/weapon/mei default.png", im.matrix.brightness(-0.25))
image meitint3 default2 = im.MatrixColor("images/characters/mei/weapon/mei default2.png", im.matrix.brightness(-0.25))
image meitint3 smile = im.MatrixColor("images/characters/mei/weapon/mei smile.png", im.matrix.brightness(-0.25))
image meitint3 frown = im.MatrixColor("images/characters/mei/weapon/mei frown.png", im.matrix.brightness(-0.25))
image meitint3 surprised = im.MatrixColor("images/characters/mei/weapon/mei surprised.png", im.matrix.brightness(-0.25))
image meitint3 frown2 = im.MatrixColor("images/characters/mei/weapon/mei frown2.png", im.matrix.brightness(-0.25))
image meitint3 angry = im.MatrixColor("images/characters/mei/weapon/mei angry.png", im.matrix.brightness(-0.25))
image meitint3 sad = im.MatrixColor("images/characters/mei/weapon/mei sad.png", im.matrix.brightness(-0.25))
image meitint3 cocky = im.MatrixColor("images/characters/mei/weapon/mei cocky.png", im.matrix.brightness(-0.25))


##### Default Outfit
image mei nor default = "images/characters/mei/default/mei default.png"
image mei nor default2 = "images/characters/mei/default/mei default2.png"
image mei nor smile = "images/characters/mei/default/mei smile.png"

image mei nor frown = "images/characters/mei/default/mei frown.png"
image mei nor frown2 = "images/characters/mei/default/mei frown2.png"
image mei nor angry = "images/characters/mei/default/mei angry.png"

image mei nor surprised = "images/characters/mei/default/mei surprised.png"
image mei nor sad = "images/characters/mei/default/mei sad.png"
image mei nor cocky = "images/characters/mei/default/mei cocky.png"

##### Armed Outfit
image mei arm default = "images/characters/mei/weapon/mei default.png"
image mei arm default2 = "images/characters/mei/weapon/mei default2.png"
image mei arm smile = "images/characters/mei/weapon/mei smile.png"

image mei arm frown = "images/characters/mei/weapon/mei frown.png"
image mei arm frown2 = "images/characters/mei/weapon/mei frown2.png"
image mei arm angry = "images/characters/mei/weapon/mei angry.png"

image mei arm surprised = "images/characters/mei/weapon/mei surprised.png"
image mei arm sad = "images/characters/mei/weapon/mei sad.png"
image mei arm cocky = "images/characters/mei/weapon/mei cocky.png"

##### Uniform Outfit
image mei uni default = "images/characters/mei/uniform/mei default.png"
image mei uni default2 = "images/characters/mei/uniform/mei default2.png"
image mei uni smile = "images/characters/mei/uniform/mei smile.png"

image mei uni frown = "images/characters/mei/uniform/mei frown.png"
image mei uni frown2 = "images/characters/mei/uniform/mei frown2.png"
image mei uni angry = "images/characters/mei/uniform/mei angry.png"

image mei uni surprised = "images/characters/mei/uniform/mei surprised.png"
image mei uni sad = "images/characters/mei/uniform/mei sad.png"
image mei uni cocky = "images/characters/mei/uniform/mei cocky.png"


image hand animated:
    contains:
        "images/characters/mei/weapon/summon-hand.png"
        block:
            linear 1 ypos 5 alpha 0.85
            linear 1 ypos 0 alpha 1.0
            repeat
    
    
image mei knight = LiveComposite(
    (593, 933),
    (0, 0), "mei weapon",
    (90, 15), "hand animated"
)

##### ANYA
##### Expression List: default, smile, frown, pain, angry, sigh
define any = Character('Anya', image="anya", who_color="#ffffff")
define anya_expression = "default"
define anya_outfit = "default"

##### Tinted Images
image anytint default = im.MatrixColor("images/characters/anya/default/anya default.png", im.matrix.brightness(-0.25))
image anytint smile = im.MatrixColor("images/characters/anya/default/anya smile.png", im.matrix.brightness(-0.25))
image anytint frown = im.MatrixColor("images/characters/anya/default/anya frown.png", im.matrix.brightness(-0.25))
image anytint sigh = im.MatrixColor("images/characters/anya/default/anya sigh.png", im.matrix.brightness(-0.25))
image anytint pain = im.MatrixColor("images/characters/anya/default/anya pain.png", im.matrix.brightness(-0.25))
image anytint smirk = im.MatrixColor("images/characters/anya/default/anya smirk.png", im.matrix.brightness(-0.25))
image anytint shout = im.MatrixColor("images/characters/anya/default/anya shout.png", im.matrix.brightness(-0.25))

image anytint2 default = im.MatrixColor("images/characters/anya/uniform/anya default.png", im.matrix.brightness(-0.25))
image anytint2 smile = im.MatrixColor("images/characters/anya/uniform/anya smile.png", im.matrix.brightness(-0.25))
image anytint2 frown = im.MatrixColor("images/characters/anya/uniform/anya frown.png", im.matrix.brightness(-0.25))
image anytint2 sigh = im.MatrixColor("images/characters/anya/uniform/anya sigh.png", im.matrix.brightness(-0.25))
image anytint2 pain = im.MatrixColor("images/characters/anya/uniform/anya pain.png", im.matrix.brightness(-0.25))
image anytint2 smirk = im.MatrixColor("images/characters/anya/uniform/anya smirk.png", im.matrix.brightness(-0.25))
image anytint2 shout = im.MatrixColor("images/characters/anya/uniform/anya shout.png", im.matrix.brightness(-0.25))

image anytint3 default = im.MatrixColor("images/characters/anya/weapon/anya default.png", im.matrix.brightness(-0.25))
image anytint3 smile = im.MatrixColor("images/characters/anya/weapon/anya smile.png", im.matrix.brightness(-0.25))
image anytint3 frown = im.MatrixColor("images/characters/anya/weapon/anya frown.png", im.matrix.brightness(-0.25))
image anytint3 sigh = im.MatrixColor("images/characters/anya/weapon/anya sigh.png", im.matrix.brightness(-0.25))
image anytint3 pain = im.MatrixColor("images/characters/anya/weapon/anya pain.png", im.matrix.brightness(-0.25))
image anytint3 smirk = im.MatrixColor("images/characters/anya/weapon/anya smirk.png", im.matrix.brightness(-0.25))
image anytint3 shout = im.MatrixColor("images/characters/anya/weapon/anya shout.png", im.matrix.brightness(-0.25))



##### Default Outfit
image anya nor default = "images/characters/anya/default/anya default.png"
image anya nor smile = "images/characters/anya/default/anya smile.png"
image anya nor frown = "images/characters/anya/default/anya frown.png"

image anya nor sigh = "images/characters/anya/default/anya sigh.png"
image anya nor pain = "images/characters/anya/default/anya pain.png"
image anya nor smirk = "images/characters/anya/default/anya smirk.png"

image anya nor shout = "images/characters/anya/default/anya shout.png"

# Sepia
image anya nor default s = im.Sepia("images/characters/anya/default/anya default.png")
image anya nor smile s = im.Sepia("images/characters/anya/default/anya smile.png")

##### Armed Outfit
image anya arm default = "images/characters/anya/weapon/anya default.png"
image anya arm smile = "images/characters/anya/weapon/anya smile.png"
image anya arm frown = "images/characters/anya/weapon/anya frown.png"

image anya arm sigh = "images/characters/anya/weapon/anya sigh.png"
image anya arm pain = "images/characters/anya/weapon/anya pain.png"
image anya arm smirk = "images/characters/anya/weapon/anya smirk.png"

image anya arm shout = "images/characters/anya/weapon/anya shout.png"

# Sepia
image anya arm shout s = im.Sepia("images/characters/anya/weapon/anya shout.png")
image anya arm frown s = im.Sepia("images/characters/anya/weapon/anya frown.png")
image anya arm pain s = im.Sepia("images/characters/anya/weapon/anya pain.png")


##### Uniform Outfit
image anya uni default = "images/characters/anya/uniform/anya default.png"
image anya uni smile = "images/characters/anya/uniform/anya smile.png"
image anya uni frown = "images/characters/anya/uniform/anya frown.png"

image anya uni sigh = "images/characters/anya/uniform/anya sigh.png"
image anya uni pain = "images/characters/anya/uniform/anya pain.png"
image anya uni smirk = "images/characters/anya/uniform/anya smirk.png"

image anya uni shout = "images/characters/anya/uniform/anya shout.png"

##### RHODO
##### Expression List: default, smile, frown, surprised, confused
define rho = Character('Rhodo', image="rhodo", who_color="#ffffff")
define rhodo_expression = "default"
define rhodo_outfit = "default"

##### Tinted Images
image rhotint default = im.MatrixColor("images/characters/rhodo/default/rhodo default.png", im.matrix.brightness(-0.25))
image rhotint default2 = im.MatrixColor("images/characters/rhodo/default/rhodo default2.png", im.matrix.brightness(-0.25))
image rhotint smile = im.MatrixColor("images/characters/rhodo/default/rhodo smile.png", im.matrix.brightness(-0.25))
image rhotint frown = im.MatrixColor("images/characters/rhodo/default/rhodo frown.png", im.matrix.brightness(-0.25))
image rhotint surprised = im.MatrixColor("images/characters/rhodo/default/rhodo surprised.png", im.matrix.brightness(-0.25))
image rhotint confused = im.MatrixColor("images/characters/rhodo/default/rhodo confused.png", im.matrix.brightness(-0.25))
image rhotint shout = im.MatrixColor("images/characters/rhodo/default/rhodo shout.png", im.matrix.brightness(-0.25))

image rhotint2 default = im.MatrixColor("images/characters/rhodo/uniform/rhodo default.png", im.matrix.brightness(-0.25))
image rhotint2 default2 = im.MatrixColor("images/characters/rhodo/uniform/rhodo default2.png", im.matrix.brightness(-0.25))
image rhotint2 smile = im.MatrixColor("images/characters/rhodo/uniform/rhodo smile.png", im.matrix.brightness(-0.25))
image rhotint2 frown = im.MatrixColor("images/characters/rhodo/uniform/rhodo frown.png", im.matrix.brightness(-0.25))
image rhotint2 surprised = im.MatrixColor("images/characters/rhodo/uniform/rhodo surprised.png", im.matrix.brightness(-0.25))
image rhotint2 confused = im.MatrixColor("images/characters/rhodo/uniform/rhodo confused.png", im.matrix.brightness(-0.25))
image rhotint2 shout = im.MatrixColor("images/characters/rhodo/uniform/rhodo shout.png", im.matrix.brightness(-0.25))

image rhotint3 default = im.MatrixColor("images/characters/rhodo/weapon/rhodo default.png", im.matrix.brightness(-0.25))
image rhotint3 default2 = im.MatrixColor("images/characters/rhodo/weapon/rhodo default2.png", im.matrix.brightness(-0.25))
image rhotint3 smile = im.MatrixColor("images/characters/rhodo/weapon/rhodo smile.png", im.matrix.brightness(-0.25))
image rhotint3 frown = im.MatrixColor("images/characters/rhodo/weapon/rhodo frown.png", im.matrix.brightness(-0.25))
image rhotint3 surprised = im.MatrixColor("images/characters/rhodo/weapon/rhodo surprised.png", im.matrix.brightness(-0.25))
image rhotint3 confused = im.MatrixColor("images/characters/rhodo/weapon/rhodo confused.png", im.matrix.brightness(-0.25))
image rhotint3 shout = im.MatrixColor("images/characters/rhodo/weapon/rhodo shout.png", im.matrix.brightness(-0.25))

### Tinting test again
image rhodo uni2 default = ConditionSwitch(
    "_last_say_who == 'rho'", "images/characters/rhodo/uniform/rhodo default.png",
    "not _last_say_who == 'rho'", "rhotint2 default"
)

image rhodo uni2 smile = ConditionSwitch(
    "_last_say_who == 'rho'", "images/characters/rhodo/uniform/rhodo smile.png",
    "not _last_say_who == 'rho'", "rhotint2 smile"
)

##### Default Outfit
image rhodo nor default = "images/characters/rhodo/default/rhodo default.png"
image rhodo nor default2 = "images/characters/rhodo/default/rhodo default2.png"
image rhodo nor smile = "images/characters/rhodo/default/rhodo smile.png"

image rhodo nor frown = "images/characters/rhodo/default/rhodo frown.png"
image rhodo nor surprised = "images/characters/rhodo/default/rhodo surprised.png"
image rhodo nor confused = "images/characters/rhodo/default/rhodo confused.png"

image rhodo nor shout = "images/characters/rhodo/default/rhodo shout.png"

# Sepia
image rhodo nor default s = im.Sepia("images/characters/rhodo/default/rhodo default.png")
image rhodo nor default2 s = im.Sepia("images/characters/rhodo/default/rhodo default2.png")
image rhodo nor frown s = im.Sepia("images/characters/rhodo/default/rhodo default.png")
image rhodo nor shout s = im.Sepia("images/characters/rhodo/default/rhodo shout.png")

image rhodo nor confused s = im.Sepia("images/characters/rhodo/default/rhodo confused.png")


##### Armed Outfit
image rhodo arm default = "images/characters/rhodo/weapon/rhodo default.png"
image rhodo arm default2 = "images/characters/rhodo/weapon/rhodo default2.png"
image rhodo arm smile = "images/characters/rhodo/weapon/rhodo smile.png"

image rhodo arm frown = "images/characters/rhodo/weapon/rhodo frown.png"
image rhodo arm surprised = "images/characters/rhodo/weapon/rhodo surprised.png"
image rhodo arm confused = "images/characters/rhodo/weapon/rhodo confused.png"

image rhodo arm shout = "images/characters/rhodo/weapon/rhodo shout.png"

# Sepia
image rhodo arm frown s = im.Sepia("images/characters/rhodo/weapon/rhodo frown.png")
image rhodo arm surprised s = im.Sepia("images/characters/rhodo/weapon/rhodo surprised.png")

##### Uniform Outfit
image rhodo uni default = "images/characters/rhodo/uniform/rhodo default.png"
image rhodo uni default2 = "images/characters/rhodo/uniform/rhodo default2.png"
image rhodo uni smile = "images/characters/rhodo/uniform/rhodo smile.png"

image rhodo uni frown = "images/characters/rhodo/uniform/rhodo frown.png"
image rhodo uni surprised = "images/characters/rhodo/uniform/rhodo surprised.png"
image rhodo uni confused = "images/characters/rhodo/uniform/rhodo confused.png"

image rhodo uni shout = "images/characters/rhodo/uniform/rhodo shout.png"


##### ULM
##### Expression List: default, smile, thinking, surprised, reluctant
define ulm = Character('Ulm', image="ulm", who_color="#ffffff")
define ulm_expression = "default"
define ulm_outfit = "default"

##### Tinted Images
image ulmtint default = im.MatrixColor("images/characters/ulm/default/ulm default.png", im.matrix.brightness(-0.25))
image ulmtint smile = im.MatrixColor("images/characters/ulm/default/ulm smile.png", im.matrix.brightness(-0.25))
image ulmtint thinking = im.MatrixColor("images/characters/ulm/default/ulm thinking.png", im.matrix.brightness(-0.25))
image ulmtint surprised = im.MatrixColor("images/characters/ulm/default/ulm surprised.png", im.matrix.brightness(-0.25))
image ulmtint reluctant = im.MatrixColor("images/characters/ulm/default/ulm reluctant.png", im.matrix.brightness(-0.25))
image ulmtint shout = im.MatrixColor("images/characters/ulm/default/ulm shout.png", im.matrix.brightness(-0.25))

image ulmtint2 default = im.MatrixColor("images/characters/ulm/uniform/ulm default.png", im.matrix.brightness(-0.25))
image ulmtint2 smile = im.MatrixColor("images/characters/ulm/uniform/ulm smile.png", im.matrix.brightness(-0.25))
image ulmtint2 thinking = im.MatrixColor("images/characters/ulm/uniform/ulm thinking.png", im.matrix.brightness(-0.25))
image ulmtint2 surprised = im.MatrixColor("images/characters/ulm/uniform/ulm surprised.png", im.matrix.brightness(-0.25))
image ulmtint2 reluctant = im.MatrixColor("images/characters/ulm/uniform/ulm reluctant.png", im.matrix.brightness(-0.25))
image ulmtint2 shout = im.MatrixColor("images/characters/ulm/uniform/ulm shout.png", im.matrix.brightness(-0.25))

image ulmtint3 default = im.MatrixColor("images/characters/ulm/weapon/ulm default.png", im.matrix.brightness(-0.25))
image ulmtint3 smile = im.MatrixColor("images/characters/ulm/weapon/ulm smile.png", im.matrix.brightness(-0.25))
image ulmtint3 thinking = im.MatrixColor("images/characters/ulm/weapon/ulm thinking.png", im.matrix.brightness(-0.25))
image ulmtint3 surprised = im.MatrixColor("images/characters/ulm/weapon/ulm surprised.png", im.matrix.brightness(-0.25))
image ulmtint3 reluctant = im.MatrixColor("images/characters/ulm/weapon/ulm reluctant.png", im.matrix.brightness(-0.25))
image ulmtint3 shout = im.MatrixColor("images/characters/ulm/weapon/ulm shout.png", im.matrix.brightness(-0.25))

##### Default Outfit
image ulm nor default = "images/characters/ulm/default/ulm default.png"
image ulm nor smile = "images/characters/ulm/default/ulm smile.png"
image ulm nor thinking = "images/characters/ulm/default/ulm thinking.png"

image ulm nor surprised = "images/characters/ulm/default/ulm surprised.png"
image ulm nor reluctant = "images/characters/ulm/default/ulm reluctant.png"
image ulm nor shout = "images/characters/ulm/default/ulm shout.png"

##### Armed Outfit
image ulm arm default = "images/characters/ulm/weapon/ulm default.png"
image ulm arm smile = "images/characters/ulm/weapon/ulm smile.png"
image ulm arm thinking = "images/characters/ulm/weapon/ulm thinking.png"

image ulm arm surprised = "images/characters/ulm/weapon/ulm surprised.png"
image ulm arm reluctant = "images/characters/ulm/weapon/ulm reluctant.png"
image ulm arm shout = "images/characters/ulm/weapon/ulm shout.png"

##### Uniform Outfit
image ulm uni default = "images/characters/ulm/uniform/ulm default.png"
image ulm uni smile = "images/characters/ulm/uniform/ulm smile.png"
image ulm uni thinking = "images/characters/ulm/uniform/ulm thinking.png"

image ulm uni surprised = "images/characters/ulm/uniform/ulm surprised.png"
image ulm uni reluctant = "images/characters/ulm/uniform/ulm reluctant.png"
image ulm uni shout = "images/characters/ulm/uniform/ulm shout.png"


##### EMERALD
define emr = Character('Emerald', image="emerald", who_color="#ffffff")
define emerald_expression = "default"

##### Tinted Images
image emrtint default = im.MatrixColor("images/characters/emerald/emerald default.png", im.matrix.brightness(-0.25))
image emrtint smile = im.MatrixColor("images/characters/emerald/emerald smile.png", im.matrix.brightness(-0.25))
image emrtint shadow = im.MatrixColor("images/characters/emerald/emerald shadow.png", im.matrix.brightness(-0.25))

image emerald default = "images/characters/emerald/emerald default.png"
image emerald smile = "images/characters/emerald/emerald smile.png"
image emerald shadow = "images/characters/emerald/emerald shadow.png"

##### RUBY
define rby = Character('Ruby', color="#ffffff")

##### WEISS
define wei = Character('Weiss', color="#ffffff")

##### PORT
define por = Character('Professor Port', color="#ffffff")

##### GLYNDA
define gly = Character('Professor Goodwitch', image="glynda", who_color="ffffff")

##### Tinted Images
image glytint default = im.MatrixColor("images/characters/glynda/glynda default.png", im.matrix.brightness(-0.25))
image glytint thinking = im.MatrixColor("images/characters/glynda/glynda smile.png", im.matrix.brightness(-0.25))

image glynda default = "images/characters/glynda/glynda default.png"
image glynda thinking = "images/characters/glynda/glynda thinking.png"

##### JILL
define jil = Character('Jillia', color="#ffffff")

##### VELVET
define vel = Character('Professor Scarlatina', image="velvet", who_color="ffffff")

image velvet default = "images/characters/velvet/velvet default.png"
image velvet thinking = "images/characters/velvet/velvet thinking.png"

##### UNKNOWN
define unk = Character('???', color="#ffffff")

##### EXPLANATION
define exp = Character('Explanation', color="#ffffff")

##### TEXT MESSAGING
define m = Character(None, kind=nvl, window_left_margin = 0)
define r = Character(None, kind=nvl, window_right_margin = 640)
# Kitty (^･ｪ･^) Li
define l = Character(None, kind=nvl, window_right_margin = 640)
define u = Character(None, kind=nvl, window_right_margin = 640)
define b = Character(None, kind=nvl, window_right_margin = 640)

define rb = Character(None, kind=nvl, window_right_margin=640)

##### MISCELLANEOUS
define ann = Character('Announcement', color="#ffffff")

define st1 = Character('Student #1', who_color="#ffffff")
image st1 = "images/characters/others/mob a.png"

define st2 = Character('Student #2', who_color="#ffffff")
image st2 = "images/characters/others/mob b.png"

define st3 = Character('Student #3', who_color="#ffffff")
image st3 = "images/characters/others/mob c.png"

define st4 = Character('Student #4', who_color="#ffffff")

define staff = Character('Administration Staff', who_color="#ffffff")
define star = Character('Starrick', who_color="#ffffff")

define gn1 = Character('Goon #1', who_color="#ffffff")
image gn1 = "images/characters/others/leader.png"

define gn2 = Character('Goon #2', who_color="#ffffff")
image gn2 = "images/characters/others/goon.png"

define gn3 = Character('Goon #3', who_color="#ffffff")
image gn3 = "images/characters/others/goon wf.png"

define gn4 = Character('Goon #4', who_color="#ffffff")

define team = Character('Team AURM', who_color="#ffffff")

### ###

### OTHER IMAGES ###

### BGM & SFX ###
##### BGM
define audio.forest = "sounds/bgm/Random-Processes_Looping.mp3"
define audio.strange = "sounds/bgm/Strange-Nature_Looping.mp3"
define audio.vidgame = "sounds/bgm/8-bit-Monsters.mp3"

define audio.contemplation = "sounds/bgm/Drinking-Alone_Looping.mp3"
define audio.confusion = "sounds/bgm/On-Thngs-to-Come_Looping.mp3"
define audio.serious = "sounds/bgm/World-of-Automatons_Looping.mp3"

# Characters' Theme
define audio.cocky = "sounds/bgm/Action-Rhythm-1.mp3"
define audio.reckless = "sounds/bgm/Quirky-Rhythm-6.mp3"
define audio.logic = "sounds/bgm/Decisions_Looping.mp3"
#

define audio.brawl = "sounds/bgm/Action-Rhythm-5.mp3"
define audio.lostisland = "sounds/bgm/Broken-Ocean_Looping.mp3"
define audio.intense = "sounds/bgm/And-the-Machines-Came-at-Midnight.mp3"

define audio.casual = "sounds/bgm/Puzzle-Dreams.mp3"
define audio.discussion = "sounds/bgm/Quirky-Puzzle-Game-Menu.mp3"

define audio.rosecombat = "sounds/bgm/Red Like Roses combat ver.ogg"
define audio.roses = "sounds/bgm/roses.ogg"
define audio.mirror = "sounds/bgm/Mirror Mirror intense ver.ogg"

##### SFX
define audio.hardhit = "sounds/sfx/hardhit.ogg"
define audio.hit = "sounds/sfx/hit.ogg"
define audio.clash = "sounds/sfx/metalhit.ogg"
define audio.door = "sounds/sfx/doorswitch.ogg"

define audio.glyph = "sounds/sfx/glyphcast.ogg"
define audio.ice = "sounds/sfx/iceformed.ogg"
define audio.ursa = "sounds/sfx/bear-roar.ogg"
define audio.foot = "sounds/sfx/footstep.ogg"

define audio.lightning = "sounds/sfx/lightning.ogg"
define audio.thunderroll = "sounds/sfx/lightningroll.ogg"
define audio.rain = "sounds/sfx/rain.ogg"
define audio.switch = "sounds/sfx/switch.ogg"

define audio.noise = "sounds/sfx/noisecan.ogg"
define audio.camera = "sounds/sfx/camera.ogg"
define audio.message = "sounds/sfx/message.ogg"

define audio.bang = "sounds/sfx/loudbang.ogg"
define audio.sbang = "sounds/sfx/muffledbang.ogg"
define audio.shot = "sounds/sfx/rifleshot.ogg"
define audio.multishots = "sounds/sfx/multishot.ogg"

define audio.ringtone = "sounds/sfx/Red Like Roses pt 2 8 bit softer.ogg"

define audio.outnight = "sounds/sfx/nightime.mp3"

##### AUDIO VOLUME SET-UP
#init:
#    python:
#        if renpy.variant("pc"):
#            if not persistent.set_volumes:
#                persistent.set_volumes = True
#                _preferences.volumes['music'] *= .5
#                _preferences.volumes['sfx'] *= .5

##### CHAPTER TITLE SCREEN CODE (STABLE)
screen chapter_screen(title, sub=None):
    zorder 1
    
    add "bg chapter"
    
    text title:
        xalign 0.5
        ypos 0.5
        yalign 1.0
        
        font "fonts/rwby_font.ttf"
        size 35
        color "#000000"
        text_align 0.5
        layout "subtitle"
        
    if sub:
    
        text sub:
            xalign 0.5
            ypos 0.5
            yalign 0.0
            
            font "fonts/rwby_font.ttf"
            size 60
            color "#000000"
            text_align 0.5
            layout "subtitle"
        
label chapter(message, sub=None):
    
    #scene black with fade
    show screen chapter_screen(message, sub)
    with dissolve
    pause 2
    hide screen chapter_screen
    with dissolve
    
    return
    
##### BAD END TITLE SCREEN CODE (TBD)

##### NOT BAD END TITLE SCREEN CODE (TBD)



##### INVENTORY CODE
init python:
    class Item:
        def __init__(self, name, comment):
            self.name = name
            self.comment = comment

# The game starts here.
label start:
    ##### "Social Link" Meter
    $ anya_pt = 0
    $ rhodo_pt = 0
    $ ulm_pt = 0
    
    ##### Boolean Flag
    $ bribe_jill = False
    $ store_weapon = False
    $ admin_office = False
    $ meet_velvet = False
    $ jills_tip = False
    $ weiss_call = False
    
    ##### Item Declaration
    $ inventory = []
    python:
        scroll = Item ("Scroll", "A device for various purposes such as communication, monitoring one's Aura, and launching our personal weapon locker to designated coordinate.")
        
        alpenaster = Item ("Alpenaster", "My weapon of choice. Alpenaster is a dagger made of a certain alloy and equipped with Dust chamber. Simply by changing the Dust cartridge in its chamber, I could deploy various kind of Dust attack with it.")
        
        cookies = Item ("Chocolate Cookies", "My favourite snack. I bring a small pack around for 'emergencies'. Could also work as a 'negotiation offer'.")
        
        codoil = Item ("Cod Oil Pills", "Ulm's item. He regularly takes one or two after a tiresome day, like exam days and upon coming back from mission.")
        
        goldenapple = Item ("The Golden Apple", "Believed to be Saul's possession. It looks the same with cod oil pills, but with a slight difference coloration.")
        
        proteinbar = Item ("Protein Bar", "An edible bar which I believe otherwise because of its thickness and bland taste. I detest this ... thing so much.")
        
        canpeach = Item ("Canned Peach", "Sweet and sour flavored peach packed in a can. It's a more favorable choice over protein bar, but I know sometimes we don't have much choice ...")
        
        flashdrive = Item ("Flash Drive", "Obtained from Professor Scarlatina. Looks like an ordinary flash drive, but I know it isn't.")
        
        photo = Item ("Photo", "A picture taken by Anya and sent to my mom against my will. Seems like Mom had a really good laugh seeing it ...")
        
        mirror = Item ("Small Mirror", "Given by Ulm as an anticipation of hallucination-based Semblance. He said that the Semblance user might manipulate what we see, but they certainly couldn't manipulate the reflection of the real objects.")
        
        recording = Item ("Recording0023", "A recording which holding a proof that team WHYT know something about Tukson's Book Trade as one of the Golden Apple distributor.")
        
    #####
    
    ## This menu is for quick access during testing
    ## Clear them upon release
    scene black with fade
    stop music fadeout 5.0
    
    #"Test item positioning"
    #show temp_item at truecenter with dissolve
    #"Test abc"
    
    #m "This is a NVL testing"
    #r "Still NVL testing"
    #r "More testing coming soon"
    
    #show hand animated with dissolve
    #$ mei_expression = "cocky"
    #show mei knight at mcenter with dissolve
    #mei "trial"
    
    $ inventory.append(scroll)
    $ inventory.append(alpenaster)
    $ inventory.append(cookies)

    jump part1
    
    ### SHORTCUTS FOR DEBUGGING PURPOSE
    #menu:
        #"The Sandbox":
        #    jump sandbox
        #"Normal play":
        #    jump part1
        #"Updated (part 8b-a)":
        #    jump part8BA
        #"Updated (part 8b-r)":
        #    jump part8BR
        #"Updated (part 8b-u)":
        #    jump part8BU
        #"Latest (part 7a)":
        #    jump part7A
        
        