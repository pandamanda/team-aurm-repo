﻿# PART 8 : STRANDED #

label part8BR:
    
    "Rhodo and I got into another argument when he told me to stop walking around so restlessly."
    "I'm ... {i}anxious{/i}. And I have plenty of reasons to feel that way."
    "However, I would admit that Rhodo is right. Walking around like an animal in a cage would not improve our condition."
    "The boat we are in, finally slows down."
    "It moves closer toward an island in the middle of nowhere."
    "I have a bad feeling about this island and Rhodo does too."
    "He signals me to go to the back of the boat, waiting near the bridge. Assuming, there would be White Fang members or others that climb and search the boat, we're going to ambush them first."
    
    "Fortunately, nobody comes."
    "Rhodo takes the initiative to climb down first and call me if all is clear."
    "... He acts carefully, to my surprise."
    "Another unexpected trait I rarely see from Rhodo, I guess. Now that I've seen this, I don't think I could associate him with the word 'reckless' anymore."
    
    rho "Schnee? All clear. You can go down."
    "I jump out of the ship and approach Rhodo, who is fiddling with his Scroll."
    scene bg island with fade
    show rhodo nor frown at midright with dissolve
    rho "Still no signal."
    show mei nor frown2 at midleft with dissolve
    mei "Oh, mine have one. Very weak."
    mei "Should I try to call Anya?"
    rho nor default2 "Yeah, do that. I'll watch your back."
    hide rhodo with dissolve
    
    show mei nor default2 with cdissolve
    "My call is answered almost immediately."
    any "Mei! Are you alright?"
    "I could hear Ulm's voice in the background, asking whether I'm safe or not."
    mei nor frown2 "I am. Even though I couldn't tell where am I now except that I'm in an island ...."
    mei "And we barely have any signal for our Scroll now. I doubt you could pick our location at the moment."
    any "Ah, 'we'?"
    any "Are you with Rhodo now?"
    mei nor default2 "Yes."
    
    any "That's a relief."
    any "Wait, hold on a minute."
    show mei nor frown2 with cdissolve
    "I hear Anya speaks with Ulm about how to pinpoint our location. Ulm speaks really low, like always, I couldn't hear what he said."
    any "Back."
    any "For now, try to find someplace you can get better signal and send your location."
    any "Keep us updated with your situation whenever you have the chance. Gather some hints about the island might be useful too."
    
    mei nor default2 "Understood."
    any "Good."
    any "I'll be waiting for the good news."
    show mei nor sad with cdissolve
    "As the call ends, I wonder if we would have 'good news' during our stay in this mysterious island."
    show rhodo nor frown at midright with dissolve
    rho "What did Anya say  about our situation?"
    show mei nor frown2 with cdissolve
    mei "We have to look for any way to tell them our position. She suggested we search for an area with better signal, so they could pinpoint our location."
    
    rho nor default "Oh, got it."
    mei "Do you find anything? Sign? Or ... someone?"
    rho nor confused "I don't like the idea of we're not alone in this island."
    mei nor frown "The idea of we are {i}alone{/i} in this island isn't any better."
    rho nor surprised "...."
    rho "Yeah. You're right."
    hide mei with dissolve
    hide rhodo with dissolve
    
    "We walk further, following the shoreline."
    "Along the way, we find burn marks--possibly caused by an explosion--on the cliff wall, piece of metal sheet, and empty metal barrels."
    "It's not just someone used to live here ...."
    "We find path leading into the deeper part of the island."
    "There's a linked fence, surrounding what looks like an abandoned factory."
    
    show rhodo nor surprised at midright with dissolve
    rho "Whoa ...."
    rho "Some serious finding we have here."
    show mei nor surprised at midleft with dissolve
    mei "Y-yes."
    mei nor frown2 "What is this place?"
    rho "Looks like a factory or mine for me, but who owned this place? Why built it here?"
    rho "And I doubt it's one of your family's. I don't see snowflake symbol anywhere."
    "I nod in agreement."
    hide mei with dissolve
    hide rhodo with dissolve
    
    "It's a standard in SDC to label their properties and cargo with the family symbol as an identification."
    "I didn't see any of them here. Instead, I find several unknown symbol that resembles a green cube."
    #growl SFX
    "One thing we know for sure is the Grimms reside in this abandoned factory."
    "Whatever security system they once deployed in this place, it's no longer functioning. The Grimms freely roams around the place."
    "Fortunately, most of them are the easy ones. We have no problem taking them down."
    
    "Continuing our search, we find what looks like a communication tower."
    "It's rusty, old, and doesn't look working."
    rho "Oh? The signal is good here."
    rho "Should I contact Anya or Ulm?"
    mei "Yes, please."
    "While Rhodo try to contact our teammates, I keep watch for our surrounding."
    "Three Beowolves creep closer to our position."
    "I wait a little bit and surprise them by summoning the knight's sword and launch it at one of them."
    "The Beowolf yelps, sword piercing its skull."
    
    "Rhodo shoots the other two Beowolves, holding his rifle in one hand."
    show mei arm cocky at midleft with dissolve
    mei "Showing off your skill, Mr. Manthus?"
    show rhodo arm smile at midright with dissolve
    rho "What do you think of it?"
    mei "Technically speaking, if your target was bigger and stronger type like an Ursa, a single bullet wouldn't take them down."
    
    mei arm frown "And if your target was smaller or its position was further from you, your chance of missing would be higher, therefore aiming with one hand is not recommended."
    hide rhodo with dissolve
    hide mei with dissolve
    
    show rhodo nor default at midright with dissolve
    show mei nor default at midleft with dissolve
    rho "I can't put better words than you, Schnee. Good explanation."
    mei "Thank you for the compliment."
    mei "Anyway, did you manage to send our location?"
    
    rho "Yup."
    rho nor surprised "What should we do next? Keep trying to find out what's this place about?"
    show mei nor frown2
    "I couldn't think of anything better, so I nod in agreement."
    hide mei with dissolve
    hide rhodo with dissolve
    scene bg island with fade
    "We proceed walking. It's quite long before we find an entrance to the facility."
    "We slow down here, feeling uncanny with the abandoned structure."
    "There are a network of pipes and more barrels. Metal sheets that are spread on the ground, creating a path."
    
    show rhodo nor surprised at midright with dissolve
    rho "I've seen one or two illegal business in Vacuo, but not this big ...."
    rho "What did they produce or mine in this island? Dust?"
    show mei nor frown2 at midleft with dissolve
    mei "No, not Dust."
    "I knock at one of the pipe nearby."
    mei "From the looks of these pipes, they are for liquid or gas. It's very rare to find Dust in liquid form, let alone gas form."
    mei nor default2 "If I might suggest ... it was probably oil mining or refinery, even though I don't know exactly what an oil mine or refinery looks like."
    
    "And that cube-like symbol I see on the barrels, metal boxes, and pipes?"
    show mei nor frown2 with cdissolve
    "I don't think any illegal business has the need to make a brand for themselves."
    "Maybe those items are stolen? From where?"
    "Why did they leave and abandon this facility? Were they attacked? Or did they have other unfortunate incidents and were forced to leave?"
    rho nor default2 "I have a question."
    mei "?"
    rho "I kinda believe leading you into the boat is part of our enemy's plan. They prepared locks, bomb, and already put this island in the boat's auto-pilot."
    rho nor surprised "What was that for? Why this island?"
    
    "It's a good question."
    mei "The only possibility I could think is they want to divide us."
    mei nor sad "...."
    mei "I'm sorry .... It's my fault."
    mei "I thought I did the right thing, but, instead, I've made things turned worse for us."
    rho nor default2 "Well ... it's already happened. I mean, we can't undo it, so might as well just fix it."
    rho nor confused "And why this island? If we can't find this island in the map, then how come the enemy know about it?"
    hide rhodo with dissolve
    hide mei with dissolve
    
    "Even though Rhodo's questions are all make sense, I don't have answer for any of them at the moment."
    "An incoming call startles me."
    "It's Ulm this time."
    ulm "Miss Schnee! Rhodo!"
    ulm "Are you alright? Is everything okay?"
    show mei nor default at midleft with dissolve
    mei "I'm okay. Thank you for worrying."
    show rhodo nor default at midright with dissolve
    rho "Nothing happened so far. How's in your side?"
    
    ulm "Oh. Yes."
    ulm "Anya is away at the moment. She said she has something to do with the leader of the thugs."
    ulm "Also, she wants me to tell you not to worry and that she isn't going to do anything reckless."
    mei "Glad to hear that."
    rho nor frown "What about the guy? The leader, I mean."
    rho "Did the police arrest him?"
    
    ulm "Well ... no. Anya struck a deal with him. In exchange of helping him fleeing the port, he would give us more information about who sent him and else."
    show mei nor frown
    show rhodo nor surprised
    with cdissolve
    "I exchange looks with Rhodo. Without saying anything, I know that both Rhodo and I disagree with Anya's decision not to give the guy to the police."
    "On the other hand, getting more information from him is also something important for our progress."
    ulm "Uhm ... if I may add, it's nothing to worry about."
    show mei nor default2
    show rhodo nor default2
    with cdissolve
    ulm "Anya got herself a help from a trusty huntress she knew."
    
    "I immediately remember the huntress she often consults with and mentions during our conversation."
    "Is it the same person?"
    rho nor default "By the way, Ulm, what about the coordinates I sent you earlier?"
    rho "No problem with it, right?"
    ulm "Ah, yes! The coordinates!"
    ulm "We've contacted Professor Goodwitch, told her what happened, and gave her your location."
    ulm "She will send an airship to pick you up, but unfortunately ... a storm is approaching Vale."
    
    show rhodo nor surprised with cdissolve
    mei nor frown2 "So they wouldn't be able to reach this place until ... when?"
    ulm "If I'm not mistaken, approximately, you will have to wait until tomorrow morning."
    rho "Damn ... that means we have to camp in this creepy island?"
    show rhodo nor frown with dissolve
    "Rhodo suddenly pauses. His face turns wary and serious."
    "There's a collective growl from behind."
    
    mei nor angry "Ulm, I'm sorry for cutting this conversation short, but we have Grimms coming."
    mei "Will contact you again later."
    hide mei with dissolve
    hide rhodo with dissolve
    
    "With weapon readied in our hands, we both turn around."
    "The scene in front of us is terrifying, I must admit."
    "There's a large pack of Beowolves. Some of them are normal, but the others have pale green crystals protruding on their back instead of bony spikes."
    "Doing a really quick calculation, there are at least thirty of them."
    scene cg cerberus with fade
    "And looming at the center of this dreadful pack is a gigantic three-headed creature."
    "The giant creature is less humanoid and more canine-like than Beowolves."
    "More importantly, it's {i}abnormally huge{/i}. I could even say this creature is bigger than a Goliath."
    "Pale green crystals are also decorating its spine, further indicating its unusual nature."
    
    "The first wave of Beowolves attack."
    scene bg island with fade
    "Rhodo kneels, shooting his rifle and killing several Beowolves with accurate shots."
    "Standing next to him, I load ice Dust to Alpenaster and send barrage of icicles toward our opponents."
    "We take down the first attacking wave just fine, but I doubt the second attack would be as easy."
    "Especially, with that giant Grimm ...."
    "I could summon the knight in its full form and fight the three-headed Grimm, but during that time, I would need Rhodo's help to protect me from the smaller Grimms."
    "Is running away could be considered as a better options for now?"
    
    show rhodo arm surprised at mcenter with dissolve
    rho "Schnee?"
    rho "I need your take on this situation."
    rho arm frown "I personally don't wanna go head-to-head with that giant dog ...."
    rho arm surprised "And {i}what{/i} are those green Grimms???"
    hide rhodo with dissolve
    "I couldn't answer Rhodo's question immediately. The next wave has came and more ready to avoid our shots."
    scene cg packfight with fade
    "Rhodo shifts his rifle into its lance form and starts taking down the approaching Beowolves."
    "I have my confidence in Rhodo's skill in handling several enemies at one time, but ...."
    "I leap back. Rhodo almost hit me when he is furiously swinging the lance and cutting down three Beowolves."
    "The fourth Beowolf shifts its attention to me. Before it attacks, I stab its neck with Alpenaster."
    
    scene bg island with fade
    "Another group of Beowolves had split up and try to ambush us from the sides."
    "To make things worse, the enormous Grimm slowly moves toward us."
    "I quickly survey our surroundings for a way out."
    "Not too far behind us, I see a large gap on the ground."
    "Branched walkways are built over the gap, each of them lead to various parts of this facility: toward control panels and valves, toward some sort of post, and toward another part of the island."
    
    show mei arm angry at midleft with dissolve
    mei "I think we could retreat to the walkway, luring as many Grimms as possible, and then destroy the walkway."
    show rhodo arm frown at midright with dissolve
    rho "Sounds okay."
    "Rhodo equips his shield, shifting into defensive stance."
    rho arm shout "Lead the way! I'll guard your back!"
    mei "Got it!"
    hide mei with qdissolve
    hide rhodo with qdissolve
    "We both managed to escape the ambush from our sides. The narrow walkway forces the Grimm creatures to walk on it one by one, making it easier for Rhodo to take any Grimm comes too close to us."
    
    "The giant Grimm is something else."
    "It leaps above us and lands in front of me, cutting our way."
    rho "Goddamn--!"
    "Rhodo goes full defense against the incoming Grimm creatures from behind, while I face the three-headed Grimm."
    "Protection Glyph appears before me, right at the moment the Grimm leaps on me. The Glyph flickers, fortunately not shattered."
    "I flick my hand, making the Glyph blasting a force toward my opponent."
    "The power isn't enough to toss it away, but sufficient to push it back a little."
    
    "The Grimm loses its step and almost falls to the pit. Not wasting this chance, I cast another Glyph. This time a summon Glyph."
    "Once a giant sword appeared, floating above my head, I swing it toward my opponent."
    "With the Grimm losing its balance, I'm confident my attack would hurt my opponent badly enough to force him retreat."
    mei "!"
    "Apparently each of this Grimm heads have consciousness of its own."
    "While it's struggling not to fall down, the left head see the incoming sword."
    "The Grimm bit the blade of the sword, stopping my attack."
    
    mei "What the--"
    "Agitated by this unexpected result, I channel more Aura to the Glyph, trying to pull the sword."
    "The Grimm pulls back, growling and snarling at me."
    "Failed with my first attempt, I try to push the sword instead. Still to no avail."
    "Something bumps on my back, hard enough to send me tumbling."
    "It's Rhodo."
    "I almost yell at him, but then realizes that he looks so tense."
    "He never shows this expression. Not even during an exam in which he couldn't answer half of the questions. Not even during a fight against a horde of Grimms."
    
    "A proof of how dire our current situation is."
    #clash sfx
    "Rhodo blocks the giant Grimm's claw with his shield."
    "Despite its awkward stepping in this narrow platform, the Grimm is quite persistent."
    "Persistent to get us killed."
    "I lunge at the incoming Beowolf before it claws Rhodo's back. Alpenaster sinks deep in its chest."
    "The momentum brings me closer to the Grimms pack."
    "I dodge the next Beowolf attack, countering it with a stab on its lower jaw."
    
    "A short moment before the Beowolf disappears into black smoke gives me enough time to cast a series of icicle and launch it toward the other Grimms."
    "The remaining Grimms look reluctant to press on."
    "A loud shriek returns my attention to Rhodo and the three-headed Grimm."
    "The walkway is crumbling apart."
    "The support and railings are twisted and bent, before breaking into pieces."
    "Rhodo has left the giant Grimm. The creature ignores Rhodo and try to find another platform to hold itself."
    
    show rhodo arm shout at midleft with dissolve
    rho "Schnee! RUN!"
    show mei arm surprised at midright with dissolve
    mei "But--Grimms--"
    rho "The bridge is going to collapse! GO!"
    hide mei with qdissolve
    hide rhodo with qdissolve
    "Lucky for us, the Grimm creatures which blocked our way had realized the situation and retreated."
    "Before we reach solid ground, the bridge shakes and collapses."
    "I cast a series of Glyph, leading to the edge of the cliff."
    
    show rhodo arm surprised at midleft with dissolve
    rho "W-wait! Don't leave me!"
    show mei arm angry at midright with dissolve
    mei "I'm not leaving you, blockhead! Come on!"
    hide mei with qdissolve
    hide rhodo with qdissolve
    "The giant Grimm lets out a deafening roar."
    "I didn't look behind to see what has happened before my feet touch the ground."
    "When I turn around, I don't see the monster anywhere. It must have fell down into the pit."
    "Even though I'm not sure the height would kill it, I have a little hope that the fall would finish it off. That way, it wouldn't bother us anymore."
    
    rho "Urgh .... Damnit .... It hurts."
    mei "!!!"
    mei "You are wounded!"
    show cg rhodowound with fade
    "Rhodo's right shoulder is bleeding. His shoulder plate is dislocated and cracked. There's a long scratch on his forehead, bleeding too."
    rho "I was careless and got bitten. That goddamn dog ...."
    rho "I guess I'm lucky it left me when I torn apart the walkway and make it fell."
    mei "We need to treat your wound."
    
    rho "Find a shelter first. Storm's coming closer and I don't wanna any Grimm leap on my back when I'm not seeing."
    "Rhodo winces and groans."
    rho "Damnit ...."
    scene bg island with fade
    "The first thing comes up in my mind is security posts, but then I remember that I didn't see any that looks like one around."
    "But, there should be a control panel room or such, right? There's no way a big facility relies solely on controls from the main building."
    "I find a small building that looks like a control post. It's on the other side of the cliff. Quite a walk, especially for Rhodo."
    
    show mei nor frown2 at midleft with dissolve
    mei "Could you walk?"
    show rhodo nor frown at midright with dissolve
    rho "To that building? I can try."
    rho nor confused "If I can't walk, what're you gonna do, though, Schnee? Carrying me?"
    mei "With the knight, of course."
    rho nor frown "Don't waste your Aura on that. These scratches will heal, but it'll take time and I doubt I can fight until then. You gotta stay on guard."
    mei nor frown "You should shut up and reserve your energy for the walk."
    rho nor surprised "Ah. Good idea."
    hide mei with dissolve
    hide rhodo with dissolve
    
    "Rhodo breathes heavily. Despite of that, he could walk until we get into the building."
    "I break open the door as the first thunder strikes on the sky above."
    scene bg outpostnight with fade
    show mei nor frown2 at midright with dissolve
    mei "We don't have any bandage or first aid kit on us ...."
    show rhodo nor frown at midleft with dissolve
    rho "Yeah. I know."
    "Rhodo removes his shoulder plate and the red shawl tied on his waist. He folds the shawl and presses it on the wound."
    mei "Shouldn't we find something to tie it?"
    rho nor default "It's going to stop soon enough."
    
    mei "Are you sure?"
    rho "Yep."
    "I don't share the same confidence with Rhodo, but ... I think he knows better than me about his own condition and what to do."
    rho nor frown "*sigh* Sorry ...."
    mei nor default2 "About ... what?"
    show rhodo nor surprised with cdissolve
    "Rhodo pauses, looking like he realizes that he doesn't need to apologize for anything."
    rho "Uh ... well. I feel like I'm slowing you down by getting wounded like this?"
    show mei nor frown2 with cdissolve
    rho "And, uh ... seeing a lot of blood is never good?"
    
    "... He doesn't need to apologize for any of them."
    "If anyone were to blame, it would be me who got ourselves into this island in the first place."
    
    #NaNoRebel material
    rho nor frown "Odd .... Usually, it's already healed already."
    mei nor frown "That couldn't be. Your wound is quite severe. It usually takes an hour or two after you regained your Aura protection to be fully healed."
    mei "Except if you have some sort of healing acceleration Semblance. That makes things differ."
    
    rho nor default2 "You know my wounds usually heal really quick, right, Schnee?"
    hide rhodo with dissolve
    hide mei with dissolve
    
    "Hmm, it actually is."
    "Now that I recall it, Rhodo caught in an accident during combat training and hurt himself."
    "It wasn't too bad like this one, but after a immediate examination, the medic told Professor Scarlatina that Rhodo wasn't capable to continue the fight."
    "However!"
    "Rhodo was up and walked like nothing serious happened not long after the medic talked to Professor Scarlatina."
    "Of course, the medic didn't believe it and insisted to check on Rhodo's condition again."
    
    "He was fine. Rhodo had totally healed and capable to continue the training if he wanted to."
    "Anya decided to end the training session, despite of Rhodo trying to convince her that he was good to go."
    "So, if Rhodo claims that his Aura healing ability is far more better than normal people, it's true."
    
    show mei nor frown2 at midright with dissolve
    mei "Maybe there are shards lodging in your wounds."
    show rhodo nor surprised at midleft with dissolve
    rho "...."
    rho "N-no way. You're joking, right? Ha-ha."
    mei "The Grimm crushed your armor plate. It's possible if there are a few shards in your wounds."
    mei "Aura healing slows down and even stops if there's something inside your body. It's a mechanism to avoid the foreign matter trapped inside your body."
    mei nor frown "Trying to remove it is out of question, Mr. Manthus. I wouldn't do it."
    mei "It's dark and we don't have the equipment to do so."
    
    rho nor confused "...."
    rho "So ... I need to bandage this?"
    hide rhodo with dissolve
    hide mei with dissolve
    "I don't answer him immediately. I know we think of same thing: what should we use to tie the shawl and keep it secure for at least a night?"
    "We need something long, flexible ...."
    "... Uhm ...."
    
    "I pick an old cable. It's long enough and not rusty. So, probably it's safe?"
    show rhodo nor default at midleft with dissolve
    rho "Oooh, good idea, Schnee!"
    show mei nor frown2 at midright with dissolve
    mei "No, I don't think this is a good idea ...."
    rho nor surprised "What can go wrong with it?"
    "Unfortunately, I don't have enough medical knowledge to answer that question."
    "Searching about it in the net might give me a proper answer, but I have to reserve my Scroll's battery."
    "Also, bad signal around here."
    
    mei "Alright .... I'll try to tie the shawl in its place. Do tell me if it hurts or you feel anything wrong."
    rho nor default2 "Sure."
    hide rhodo with dissolve
    hide mei with dissolve
    "I proceed tying the shawl with cable."
    "Rhodo sits still until I finished my work. I really couldn't be proud of this 'makeshift and questionable bandage'."
    "I hear no complaint from Rhodo."
    
    show rhodo nor default2 at midleft with dissolve
    rho "It sure is pouring down outside."
    show mei nor default2 at midright with dissolve
    mei "Yeah ...."
    rho nor default "At least we have shelter. And plenty of water. I'll go out and get something to store some rainwater then."
    mei nor frown2 "Do remember you are wounded and need to be very careful or you're going to rip it open again ...."
    rho nor smile "I know, I know. I'll be careful."
    mei nor surprised "Wait. Is rainwater drinkable?"
    rho nor default "Better than melting snow, I heard."
    hide rhodo with dissolve
    hide mei with dissolve
    
    "I follow Rhodo outside, where he finds a metal barrel nearby. We're lucky it isn't rusty, both inside and outside."
    "I help Rhodo pushing the barrel so it would collect rainwater."
    rho "Alright! I think that's good for now!"
    "I could only nod."
    "On one side, it's good to see Rhodo is okay and keep being positive."
    "On the other side ... my guilty feeling for dragging him into this situation grows bigger."
    
    show rhodo nor default2 at midleft with dissolve
    rho "Schnee? Hey."
    show mei nor frown2 at midright with dissolve
    mei "?"
    mei "Yes?"
    rho "Don't look so gloomy. It's not like we're gonna be doomed."
    mei nor sad "Well ... we're nowhere safe either ...."
    rho nor default "Yeah, that's true. But we can't always avoid trouble, so when we got into one, better do our best to overcome it, right?"
    mei "I thought you're going to blame me for dragging you into this."
    rho "Can I?"
    
    mei nor frown "No. Keep it for yourself."
    rho nor smile "Ohhh, look! That's the Schnee I know! Glad to have you back!"
    "I frown, but amused by what Rhodo said at the same time."
    mei nor sad "I do apologize for these troubles, Mr. Manthus."
    mei "Seriously."
    rho nor default2 "Father always said if we got too focused on something, we might do stupid, reckless things."
    rho nor default "Glad to know you can do stupid, reckless things."
    
    show rhodo nor smile
    show mei nor frown
    with cdissolve
    "Rhodo puts up a silly grin. I reply with an annoyed frown."
    rho nor frown "Damn ...."
    show mei nor default2 with dissolve
    rho nor confused "... I want pancakes."
    rho "I'm hungry."
    
    mei nor frown2 "Same."
    rho "I'm gonna make pancakes with steak and burger patty once we get back to school."
    mei nor surprised "... That sounds too ... 'meaty'."
    rho nor smile "I'll cook some with bacon for you then."
    mei nor angry "Okay, stop. Stop talking about pancakes!"
    mei nor frown2 "Now I feel hungrier than before!"
    rho nor frown "I'm thinking where can we get something to eat in this island ...."
    rho nor confused "We're not going to die if we skip dinner. And breakfast. But, it'll be better if we have something to eat."
    
    "I suddenly remember something and rummage my pocket."
    show mei nor surprised
    show rhodo nor default2
    with cdissolve
    "It's still there. {i}That protein bar{/i}."
    "I have my usual pack of cookies too."
    rho nor surprised "That's .... Whoa, no way!"
    show mei nor frown with dissolve
    "Rhodo laughs when he sees the half-eaten protein bar."
    rho nor smile "I think--I think I still have that rocky bar. Ah, here it is."
    
    mei nor default "Anya was the only one who finished eating this 'thing'."
    mei "We ended up eating the canned peach and kept the bars just in case we need it later."
    rho "I forgot to take it out of my pouch bag."
    mei nor smile "Same."
    hide mei with dissolve
    hide rhodo with dissolve
    "This unexpected findings cheer us up a little."
    "Our 'supplies' barely fill our stomach, but it's better than nothing. Beggars cannot be choosers."
    "While trying my best to chew the protein bar (it's a mystery how could this 'thing' is still as solid as a rock after all this time), I notice there's a little embroidery on Rhodo's red shawl."
    "I never saw it before."
    
    "It's an embroidery of four letters: J, N, P, R."
    show mei nor default2 at midright with dissolve
    mei "Did you inherit your shawl from someone?"
    show rhodo nor default2 at midleft with dissolve
    rho "Nope. Though, I got plenty of hand-me-downs or charity clothes, that shawl is a little gift from Father. Brand new."
    "It took me a few seconds to recall that Rhodo lives in an orphanage."
    show mei nor sad with cdissolve
    "Why in Remnant I keep forgetting it?"
    show mei nor default2 with cdissolve
    "Before I apologize, Rhodo continues:"
    rho nor surprised "Why asking?"
    
    mei nor frown2 "Oh, I'm just wondering what does the embroidery on it mean."
    "Rhodo takes a glance at his red shawl and then frowns."
    rho nor frown "You might've guessed it's a team name, huh?"
    show mei nor default2 with cdissolve
    rho "It reads 'juniper'. Father's team. {i}Former{/i} team."
    "Rhodo falls silent. He looks sad and confused."
    rho "It's complicated."
    
    mei nor sad "I'm sorry for asking ...."
    rho nor default "I don't mind, really. To be honest, it's one thing that has been bugging me for a while."
    rho "Where should I start ...?"
    mei nor frown2 "You don't need to talk about it if you don't want to."
    rho nor frown "It's the opposite, actually, I feel like I need to talk about it with someone."
    
    rho nor default2 "...."
    rho nor frown "This is going to sound ridiculous."
    mei nor default2 "I'm not going to laugh. I swear."
    rho "I think ... I somehow keep reminding Father of his old team."
    rho "I heard things from the older kids. About me being delivered to the orphanage after Ms. Valkyrie went missing. About her weapon was also returned to the orphanage that night, but not Ms. Valkyrie herself."
    rho "She was declared missing."
    
    rho nor default "Oh, right. Miss Valkyrie is the 'N' in the team. Nora Valkyrie if I'm not mistaken. The 'R' is Father, Fu Rong."
    rho "I know what she looks like from the photograph Father has in his office, though there are only three of the whole team."
    rho "Father and Ms. Valkyrie were engaged, so I heard. They decided to build the orphanage to help kids in Vacuo."
    rho "They were orphaned at young age and just don't want other kids going through the same sufferings."
    mei nor smile "That's a very noble cause."
    rho nor smile "I know, right? Really proud of 'em, though I never get to know Ms. Valkyrie."
    
    rho nor surprised "So, uh, yeah. Back to topic."
    show mei nor default2 with cdissolve
    rho "I have some wild guess about maybe Ms. Valkyrie found me and sent me to the orphanage, but whatever she faced that time, she failed to return."
    show mei nor frown2 with cdissolve
    rho nor frown "Like ... I'm alive in exchange of her."
    mei nor sad "And Mr. Rong keeps being reminded of Ms. Valkyrie whenever he sees you."
    rho "Yeah."
    
    rho "What I don't understand is it's not just Ms. Valkyrie."
    "Rhodo taps his armor."
    rho "Father had this armor made for me. I'm the one who came up with the spear's design."
    mei nor default2 "That explains why there's a blatant mismatch between your armor and your weapon."
    rho nor shout "Oh, I tell you what, I'm glad the mismatch is such an eyesore."
    rho nor frown "I've known for a while that Father had some sort of dispute with his team leader, Father refused to mention his name whenever talking about his team."
    rho nor shout "What I don't understand is, if Father hates him so much, why did he give me a set of armor that somewhat resembles his leader's armor?"
    rho "Why does he keep their photo together?"
    
    show rhodo nor frown
    show mei nor sad
    with cdissolve
    "Rhodo pauses. I open my mouth, but couldn't find anything to say."
    rho "{size=15}I'm not him. Just because we have same hair color, I'm not him.{/size}"
    show mei nor default2 with cdissolve
    rho nor shout "Not Ms. Valkyrie too .... Just because I'm the only one who can use her weapon, Magnhild."
    show rhodo nor frown
    show mei nor frown2
    with cdissolve
    "I'm not an expert in this matter, but from what I heard just now, Mr. Rong tried to 'relive' his lost team in Rhodo."
    "Rhodo is thankful to Mr. Rong for taking care of him, especially because it's plausible that Rhodo was alive thanks to Ms. Valkyrie."
    "But ... it really makes sense and acceptable if he gets more and more uncomfortable with this circumstances."
    
    mei nor sad "Do you ever hear anything about the fourth member of the team?"
    mei "You said there are only three of them in the photograph, right?"
    "Surprisingly, this question flips Rhodo's expression in a flash. From confused and upset into excitement."
    rho nor smile "I do."
    show mei nor default2 with cdissolve
    rho nor default "She was from Mistral, a brave and strong fighter, winning a lot of competition during her time in combat school."
    rho "A really great person. She practically sacrificed herself during The Fall of Beacon, fighting a strong opponent long enough and avoiding Vale from total destruction."
    show mei nor default with cdissolve
    rho nor smile "That was really heroic. Not everyone can do such a thing."
    mei nor smile "It sure is."
    rho nor default "Oh, yeah. She was the 'P' in the team. Her name is--"
    rho nor surprised "!!!"
    show mei nor frown2 with cdissolve
    
    "Rhodo lets out a low groan. He looks like he's in pain."
    mei nor surprised "W-what's wrong?"
    mei "Does your wound act up again?"
    rho nor surprised "No ... that's not it."
    rho "Sudden headache."
    show mei nor frown2 with cdissolve
    rho nor frown "... Gah."
    rho "...."
    rho "Urgh ...."
    
    "Rhodo blinks several times. He looks as if he couldn't focus his sight at the moment."
    "Whatever hit him had quite an impact."
    mei "Rho? Are you okay?"
    rho nor surprised "... Huh?"
    rho "...."
    rho "I'll go out a little bit. Fresh air. Sorry."
    hide rhodo with dissolve
    hide mei with dissolve
    
    
    "I stand up and follow Rhodo, but then he stops me."
    show rhodo nor frown at midleft with dissolve
    rho "I'll be okay, Schnee. Just stay inside."
    show mei nor frown2 at midright with dissolve
    mei "Are you sure?"
    rho nor default "I'll make some noise if I'm collapsed or something."
    mei "Yes, please do that."
    hide rhodo with dissolve
    hide mei with dissolve
    
    "Rhodo nods and leaves."
    "I still haven't learned about the last member's name."
    "Anyway ...."
    "I could somehow understand Rhodo's situation. Even though it is not the exact same, I know how it feels like when people put expectations on you."
    "People expects me to be a fine, respected lady who is suitable to bear the name Schnee. Mr. Rong expects Rhodo to become his lost teammates."
    "No matter how people try to convince me that I still has my freedom to choose my own path, to choose what I want to become, those expectations already set limits for me."
    "I've long given up the chance to stray away from the path of Schnee inheritage."
    
    "Rhodo might be luckier."
    "{i}I hope{/i}, he's luckier than me."
    rho "'Ey, Schnee?"
    "Rhodo's calling interrupts my thoughts."
    show mei nor default2 at mcenter with dissolve
    mei "Yes?"
    rho "Go ahead and get some sleep."
    rho "I'll wake you up when I'm too tired to stay on guard."
    rho "At least one of us need to keep a watch, right?"
    
    mei nor frown2 "Your headache?"
    rho "Already gone."
    mei "Oh ... that's great then."
    hide mei with dissolve
    "What's with that sudden headache? Headache doesn't usually come and go like that."
    "Strange."
    "Deciding not to think about it further, I settle in the corner of the room, using my cloak as a makeshift blanket to keep me warm."
    
    rho "Schnee?"
    show mei nor frown at mcenter with dissolve
    mei "Are you serious about letting me sleep first or not?"
    rho "Just wanna say thank you for listening to my rants."
    rho "Feel much, much better now."
    mei "Hmph."
    mei nor default "You're welcome."
    mei "And good night."
    hide mei with dissolve
    
    scene black with fade
    call chapter("End of Part 8", "Stranded") from _call_chapter_14
    
    jump part9BR
    
    #return