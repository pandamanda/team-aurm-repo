# PART 8A: CAPTURED #
# Anya Route #

label part8AA:
    "My first thought when I get myself together was to summon the knight and wreck this train."
    "The anger was overwhelming. The urge to act immediately was really, really strong."
    "But then, I stopped myself from doing so. It isn't right. Not quite."
    "Without Anya, I'm the one responsible to lead the team, that's what we all agreed."

    "What would a leader do in this situation?"
    "What would Anya do? What would my mom do?"

    scene bg train with fade

    show rhodo nor frown at mcenter with dissolve
    rho "Damnit ... damnit ...."
    #kicking sfx
    rho "Say, it's all metal in here. Pretty sure I can bend them around and wreck this train in no time."
    show rhodo nor frown at midleft with dissolve
    show mei nor surprised at midright with dissolve

    mei "NO! Don't!"
    mei nor angry "Why do you think doing that would solve our problem?!"
    rho "At least by wrecking this car, we can get out of this damn train, right?"
    ulm "N-no!"

    show rhodo nor surprised
    show mei nor frown2
    show ulm nor surprised at mcenter with dissolve
    "Even Rhodo is startled with Ulm's interruption."
    ulm "Thisisahostagesituation wecannotjustbreakourwayout!"

    "...."
    "Does Ulm usually do this? He speaks really, really quick when he's panic?"
    rho "Whoa, slow down! I don't understand what you said."
    mei "They have Anya and I don't think they wouldn't hesitate to harm her if we do something recklessly."
    "Ulm nods, agreeing with my explanation."

    ulm nor reluctant "{size=15}We have to think of another plan ....{/size}"
    rho "But, what? I don't have anything in mind."
    hide rhodo
    hide ulm
    hide mei
    with dissolve

    "What should we do now? Could I think of a plan or two?"
    "...."
    show mei nor default2 at mcenter with dissolve
    mei "For now, let's wait and see."
    mei "We need to assess our situation further and there isn't much we could do for the moment."
    mei nor frown2 "Let's not be reckless and rush things up."
    hide mei with dissolve

    "With that said, I need to keep myself calm too."
    "Everything is going to be alright."
    "... I hope."

    scene black with fade

    "There isn't any windows or any similar openings in the train car we are in, so we couldn't see for sure where are we when the train slow down and finally stops."
    "It's just a hunch, but I think they brought us to one of the old tunnel around Forever Fall."
    "It might be the very same tunnel where I met Emerald."
    "What are they plan to do with us?"

    "As if answering my question, there's another call in my Scroll."
    "It's from Anya, but assuming she hasn't managed to free herself, I expect it to be Emerald."

    scene bg train with fade
    show mei nor default2 at mcenter with dissolve
    mei "Yes?"
    emr "Do not do anything reckless and please get off from the train."
    emr "Before you asking, yes, your leader is alright."
    emr "She's a lot more weaker than I thought. Interesting."

    "Emerald ends the call."

    show mei nor frown2 at midleft with dissolve
    show rhodo nor confused at midright with dissolve

    rho "Well ...?"
    mei "Let's go. Nothing's else we could do for now."
    hide mei
    hide rhodo
    with dissolve

    scene bg tunnel with fade

    show emerald default at mcenter with dissolve

    emr "Glad you decide to behave."
    emr "Nuh-uh, do not try anything suspicious, kids."
    
    hide emerald with dissolve

    "Two guards drag Anya off from the front train car."
    "Both of her hands are cuffed on the back. The guards on her sides put their weapon close on her back and neck."
    "Except for trail of blood from her nose, I think Anya is pretty much okay."

    show emerald default at midleft with dissolve
    show gn3 at midright with dissolve

    emr "Tied those kids up and bring them inside."
    emr "Five of you, keep an eye on them."
    gn1 "Ma'am, don't you think it's too many men for guarding these brats?"
    emr smile "Such confidence after they beat half of you guys when our train stopped."
    gn1 "Uhh ...."

    emr default "No. Five guards aren't too many. Do it."
    gn1 "Yes, Ma'am."

    hide emerald
    hide gn3
    with dissolve

    "I look at Anya, waiting for any signal from her."
    "Anya smirks and looks at Emerald."
    
    show emerald default at midleft
    show anya nor default at midright
    with dissolve

    any "Do you know that you're about to hold a certain Mei Schnee as a hostage?"
    emr "Of course I know."
    any "Well ...."
    any nor smirk "I don't need to warn you about anything then."

    hide emerald
    hide anya
    with dissolve

    "Huh? I was expecting a clearer signal than that."
    "But from the way she said it, Anya might be provoking Emerald."
    "Emerald, however, shrugs and unsheathes her weapon without warning, pressing the blade on Anya's neck."

    show mei nor angry at mcenter with dissolve

    mei "No! Stop!"
    mei "Leave her alone!"

    hide mei with dissolve

    show emerald default at mcenter with dissolve

    emr "Just behave and we don't need to hurt her, okay?"
    emr "This won't be long. Once we're done with the goods, you can go home safely."

    hide emerald with dissolve

    "I do not trust her words. Not a single one."
    "But we are currently at disadvantage. There's nothing much we could do now."
    "Ulm, Rhodo, and I reluctantly give up our weapon and let the guard tie our hands."
    
    "I keep watching my surroundings for any openings as Emerald's underlings herd us away from the train but not leaving the tunnel."
    "There has to be something we could do to get out from this situation ...."

    scene bg storage with fade

    "Emerald's underlings work quickly."
    "They unload cargo from the train and bring them to the deeper part of the tunnel."
    "Five men stand on guard around us."
    "They didn't put us inside a locked room or something. The three of us sit down in the corner of the train platform."

    rho "You guys should really look for another job."
    gn1 "*sigh* I'm tired of being lectured by some random kids, but okay ...."
    gn1 "Listen, kiddo, I've got mouth to feed, alright?"
    gn1 "If I don't work, there's no way I can provide for my family."

    rho "But--"
    gn2 "But we should pick another job, that's what you're gonna say next, hm?"
    gn2 "A safer, legal one, perhaps?"
    gn2 "Unlike you guys, our only choice are becoming illegal Huntsmen."

    gn1 "I tried applying to Huntsman academy, but ... nah."
    gn1 "Five times already and I'm getting too old to apply."
    gn2 "Becoming illegal Huntsmen is pretty much similar to this."
    rho "That's ...."

    "I steal a glance at Ulm and notice a glow on his left eye."
    "Whom is he Sightjack-ing?"
    gn3 "Oi! One-eye! What do you think you're doing?"
    ulm "I ... nothing!"

    gn3 "I saw your eyes glowing! Are you trying to use whatever your Semblance is?!"
    rho "What do you mean Semblance? You're just seeing things!"
    rho "Ulm did nothing!"
    "The goon glares at Ulm, who whimpers and looks away."

    "... Ulm shouldn't have done that. Averting his gaze, I mean."
    "It gives away a hint that he {i}really{/i} did something and lied about it."
    gn3 "Catch you do that again and you'll be sorry."
    gn3 "{size=15}Geez. I hate punching kids even if they deserve it.{/size}"

    "I twist my wrist a little, trying to feel what they used to tie us."
    "It's thin and not flexible. A little too tight around my arm."
    "If only I have something to cut this without noticing the guards around us ...."
    "Where did they keep our weapons? Even if we can untie ourselves, could we manage without our weapons?"

    "Think, Mei ... think ...."
    gn1 "Hey, red girl."
    mei "?"
    gn1 "Are you really {i}that{/i} Mei Schnee?"

    "I'm a little upset to hear that question."
    "Which part of me that makes him doubt me???"
    mei "Well ... yes."
    gn1 "You're not someone pretending to be her?"

    "Now I'm not just upset anymore. I'm {i}furious{/i}."
    mei "How dare you questioning my identity?!"
    gn1 "There's a lot of impostors out there, missy!"
    gn1 "You cannot blame me for doubting."

    gn2 "So ... that pigtail wasn't lying when she said we hold a Schnee as a hostage?"
    gn3 "Tsk. This will get us into trouble. {i}Big trouble{/i}."
    gn3 "And I just started to like this job."
    gn1 "Miss Emerald will know what to do. I don't think we should be worried too much."

    "I try pulling my wrists again. The ties bites into my skin."
    "..."
    extend "..."
    extend "..."
    "I think I have an idea how to untie myself. It's worth trying."

    "I nudge Rhodo who sits next to me."
    mei "{size=18}Could you keep them talking so they don't pay attention to me or Ulm?{/size}"
    rho "{size=18}What're you--oh.{/size}"
    rho "{size=18}Okay. I'll try.{/size}"

    rho "Hey, I'm quite sure you're not talking like Vacuans or Valeans. Where are you guys come from?"
    gn1 "We're from Mistral. I thought you notice since you have a Mistralian accent too."
    rho "Everyone told me that. I'm from Vacuo, though. I kinda took the accent from my fa--head of the orphanage."
    gn2 "Oh? From Vacuo? That explains why I feel weird when hearing you speak. You have Mistralian accent but speak slightly like Vacuan."

    "It's a little surprising, to see the goons immediately engage in a friendly conversation with Rhodo."
    "Rhodo gets along easily with people, I admit it. Some people does have a bad first impression about him, but once they know Rhodo is nowhere bad, they'll get along just fine."
    "I wait a little bit more before casting a small Glyph behind my back."
    "I could summon a gigantic armored knight, everyone knows that."

    "Only a few know that I could summon a mini version of the knight too."
    "It's enough to cut my ties right now without noticing the guards."
    "I just need to keep calm and maintain control ... not looking suspicious ... at all."
    "I looked at Ulm, noticing the glow on his eye has just faded away."

    "He sighs, before looking at me and Rhodo."
    ulm "Wait and you'll know, she said."
    "Huh? Who said that?"
    "Who did Ulm Sightjack just now? Anya? But Ulm could only see through other people or other creature eyes, not hearing what they said."
    "Besides, how come Anya knows if Ulm Sightjack-ed her? There's no way to tell, as far as I know."

    "The guards, apparently heard what Ulm said just now."
    gn1 "What did you say?"
    ulm "{size=15}N-nothing!{/size}"
    gn2 "I {i}did{/i} hear you say something just now! I'm not smart, but I'm no deaf!"
    rho "Just don't mind him!"

    rho "Something's off with his head, okay? Just let him. That's nothing. Really."
    "...."
    "I appreciate Rhodo for trying, but his expression really gives away his lie."
    "It's only a matter of time for the goons to notice Rhodo was lying."

    "On the other hand, I managed to cut my ties and already dismissed the summon."
    "Back to ... waiting, I guess?"
    mei "What are you going to do with us?"
    gn1 "Nothing."

    mei "Huh? What do you mean 'nothing'?"
    gn1 "*sigh* You kids read too much comics, really ...."
    gn1 "Do you think we're gonna toss you to the sea and let you guys got shredded to bits and pieces by the Grimms?"
    gn1 "I'm sorry to disappoint, but no."

    gn2 "Miss Emerald isn't that kind of person."
    gn2 "She won't deliberately finish people off when someone's getting on her way."
    gn1 "True, true."
    gn2 "I'm not sure the big boss will do the same, though."

    gn2 "How are we going to report to him about the camouflage barrier damage, by the way?"
    gn2 "That train is one of his--huh?"

    "Old fire alarm noise echoes throughout the tunnel, immediately followed by drizzle of water from the sprinkler on the ceiling."
    gn1 "What's ... happening? There's fire?"
    gn3 "See no fire around here."
    gn1 "We should check it out just to make sure."
    gn1 "I'll go with Archie to check around the train, you three stand here."

    "Before any of them get any close to the train, there's a loud noise from deeper part of the tunnel."
    "If I'm not mistaken, it comes from the direction where they brought the crates."
    "Rhodo, Ulm, and I take a glance at each other. I mouth 'wait' to them."

    gn1 "What the heck is happening?!"
    gn1 "Archie, let's go!"
    "Once they turn around and far enough from us, I summon one of the knight's gauntlet, covering my own arm."

    "I hit the closest guard, sending him to the wall at the end of the room."
    "The other two--surprisingly--react accordingly with the situation."
    "From the corner of my eyes, I see one aiming his rifle at me."
    "With distance as close as we are now, I doubt he would miss his shot. I could only hope I move fast enough to block it with the summoned gauntlet."

    "A split second before he shoot me, the guard lose his balance and falls down."
    "The bullet still hits the knight's gauntlet."
    "I take the chance to disarm and knock him down."
    "Rhodo somehow manages to take down the last guard, stomping and kicking him unconscious."

    rho "Whew ... that should do it."
    mei "That's a little brutal."
    rho "You smashed one to the wall with a giant hand and me, using my own boots, is the brute one here???"
    "It's difficult to argue with that one, isn't it?"

    "I untie both Rhodo and Ulm."

    rho "Thanks."
    rho "I could do it myself if it's metal handcuff, but, well, this one is cable tier, I think."
    rho "What's our plan now?"
    mei "Get our weapon back and rendezvous with Anya."
    mei "I have a feeling it was her who caused the ruckus inside and triggered the water sprinkler."

    ulm "They keep our weapons in the train car."
    mei "Let's go then."
    "Fortunately, they don't store our weapons in hidden place or such. We find them without any difficulties."
    ulm "Umm ... I want to make sure of something first."
    ulm "I'll be real quick."

    "Ulm's left eye glows as he presses his palm on his right eye."
    "It only takes him around fifteen seconds before Ulm's eye returns to normal."
    ulm "Anya already escaped. She's ... safe, I think."
    mei "You think?"

    ulm "I Sightjack-ed her just now and from what I see, she's already moving freely."
    ulm "From the looks of it ... she manages to hide somewhere inside."
    rho "Ha! At least they don't have anything to threat us with now!"
    rho "Let's smash them!"

    mei "We still don't know how's the situation inside, so I think it's better if we move without notifying other guards too early."
    rho "But if they notice us?"
    mei "That's when we go all out, but before that, let's not rush ourselves."

    rho "Okay. Sounds good enough."
    "The tunnel is nothing complicated. It isn't a secret-boss-lair with confusing pathway and rooms scattered here and there."
    "This tunnel was built as a mix between a temporary storage room and quick stop spot for maintenance or refueling. There's no need for any complicated layout for those purposes."
    "The original planner for this place even reused an old ruin."
    

    


    return