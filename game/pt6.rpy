# PART 6: #

label part6:
    
    
    scene bg dormnight2 with fade
    "Now, with everyone has returned to the dorm, we gathered to recap what we have found so far."
    play music confusion fadein 10.0
    show anya nor default at midleft with dissolve
    any "So? Anyone want to go first?"
    show rhodo nor surprised at midright with dissolve
    rho "Uh, can I?"
    rho "Before I forget whatever I wanna say?"
    "It's very good of Rhodo to realize his weakness and actually try to overcome it."
    "I don't show or say it, but I appreciate his effort."
    
    any "Sure. Go ahead."
    rho "Uh, 'kay ... Where should I start?"
    rho nor default2 "I'm not even sure is this even a good news."
    show mei nor frown2 at mcenter with dissolve
    mei "What do you mean?"
    rho nor frown "Like you've said to me before, Schnee, Darion tried to dig around team SKYE."
    any nor frown "As expected from him."
    any nor default "Go on."
    
    
    show mei nor default2
    rho nor default2 "Darion asked them about Saul going out and how did he have his approval."
    rho nor frown "Team SKYE didn't even realize it was odd that Saul got his permission approved while their handler is away."
    rho "They were confused and clearly worried about what happened."
    rho nor smile "It also threw them off from the game too. I was no longer the loser after Darion raised that question."
    any nor smile "Well, making your opponent stressed out and under pressure is one trick to win a game."
    mei nor frown2 "But that wasn't Darion's intention, was it?"
    "Anya shrugs. The smirk follows after that indicates that she doesn't share the same positive thoughts as mine."
    any nor smirk "Who knows?"
    
    rho nor surprised "And, y'know? Team SKYE's condition is way more sadder than I've thought."
    show mei nor default2
    show anya nor default
    rho "They kinda opened up and talked about various things during the game."
    rho nor frown "Other teams have been harassing them for a while now."
    rho "They even a bit ... how to say it? Detached? From Saul?"
    rho "He's their leader, but he's also the one who caused so much trouble for them."
    mei nor sad "That's ... sad."
    rho nor default2 "Yeah."
    
    rho nor default "Anyway. It makes me thinking that we can ally with team SKYE."
    rho "So we can figure out things faster!"
    any nor frown "They're a potential ally, I agree."
    rho nor smile "I know, right!"
    any "But I don't know them really well to determine whether they can be trusted to be discreet about this or not."
    mei nor frown2 "I would say we could still dig a few things from them as long as we're careful not to raise any suspicion."
    show rhodo nor default2
    
    any nor default "Yes."
    show mei nor default2
    any nor frown "The next time we have the chance to speak with them, try to ask them if they notice when did Saul start taking his 'vitamin'."
    any "Tell them we're concerned since Ulm probably has mistaken it for quite a time."
    any "And make sure you keep each of us informed if you've asked them. We don't want to ask them twice or thrice."
    rho nor surprised "Ah ... they're going to be suspicious if we do that."
    any nor sigh "Exactly."
    
    mei nor frown2 "Are you sure asking them would provide us with anything useful?"
    any nor default "We might miss something if we're not asking them, right?"
    mei nor sad "I just think it might lead our investigation to a wrong direction."
    any nor frown "Yeah, it's possible."
    any "But we can't just let it slip away too."
    any nor default "If you have an idea or two, I'm all ears as always."
    hide anya with dissolve
    hide rhodo with dissolve
    hide mei with dissolve
    
    "I could have all the concerns I want, but the truth is I don't have any better plan nor idea for now."
    "We don't have any lead and Professor Goodwitch didn't provide us with any solid starting point."
    "It feels like we're chasing wild goose, but I couldn't think of anything better."
    
    show mei nor sad at mcenter with dissolve
    mei "I'm sorry ... I don't have anything to suggest at the moment."
    mei "It's not that I doubt you or anything, but ... I couldn't help not worried."
    hide mei with dissolve
    "Rhodo has been making a slight nervous movement for a while now. When nobody speaks after a few seconds, he speaks with a low voice, unlike his usual self."
    show rhodo nor default2 at midright with dissolve
    rho "It's not really useful, isn't it ...?"
    rho "Everything I've gotten from team SKYE."
    
    show anya nor smile at midleft with dissolve
    any "What're you saying? It gives us a new perspective for the situation, Rho."
    any "Thank you for the hard work."
    rho nor surprised "Uh ... 'kay?"
    
    "Rhodo frowns and scrathes his head. He looks like he's reluctant to accept Anya's thanks."
    rho "It's good then, I suppose."
    any "It is, Rho, it is."
    any nor default "Next ... Mei?"
    hide rhodo with dissolve
    show mei nor default2 at midright with dissolve
    mei "I went to the Administration Office as we planned and got a print-out of delivered packages from Tukson's."
    "I hand over the paper to Anya."
    mei nor frown2 "If I may offer my opinion, I don't think it's a good lead."
    any "And why is that?"
    mei "Every packages arrive in Beacon Academy would have to go through a scanning machine to see its content without opening it."
    mei "I happen to know the machine and how it works. The book trick Ulm showed me previously wouldn't be overlooked that easily."
    
    any nor sigh "Except if they pull more tricks than that. Like, stuffing it so the view from the machine shows no different than your usual book."
    show mei nor surprised
    "... I feel a bit dumb for not thinking about tricking the machine."
    mei "It might be."
    any nor frown "I don't know half of the names in this list. I won't be able to reduce them into the most potential source in a short time."
    show mei nor default2
    "However, Anya hands the list to Ulm."
    any nor default "I want to know which team they come from and their team's performance in the last two semesters."
    any "I'm going to ask Professor Goodwitch for the record. Once I get it, I'll send it to you."
    ulm "Okay. I'll do it."
    
    "I hand over the flash drive I got from Professor Scarlatina to Anya."
    any nor smile "Thanks."
    mei nor frown2 "What's inside this flash drive anyway?"
    any "I will show you after this."
    any nor default "Ulm?"
    hide mei with dissolve
    show ulm nor default at midright with dissolve
    ulm "Ah, yes."
    ulm "I didn't get any difficulties following Saul to his house."
    ulm nor thinking "It's an area I know, though not very well."
    
    
    if bribe_jill:
        ulm "After making sure I wasn't being followed, I climbed somewhere nearby to get a better view."
        ulm "I waited and watched but couldn't find anything suspicious around the house."
        ulm nor reluctant "As far as I see, Saul didn't leave his house."
        ulm "{size=15}I'm sorry I didn't make any progress ...{/size}"
        any nor frown "We overlook one important thing: we don't know how long Saul will take his absence."
        any "And we can't watch him 24/7 if he took more than a day off."
        
        ulm "Yeah ... {size=15}That's right.{/size}"
        ulm "{size=15}I'm sorry ...{/size}"
        any nor default "Don't be."
        any "We'll think about it again later."
        any "Anything else?"
        ulm nor default "Oh! Yes!"
        
        ulm "I saw one of team WHYT near the apartment."
        ulm "He was walking toward the apartment and then hesitated before left."
        hide anya with dissolve
        show rhodo nor surprised at midleft with dissolve
        rho "Team who?"
        ulm "Team WHYT. I looked about them a little after the incident in the locker room."
        rho "Oh ... them."
        rho nor default2 "..."
        hide ulm with dissolve
        
        "Rhodo looks as if he is thinking really hard. The three of us automatically wait for him to say something."
        rho nor surprised "Why are you suddenly quiet?"
        show mei nor frown at midright with dissolve
        mei "We're waiting for you to say something, blockhead."
        rho "Oh!"
        rho nor default2 "Uhm, it slipped my memory."
        rho "Just continue. I'm gonna remember it later."
        mei nor frown2 "Let's hope it's something important."
        
        rho nor confused "How can I say it's important or not if I can't even remember it?"
        mei "Well ... that's true."
        hide mei with dissolve
        hide rhodo with dissolve
        show anya nor default at midleft with dissolve
        any "Did you notice anything else, Ulm?"
        show ulm nor reluctant at midright with dissolve
        ulm "Nothing in particular."
        any "My turn then."
        hide ulm with dissolve
        any nor sigh "Tukson is closed today. Bad timing."
        
        any "I didn't see notes or anything that explains why it's closed, so I tried asking around."
        any nor frown "And guess what?"
        any "I met someone I believe is from team WHYT."
        "I find it strange that both Anya and Ulm each met a member of team WHYT."
        "The City of Vale is vast and I bet the chance to accidentally meet like this is very slim."
        "Why did they visit both Saul's place and the bookshop?"
        
        show mei nor frown2 at midright with dissolve
        mei "Did you do something to them?"
        any default "It's just one of them."
        any "I waited in the corner until he walked to the shop and stopped in front of it."
        any "I asked him if he knew why Tukson is closed today."
        mei nor default2 "Did he know?"
        any "Nope."
        any nor frown "I couldn't really figure him out, but I would like to dig more from him."
        any "He seems to know something we don't."
        
        hide anya with dissolve
        hide mei with dissolve
        show rhodo nor surprised at midleft with dissolve
        rho "I remember it now!"
        rho nor frown "That team! Team WHYT!"
        rho "Elmo told me earlier that Saul's cousin is part of that team. His name is Teal Bran!"
        show ulm nor thinking at midright with dissolve
        ulm "Is he the one with blue bandana?"
        rho nor surprised "Uh ... Do you have something more specific?"
        ulm nor default "Platinum blonde, shorty, wield a twin blade?"
        rho nor frown "Right! That's him!"
        
        "This new knowledge changed my view about team WHYT in a mere second."
        "Are they trying to reach Saul before anything bad happened to him?"
        "Apparently, I'm not the only one who come up with the question."
        rho "They're persistent, aren't they?"
        rho "I mean, what's their purpose?"
        ulm nor reluctant "What if ..."
        
        "Ulm frowns. He looks hesitated, but then shows his expression when thinking hard and carefully."
        ulm nor thinking "What if they deliberately tried to get information from me to track where Saul got his goods?"
        ulm "They don't know it was an accident in my part."
        ulm "Now that we've met them in the city ..."
        hide rhodo with dissolve
        hide ulm with dissolve
        show anya nor shout at midleft with dissolve
        any "They probably notice we're up into something."
        any "At least, they probably think {i}I{/i} am up into something."
        
        show mei nor surprised at midright with dissolve
        mei "But we don't know for sure. It's all guesses and assumptions."
        any nor frown "This is important. We have to get into this again later."
        mei nor frown2 "What if they really are just into getting their hands on the Golden Apple?"
        mei "They would see us as an obstacle for their plan--"
        stop music
        any "Then we will remove them from the game, making sure they won't get into our way."
        hide anya with dissolve
        hide mei with dissolve
        
        "There is a sudden silence in the room."
        "When Anya said about making sure team WHYT won't get into our way, she sounded so ruthless."
        "It is unusual of her to show such emotion."
        "I steal a glance at Rhodo and Ulm. They both don't seem less surprised than me."
        "However, Anya doesn't notice our reaction and continue on to the next topic."
        
        
    else:
        ulm "I found myself a higher place for a better view."
        ulm nor surprised "Uhm ... {size=15}I'm sorry, it's a bit off topic{/size}."
        ulm nor default "I did what you asked me, Anya."
        ulm "I surveyed the surrounding area and found several spots that are suitable for hiding and watching the surroundings."
        ulm "I've also mapped possible escape routes from both the streets and rooftops."
        
        any nor smile "Great. I know I can depend on you."
        show rhodo nor surprised at mcenter with dissolve
        rho "Wait! What do you even need that for?"
        any "Escaping."
        ulm nor smile "Escape from danger."
        any "And to figure out potential places where our opponent might hide."
        
        "The last point actually makes sense."
        "If someone wanted to ambush or stalked Saul, they would need hiding places around his residence."
        rho "Oh ... I see."
        hide rhodo with dissolve
        any nor default "So, Ulm, did you see anything?"
        ulm nor reluctant "Unfortunately, nothing suspicious happened."
        ulm "Saul didn't leave his house at all."
        
        ulm "I thought he might leave from other doors, so I moved and checked from the other side, but didn't see Saul leaving."
        any nor frown "Hmm ... this is difficult."
        any "We forgot to consider how long Saul will take his leave."
        any "We can't watch him 24/7 if he takes longer than a day."
        mei "Do you think Professor could assist us with this matter?"
        
        any "I will ask her later."
        any nor default "Is that all, Ulm?"
        ulm nor default "Yes."
        ulm nor reluctant "{size=15}I'm sorry ....{/size}"
        any nor smile "What're you sorry for?"
        any "You did great and I appreciate it."
        
        "Ulm whisper something. His voice is really soft, I couldn't make what he said, but if I might guess, he said 'thank you'."
        hide ulm with dissolve
        any nor default "It's my turn then."
        any nor sigh "Tukson is closed today, so I didn't get much from the place."
        any "There's no sign or message why the store is closed, so I walked around a bit."
        any nor frown "Found nothing, except that the store doesn't have a back door if I didn't mistake its layout."
        show mei nor default2 at midright with dissolve
        mei "Did you stroll around there and waiting to see if the store opened in the end?"
        
        any nor default "Actually, no."
        any "I watched the place until about an hour past lunchtime and left."
        hide mei with dissolve
        show rhodo nor frown at midright with dissolve
        rho "Don't you think people will find it suspicious if you stayed in one place for a long time?"
        any nor smirk "Not many people look up above their heads and check the rooftops, Rho."
        rho nor surprised "?"
        rho "Uh, sorry? What do you mean?"
        any "I climbed the building across Tukson's and watched from its roof."
        
        rho nor smile "Ah! Now I see what do you mean!"
        hide rhodo with dissolve
        hide anya with dissolve
        "I really wonder does Rhodo ever notice our team leader's preferences and habits while in action?"
        "Ulm even talked about it less than ten minutes ago, when he reported his findings to Anya."
        "It isn't the first time for Anya to get into high places and rooftops whenever she could."
        "Those places, she said, are perfect for scouting, sneaking through restricted areas, and ambushing enemies."
        "I could agree with the reasons, but I wouldn't practice it if the situation didn't demand me to do so."
        
        "I prefer my feet standing on solid ground, thank you very much."
        show mei nor frown2 at midleft with dissolve
        mei "Where did you go after that?"
        mei "You said you left at around afternoon, but you returned to the dorm later than that."
        mei nor default2 "I assume you went somewhere else."
        "Anya looks amused with my guess."
        show anya nor smirk at midright with dissolve
        any "Visiting an acquaintance. Saying hi to her."
        
        "From the way she said it, I don't think Anya would say who did she meet."
        "I exchange a glance to Rhodo and Ulm. Rhodo shrugs. Ulm shows a disapproval face, but say nothing."
        "We already learned that Anya keep some of her acquaintances' identity from us and never fall for any trick we tried to make her reveal them."
        "It's one of her admirable feat."
        mei nor frown "Fine, if you don't want to talk about them."
        mei "Shall we continue to the next topic if you already finished?"
        
        any nor default "Yeah, that's all from me."
        any nor sigh "It's disappointing that I didn't get anything worth the effort."
        stop music fadeout 5.0
        hide anya with dissolve
        hide mei with dissolve
        
    
    any "Now for the flash drive ..."
    "Anya connects the flash drive to her Scroll with a cable."
    ulm "?"
    ulm "What is that?"
    "Just now, Anya opened an application unknown to any of us."
    
    "The mysterious application immediately recognizes the flash drive and then requests password."
    "Anya puts in several passwords. She doesn't immediately answer Ulm's question."
    "I notice how she took a glance to each of us in her usual calm manner."
    
    play music discussion fadein 10.0
    show anya nor default at midleft with dissolve
    any "Let's say I happen to have a connection with some people who has information and this is one of them."
    any "And I'm good enough to have their trust for accessing this information."
    show mei nor surprised at midright with dissolve
    mei "Professor Scarlatina is an information broker???"
    
    any nor frown "It's not really nice to call them 'information broker'. They don't trade information for money."
    any nor default "These people have a specific interest on the new White Fang group."
    any "Professor Scarlatina is part of them, but she isn't the leader of this information network. You can't even be sure she's the one who's walking around gathering info and rumors."
    hide anya with dissolve
    hide mei with dissolve
    "With her timid tendency, it's hard to imagine Professor Scarlatina works gathering information in the city of Vale."
    "But who knows how she works anyway?"
    "She might be secluded in her office, but noone is sure what she does in there. She couldn't just work on weapon blueprints every days and nights, right?"
    
    show rhodo nor surprised at midright with dissolve
    rho "Wait ..."
    rho "Did you say 'White Fang'? {i}That{/i} White Fang?"
    show anya nor default at midleft with dissolve
    any "Yes, {i}that{/i} White Fang."
    hide anya with dissolve
    hide rhodo with dissolve
    "Anya shows us the content of the flash drive on her Scroll."
    "It's map of Vale, marked in several spots. It's either an exclamation mark inside a circle or a question mark inside a circle."
    show anya nor default at midleft with dissolve
    any "The exclamation mark are where suspicious activities have been seen. The question mark are places need further investigation."
    
    show mei nor surprised at midright with dissolve
    mei "If they know there are suspicious activities, why don't they tell the authorities right away?"
    any "I don't know."
    mei nor angry "We should give this information to--"
    any nor sigh "Mei, listen."
    any nor frown "Whatever reasons they haven't given this to the authorities is not our business."
    mei nor frown2 "But--!"
    
    any "And my contact trusted me with this information not to be leaked to anyone else."
    hide mei with dissolve
    show rhodo nor shout at midright with dissolve
    rho "If we let them, they're going to grow bigger and it'll be too late!"
    "I gesture at Rhodo, totally agree with him in this matter."
    "Ulm bite his lips, nervous. But he nods while looking at me and Rhodo."
    any nor sigh "*sigh*"
    any "Fine. I'll discuss this with Professor Scarlatina later."
    
    any nor default "Now, can we back to our mission?"
    hide rhodo with dissolve
    show anya nor default at mcenter with dissolve
    any "I asked this map from Professor to limit our investigation places."
    rho "So this case is related to White Fang?"
    any nor frown "Hard to say at this point. This is more of a precaution move I take here."
    any "It's better safe than sorry."
    any default "So, whenever our investigation lead to any of these marked places, don't you ever try to go there alone."
    
    any "We will save these places for the last. Only if three or more information lead us to these places."
    any "We don't know what will be waiting for us, so I want to make sure we stick together."
    any "{i}Capiche?{/i}"
    rho "Yes, Ma'am."
    ulm "Yes."
    mei "Understood."
    
    any nor smile "Great. We're done for today."
    any nor frown "I'm sorry I can't give the map to you, but please tell me everything you got after each investigation."
    any nor default "I'll keep track of every lead we have."
    hide anya with dissolve
    show mei nor default at mcenter with dissolve
    mei "Don't forget we would have end semester test next week."
    "Anya and Rhodo stare at me in horror."
    show mei nor frown with cdissolve
    "Why am I not surprised? It almost feels normal for the two of them to keep forgetting about tests."
    show mei nor frown at midleft with dissolve
    show rhodo nor surprised at midright with dissolve
    rho "It's that time already???"
    
    "Anya opens her mouth, almost saying something, but then she just shakes her head and leave for the self-made refrigerator in the corner of the room."
    rho "That means we have to finish everything before next week?"
    rho "And we haven't gotten anywhere today!"
    hide rhodo with dissolve
    hide mei with dissolve
    stop music fadeout 5.0
    "Just now, I had the wrong priority in my mind."
    "I thought Anya and Rhodo freaked out because of the test itself, not because we need to wrap things up before next week."
    "We wouldn't be able to move freely for investigation once the test week starts and who knows how the situation has developed during that time."
    
    "The culprit might find us first and either escape or ..."
    "I swallow, trying to get the thoughts out of my mind."
    
    scene bg dormnight with fade
    "Later, when I already settle in my bed, I remember something that refrain me from immediately fall asleep."
    "{i}Li hasn't answered any of my text message.{/i}"
    "They aren't even delivered or read, based on the check mark on each message."
    "I've tried to call her, but to no avail. I didn't even heard the answer tone that said the number is busy or out of reach."
    "It's as if she turned off her phone, which is really strange because Li never turns off her Scroll except at night."
    
    "I lost contact with Li."
    "I know it's probably nothing."
    "Maybe she lost her Scroll device somehow? Sometimes that little kitty is reckless and clumsy."
    "I'm trying not to panic here, but I couldn't."
    "It has been too long since our last contact."
    "... I'm really worried about her."
    
    "I open my Scroll and tap a name from my contact list: "
    extend "Blake Belladonna."
    "Li has told me that whenever I feel the need to contact her mother, don't make a phone call. Just use text message."
    "Her mother--or, technically, my aunt--isn't fond of phone call and her significant other is the type who easily panicked at the slightest sign of trouble with their daughter."
    
    nvl clear
    m "M: Aunt Blake? are you still awake?"
    m "M: I wanna ask a few things if its not too much"
    "The reply doesn't come until a few minutes later."
    "I already drifted to sleep when my Scroll beeps and its screen flashes."
    b "B: Yes?"
    "... This short reply somehow makes me anxious ..."
    
    "There's something about Aunt Blake that I couldn't really grasp."
    "She's a kind person--there's no doubt about it--but Aunt Blake is ... hard to 'read'."
    "If Li looks like a kitty that shy away from strangers, Aunt Blake looks like a cat that hisses and scratches strangers who get too near to her."
    "Even though I've known her for years, I don't feel confident enough to casually converse with her."
    m "M: Is li ok?"
    "I don't need to wait long before I see 'Blake Belladonna is typing ...' on the screen, but it's quite long before the actual reply pops up."
    "What made Aunt Blake type so long?"
    
    "From my experience with Mom, it's usually because Mom changes her mind when typing and erase everything to write something new. "
    "Type, erase, type, erase, type, and erase again. Back and forth. "
    "That's Mom."
    b "B: Oh"
    b "B: She's ok"
    m "M: Oh i see"
    
    b "B: What's wrong?"
    m "M: Oh nothing"
    m "M: Its just she hasnt reply or answer my call"
    b "B: She lost her scroll"
    m "M: Oh?"
    m "M: I see"
    
    b "B: She didn't reply us too"
    b "B: Yang panic"
    m "M: Scold her for making her parents panic Auntie :D"
    b "B: I will"
    b "B: I called her teammate to ask"
    "There's another long 'Blake Belladonna is typing'."
    
    b "B: She dropped her scroll somewhere"
    b "B: I need to remind her to get a new one"
    m "M: :))"
    m "M: Yea i think you need to remind her"
    m "M: I dont understand how li could live without scroll"
    b "B: Yeah"
    
    "... Aunt Blake ... do you have any idea how difficult it is to reply your short, emotionless messages?"
    m "M: Oh well"
    m "M: I guess thats all"
    m "M: Thanks Auntie :D"
    m "M: And goodnight"
    "Her last response arrives a bit later."
    
    b "B: Night"
    "I think that's the last of our conversation, but then Aunt Blake send another message."
    b "B: Li is going to be alright. Don't worry"
    b "B: You otoh need to be careful too"
    "..."
    "It takes me a few seconds to realize that 'otoh' stands for 'on the other hand'."
    "Something feels a bit off with those last replies."
    "The 'Li is going to be alright. Don't worry' sounds not only trying to reassure me, but also to reassure Aunt Blake herself."
    
    "And the next one ..."
    "It's as if Aunt Blake knows something about our current situation."
    "I almost ask her back about those two replies, but I refrain myself and type a 'thank you' instead, ending our conversation."
    
    
    scene bg classroom with fade
    
    play music casual fadein 10.0
    "Even though there isn't any proof that Emerald Sustrai is involved in our current investigation, I couldn't seem to get her out of my mind."
    "Mom said about Emerald might be there in the abandoned tunnel for something and used her Semblance to keep us away from it."
    "Whatever it is, I bet it's something illegal, perhaps even dangerous."
    "My curiousity is nagging me to do a 'side investigation' by myself."
    
    "I asked Anya previously about what kind of accessibility Professor Goodwitch would grant us with our current status."
    "She didn't know for sure, but said that there's high chance we're allowed to view mission reports and recordings like what we had from the Forever Fall mission."
    "I'm about to discuss this matter with Anya during our free time today, but she is off somewhere before I could have a word with her."
    
    scene bg dormday with fade
    
    if bribe_jill:
        show ulm uni default at midright with dissolve
        ulm "Oh, she went somewhere with Rhodo."
        show mei uni surprised at midleft with dissolve
        mei "Rhodo???"
        mei uni frown2 "Where are they going?"
        ulm uni reluctant "Um ... {size=15}I'm not sure ...{/size}"
        ulm "They're not going out to the city, that's what I know."
        hide ulm with dissolve
        hide mei with dissolve
        "Of course. They would need another permission granted by our handler and others would find it strange if Anya leave for two consecutive days."
        
        "Well ... it's not an urgent matter anyway. It could wait until Anya comes back."
        "I watch Ulm as he is taking notes from something his Scroll."
        "The tables he's reading is full of text and numbers."
        show mei default at midleft with dissolve
        mei "Could I help you with anything?"
        show ulm uni surprised at midright with dissolve
        ulm "Oh! Uhm ..."
        ulm "{size=15}Everything's good, Miss Schnee ... Thank you very much.{/size}"
        
        mei uni frown2 "Are you sure?"
        mei "You seem to have a lot of data there. What is that?"
        ulm uni default "It's record of teams ranking during the years."
        ulm "Anya sent it to me this morning."
        "I immediately recall that Anya asked Ulm to gather team rankings from the list I got from the Administration Office."
        "Despite of the record that seems overwhelming, Ulm already jotted down quite a lot of team names on his notebook."
        "I'm pretty sure Ulm would finish his work in another minute."
        
        mei uni default "Find anything interesting?"
        "Ulm nods."
        ulm "There are several teams whose rank were stagnant but then showed a high raise during the semester."
        ulm uni thinking "I thought this record only has the teams' rank at the beginning and at the end of the semester, but there are also weekly ranking."
        hide ulm with dissolve
        hide mei with dissolve
        "Ulm falls silent and he quietly continue browsing the data table."
        "He writes down two more team names."
        "We don't say anything for a minute or two. It's kinda fun watching Ulm working."
        "However, it doesn't apply the same with him."
        show ulm uni surprised at midright with dissolve
        ulm "Uhm ... I'm sorry if I don't talk much, Miss Schnee."
        
        show mei uni smile at midleft with dissolve
        mei "That's okay."
        mei "I don't talk that much too when Rhodo isn't around."
        ulm uni default "You and Rhodo are sure getting along lately."
        mei uni frown2 "Does it look that way for you ...?"
        ulm uni surprised "Uh, uhm ...."
        ulm uni reluctant "I mean, your relationship has been improved, compared to around the first time we entered Beacon ...."
        ulm "Even though you still fight about little things, it feels ... less hostile."
        hide ulm with dissolve
        hide mei with dissolve
        
        "I try to recall my last 'fight' with Rhodo."
        "I don't feel too irritated when I criticized him over something lately."
        "Back then, almost everything he did made me snapped. We got head-to-head so often, Anya had to separate us. "
        extend "With force."
        "Rhodo and I have a very good reason to jerk in wary whenever Anya moves her cane in a certain way."
        "We didn't know Anya would do as far as to hit us with her cane when her attempt to separate us showed no good result."
        "To think that we both tested her patience ...."
        
        show mei uni sad at midleft with dissolve
        mei "Rhodo is not that bad, actually."
        mei uni default2 "I don't hate him. Just ... disagree with too many things. I guess."
        show ulm uni smile at midright with dissolve
        ulm "Maybe it's a bit culture shock for you, Miss Schnee."
        ulm "I can somewhat tell from how you organize things and act that you're used to live in orderly manner."
        ulm "On the other hand, I can understand Rhodo has less sense of privacy because he lives in an orphanage where various things are used by many."
        hide ulm with dissolve
        hide mei with dissolve
        
        "Speaking of culture shock reminded me of something."
        "I never tell anyone, but I previously had some problem with Ulm and what seems to me as his lack of awareness to personal hygiene."
        "Ulm could wear the same uniform for a whole week and it was more than easy for him to skip taking a shower."
        "I didn't understand what was his problem."
        "Before I pointed this out to him, Rhodo--who is Ulm's partner--was being frontal and dragged Ulm to the bathroom if he caught him skipping bath."
        "Apparently, Ulm only had two sets of uniforms, explaining why he wears the same uniform in a week."
        "As for his habit of skipping bath ...."
        "He lives with two other families--his uncles, aunts, and cousins--under the same roof."
        "With limited availability of bathroom, Ulm grows a habit of taking a really quick bath or skipping it entirely so that he wouldn't be late to work."
        "Even the idea that Ulm is working before attending Beacon Academy baffled me so much."
        "There are so many differences between the four of us, it's amazing that we could reach our current state now."
        
        stop music fadeout 5.0
        play sound door
        "The door swings open."
        show ulm uni smile at midleft with dissolve
        ulm "Welcome back."
        ulm "I've gotten the teams' ranking progression like you've asked."
        show anya uni smile at midright with dissolve
        any "Oh, that's great."
        ulm "I marked several that show unusual weekly progress too."
        any "Thank you very much."
        hide ulm with dissolve
        
        show mei uni frown at midleft with dissolve
        mei "Where did you go?"
        mei "And why did you bring Rhodo with you instead of me?"
        hide anya with dissolve
        show rhodo uni smile at midright with dissolve
        rho "Because it's important to pick the right party member for the right quest!"
        "I roll my eyes at Rhodo's answer."
        hide rhodo with dissolve
        show anya uni smirk at midright with dissolve
        any "That's one creative way to put it. The right person for the right job."
        any uni default "We went to meet up with team WHYT."
        mei uni surprised "W-what?"
        play music discussion fadein 10.0
        any uni smirk "It's pronounced 'white', not 'what'."
        
        show mei uni frown with cdissolve
        "I pout and smack Anya's arm, while she is laughing at her own joke."
        hide mei with dissolve
        show ulm uni surprised at midleft with dissolve
        ulm "{size=15}Did it go well?{/size}"
        ulm "{size=15}They didn't ... pick a fight with you or something, right?{/size}"
        any uni default "They probably thought twice when they saw me approach them with Rhodo instead of Mei."
        hide ulm with dissolve
        show rhodo uni confused at midleft with dissolve
        
        rho "I still don't understand about it, by the way."
        rho "If you really want them to talk, you bring badder arsenal and stronger fighters, right?"
        rho "In that case, you'd bring Schnee instead of me. She's way more dangerous."
        hide rhodo with dissolve
        show mei uni angry at midleft with dissolve
        mei "HEY!"
        any uni sigh "It will be considered as a threat. A threat to 'speak up or be eliminated'. I don't want to give them such impression."
        show mei uni default2 with cdissolve
        any uni default "I went to them to offer exchange rather than forcing them to tell me what I want to know."
        any "And, yes, they need to know I won't go back easily if they resorted to violence as their way to express refusal."
        "Anya shrugs."
        any uni frown "A lot of people are easily terrified and bite back nowadays. I need my safety measure."
        mei uni frown2 "So, how was the meeting?"
        
        any "It was difficult to assure them to open up about Saul without spilling too much things from what we've known."
        any "But they buy our reasonings in the end and shared a few information."
        show mei uni default2 with cdissolve
        "If Rhodo, Ulm, and I had animal ears, they would have perked up in anticipation and curiosity."
        "What kind of information did Anya get?"
        any "They told me that Saul has been seen meeting a few people with thick Mistralian accent."
        any "Saul's cousin, Teal, was curious and asked Saul about them."
        any uni default "But Saul avoided his questions, making Teal suspicious."
        any "He didn't try to nag Saul further about it, though. Until that incident in Emerald Forest."
        
        hide mei with dissolve
        show rhodo uni default2 at midleft with dissolve
        rho "Teal got too anxious about all of that and asked his team for help."
        rho uni frown "I don't like the fact that they cornered Ulm because of it."
        any uni sigh "I told you about people are easily terrified and bite back, right? That's one example."
        rho "Still not acceptable."
        rho uni default "But they did feel guilty about it, I think. They agreed to tell you more info after you talked about Ulm."
        
        hide rhodo with dissolve
        show mei uni default2 at midleft with dissolve
        mei "What did you tell them about Ulm?"
        any uni default "Nothing much. "
        any "Concern about the side-effect of the doping. "
        any "And a little about he might be targeted if the dealer found out about his misfortune."
        any "They cooperated once I mentioned the last point."
        mei uni surprised "Wait--"
        
        mei "What about the last one? Why would they target Ulm?"
        any uni frown "Because Ulm is the one who mistook Saul's stash and they might already notice our investigation."
        any "I don't know how, but remember Saul got his permission approved while Professor Scrooge is away?"
        any uni default "They have access to the academy."
        hide mei with dissolve
        show rhodo uni frown at midleft with dissolve
        rho "And don't you think they might have seen him during your visit to the city yesterday?"
        "Anya doesn't immediately answer. She nods slowly."
        any uni frown "That was my mistake to overlook that possibility ...."
        any "I'm sorry."
        hide anya with dissolve
        hide rhodo with dissolve
        
        "I'm sure Ulm is really scared and maybe angry at this point. Even though he doesn't say a word, he has been biting his lips and move restlessly on his chair."
        show mei uni angry at midleft with dissolve
        mei "I'm not going to let them get Ulm."
        show rhodo uni frown at midright with dissolve
        rho "Yeah! They gotta fight us if they want to get him!"
        "Ulm mumbles a faint 'thank you'."
        hide rhodo with dissolve
        hide mei with dissolve
        
        "Anya, however, doesn't look too optimistic with our support."
        "It makes my heart sunk."
        "Did I just boast an empty support without sufficient capabilities to make it come true?"
        "What else does Anya have in her head that she wouldn't tell us? Is it something really bad?"
        show anya uni default at mcenter with dissolve
        any "Anyway ...."
        any "The most intriguing information is the high possibility that the Golden Apple is produced in Mistral area."
        #hide anya with dissolve
        
        "Despite of its fame with arts and crafts, Mistral region is said to be the perfect hiding place for illegal activities."
        "It's because Mistral's landscape is dominated by thick jungle and marshes. Travelling isn't particularly easy, except if it's done using an airship."
        "Even using airship faces another difficulty of landing space ...."
        #show anya frown at mcenter with dissolve
        any uni frown "At first, team WHYT didn't know how the drugs arrive in Vale."
        any "But then they overheard a rumor about the Ghost Train."
        any uni default "Just for your information, the leader of team WHYT is a reluctant member of the Conspiracy Theory Debunker."
        hide anya with dissolve
        
        show rhodo uni default at midright with dissolve
        rho "He looked really pained when telling us about him being a member."
        rho uni confused "Why did he join if it's so embarrassing for him, anyway?"
        show mei uni frown at midleft with dissolve
        mei "Maybe he has a crush on one of the member."
        rho uni default "Oh, that makes sense."
        hide rhodo with dissolve
        hide mei with dissolve
        
        show ulm uni reluctant at midleft with dissolve
        ulm "There's no such thing as 'ghost train'."
        ulm "They're lying."
        
        stop music fadeout 5.0
        show anya uni sigh at midright with dissolve
        any "Surprisingly, no. They're not lying."
        show ulm uni surprised with cdissolve
        any "It's a modified small train equipped with Atlessian military-standard camouflage barrier."
        play music intense fadein 10.0
        any "The Conspiracy Theory Debunker wanted to set-up cameras and various devices to prove the truth about the Ghost Train."
        any uni frown "Warren, team WHYT leader, was against their plan and did everything he could to stall them and even stop them from doing it."
        any "Another member from his team is a tech-wiz. While Warren delayed the group, the rest of team WHYT made a modified drone and sent it to survey the area."
        any uni default "It caught a glimpse of the train before being shot down by one of the train guards."
        hide anya with dissolve
        hide ulm with dissolve
        
        "Anya stops, giving us a few seconds to process the last huge information."
        "I, especially, need that break because my mind have the wrong priority between commenting about how the Conspiracy Theory Debunker is actually useful and about where in Remnant the high-tech 'ghost train' come from."
        "I've heard about the camouflage device--Mother's personal airship, Falcon-1, has it installed after she formally received permission from Atlessian military."
        "The device is developed by Schnee Dust Company and another smaller company, under strict supervision from the military."
        
        "Nobody should have access to the technology."
        "Except ...."
        "Except if the device blueprint was also taken from the Research and Development during the break out."
        "How many things were stolen from the research facility?"
        "This isn't good at all ...."
        "While I do my thinking, Rhodo and Ulm are already asking questions in disbelief and anxiety."
        
        show rhodo uni surprised at mcenter with dissolve
        rho "Military-standard camouflage barrier!!!"
        rho "They have such things? What else they have then?"
        rho "A blaster canon? Paladin units? Spike-armored van from hell???"
        rho "Or the R.I.P. tire? Truck tire strapped with explosions?"
        hide rhodo with dissolve
        show ulm uni surprised at mcenter with dissolve
        ulm "They have guards on the train? A weapon-equipped guards?"
        ulm "Who are they???"
        ulm "And how can they get a hi-tech equipment like the camouflage barrier???"
        hide ulm with dissolve
        "..."
        "So this is the reason why SDC has kept the investigation and retrieval mission a secret ...."
        "The amount of danger of the stolen goods could quickly set people in panic."
        "Imagine the whole Vale heard about it."
        
        "And now, I think I need to interfere a bit and bring a little order, hopefully."
        show mei uni frown2 at midleft with dissolve
        mei "Was the train going through old Trans Vale-Mistral railway?"
        show rhodo uni surprised at midright with dissolve
        rho "Huh? Trans-what? What is that?"
        mei uni default2 "It's a train track connecting Vale and Mistral, going through Forever Fall."
        mei "It was shut down over a decade ago due to frequent attack from the White Fang."
        mei "Besides cargo train, the railway was supposed to be used by passenger train too. But it was canceled altogether."
        rho uni default2 "... Did Professor Oobleck cover that topic in class?"
        hide mei with dissolve
        
        show ulm uni thinking at midleft with dissolve
        ulm "No."
        ulm "The Trans Vale-Mistral was sponsored by Schnee Dust Company. For easier Dust material transportation."
        "The more Schnee Dust Company name mentioned while we're chasing this case, the more I grow uneasy."
        "Who would I find at the very end of this? Who is the mastermind? Someone from SDC too?"
        rho uni confused "Okay .... Train track that goes through Forev--"
        stop music fadeout 5.0
        rho uni surprised "Wait a minute."
        rho "Is this the same train track in the tunnel? The one we took shelter in?"
        hide ulm with dissolve
        show mei uni surprised at midleft with dissolve
        
        mei "!!!"
        mei "Yes, it is!"
        hide mei with dissolve
        hide rhodo with dissolve
        "We turn around to Anya, waiting for her to say something about this development."
        "She maintains her serious expression, but there's a glint of enthusiasm on her eyes."
        any "Think we can find the railway map in library archive?"
        
        scene black with fade
        call chapter("End of Part 6", "The Chase") from _call_chapter_16
        jump part7A
        
    else:
        show rhodo uni confused at midleft with dissolve
        rho "Huh? I thought she went with you, Schnee."
        rho uni default2 "I didn't see her after lunch break. Did you, Ulm?"
        show ulm uni thinking at midright with dissolve
        ulm "Uh, I think I saw her left the dining hall, but that's all."
        ulm uni surprised "{size=15}Should we look for her?{/size}"
        hide ulm with dissolve
        show mei uni frown at midright with dissolve
        mei "She doesn't go to the city, right?"
        mei "I think she's going to be alright as long as she stays in the school's vicinity."
        rho uni default "Agree. Pretty sure Anya won't get into trouble."
        rho uni smile "Even if she get into one, she can handle it."
        
        mei uni default "Yeah ...."
        "Ulm mumbled something to Rhodo, who proceeds to write down notes on the back of his notebook."
        mei uni frown2 "What are you doing?"
        hide rhodo with dissolve
        show ulm uni default at midleft with dissolve
        ulm "Taking note on teams' ranking progression during this semester and last semester."
        ulm "Anya told me to do so, based on the list you've got, Miss Schnee."
        hide ulm with dissolve
        hide mei with dissolve
        "Oh, now I remember that list from Administration Office."
        "I sit down on the floor with the two of them, curious with their work so far."
        "Ulm quietly swipes his Scroll screen, searching manually. Rhodo looks bored waiting for him."
        
        show mei uni frown2 at midright with dissolve
        mei "Why don't you use the search bar?"
        show ulm uni surprised at midleft with dissolve
        ulm "Uhm ...."
        show ulm uni surprised at mcenter with dissolve
        show rhodo uni confused at midleft with dissolve
        rho "He did and his Scroll crashed. It's not working and we had to force restart it."
        show mei uni surprised with cdissolve
        "That's impossible! How in Remnant a simple search could make a Scroll device crashes and freezes???"
        
        ulm uni surprised "{size=15}This is an old device .... It's not that powerful ....{/size}"
        show mei uni frown2 with cdissolve
        rho uni confused "Even if it's old, how old is it???"
        ulm uni reluctant "{size=15}This Scroll is 7 years old ....{/size}"
        stop music fadeout 5.0
        show mei uni surprised
        show rhodo uni surprised
        with cdissolve
        mei "...."
        rho "...."
        ulm uni reluctant "{size=15}It used to be my uncle's ....{/size}"
        play music reckless fadein 10.0
        rho uni frown "Schnee, you still have your Scroll from the first semester, right? You said about making it as a spare device."
        
        mei uni frown2 "It would need to be recharged, but, yes, I still have it."
        mei uni frown "And, yes, it's better for Ulm to have it now."
        ulm uni surprised "B-but--!"
        ulm "I shouldn't use {i}your{/i} Scroll, Miss Schnee!"
        mei "If I said you could use it, then you could."
        
        rho uni shout "You've heard her."
        ulm "But--"
        mei uni frown2 "You're using your uncle's Scroll too, so what makes this time any different?"
        rho uni frown "Yeah, Schnee's right."
        rho "Now, keep looking for the data. I'm waiting you."
        hide rhodo with dissolve
        hide ulm with dissolve
        hide mei with dissolve
        
        "I feel like both Rhodo and I kind of bully Ulm by forcing him to replace his current Scroll device with mine."
        "But it's really painful to see Ulm works with such a slow and lagging gadget!"
        "I plug the Scroll's adapter to the electrical socket near our desks. A white battery icon flashes on the screen, showing how much power left in it."
        mei "Why, by the way, you're the one writing while Ulm is looking for the relevant data?"
        "I pull a chair and sit down."
        
        show rhodo uni default2 at midleft with dissolve
        rho "Because Ulm's handwriting is so bad, nobody can read them."
        show mei uni frown2 at midright with dissolve
        mei "And you could?"
        rho uni smile "I borrow his notes a lot."
        mei uni frown "Oh. That makes sense."
        hide mei with dissolve
        hide rhodo with dissolve
        
        "Ulm finds another data entry, which Rhodo writes down."
        "I personally think it isn't practical to do like what they're doing now, but I decide to say nothing."
        "And I think it makes Rhodo feels better to contribute even the smallest thing in our current work which doesn't seem to suit him well."
        "I see Ulm says something to Rhodo in a really low voice, I couldn't hear him."
        "However, Rhodo's answer is loud and clear, almost like fire alarm in the middle of the night."
        show rhodo uni confused at midleft with dissolve
        rho "Well, I don't think Schnee need it. At least, she won't need it until ... I dunno, later?"
        
        show mei uni frown at midright with dissolve
        mei "Now what are you talking about me, hm?"
        rho uni default2 "Ulm said I shouldn't have asked you to lend your Scroll like that."
        rho uni surprised "...."
        rho "Now that I think about it, it's rude, isn't it?"
        mei uni frown2 "Yes. It is rude."
        rho "Uh ... yeah. I'm sorry ...."
        
        "I shrug, secretly amused at how Rhodo didn't show the slightest hesitation when he asked me about the spare Scroll and now feeling guilty about it."
        "He's reckless, no doubt about it. But I think everyone would know from his action that Rhodo is a honest and simple person that takes great care of his teammates."
        mei uni default "It's okay. No offense taken."
        mei uni frown2 "But, seriously, learn how to control what are you going to say. If you keep saying things recklessly like this, it might cost us in the future."
        rho uni frown "I know, I know!"
        
        rho "It's just ... if you grow up with a dozen of noisy kids, you gotta speak and act quickly before they get themselves in trouble."
        mei "What kind of trouble they could possibly get into?"
        rho uni default2 "Crashing a spike-armored van to a house or two? Not to say they might flatten people ...."
        stop music fadeout 5.0
        hide rhodo with dissolve
        hide mei with dissolve
        
        "Ulm pauses and looks at Rhodo with horrified face. I stare at the blonde boy with disbelief and confusion."
        "Now wait a minute ...."
        "What kind of environment did Rhodo grow up in???"
        "He had told us about growing up and living in an orphanage in Vacuo, therefore his claim of dealing with a dozen of kids (and maybe more) is true."
        "I really couldn't imagine the initial reason of the existence of a 'spike-armored van', followed by how the kids could get into such vehicle."
        "Or, why in the first place that dangerous modified vehicle was there to be accessed by the kids?"
        
        show mei uni surprised at midright with dissolve
        mei "SPIKE-ARMORED VAN???"
        show rhodo uni default2 at midleft with dissolve
        play music reckless fadein 10.0
        rho "Yeah. It's dangerous."
        mei "Why--I mean, how is there such thing?"
        show ulm uni surprised at mcenter with dissolve
        ulm "I want to know about that too ...."
        
        rho uni default "Oh, that's not surprising if you're in Vacuo. It's pretty wild over there. Modifying vehicles and making weapons or traps from scraps isn't odd."
        rho "The spike on that van is for protecting the vehicle from both Grimms and enemies attack. If you happened to be caught in the middle of battle, it's very useful."
        mei uni frown2 "But I thought you live somewhere away from conflicts and imminent danger?"
        rho uni frown "True."
        rho "But things happened and sometimes you've got yourself a refugee or two in your village who rode the van from a super dangerous place, escaping from a complicated conflict."
        rho uni confused "And someone wanted the refugees to be captured, so, yeah, battle ensued, and the van was left behind when they left."
        
        hide rhodo with dissolve
        hide ulm with dissolve
        hide mei with dissolve
        "From his description, Vacuo is indeed a pretty wild place."
        "I mean, Vacuo is indeed a {i}very{/i} wild place."
        "However, it leaves me with fascination and a mental note to be really, really, really careful when I have to visit Vacuo sometimes in the future."
        stop music fadeout 5.0
        
        play sound door
        "Anya's arrival cut our conversation short."
        show mei uni frown at midleft with dissolve
        mei "Where have you been?"
        rho "Whoa, Schnee, you sound like leaving you for an hour is a crime."
        mei "Don't be ridiculous. I didn't sound like that. Not even close."
        show anya uni default at midright with dissolve
        any "?"
        any "Something's happened when I'm gone?"
        mei "No. Nothing in particular."
        mei uni frown2 "Just want to know where did you go."
        "It must have came up in a wrong way."
        "Rather than concerned, I sounded more like nagging, because Anya immediately starts teasing me."
        any uni smirk "It seems I've made the princess upset."
        any "I apologize, your highness."
        
        "I'm currently in a good mood and decide to play along."
        mei uni cocky "Yes, servant. Know your place and explain to me where have you gone and what kind of affair you planned."
        rho "You two sounds weird, you know?"
        show mei uni frown with cdissolve
        "I roll my eyes, disappointed with Rhodo's inability to find the fun in our act."
        
        play music discussion fadein 10.0
        any uni default "I got a phone call. It took longer than I thought, apparently."
        mei uni frown2 "Phone call? From the huntress?"
        hide mei with dissolve
        hide anya with dissolve
        "I still doubt it was only a phone call."
        "Several times, Anya skipped telling us more details of what happened, because she decided it wasn't necessary for us to know."
        "Yes, it is true for those several times, what had happened could be considered 'insignificant' to us. She already dealt with them, but ...."
        
        show anya uni sigh at midright with dissolve
        any "No, no, it wasn't the huntress."
        any uni default "Remember I told you about visiting an acquaintance after watching Tukson's place?"
        any "I dropped by the Crow Bar since it's located in the port too."
        show rhodo uni confused at midleft with dissolve
        rho "That's the place in your list which Ulm reluctant to go."
        rho "Why did you visit it? And not telling us?"
        rho uni smile "By the way, here's the teams' ranking list."
        
        any uni smile "Thanks."
        "Agree with Rhodo, I press on the question of why Anya went to some 'shady place'."
        show mei uni frown2 at mcenter with dissolve
        mei "Why did you visit it?"
        show rhodo uni default2 with cdissolve
        any uni default "I was thinking the drug dealer need a 'meeting place' or two in their operation."
        any "They have to meet their potential client somewhere. Place where people can visit without drawing too much attention or too much suspicion."
        
        rho "So, they need a hidden and safe place?"
        mei "But if it's hidden, how people suppose to find the place and visit it?"
        rho uni surprised "... Uh, you've got a point there."
        any uni frown "That's simple. The place should have enough person to blend in and relatively easy to access."
        any uni default "Remember I wrote down two bars in my note?"
        mei "The ones Ulm said located in a not-too-safe area?"
        
        any "Yep. I've visited the one in the port and called the other one."
        any uni sigh "Mr. Xiong from Mitsuguma was really upset when I asked him. It seems one of them tried to hang out there."
        show rhodo uni default2
        show mei uni default2
        with cdissolve
        any "He found the guy and kicked him out. He doesn't tolerate anything related to drug dealings. Not a single bit."
        any uni frown "Most bars aren't welcoming dealers or contacts for such business. It puts them in dangerous position. Not worth the risk."
        rho uni confused "If he kicked the contact out of the door, then there's nothing he can tell us, right?"
        
        any uni default "Oh, Mr. Xiong did question him a bit."
        any "... I'm afraid it's more likely that he beat the poor guy to half-death ...."
        hide anya with dissolve
        hide rhodo with dissolve
        hide mei with dissolve
        "I would like to know what kind of person this 'Mr. Xiong' Anya spoke of, but I would keep it for another future conversation."
        "I don't understand how could Anya associate herself with a bar owner that doesn't mind beating people."
        "I thought bar is similar to restaurant or cafe where you need to be nice to your customer."
        "Even if there are annoying guests, they should deal with them in ... a less barbaric way."
        
        show anya uni default at midright with dissolve
        any "Mr. Xiong only told me that man comes from Mistral and claimed to know nothing more."
        any "I think they arrange the organization with strict information between ranks, so the ones in the bottom know only the detail they need."
        show mei uni default2 at midleft with dissolve
        mei "We would need to capture the one in the higher rank to get into the leader."
        any "Yes."
        any "The Crow Bar in the port, however, called and told me that he saw a few people arranged a meeting around the place."
        any uni frown "He doesn't want to discuss it further over the phone and told me to come and see on our own."
        
        mei uni frown2 "Did you agree?"
        any uni default "I gave him a vague answer. I need to ask you first."
        any uni frown "I don't have a good feeling about this, but it might lead us somewhere."
        any uni default "So, what do you say about it?"
        
        menu:
            "I think it's worth the risk.":
                jump worthrisk
            "We should scout the surroundings first.":
                jump scoutfirst
                
    label worthrisk:
        $ rhodo_pt += 1
        hide mei with dissolve
        show rhodo uni default at midleft with dissolve
        rho "I agree with Schnee."
        rho uni smile "Father said sometimes we gotta take the risk to move forward."
        rho "I think it's one of those times."
        
        any uni smile "Well said."
        any uni default "Ulm? What about you?"
        hide rhodo with dissolve
        show ulm uni surprised at midleft with dissolve
        ulm "Oh, uhm ...."
        ulm uni thinking "{size=15}I'm not against taking action, but ... shouldn't we prepare ourselves by scouting the place?{/size}"
        any uni smirk "I'm counting on you then."
        ulm uni smile "I'll do what I can."
        hide ulm with dissolve
        hide anya with dissolve
        "... I'm ashamed of myself because I entirely forgot to take the importance of surveying the vicinity into consideration."
        "No doubt, there's part of me craving for 'real' combat rather than scoutings or investigations."
        "I don't know for sure whether it's good or bad."
        
        jump endofchoice10
        
    label scoutfirst:
        $ ulm_pt += 1
        hide mei with dissolve
        show ulm uni smile at midleft with dissolve
        ulm "It's a wise thinking, Miss Schnee."
        ulm "Allow me to do the scouting."
        ulm "Even though we're about to enter an unsafe area, it'll be better if we know what to expect."
        
        any uni smile "I'm counting on you then."
        any uni default "And, Rho? What about you?"
        hide ulm with dissolve
        show rhodo uni smile at midleft with dissolve
        rho "Ready for some action, Ma'am!"
        rho uni default "I kind of expecting a fight or two, but ... I know you don't really want it?"
        any "Make sure you prepare yourself for one."
        hide rhodo with dissolve
        hide anya with dissolve
        "When I think about it again, the thoughts that we might engage in a fight makes me anxious."
        "Fighting people would be so much different than fighting Grimms ...."
        "We did participate in several showcase matches against other teams in school, but I'm not really confident with the experience I got from them."
        
        jump endofchoice10
    
    label endofchoice10:
        show anya uni default at mcenter with dissolve
        any "Alright then ...."
        any uni smile "Glad to hear we agree on this matter."
        any uni default "I'll take care the permission for tomorrow. In the meantime, get yourself ready."
        rho "Yes, Ma'am."
        ulm "Okay."
        mei "Understood."
        any uni smile "That's all from me."
        any uni default "And, Ulm?"
        hide anya with dissolve
        
        show ulm uni surprised at midleft with dissolve
        ulm "Y-yes?"
        "Ulm straightens his back, as if he is about to be inspected. Anya waves the teams' ranking notes."
        show anya uni smile at midright with dissolve
        any "Thank you very much for your help."
        ulm "Oh--that's--"
        ulm uni smile "You're welcome."
        hide ulm with dissolve
        hide anya with dissolve
        stop music fadeout 5.0
        scene black with fade
        call chapter("End of Part 6", "The Chase") from _call_chapter_17
        jump part7B
        
#label endpt6:
    
    #call chapter("End of Part 6", "The Chase") from _call_chapter_4
    #centered "End of Part 6 - The Chase"
    #centered "End of Demo Version 0.6\nThank you for playing!"
    
#return