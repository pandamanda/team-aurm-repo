﻿# PART 4: ENTANGLED #

label part4:
    #$ mei_outfit = "default"
    
    "I wake up with excruciating pain in my head."
    mei "Ugh ..."
    "Both my neck and shoulder are stiff too. Did I sleep in wrong position last night?"
    
    scene bg outpostday with fade
    "I move my hand to rub my head, but then I realize ...."
    "I can't move my hand."
    "My hands are ... tied? Behind my back?"
    mei "Huh ... What ...?"
    play music strange fadein 10.0
    "I have to struggle a little before I could sit."
    "It's already morning and the sun shines bright, no hint of any heavy rain except for the scent of wet earth."
    "I'm inside the outpost where we spent our first night and that leads to the next question: "
    extend "'How did I end up here?'"
    "The last thing I remembered is we were in the abandoned tunnel ... "
    
    extend "Malfunction androids ... "
    extend "And--WHAM!--something hit my head."
    "Ah, that must be the cause of this pain."
    "But it doesn't explain why am I tied like this."
    "I could try untying myself if only the rope ties around my wrists instead of my thumbs."
    mei "Anya? Rhodo?"
    "I hear some sort of low grunt from the furthest part of the room. But nothing more."
    mei "Anya?"
    mei "Rhodo?"
    mei "Guys?"
    "If this is a prank, this is not funny."
    
    "If this is not a prank, this is ... unsettling."
    "I look around to locate my dagger. Maybe I can cut the ties with it."
    "Problem is, someone has removed Alpenaster and its sheath from my belt."
    play sound foot
    "Footsteps sound distracts me from my current objective."
    "I turn around to face the outpost door."
    
    
    show rhodo arm default2 at mcenter with dissolve
    rho "Anya? I fin--"
    rho surprised "Whoa! "
    rho "You ... awake?"
    "Rhodo takes a few steps away, holding his rifle-spear in a what I would call a 'weak stance'."
    "'Weak stance' as in he's anticipating hostility, but reluctant to counter-attack at the same time. It doesn't like the usual Rhodo."
    mei "What ... happened?"
    
    mei "Why am I tied like this?"
    rho "Uhh ..."
    rho arm default2 "You're not going to attack me, aren't you, Schnee?"
    mei "???"
    mei "Why am I going to attack you???"
    
    rho arm surprised "Oh, okay ..."
    rho "I need to check on Anya first."
    mei "First thing first, how about untying me?"
    rho arm default2 "You have to wait for that."
    mei "Wait for what???"
    hide rhodo with dissolve
    "Not answering my question, Rhodo walks to the back of the outpost, to the corner where there are shelf and wooden crates."
    
    "I could hear him calling Anya once again and talking with low volume, before going back to meet me again."
    show rhodo nor default2 at mcenter with dissolve
    mei "Is Anya there? Why she didn't answer when I call her earlier?"
    rho "She took painkillers and it made her sleepy."
    rho "I wake her up just now. Need a minute before she's fully awake."
    "I ... don't understand."
    "I'm pretty sure I missed a huge chunk of what has happened."
    mei "Anya's ... injured?"
    mei "{i}What has happened?{/i}"
    rho nor surprised "...."
    rho "You really don't remember?"
    rho "Nothing {i}at all{/i}?"
    mei "The only thing I remember is we were in abandoned tunnel, I searched to the deeper part of the tunnel, stray androids attacked me, and something hit me on the head."
    mei "That's all."
    
    rho "...."
    mei "I don't know what happened after that."
    "Instead of pissed off by Rhodo's silence and confusion looks, I'm ... {i}afraid{/i}."
    rho "I ... I don't know how should I say this ...."
    rho nor confused "Okay, okay, no need to yell at me for dragging."
    rho nor default2 "I'll just ... tell you what happened. To us, at least."
    stop music fadeout 5.0
    hide rhodo with dissolve
    
    scene bg tunnelsepia with fade
    play music rain fadein 10.0
    # sepia flashback mask
    show rhodo nor frown s at midleft with dissolve
    rho "Arrrggh, the fire's out again!"
    rho nor shout s "Damnit, wind! Stop blowing, will ya?!"
    rho "Or blow other way! Stop pissing me off! Damnit!"
    # Scroll message sfx
    play sound message
    show anya nor default s at midright with dissolve
    any "... "
    extend "Hmm, I see."
    show rhodo nor default2 s with cdissolve
    any "Mei said she found bigger room inside and ask us to go there."
    rho nor default s "Sounds like a good idea. At least I can keep the fire burning in there."
    rho nor confused s "Hopefully."
    any nor smile s "It will be warmer inside, for sure. Let's go."
    hide anya with dissolve
    hide rhodo with dissolve
    stop music fadeout 5.0
    
    scene black with fade
    centered "{i}So, we walked to the room you mentioned.{/i}"
    centered "{i}And, well, uhm ... You attacked us.{/i}"
    centered "{i}You looked, I don't know, angry? Not friendly?{/i}"
    centered "{i}You treated us like real enemies.{/i}"
    centered "{i}Anya and I didn't understand why you did that.{/i}"
    centered "{i}You didn't respond to our call too.{/i}"
    centered "{i}What we know was it got real serious when you summoned your knight.{/i}"
    
    scene bg tunnelsepia with fade
    # sepia flashback mask
    # play sound clash
    
    show rhodo arm surprised s at midright with dissolve
    rho "Ugh ...."
    show anya arm shout s at midleft with dissolve
    any "Rho! Are you okay?"
    rho arm frown s "No, Ma'am .... Not sure I'm okay ...."
    any "Can you--"
    play sound clash
    
    show anya arm pain s
    show rhodo arm surprised s
    with cdissolve
    
    any "Gah!" with vpunch
    # probably could be change to slash image effect
    any arm arm frown s "What's wrong with her ...?"
    any arm arm shout s "I'll keep Mei busy! Go around and knock her out!"
    # play sound clash
    # probably could be change to slash image effect(2)
    any "NOW!"
    hide rhodo
    hide anya
    with qdissolve
    
    scene black with fade
    rho "I'm at fault for your headache, you can say that ...."
    rho "It's the fastest way I could think of."
    
    scene bg outpostday with fade
    
    "I'm at loss of words."
    "It doesn't sound real."
    play music mirror fadein 10.0
    "Back in the tunnel, what I saw was {i}two security androids{/i}. Not Anya and Rhodo."
    "Is that why those androids didn't attack me and kept defending?"
    "Because they were Anya and Rhodo?"
    "I ... I don't understand ...."
    "Why ... how--?"
    "And then I recalled what I did to the last 'android' who fought me."
    "I grabbed its hand with the knight and threw it to the ceiling."
    "No ...."
    
    "No, "
    extend "no, "
    extend "no."
    "What have I done?!"
    "My voice is shaking when I brace myself to ask the question."
    mei "Anya ... how is she ...?"
    "Oh my God .... How bad did I hurt her?"
    mei "I didn't .... I didn't ...."
    any "I can assure you that I still have my limbs."
    "A silhoutte rises and moves from the other side of the room."
    stop music fadeout 5.0
    
    scene cg anyawound with fade
    "It's Anya."
    "Her right arm is in a makeshift sling made out of her jacket. Her shoulder is swelling."
    "And she looks ... awful."
    "No matter how she try to put a grin on her face, she couldn't hide the pain she has to endure."
    
    any "I know some first aid trick, but apparently I don't know how to fix a dislocated shoulder."
    rho "Well, don't!"
    rho "You promised me not to pull anything strange to fix your shoulder while I'm away!"
    rho "The medic should be on their way now. Just let them do their work. They know what to do."
    mei "Anya ...."
    mei "I'm sorry."
    mei "I'm really sorry ...."
    "I grit my teeth. Guilt swells up inside my chest, suffocating me."
    
    mei "I ... I don't know what had gotten into me ...."
    mei "I'm sorry .... {size=15}I'm sorry ...{/size}"
    any "..."
    any "Rhodo, help me untie her, will you?"
    rho "Are you sure about that?"
    rho "I mean, what if ...."
    any "She looks sane enough for me, so I trust her."
    any "Besides, you already finished patrolling, right? Stand by here until the ship come."
    rho "Well, if you say so ...."
    
    scene bg outpostday with fade
    show anya nor frown at midright with dissolve
    any "Mei, I'm really sorry, but we won't go easy if you attack us again since we have no idea what happened to you."
    show mei nor sad at midleft with dissolve
    mei "I understand."
    mei "{size=15}I'm sorry .... I'm sorry .... I'm sorry ....{/size}"
    "Anya taps my head lightly with her knuckles."
    any nor smirk "I hear you, Mei."
    
    mei "You're not angry? After what I did?"
    any nor sigh "I'm more confused than angry."
    any nor pain "And ... in pain."
    any "I'm not trying to make you feel guilty, but this {i}is{/i} painful."
    any nor frown "Maybe I should take more painkillers--"
    hide mei with dissolve
    
    show rhodo nor shout at midleft with dissolve
    rho "No! You wanna kill yourself or what?!"
    rho "Lay your hands on the pills and I'm gonna knock you down!"
    any nor sigh "Okay, okay ... I won't."
    hide anya
    hide rhodo
    with dissolve
    
    "The requested help from Beacon Academy arrives about two hours later."
    "When she is asked about what happened, Anya lies about it, telling the staff that she fell from a cliff and her arms got stuck during the fall."
    "The staff doesn't look so convinced, probably because Anya's wound doesn't match her description, but Anya just shrugs."
    any "You guys wanna fix me or not? That's the question."
    "Before we are going back to Beacon, the medic staff 'fix' Anya's shoulder."
    "Rhodo and I don't see the process clearly, but we both wince upon hearing Anya sharp cries."
    "No need to look at it to say it's a painful 'fix' Anya went through."
    
    scene bg walkway with fade
    "Back in Beacon, I thought Anya would drag me to explain what had happened to Professor Goodwitch, who is responsible for missions and assignment."
    "She doesn't."
    play music confusion fadein 10.0
    show anya nor frown at midright with dissolve
    any "I don't feel really good right now. I'll tell her that I'll explain what had happened tomorrow."
    show rhodo nor default2 at midleft with dissolve
    rho "She won't be too happy about it."
    
    any "Well, this is Professor Goodwitch we're talking about."
    any nor sigh "When did you see her 'happy'?"
    rho nor confused "Uhh ... never?"
    any nor default "Right."
    any nor smirk "Never."
    hide anya with dissolve
    hide rhodo with dissolve
    "Just as we suspected, Professor Goodwitch isn't satisfied with what we said, but she eventually let us go."
    
    scene bg corridor with fade
    "Anya goes to the infirmary, mumbling something about 'painkiller' again that makes Rhodo glaring at her."
    show rhodo nor shout at mcenter with dissolve
    rho "Get the nurse prescribe something for you! Don't go around taking some random pills by yourself!"
    any "Will do, will do."
    rho "And if you have any allergy, tell them too."
    "It's really unusual to see Rhodo got worked up over something crucial like medication."
    
    "With his Crimson Aries daily intake, I even think Rhodo is reckless when it comes to what he takes."
    "Maybe ...."
    "Maybe the whole accident agitated him ...."
    show rhodo nor default2 at midright with dissolve
    show mei nor frown2 at midleft with dissolve
    mei "Are you okay?"
    rho nor surprised "Huh? Me?"
    
    rho "I should be the one who ask that question to you."
    rho "Are {i}you{/i} okay?"
    mei nor sad "I don't think so ..."
    mei "I still don't understand what happened ..."
    mei nor default2 "Speaking of it, are you hurt?"
    mei "What did I do to you?"
    
    rho nor default "Ooh, not really bad compared to Anya. Your knight punched me and threw me to the wall."
    rho nor smile "I blocked the attack. It's not that bad. Really. My back was bruised, but it's fine now."
    show mei nor sad with cdissolve
    "While I saw Anya as the android with baton, Rhodo, apparently, was the android with shield."
    show rhodo nor default with cdissolve
    "What happened to me back then?"
    "Was that hallucination?"
    
    "Like what happened to Saul from team SKYE?"
    rho nor default2 "Schnee? You okay?"
    mei "I ... I need some time ...."
    mei "Thinking. Alone."
    mei "I'm sorry, but could you leave me for now?"
    rho nor surprised "Uhh, sure?"
    mei "I'm sorry."
    rho nor smile "No, really, that's okay."
    
    rho nor default "Just don't go out of the school area. I'll tell Anya when she comes back later."
    mei nor default "Thank you."
    mei nor sad "And sorry."
    rho "Yeah. No problem."
    rho nor default2 "Okay then."
    rho "See you later, Schnee."
    hide rhodo with dissolve
    hide mei with dissolve
    
    scene bg amphitheater with fade
    "I don't see anyone when entering the amphitheater."
    "With my sight locked on one of the holographic screen that shows team ranking, I climb the stairs on the spectators balcony."
    "Anya acts as if the incident isn't something big."
    "But I know there is {i}no way{/i} she would forgive me after what I did."
    "I could feel that Rhodo is {i}afraid{/i} of me now ...."
    "He probably try his best to act normal around me, but he couldn't."
    "I appreciate his effort, however."
    "...."
    
    stop music fadeout 5.0
    "I don't know. "
    extend "I really don't know. "
    extend "I couldn't think of anything."
    "I have no idea how I could fix this."
    "For I don't know how long, I stay in the amphitheater."
    "Fortunately, nobody comes here."
    play sound ringtone loop fadein 5.0
    show mei nor frown2 at midleft with dissolve
    mei "?"
    # show up Scroll device image in the middle
    "I reach for my Scroll, wondering who is calling at this moment."
    "And I immediately swipe the interface to answer the call after seeing the name on the screen."
    
    stop sound
    mei nor default2 "Hello ... "
    play music roses fadein 25.0
    mei nor default "Mom."
    show mei nor surprised with cdissolve
    "Uh-oh. I hope Mom doesn't notice my tone."
    "I couldn't force myself to sound cheerful right now."
    show mei nor frown2 with cdissolve
    "Oh, but I think I could at least trying to sound less gloomy."
    "I hope she doesn't notice."
    rby "Mei!"
    rby "Are you okay???"
    "?"
    
    "It takes me a few seconds to process what she said."
    "I especially confused about why her first question is 'Are you okay?'."
    "I did tell her two days ago about my mission to Forever Fall, but I haven't told her that I come back early to school because of the unfortunate events."
    "Besides ... She sounds panicked."
    "It's as if Mom already knew that our mission went horribly wrong and I'm the one to blame for it."
    mei nor default2 "I'm ... okay."
    rby "You sure? Like, double sure?"
    show mei nor frown with cdissolve
    "Now I'm suspicious. Like, {i}double suspicious{/i}."
    mei "Mom, why are you calling, by the way?"
    rby "Huh?"
    
    mei nor frown2 "I mean, how do you know I've come back from the mission?"
    mei "I haven't told you anything."
    "There is a brief silence before she speaks again, this time with a tone that makes me imagining a sad puppy."
    rby "Can't I call my own daughter?"
    rby "I just wanna know how are you doing."
    rby "Why are you so cold to me? Now I'm sad ...."
    rby "Super sad ...."
    mei nor surprised "No, no, no, no, no! "
    mei "I don't mean it that way!"
    "Ruby Rose, one of the most renowned huntress Remnant ever have, is now making the worst fake sobbing one could do."
    
    mei nor frown "Mom ...."
    mei "Forget I asked."
    mei nor default "I'm okay. And I'm sure--double sure--that I'm okay."
    show mei nor sad with cdissolve
    "Which is a {i}double lie{/i}."
    "I blame Mom for this excessive usage of the word 'double'."
    mei nor default2 "...."
    mei nor sad "No ...."
    hide mei with dissolve
    
    scene cg rubycall0 with fade
    mei "I'm sorry, Mom, but I don't feel okay right now."
    mei "Everything's just ... wrong."
    mei "And I don't know where to start ...."
    
    scene cg rubycall with fade
    rby "You can start from the beginning."
    rby "Mind if I ask you what happened?"
    rby "Is it about the mission?"
    mei "Yeah ...."
    "I bit my lips before continue."
    
    mei "I ... attacked my own teammates."
    mei "We were in one of the abandoned train tunnel in Forever Fall, taking shelter from a sudden rain."
    mei "I explored to the deeper side of the tunnel and met someone that also took shelter in there."
    mei "We were to go back to meet with the others, when ...."
    "I pinch the bridge of my nose and shake my head."
    mei "I saw two security androids. Those things that usually deployed in the distribution train."
    rby "Oh, yeah. I know them."
    
    rby "Did the androids attack you?"
    mei "No. I attacked them first."
    mei "And later, I found out those androids were my own teammates."
    "I swallow, couldn't help not remembering the incident with Saul from team SKYE."
    "...."
    
    "Did I somehow unknowingly take the Golden Apple which led to the incident?"
    "Now I'm more than angry to whoever distribute the foul drugs in the school."
    rby "Mei, do you remember the person you met in the tunnel?"
    rby "What did she or he look like?"
    "Now that Mom mention it ...."
    "I don't know what happened to the mysterious person after I fell unconscious."
    
    "The fact that I attacked my own teammates is taking too much space in my mind."
    "That woman might see what truly happened back then."
    mei "Um ...."
    mei "She is a woman ...."
    mei "Older than us, but I couldn't say how old is she to be exact."
    mei "She has short, pale green-ish colored hair, dark skin, red eyes."
    "There is a silence for a couple seconds, long enough to make me think the connection is disrupted."
    
    scene bg amphitheater with fade
    show mei nor default2 at midleft with dissolve
    mei "Mom?"
    rby "If you ever see her again, Mei, avoid her at all cost."
    mei nor frown2 "You know her?"
    rby "Yes. I {i}know{/i} her."
    rby "I can even say we {i}were{/i} friend. Or, I thought we were one."
    rby "She's Emerald Sustrai."
    
    show mei nor default2 with cdissolve
    "Mom sounds angry."
    "That kind of anger when talking about someone that have done unforgiven deed in the past that we try to forget, but we just couldn't."
    "An old 'scar' that is forgotten until it hurt when someone touch it."
    "Mom has more than one."
    show mei nor sad with cdissolve
    "I was once told that Mom was a super-cheerful person that didn't aware there are people with foul intentions around her."
    "It isn't clear what event changed her--everyone kept this detail from me--but now Mom is way more careful and wary than in her youth."
    mei nor frown2 "Is she dangerous?"
    rby "Oh, she {i}{b}is{/b}{/i} dangerous."
    
    rby "Her Semblance is making people seeing what isn't really happening."
    rby "I'm not sure is it causing hallucination or something else, but that's her Semblance."
    mei nor surprised "Wait--"
    mei "You mean she might be the one who ...?"
    rby "Making you attacked your teammates, yeah."
    mei "But why?"
    
    mei nor angry "I did nothing to her!"
    rby "As far as I know ... she's not the kind who attacks people for fun."
    rby "She always has reason. Or working under other people."
    show mei nor default2 with cdissolve
    rby "... Wait ...."
    rby "You didn't do anything to her ...."
    
    rby "But what if you almost do something to ... other?"
    mei nor frown2 "I didn't see anyone else beside her in there."
    rby "Did you explore the whole tunnel? Every corner?"
    mei "Err, no."
    rby "There's a chance whatever Emerald try to keep you away from is somewhere deeper in the tunnel."
    rby "You hadn't found it. But almost."
    show mei nor surprised with cdissolve
    "?!"
    "My thoughts immediately jump right to the Golden Apple brewery/stash upon hearing it."
    "{i}Could it be?{/i}"
    
    mei nor angry "Then I have to go back there--"
    rby "No!"
    rby "You will not go back there, Mei. Why do you think you {i}have to{/i}?"
    rby "Because she made you attacked your friends?"
    rby "Or, because you have something else you don't tell me?"
    show mei nor frown2 with cdissolve
    
    "If Mom knows anything about me pursuing the Golden Apple, I would yell at Roman."
    show mei nor frown with cdissolve
    "It must be him who told Mom. Who else?"
    mei nor angry "But we have to do something about it!"
    mei "You're the one who told me to take action if it's necessary to do so!"
    "I could hear Mom groans on the other side."
    "We both already know how stubborn each of us could be."
    
    show mei nor frown2 with cdissolve
    rby "*sigh*"
    rby "Okay ... I should've known better than anyone else that it's never easy to tell you to back off something you've set your eyes on."
    rby "And you're no kids anymore, so ... it's just stupid for me to try to scare you off."
    rby "By that, you know I don't intend to scare you off."
    rby "*sigh*"
    
    rby "Emerald Sustrai ... worked for Cinder Fall in the past."
    hide mei with dissolve
    stop music fadeout 5.0
    "I could say and insist that hearing the name 'Cinder Fall' doesn't scare me even the slightest, but it is a lie."
    "My hands shake uncontrollably. My Scroll device slips from my fingers and falls on the floor."
    # flickering flashback; CG
    "Cinder Fall ...."
    
    "{i}That incident {/i}...."
    "Cinder Fall was the woman who managed to trick, overpower me, and even though she failed to get what she originally wanted, she stole my Semblance and almost killed me with it."
    "It happened in the middle of my 2nd semester at Beacon, about a year ago."
    "I should have forgotten it by now, but I couldn't."
    "It was my first time feeling the fear of death."
    
    "It was my first time realizing how helpless I am without my Semblance."
    "It was the first time the painful reality of being lost and weak hit me."
    "It might not look like it from the outside, but that incident affected me so much."
    "It left behind an invisible scar I don't know when or how to heal."
    "I grit my teeth and grab back my Scroll. Mom has been calling me for a moment now and she grows worry."
    show mei nor sad at midleft with dissolve
    mei "I'm sorry."
    mei "I dropped my Scroll."
    
    rby "Oh ... okay."
    rby "I'm sorry. I didn't mean to."
    mei nor frown2 "But you said Cinder is no longer around?"
    mei "I mean, you defeated her ...."
    mei "She has gone, right?"
    rby "It seemed like it. I gotta admit I'm not sure if she's already gone for good or not."
    rby "From our past experience, Cinder isn't easily defeated."
    rby "She's ... what is it called? An anomaly in the system?"
    rby "So, yeah, please understand. I know it's not easy to stay back when you sense something's wrong."
    rby "Uhh, well, you can say I've been through something similar to it."
    
    play music roses fadein 30.0
    rby "You gotta understand, sweetheart, not everything happened is your responsibility."
    rby "This is not a game of Heroes and Monsters."
    mei nor sad "I know."
    mei "I still hate it if there's nothing I could do."
    rby "Well ...."
    rby "You're smarter than me. Think about it."
    
    rby "How many times did you trick me around the house and sneak to Aunt Winter's place?"
    "I really couldn't help not to smile."
    mei nor default "A dozen times. Perhaps more."
    rby "I'm still not allowing you to go recklessly investigate this matter, alright?"
    show mei nor default2 with cdissolve
    rby "One word from Professor Goodwitch about this and you're done for."
    
    rby "No cookies for you when you're home."
    mei nor frown "Hmph! Why do you think zero availability of cookies sound like a threat for me?"
    # emoticon balloon?
    "My actual response: "
    show mei nor surprised with cdissolve
    extend "NOOOOOOOOOOO!!! Anything but cookies!!!"
    show mei nor frown with cdissolve
    rby "Fine! I'll bake you cookies, but I'll change the sugar to ..."
    rby "{i}{b}SALT{/b}{/i}."
    rby "Your move now~"
    show mei nor surprised with cdissolve
    
    "Now this is a serious matter of health. Does Mom ever think about how hazardous too many salt in human body??? (Not that too many sugar isn't hazardous, though.)"
    mei nor frown2 "Okay, okay!"
    mei "I hear you!"
    mei nor default2 "'I won't go recklessly investigate this matter'."
    
    mei nor frown "There. Convincing enough?"
    show mei nor default2 with cdissolve
    rby "Deal."
    rby "And not just from Professor Goodwitch!"
    rby "Any other professors too! "
    rby "And your team leader! Or your teammates!"
    rby "One word from any of them and you'll spend your days at home being salty of salty cookies."
    mei nor frown "You know what? I could just run away to Aunt Winter's house and stay there, saying I'm a refugee from the tyranny of Ruby Rose."
    "..."
    extend "..."
    extend "..."
    
    show mei nor default with cdissolve
    "I can imagine Mom is pouting."
    mei nor smile "Just kidding. I'm not going to do that."
    mei nor frown "And 'being salty of salty cookies' is lame, Mom. I believe Aunt Yang could do a better joke than you."
    show mei nor default with cdissolve
    rby "No, she can't! Yang is {i}never{/i} funny! Neveeeeeerrrrr!"
    rby "N-E-V-E-R!"
    "In contrary, I find Aunt Yang to be one of the funniest person around me. I couldn't stop laughing at her joke."
    mei nor default2 "By the way ...."
    mei nor default "Thank you for calling."
    mei "I feel better now."
    rby "Glad to hear that."
    rby "Give me a call anytime you need to talk to someone, sweetie."
    rby "Uhm ... yea, I don't always answer phone, so you might want to drop a message or two."
    
    rby "I'm sorry. Sometimes my works are unpredictable."
    mei nor smile "I know, I know."
    mei nor default "I'll message you again later then."
    rby "Great."
    rby "Be careful and be a good girl, sweetie."
    mei "I would."
    mei "Thank you again, Mom."
    rby "No problem."
    hide mei with dissolve
    stop music fadeout 5.0
    "We end our conversation there."
    "I feel way more better after talking to Mom."
    
    "She is childish, a ridiculous person despite of her age, and her work as a huntress occupy most of her time, but Mom ... is a mother."
    "She isn't perfect, but she does her best."
    "And I'm always proud of her. Not just because she's a great huntress, but also because she's a great mother."
    "Now ... she did forbid me from chasing the woman I met in Forever Fall."
    "Mom has a solid reason about it, which I agree."
    "I'm not that dumb to follow a cliche pattern about going against what parents said, getting into trouble, being saved, and leading everyone in unnecessary danger."
    "Why should I?"
    "I prefer proving myself able to look for another way to approach this matter at hand."
    "Thinking. "
    extend "Planning. "
    extend "That's more like it."
    "Now I think it's time for me to leave the amphitheater."

    play music casual fadein 10.0
    
    menu:
        "Back to team AURM's room":
            jump aurmroom
        "Walk around school vicinity":
            jump vicinity
    
label aurmroom:
    "I think the best option for now would be ... talking to Anya about this."
    "It would be awkward. I do have an explanation now about the accident, but, still, it would be awkward."
    "I take a deep breath and stand up, leaving the amphitheater and going to the dorm."
    
    scene bg dormday with fade
    "I wouldn't be surprised if Anya hasn't came back to our room."
    "She probably needs to spend more time in the infirmary due to her injury."
    "But, Anya is in the room. On the other hand, I don't see Rhodo and Ulm."
    "Anya is twirling a short wooden stick with her left hand. Her right hand is still bundled in a sling--a proper one, not the makeshift one."
    stop music fadeout 5.0
    
    show mei nor frown2 at midleft with dissolve
    mei "What ... are you doing?"
    show anya nor default at midright with dissolve
    any "Haven't used my left hand for a while, so I think I do a little warm up just to get used to it again."
    
    mei "But your wound?"
    any "Will heal."
    show anya nor pain with cdissolve
    "However, Anya grimaces."
    any "They gave me a shot and now it's numb. No pain, though."
    mei nor sad "You need to get some rest, Anya ..."
    mei "Don't work yourself too hard."
    show anya nor default with cdissolve
    show mei nor frown2 with cdissolve
    "Anya looks at me for a moment and then smiles."
    play music cocky fadein 10.0
    show anya nor smirk with cdissolve
    "That crooked smile that reminds me of a sly fox ...."
    show mei nor frown with cdissolve
    "A fox with pink fur, for Anya's case."
    "Despite of the cute factor of having pink fur, one cannot dismiss the 'sly' part of a 'sly fox'."
    "Anya thinks of something. Probably something to tease me with."
    
    "Of all the times, Anya ...."
    mei "What?"
    mei "I'm not in the mood of playing around."
    any "Yeah, I know."
    any "So ... I met Rhodo when going back here. He said you're off somewhere."
    any "Wherever you've been, you seems ... lighten up a bit?"
    any nor smile "Something's up?"
    mei nor frown2 "Can I hide anything from you?"
    any "Oh, yes, you can."
    any nor smirk "But, I'll keep learning how to get through."
    mei nor sad "*sigh*"
    
    stop music fadeout 5.0
    "I sit on my bed."
    show anya nor default with cdissolve
    mei nor default2 "Mom called."
    mei "She somehow know there's something wrong. I told her what happened."
    any nor smile "Mother's instinct, as sharp as they said."
    mei nor default "Yeah."
    mei nor default2 "Mom ... knows the person I met in the tunnel."
    play music confusion fadein 10.0
    any nor shout "???"
    any "You were with someone else back then? I don't remember seeing you with another person."
    show mei nor surprised with cdissolve
    "That startles me. "
    "Is even 'Emerald Sustrai' only my imagination?"
    show mei nor frown2 with cdissolve
    "But ... no, no, no."
    
    "{i}Calm down, Mei.{/i}"
    show anya nor default with cdissolve
    mei "There {i}is{/i} another person in there."
    mei "She isn't someone I've met before, but when I mentioned her to Mom, she recognized her."
    mei "There's no way I imagined or made things up."
    any "Right. Points taken."
    mei "She--Emerald Sustrai--has the ability to distort what is seen."
    mei "Her Semblance could do that."
    "I swallow nervously."
    "I do have an explanation with what had happened, but ... blaming someone my teammates didn't even see doesn't sound acceptable nor convincing enough."
    mei "... It's okay if you don't believe me."
    
    mei nor sad "I don't really expect you to."
    any "Hey, not saying anything doesn't mean I don't believe you."
    any nor sigh "Too many things happened at once. I'm getting slow here."
    any "Gimme a break and let me think."
    mei nor default2 "Oh. Right. "
    mei nor sad "I'm sorry."
    "Of course Anya needs a break. I went a little overboard by assuming she didn't believe me a moment ago."
    "It's just ... I'm both excited to find a new insight about the accident, but also still feel guilty with my action back then."
    "It's weird, I know."
    any nor frown "It sounds familiar ...."
    any "Jog my memory a bit, Mei. Did I say something similar to this to you guys?"
    
    menu:
        "Yes. The huntress' case.":
            jump yhuntress
        "Not really ...":
            jump nhuntress

label yhuntress:
    $ anya_pt += 1
    any nor shout "... You're right."
    any "The huntress' teammate attacked her opponent after she won the round, claiming that her opponent attacked first."
    any nor frown "Is it possible that the culprit was the same person ...?"
    jump endhuntress
    
label nhuntress:
    any nor sigh "I probably remember later."
    anya nor frown "Hopefully."
    jump endhuntress
    
label endhuntress:
    any nor default "By the way, if you don't mind, I think I'll get some sleep before dinner."
    any "Don't make any trouble while I'm sleeping."
    any nor smirk "Or starting another childish argument with Rhodo."
    mei nor angry "We wouldn't! "
    mei nor frown2 "Well, at least, {i}I{/i} wouldn't!"
    any "Great--"
    stop music fadeout 5.0
    hide mei
    hide anya
    with dissolve
    
    play sound door
    "But before Anya settles in her bed, the door clicks open."
    "It's Rhodo and Ulm."
    ulm "Oh! You're here."
    ulm "Anya? Are you okay ...?"
    rho "Ulm found out something, so he said."
    rho "I dunno what is it, but he said it's quite big."
    rho "And a secret."
    "Anya's response for Rhodo is a long sigh."
    
    jump endofchoice7
    
label vicinity:
    "I think I can find Anya in the infirmary."
    "Ah, right, I forgot to store Alpenaster in the locker."
    "If not deployed in a mission or not in a training session, students are prohibited from carrying their weapon around the school."
    "It's common sense, since not all students practice no violence policy when engaging in conflict."
    "But, I heard from Mom that Professor Goodwitch once arranged a mock battle between Mom against a student in her year who made trouble in the dining hall."
    "I kind of admire Professor Goodwitch's effort."
    "Mon won the match, by the way. "
    extend "Yes, she's that good even when she was a student."
    "So, where should I go now?"
    
    menu:
        "Infirmary.":
            jump infirmary
        "Locker room.":
            jump locker
            
label infirmary:
    $ ulm_pt += 1
    scene bg corridor with fade
    "I walk to the infirmary."
    "And, guess what, Anya isn't there."
    "She already left and since I was in the amphitheatre and she is going back to our room, we didn't cross path."
    "I am about to leave when someone run in the hallway. The nurse yell at him."
    "I recognize the voice replying the nurse and catch up before he reaches the end of the corridor."
    mei "Ulm?"
    show ulm nor smile at mcenter with dissolve
    ulm "Oh? Miss Schnee?"
    ulm "I thought you'll be back tomorrow morning?"
    
    mei "*sigh* "
    show ulm nor smile at midleft with dissolve
    show mei nor default2 at midright with dissolve
    mei "You haven't heard anything then, I suppose?"
    ulm "Nothing about your early comeback."
    ulm nor surprised "Well ... I spend the entire morning working on my detention."
    stop music fadeout 5.0
    
    ulm nor default "...."
    ulm "I can tell something isn't right."
    ulm nor reluctant "{size=15}But I don't think I should ask.{/size}"
    mei nor default "I can hear you, Ulm."
    ulm nor surprised "Erm ... Sorry. {size=15}I'm sorry.{/size}"
    "Remembering how Ulm has been going through similar situation as mine, I feel it would be easier for me to tell him about what happened."
    mei nor default2 "I'll fill you in with everything you've missed."
    
    mei "Just not here. Come on."
    hide mei with dissolve
    hide ulm with dissolve
    
    scene bg walkway with fade
    "I tell Ulm everything I know--at least from my point of view--and add the new lead I got from Mom."
    "He looks seriously thinking about it by the end of my explanation."
    play music logic fadein 10.0
    show ulm nor thinking at midleft with dissolve
    ulm "It's ... an interesting Semblance, for sure."
    show mei nor frown at midright with dissolve
    mei "I'm not going to approve your praise right now."
    ulm nor surprised "I-I'm sorry! I don't mean to! I'm really sorry, Miss Schnee!"
    mei nor default2 "It's okay."
    
    mei nor default "I believe you already have a thought or two about that unfortunate accident."
    mei "Especially about Emerald Sustrai's Semblance?"
    ulm nor default "Yes. "
    ulm nor thinking "It's fascinating. "
    ulm nor reluctant "And will be troublesome if we were to face her in the future."
    ulm nor thinking "I have to figure out how to possibly handle this Semblance."
    mei "That would be a great advantage for us."
    mei nor smile "Thank you, Ulm."
    ulm nor surprised "Eh? Y-yes ...."
    
    ulm "{size=15}You're welcome.{/size}"
    mei nor default "By the way ...."
    mei nor frown2 "Is everything okay when we're away?"
    mei "I mean, did you get into trouble with 'those third years' or anyone else?"
    ulm nor thinking "Fortunately, my detention keeps me in either Professor Goodwitch or Professor Oobleck's office most the time, so I didn't meet any third years."
    ulm nor smile "Oh, that's right!"
    ulm "I got something really important to tell you guys!"
    mei nor default2 "What is it?"
    ulm "It's ... confidential, I think."
    ulm "Do you mind if we talk about it in the room? With Anya and Rhodo too?"
    mei "Well, I looked for Anya in the infirmary and she wasn't there. High chance she already went back to our room."
    
    mei nor default "Let's go."
    stop music fadeout 5.0
    hide mei
    hide ulm
    with dissolve
    "There's a significant change on Ulm's expression when he said that he had something important to tell us."
    "He looks excited, which is good considering how depressed he was before the three of us went for the mission."
    scene bg corridor with fade
    "I don't know whether Ulm messaged Rhodo to come or it's simply a coincidence, but when we arrive Rhodo has just unlocked the door with his Scroll device."
    play sound door
    scene bg dormday with fade
    "Anya is there, sitting on her bed. Her injured arm is bound in a sling, a clean and proper one."
    "She does look better now, but I don't think she already gets {i}that better{/i}."
    "Ulm hesitates, probably doesn't expect how worse Anya's wound is."
    ulm "Um ... Anya? Do you mind if I have a word with you?"
    
    any "Is it exactly {i}a word{/i}?"
    ulm "{size=15}Well ... no ...{/size}"
    ulm "{size=15}Sorry ...{/size}"
    "Anya let out a long sigh and looks at us."
    
    jump endofchoice7

label locker:
    $ rhodo_pt += 1
    $ store_weapon = True
    "I think I better go to the locker room first and store Alpenaster."
    scene bg locker with fade
    "Our locker is our only thing that isn't arranged by the team."
    "We got our locker in the very first day we step our feet at Beacon Academy, even earlier than having our uniform."
    "The difference is, back then, because we hadn't passed the initiation, we didn't have our Scroll device that allow us to launch the locker to predetermined location."
    "Students in the same year have their lockers in at least in the same row."
    stop music fadeout 5.0
    "That's why I immediately notice Rhodo who is sitting on one of the bench."
    mei "Rhodo?"
    show rhodo nor surprised at midleft with dissolve
    rho "Wh--WHOA!"
    
    # sfx loudbang
    play music reckless fadein 10.0
    "There's a loud bang. Three lockers right in front of Rhodo are leaning forward, falling. Out of reflex, Rhodo pushes them back, but could only reach two of them."
    "I run and manage to hold and push back one of them before it falls."
    show mei nor angry at midright with dissolve
    mei "What did you do, blockhead?!"
    rho nor shout "I just ... train my Semblance a bit!"
    hide mei with dissolve
    hide rhodo with dissolve
    
    "Rhodo's Semblance is something related to electromagnetism."
    "I don't know how exactly it works, but as far as I could see, Rhodo could moving any metal objects without touching them."
    "To put it simple, telekinetic limited to metal objects."
    "We found out the 'electro-' prefix part after Rhodo accidentally got hit by lightning Dust in one of our mock battle training."
    "Instead of collapsing, he become stronger."
    mei "Well! Don't do it here!"
    rho "What's wrong with training--"
    
    "The answer for his protest come up next: one of the locker Rhodo lifted with his Semblance is now beeping with urgency. A countdown number appear on its touch screen."
    show rhodo nor surprised at midleft with dissolve
    rho "No, no, no, NO! NOO!"
    "Rhodo desperately taps the 'Cancel' button on the screen."
    rho nor frown "Damnit!"
    rho nor shout "Don't you dare launch yourself--AH, DAMNIT! Why it won't cancel!!!"
    
    show mei nor surprised at midright with dissolve
    mei "Hold it down!"
    rho nor surprised "How?!"
    mei nor angry "With your Semblance, dunce!"
    mei "Pull it back to its place!"
    rho "Oh? "
    rho "Yeah."
    rho nor frown "Right!"
    
    hide rhodo
    hide mei
    with dissolve
    "He manages to use his Electropolarity in the last second. The locker already fly a few inches away from the floor."
    "..."
    extend "..."
    extend "..."
    "After a few moment, we both realize there is something we failed to foresee: "
    extend "when will this locker's propeller stop?"
    rho "Schneeeeeee?"
    rho "Is it just me or it's getting smoky here?"
    "Uh-oh."
    "He's right. We have to do something if we don't want to trigger the fire alarm because of this smoke."
    "I have an idea ... Worth to try, although I couldn't make sure of its chance to succeed."
    
    mei "If Professor Goodwitch ever call us because of this, I refuse to take any responsibility."
    rho "Can we talk about it later?!"
    "I take out Alpenaster and load lightning Dust in it. The blade glows in a faint yellow the moment I channel my Aura into it."
    "Crackling sound follows the electrical blast I aimed at what I believe the main machine of the locker."
    "Without any audible warning, the propeller shuts down--"
    extend "which brings another unpredictable result."
    
    # sfx loudbang
    "Rhodo is still pulling the metal cache when the propeller shutted down, resulting in a loud bang that echoes in the entire room."
    "I don't need to make sure that locker is heavily damaged after what we did."
    show mei nor angry at midright with dissolve
    mei "Rhodo! You--!"
    show rhodo nor shout at midleft with dissolve
    rho "Well, sorry!"
    rho "You surprised me! If you didn't, those goddamn locker won't fly away like that!"
    
    mei "You're not even supposed to practice your Semblance with them!"
    rho "If I didn't drop them because of you--"
    mei "Electricity, magnetism, electromagnet itself could affect the whole navigation system! It IS a machine after all!"
    rho "Grrrr ...."
    rho nor frown "Fine."
    rho "My fault then."
    rho nor confused "Satisfied?"
    "I glare at him and walk away to my own locker to stash Alpenaster."
    hide mei with dissolve
    hide rhodo with dissolve
    stop music fadeout 5.0
    "I grit my teeth. Anger builds up inside of me."
    "I don't dislike Rhodo nor hate him. It's just ... sometimes he's so unbearable."
    # sfx loudbang
    "I slam my locker door, ensuring it's locked properly, and walk away."
    
    "When I almost leave the room, Rhodo calls me."
    show rhodo nor default2 at midleft with dissolve
    rho "Hey, Schnee?"
    "I turn around."
    rho "Sorry."
    rho "I mean it. I'm sorry."
    show mei nor default2 at midright with dissolve
    mei "... Alright."
    "But then I stand there in silence for seconds. Guilty feeling creeps on me."
    show mei nor sad with cdissolve
    "It's normal for Rhodo and me to yell at each other on daily basis. But it's not like this."
    "I think I unconsciously vent my frustration to him."
    mei nor frown2 "I ... apologize too."
    mei nor sad "I'm sorry for yelling at you like that."
    
    play music reckless fadein 10.0
    mei nor frown "But it's still stupid, using lockers to train your Semblance which related to magnetism."
    rho nor confused "... You're sincere with your sorry, right?"
    mei "Of course I'm sincere. Why?"
    rho nor default2 "Nothing."
    rho nor smile "Sooo, are we in good terms now?"
    mei "For now, yes."
    rho nor default "I'm going back to the dorm. You wanna tag along or ... you still need some time ...?"
    mei nor default2 "I'm actually thinking to go look for Anya. She should be in the infirmary, right?"
    mei "I have something to tell her."
    rho nor surprised "Er ...."
    rho "Can I tag along? To the infirmary?"
    
    rho "I kinda worry about her too. Or maybe she needs something."
    "Rhodo doesn't exactly have what it takes to hide his true intention."
    mei nor frown2 "Are you still worry about I'm going to see things and attack her again?"
    rho nor frown "I didn't say it that way ...."
    rho nor surprised "But, yeah ...."
    rho nor default2 "I don't mean to say you're crazy or something, Schnee."
    mei "That's what I actually want to tell Anya about."
    rho nor surprised "Eh? You found out something?"
    stop music fadeout 5.0
    hide mei
    hide rhodo
    with dissolve
    
    scene bg corridor with fade
    "I tell Rhodo about my conversation with Mom. He is especially surprised when with Emerald Sustrai's Semblance."
    play music strange fadein 10.0
    show rhodo nor surprised at midleft with dissolve
    rho "No way! There's a Semblance like that???"
    rho nor frown "It sounds like cheating!"
    rho "What if she made us seeing million swords raining on us?"
    rho nor surprised "I'm gonna totally freaked out if it happens ...."
    show mei nor frown at midright with dissolve
    mei "You shouldn't, you know? You have your Electropolarity to stop the swords, even though it's not real or there are only a dozen swords."
    rho nor surprised "Uh ... you're right. Yeah. That can be done. Why I didn't think to do that?"
    stop music fadeout 5.0
    mei "Maybe because you're such a blockhead?"
    rho nor confused "And here I thought we're already in good terms."
    mei nor cocky "I told you 'for now', which already passed minutes ago."
    
    rho nor frown "Tsk. Damn you, Schnee."
    mei nor smile "Aw, c'mon. I'm just kidding."
    mei nor default "Are you angry, Mr Rhodo Manthus?"
    rho nor confused "Nah."
    rho nor default "It takes more than that to make me angry."
    hide rhodo with dissolve
    hide mei with dissolve
    
    play music casual fadein 10.0
    "We arrive in the infirmary in relatively peaceful state between us."
    "And, guess what, Anya isn't there."
    "She already left and since I was in the locker room and she is going back to our room, we didn't cross path."
    rho "I have a feeling that Anya is that type who doesn't like infirmary."
    mei "I'm not really fond of it too. Chemical odor are not for me."
    rho "Oh, I can agree with that. We all hate infirmary then?"
    
    mei "Most likely."
    "We turn around and walk back to the dorm."
    "Right on the stairs, we meet Ulm. He looks surprised to see us."
    show ulm nor default at midleft with dissolve
    ulm "You guys already back???"
    ulm "I thought you'll be back tomorrow morning?"
    show rhodo nor default2 at midright with dissolve
    rho "Nah, dude. There's a slight change in the plan."
    ulm "I see."
    ulm nor smile "That means Anya is back too?"
    rho nor default "She's in the room, I think."
    rho nor frown "What's wrong? Someone picked on you when we're away?"
    
    ulm nor surprised "No, no, no. Nobody pick on me."
    ulm nor smile "I found something's important {size=15}and confidential{/size} to tell you."
    hide ulm with dissolve
    hide rhodo with dissolve
    "Before we could say anything, Ulm has run upstairs. Rhodo shrugs when I look at him and we both follows Ulm to our room."
    "Ulm already unlocks the door and enter the room when we arrive."
    "I peek inside."
    stop music fadeout 5.0
    scene bg dormday with fade
    "Anya is inside, sitting on her bed and still holding her wooden practice stick."
    "Obviously, despite of having her right hand bound in a sling, Anya tried to practice some moves with her left hand."
    "Maybe because of that, Ulm doesn't immediately realize that she is injured."
    ulm "Anya! I found something! You gotta--"
    "Anya interrupts him before he finishes his sentence."
    
    jump endofchoice7

label endofchoice7:
    show anya nor sigh at mcenter with dissolve
    any "Guys."
    any "I beg you, please leave me sleeping for an hour or two."
    any nor frown "Or three. Maybe four."
    any nor default "... Please?"
    "For a moment there, Anya looks really, really frustrated and exhausted."
    "I push Rhodo and grab Ulm's vest, herding them out of the room."
    mei "We'll fill you in with whatever we're talking about."
    any "That'll be great."
    any nor smile "Thanks."
    "Anya waves her hand, telling us to get out."
    hide anya with dissolve
    
    play music casual fadein 10.0
    scene bg corridor with fade
    "The door click close behind us and before we leave, the three of us hear a muffled yelp from inside."
    show ulm nor surprised at midleft with dissolve
    ulm "Um ... should we check on her again? Just to make sure?"
    show rhodo nor surprised at midright with dissolve
    rho "I don't think so ... Anya told us to scram."
    show mei nor frown2 at mcenter with dissolve
    mei "She probably lay on the wrong side."
    rho "Ouch."
    
    if store_weapon:
        ulm "By the way ... is Anya injured?"
        ulm "What happened?"
        "Rhodo and I exchange looks."
        rho "Erm ... Accident?"
        ulm "What {i}accident{/i}?"
        rho "Schnee?"
        "Of course Ulm has the rights to know what happened to us during the mission."
        "I don't feel like repeating the story. But, well ..."
        mei "Shall we find a decent place to continue this discussion?"
        mei "The library or the amphitheater?"
        ulm "Library."
        
        rho nor confused "Library. Because why amphitheater? We aren't going to practice or something."
        mei nor frown "Because it's an open place where we can see if someone's coming."
        ulm nor default "Or classroom?"
        ulm "Some classroom should be empty by now and still unlocked."
        ulm "{size=15}Only if you agree, though ...{/size}"
        stop music fadeout 5.0
        hide ulm with dissolve
        hide rhodo with dissolve
        hide mei with dissolve
        
        "Despite of Ulm's hesitation, Rhodo and I agree that classroom is a better choice."
        scene bg classroom with fade
        play music confusion fadein 10.0
        "Rhodo tell what happened to Ulm from his point of view. I add with my version after he finished."
        "Ulm looks both fascinated and baffled by the story."
        show ulm nor reluctant at mcenter with dissolve
        ulm "I'm ..."
        ulm "{size=15}I'm sorry to hear that ...{/size}"
        
        ulm "{size=15}If I was there, it probably wouldn't happen.{/size}"
        show rhodo nor frown at midright with dissolve
        rho "Hey, c'mon! Quit blaming yourself, dude!"
        rho "Not everything in Remnant is your fault."
        "I feel a little deja vu when hearing Rhodo said that."
        "Oh, right. Mom said it to me ..."
        stop music fadeout 5.0
        "{i}You gotta understand, sweetheart, not everything happened is your responsibility.{/i}"
        
    else:
        
        rho nor default2 "By the way, Schnee, you still bring your weapon."
        show mei nor default2 with cdissolve
        "I reach to my back and touch Alpenaster, still sheathed on my belt."
        mei nor frown2 "Ah ... right. I haven't gone to the locker room."
        mei nor default2 "I'll be really quick. Do you want to go ahead?"
        rho nor default "Well, where are we going now? Library?"
        ulm nor default "How about in one of the classroom?"
        ulm "Some classroom should be empty by now and still unlocked."
        
        ulm nor surprised "{size=15}Only if you agree, though ...{/size}"
        rho nor default "We pass the locker on our way there. Let's just stick around."
        hide rhodo with dissolve
        hide ulm with dissolve
        hide mei with dissolve
        scene bg locker with fade
        "Rhodo and Ulm wait by the door, while I'm going to my locker and put Alpenaster away."
        
        "Our locker is our only thing that isn't arranged by the team."
        "We got our locker in the very first day we step our feet at Beacon Academy, even earlier than having our uniform."
        "The difference is, back then, because we hadn't passed the initiation, we didn't have our Scroll device that allow us to launch the locker to predetermined location."
        "Students in the same year have their lockers in at least in the same row."
        "... Rhodo is loud, by the way."
        "I can hear him questioning Ulm about what did he find out."
        "Ulm would most likely answer him in low voice, but then Rhodo repeats it with no hint of subtlety at all."
        stop music fadeout 5.0
        "I remain true to my word about being quick and back to my teammates, proceeding to our next destination."
        scene bg classroom with fade
        show rhodo nor default at midright with dissolve
        show ulm nor default at mcenter with dissolve
        
    #scene bg classroom with fade
    show mei nor default2 at midleft with dissolve
    rho "So, what's the big news you got?"
    "Ulm takes a deep breath. Whatever he knew, I guess it is big enough to make him nervous."
    play music discussion fadein 10.0
    ulm nor thinking "When I was doing my detention in Professor Goodwitch's office, I met Saul."
    ulm "He already recovered and Professor called him to meet her and asked some questions."
    ulm "Professor asked him about where did he get the drugs."
    show mei nor surprised
    show rhodo nor surprised
    with cdissolve
    "My heart skips a beat."
    mei "What did he say?"
    ulm nor default "He was lying."
    ulm "He said he got it from third year student he doesn't know and gave Professor description of what the student looks like."
    
    show rhodo nor default2
    show mei nor default2
    with cdissolve
    ulm nor reluctant "Professor Goodwitch seemed to believe him."
    ulm "I know it's a lie."
    "Ulm pause, as if he waits for us to ask him how did he know Saul was lying."
    rho "How did you know Saul's lying?"
    ulm nor default "My detention had finished by the time Saul left the office. We both went back to the dorm after that, I was first, then Saul."
    show ulm nor smile with cdissolve
    "Between the four of us, Ulm rarely put a wide smile on his face."
    "This time, he beams, almost grinning, with utmost satisfaction and pride, which is almost never happened."
    ulm "I Sightjack-ed him."
    show rhodo nor surprised
    show mei nor surprised
    with cdissolve
    "Ulm takes out his Scroll device and show a note to us."
    ulm nor default "He called this place."
    "What's written in the note is 'Tukson's Book Trade'."
    
    show mei nor frown2 with cdissolve
    rho "Err ... I never know Saul is the kind who reads ...."
    rho nor frown "He doesn't even have {i}Grimm's Anatomy: Inside Out Edition{/i}. The one Professor Port told us to have."
    mei "He keeps borrowing Anya's textbooks."
    mei "I'm especially curious as why Saul call a bookstore right after Professor Goodwitch asked him questions about the drugs and he also has just recovered lately."
    "I said my sentence slowly, hoping Rhodo could catch up with what I think."
    mei "If it's Zana from team DJJT, I would really understand it. She's an avid reader and book collector."
    show mei nor default2
    show rhodo nor confused
    with cdissolve
    "Rhodo raises his hands. His face looks as if a part of his brain has stopped working and he would shut down anytime soon."
    rho "I don't understand."
    rho "Why bookstore?"
    
    rho nor frown "There's no relationship between bookstore and drugs!"
    rho "I mean, if it's about drugs, it should be {i}drug{/i}store not {i}book{/i}store."
    ulm nor thinking "My best guess for now is ...."
    show rhodo nor default2 with cdissolve
    "Ulm looks around the classroom, looking for any textbooks left behind. It takes him a while to find one. (I never understand people who leaves behind their books.)"
    ulm nor default "There's a 'classic' example of hiding illegal goods--"
    "Ulm opens the book, trailing his finger near the edge of the pages, forming an imaginary rectangle."
    ulm "--by cutting the pages--"
    
    "He clamps his finger and thumb on the hundred pages of the book."
    ulm "--and creating a hidden 'container' inside a book."
    show rhodo nor surprised with cdissolve
    ulm nor smile "Hardcover books are the best."
    ulm nor surprised "{size=15}It's a really old trick, I doubt anyone still use it, but ... perhaps?{/size}"
    mei nor frown2 "There's possibility for it."
    mei "Even if they don't use this trick, a frequent calling to a bookstore is way less suspicious than to a drugstore."
    mei "Did you hear anything from Saul back then?"
    
    ulm nor thinking "I could only hear a few things from him back then, but he said ...."
    ulm "'You didn't warn me!'"
    ulm nor default "It's still too early to see this as something related, but I think this might lead somewhere."
    mei "I agree."
    "Rhodo opens his mouth, wider and wider. Realization--finally--appears on his eyes."
    
    ulm nor surprised "{size=15}Not too loud, please ....{/size}"
    mei nor frown "No shouting."
    "I push back Rhodo's lower jaw, closing his mouth before he screams."
    rho "You mean--"
    rho "I mean--"
    stop music fadeout 5.0
    rho "This is ... {size=15}BIG NEWS!{/size}"
    rho "{size=15}WE GOTTA DO SOMETHING ABOUT IT!{/size}"
    
    scene black with fade
    call chapter("End of Part 4", "Entangled") from _call_chapter_6
    
    #centered "End of Part 4 - Entangled"
    
    jump part5
    
#return