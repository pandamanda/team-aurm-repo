# PART 9: CERBERUS #

label part9BU:
    
    "It was raining heavily all night long."
    "Ulm managed to make a little fire to keep us a little warm through the night."
    "I really appreciated his work. Even though I still had hard times falling asleep."
    "Not only because the air was too chill, I kept thinking the giant Grimm would come back at us anytime."
    
    "Sometime later, without I even realized it, I fell asleep. {i}Finally.{/i}"
    "I think I had a bad dream, but the next morning I couldn't recall anything about it."
    "The first thing come up into my mind now is that my body feel stiff and I'm starving."
    "... Right. We didn't eat anything last night."
    
    "And I think I smell roasted meat somewhere from outside ...."
    "Am I imagining things? It doesn't good .... Not the time for such thing."
    "I walk out from the temporary shelter and find out--to my surprise--that I wasn't hallucinating."
    "Ulm is cooking something on what would I call a makeshift cooker from ... things."
    
    "Things like a chunk from broken wall, a bit of metal wire or small rod. Things that I can't really figure out where they came from."
    ulm "Oh. Miss Schnee! M-morning ...."
    mei "What are you cooking?"
    mei "Did you ... somehow bring something to eat all along?"
    
    ulm "No, no, no, no, no! I didn't!"
    ulm "It's ... it's not something I brought ...."
    ulm "{size=15}It's something I captured.{/size}"
    mei "... Huh?"
    
    mei "Something you {i}captured{/i}???"
    ulm "{size=15}Or maybe I should say 'trapped'? I used trap to get them ....{/size}"
    "... Wow."
    "We're in the middle of nowhere, with very lack of resources and Ulm somehow managed to get himself something edible?"
    
    "We always know Ulm has admirable knowledge and skill to survive in the wild, but we never had the chance to see Ulm actually practicing it."
    "It's ... simply amazing."
    "Ulm had cut the meat into thin slices, making it difficult to guess what meat is that."
    "I would have the pleasure not knowing it for now. There's a chance I would lose my appetite if I ask further about the meat."
    
    "Let's just forget it and eat. I know my priorities right now is to replenish as much energy as I could."
    ulm "I received message from Anya early this morning."
    ulm "She sent me a map of this island and told me that an airship would be here ...."
    "Ulm pauses. He closes his eyes and mumbles something."
    
    ulm "The airship will arrive in one hour and forty three minutes from now."
    mei "Where are we supposed to meet them?"
    ulm "Ah, yes. There's also a rendezvous point on the map."
    ulm "I've drawn several possible routes for us to take."
    
    ulm "But ... considering how old this place has been abandoned, I think the layout of the buildings and landmarks have changed."
    mei "That's unfortunate, but we could do nothing about it."
    ulm "Right ...."
    mei "Have you decided which is the fastest route?"
    
    ulm "Yes, I do."
    ulm "{size=15}But again ... it depends on whether the path is blocked or not ....{/size}"
    "If only Ulm was more confident, I'm sure he could become a good team leader."
    "He has initiatives, always plans ahead, and his calculations rarely missed."
    
    "Maybe this is what we called 'balance'. You couldn't have everything good in this world in your hand. That would be unfair."
    "You wouldn't need anyone if you already had everything. That's ... lonely and sad, I think."
    "...."
    "Lonely. And sad, huh?"
    
    mei "Let's go to rendezvous point after we finished eating."
    ulm "Yes."
    ulm "Umm ... Miss Schnee?"
    mei "?"
    
    ulm "{size=15}I'm not a really good cook, so I'm really sorry if it doesn't taste good ....{/size}"
    mei "That's not true."
    mei "Beside, taste isn't important right now. Beggars cannot be choosers."
    ulm "R-right."
    
    ulm "{size=12}Thank you.{/size}"
    "We finish up our 'breakfast' and walk following one of the quickest routes Ulm had traced."
    "There's no sign of the giant Grimm nearby, fortunately."
    "However, I doubt the creature would leave us alone after our last encounter."
    
    "We should think of a plan or two in case it find and attack us again."
    mei "Ulm?"
    ulm "Yes, Miss Schnee?"
    mei "I need your opinion about the three-headed Grimm we met before."
    
    mei "I believe we have to be prepared for another encounter with it."
    ulm "I agree."
    ulm "{size=15}Especially, because some Grimm is not going to forget the human who has wounded them.{/size}"
    mei "?"
    
    mei "Come again?"
    ulm "N-no. It's nothing."
    ulm "So, the three-headed Grimm ...."
    ulm "We did good in our last battle with it. However ... I can't pull the same trick anymore."
    
    ulm "I used the only explosive prototype I've brought."
    ulm "I still have a few fire-based crossbow bolts that explodes in impact, but it's too weak if we're going to do significant damage to the Grimm."
    mei "How about the electricity ones? You managed to disable the Grimm for a moment with the electric shock."
    ulm "For a {i}very{/i} short time, Miss Schnee. It'll only enrage the creature."
    
    ulm "... Wait."
    ulm "I designed the electric bolts just enough to disable human beings."
    ulm "Following the size of my opponent, the power needed to disable my opponent would have to be increased."
    ulm "With rough calculation, I should've needed around ten bolts to disable the three-headed Grimm for that short time."
    
    ulm "But I managed to paralyze the creature with only {i}one{/i} bolt."
    mei "Is it weak to electricity?"
    ulm "There's possibility .... We don't have enough data to determine that."
    mei "We don't have time for gathering data, mind you?"
    ulm "I-I know!"
    
    ulm "Can we stop here for a bit, Miss Schnee?"
    ulm "I'll modify the bolts a little."
    ulm "{size=15}Let's hope it works ....{/size}"
    mei "Sure. Let's stop here."
    
    mei "I'll be on guard while you're doing your thing."
    ulm "I'm sor--"
    "I glare at Ulm before he completes his sentence."
    mei "I would appreciate it if you say 'thank you' instead."
    
    ulm "Uhm ... I'm--"
    ulm "Thank you, Miss Schnee."
    mei "That's better."
    "While Ulm tinkering with his equipment, I check Alpenaster."
    
    "Due to my nature as a Schnee family, I tend to use more ice Dust than other elemental Dust."
    "Besides a bunch of ice Dust, I have a few fire, lightning, and wind Dust, each of them are contained in small cartridges that could be slotted inside Alpenaster."
    "Maybe I should give some of the lightning ones for Ulm so he could have more ammunitions at hand?"
    "We haven't proved that the giant Grimm is weak against electricity, but I think it's better to have too many than too few."
    
    mei "Ulm?"
    mei "Here, you can have these."
    ulm "Your Dust cartridges, Miss Schnee???"
    mei "Only the lightning ones."
    
    ulm "Are--are you sure you're giving me these?"
    ulm "I mean ... uhm ...."
    mei "I rarely use the lightning ones too."
    mei "Don't worry, I still have some on me. You can take these ones."
    
    "Ulm accepts the cartridges as if he was given pure gold ingot instead of lightning Dust cartridges."
    "It doesn't take long for Ulm to take out the Dust from the cartridges and put them into some of his crossbow bolts."
    "I'm amazed seeing how skillful he works only with an aid from a small knife to prick open the Dust cartridge."
    "The trap he spoke about earlier this morning? Maybe Ulm only used the same knife to set those traps up."
    
    ulm "I'm good to go, Miss Schnee."
    ulm "{size=15}Let's hope it's working or we don't meet the Grimm ....{/size}"
    "I elbow Ulm softly, giving him a disapproval look."
    mei "Ever heard pessimism could jinx you?"
    
    mei "Things look bad now and we don't need you to jinx it, alright?"
    ulm "O-okay ...."
    ulm "{size=15}That jinx thing is a superstitious, isn't it ...?{/size}"
    mei "Come again?"
    
    ulm "N-no! It's nothing!"
    "Except a few Grimm creatures--most of them are Creepers--there's nothing worth to be worried about during our trip."
    "It's a good thing, I guess?"
    "Hopefully, the three-headed Grimm wouldn't even notice when we leave the island."
    
    "We stumble upon a dead end, however."
    "Without wasting any second, Ulm--who already memorized all routes he drew this morning--lead us into an alternative route."
    "At this pace, we would arrive at the rendezvous point in no time."
    "If and {i}only if{/i} there is no trouble awaiting for us."
    
    "Surprisingly, it is."
    "We arrive at the rendezvous point without crossing path with the giant Grimm."
    "We did have a few combats with small to medium sized Grimms, but none of them were as troublesome as the three-headed monster."
    "I think all that's left is waiting for the airship to arrive and pick us up."
    
    ulm "Thank goodness everything went well."
    mei "Yeah, thanks to you, Ulm."
    ulm "{size=15}Thanks ... to me?{/size}"
    ulm "{size=15}Well, I'm glad I can be useful.{/size}"
    
    "Let me correct his statement: Ulm is {i}always{/s} useful."
    "I don't know how we should convince him to have more confidence about himself."
    "We keep our guard up while waiting for the airship to arrive."
    "Again, things go smoothly."
    
    "No attacks until we hear airship engine get closer and closer."
    #roar
    "... And then a thundering roar."
    mei "W-what?!"
    ulm "!!!"
    "The three-headed Grimm dashes toward us."
    
    "Surprised, both Ulm and I launch an attack from our weapon."
    "The Grimm dodges my ice Dust blast with ease."
    "Ulm's crossbow bolt hit the creature's shoulder, but failed to stop the monster's movement."
    "I see no sign of electricity Dust reaction from the bolt."
    
    "Apparently, Ulm had changed his default crossbow ammo into the normal one."
    "From the corner of my eye, I see him frantically taking out the modified bolt and load it on his weapon."
    "The Grimm doesn't stop."
    "Rather than leap at us, it keeps charging ahead."
    
    "... Oh no."
    "Nonononono!"
    "The Grimm's target isn't us!"
    "It aims for the airship!"
    "I quickly cast a Glyph, summoning the knight's armored arm from it."
   
    "The Grimm is even faster. When the arm is fully summoned, the monster already jumped toward the airship."
    "Ulm stands still, aiming his crossbow but doesn't immediately shoot."
    mei "What are you waiting for?! Shoot it!"
    ulm "I--I can't, Miss Schnee!"
    ulm "The moment the bolt hit it, the Grimm would already reach the airship!"
    ulm "The electricity is going to affect the airship too!"
    #blast sfx
    "However, I clearly underestimate Beacon Academy pilot."
    
    "An inch away from the Grimm's claw, the airship flies away."
    "The Huntsmen or Huntresses aboard the airship return the Grimm's attack with bullets."
    "Mid-air, the Grimm has no chance to avoid the counter-attack."
    #roar sfx
    "The ground shakes as the creature lands."
    
    "Above us, the airship turns around and adjusts its position, ready to launch next attacks."
    "Ulm takes this chance to shoot the Grimm with his modified arrow."
    #roar sfx again
    "High voltage electricity shock the creature."
    "It stops moving and clearly get weakened."
    
    "Before it turns around and run away, another series of bullets rains on the giant Grimm."
    "Forgetting for a moment that I still have my summon, I punch the Grimm's head, slamming it to the ground with everything I've got."
    "The Grimm let out a weak growl and try to stand up again."
    "Ulm shoots another crossbow bolt."
    
    "The tip of the arrow lodges in the  "
    
    
    