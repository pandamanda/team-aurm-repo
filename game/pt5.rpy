# PART 5: GREY AREA #

label part5:
    
    "Professor Goodwitch called for us--Anya, Rhodo, and me--this morning. Ulm tags along despite he doesn't get the calling."
    "I don't need to make a guess what is Professor Goodwitch going to talk about."
    "Instead of meeting Professor Goodwitch at her office, this time, she asked us to go to the tower of Beacon."
    "I've never been there. I heard the previous headmaster's office is located in a room on top of the tower."
    "Why our headmaster--Ebonezer Scrooge, who is away at the moment--nor Professor Goodwitch as the current acting headmaster never use the room as an office is a mystery."
    "There are various rumors about it."
    "Sentimental reason about Professor Goodwitch simply couldn't be in that room because it reminds her of the previous headmaster."
    "Supernatural gossip about the spirit of the headmaster, who was missing during the fall of Beacon, resides in the room."
    "And many others. You name it."
    "We even have the most ridiculous one about there's a dragon Grimm trapped inside the headmaster's room."
    "The School Journalist Club and the Conspiracy Theory Debunker Group members' imagination knows no bound."
    
    show rhodo uni default2 at midleft with dissolve
    rho "You guys have been too quiet."
    rho uni surprised "I'm nervous. "
    rho "It's as if we're going to meet some mysterious man on top of the tower to be judged."
    rho "A man with white hair, glasses, with a staff with silver ornament on its handle ..."
    rho "Whoa ... Talking about 'active imagination'."
    
    show mei uni frown at midright with dissolve
    mei "There's no such childish thing like that."
    mei "Quit daydreaming, will you?"
    rho uni frown "I'm not daydreaming, Schnee."
    hide mei with dissolve
    show ulm uni default at midright with dissolve
    ulm "Another vivid mind picture?"
    rho uni surprised "Yes. This elevator somehow makes me having it."
    
    rho "It's so random."
    hide rhodo with dissolve
    hide ulm with dissolve
    "The elevator stops right after Rhodo finishes his sentence."
    "Anya, who hasn't said a word since we left our room, is the first to step out of the elevator."
    "I follow her closely. Rhodo is the next, walking behind me. Ulm try to match Rhodo's pace beside him."
    
    scene bg beacontower with fade
    play music confusion fadein 10.0
    "This room is impressive."
    "My attention is drawn toward the huge clockwork machine above us."
    "Everyone ever see Beacon tower must have noticed its famous orb mechanism that resemble the sun and moon orbital movement on top of the tower."
    "I never imagine the machine which moves it could be seen from this room."
    show glynda default at midright with dissolve
    gly "Mr Manthus, I suggest you don't try to make a show of your Semblance with those gears."
    "Rhodo, halfway outstretched his hand to the gears, put both hands behind his back."
    show rhodo uni surprised at midleft with dissolve
    
    rho "I'm sorry, Professor."
    hide rhodo with dissolve
    hide glynda with dissolve
    "Professor Goodwitch stands by the wide window on the other side of the room."
    "From there, you can see the city of Vale and Patch Island, which lies further on the west of Vale."
    "Standing here and looking outside makes a false impression of owning Vale and its surroundings."
    "I wonder what the previous headmaster looked like."
    "I couldn't help not thinking that he or she is an arrogant person who loves everything under his or her watch and control."
    "This high place and wide window suits such person, in my opinion."
    "I've seen SDC's business partners who have office similar to this one and none of them are of my liking."
    show glynda default at midright with dissolve
    
    show anya uni default at midleft with dissolve
    any "If you will excuse me, Professor, I let Ulm come with us, since I think it's okay for him to know why we were called."
    gly thinking "Very well."
    gly "Do you know why I call you here?"
    any "I believe I do."
    any "I promised you yesterday that I'm going to explain in more detail about what had happened in our mission in Forever Fall."
    show glynda default with cdissolve
    "Professor Goodwitch squints her eyes, shifting her gaze on Anya and me back and forth."
    hide glynda with dissolve
    hide anya with dissolve
    "She walks toward the table near the window and access a holographic control panel on its surface."
    
    "A screen window pops up from the table, floating a few inches from the table's surface."
    "It is about to play a video recording."
    gly "You probably didn't realize it when you uploaded your mission recording I required, Ms. Lyre, but I've watched it and found an intriguing detail about what had happened."
    gly thinking "And it would be better if you provide me with a satisfying explanation, Ms. Schnee."
    stop music fadeout 5.0
    show anya uni frown at mcenter with dissolve
    any "Ah ...."
    any "I forgot to turn the recording off ...."
    any "It rained and I just put my Scroll to my pocket without stopping the recording ...."
    
    hide anya with dissolve
    "The video Professor Goodwitch played is a recording of the event inside the abandoned tunnel."
    "Just like Anya said, there's not much to be seen since the Scroll was inside her pocket. But the audio sounds clear."
    "Everything's just like what Rhodo told me earlier in the outpost."
    "What makes it different is there is a loud noise near the end of the recording, followed by Anya's pained cry."
    "Anya's Scroll must've been slipped from her pocket. The next view is blurry images of the tunnel, cracking noise when the Scroll hit the floor, and then a heavy thud nearby."
    show glynda default at midright with dissolve
    gly "Well, Ms Schnee?"
    gly "I do believe you were at the exact same place as Ms Lyre and Mr Manthus."
    
    show mei uni sad at midleft with dissolve
    mei "Yes, I did."
    gly "Care to explain?"
    mei "I was the one who attacked Anya and Rhodo."
    gly thinking "...."
    gly "I see."
    hide mei with dissolve
    
    $ most_pt = max(anya_pt, rhodo_pt, ulm_pt)
    
    if most_pt == anya_pt:
        show anya uni default at midleft with dissolve
        any "It's true."
        any uni frown "But as the one directly involved in the incident, I will say something was off."
        any "Mei Schnee will {i}never{/i} attack her own teammates whatever the reason is."
        gly default "Your proposition hold a very weak point, Ms. Lyre, if it is said by anyone else."
        gly "But, very well. It could be taken as consideration."
        gly "As long as the evidence don't say otherwise."
        hide anya with dissolve
        
    elif most_pt == rhodo_pt:
        show rhodo uni shout at midleft with dissolve
        rho "It's because someone who was there used her Semblance to make Schnee seeing things!"
        rho "It didn't show in the recording, but {i}there was{/i} someone else in that tunnel!"
        gly default "And did you happen to see them, Mr. Manthus?"
        rho uni surprised "Uh ...."
        rho "No. Actually I didn't see anyone."
        rho uni frown "But, what else can explain this?"
        rho "I believe Schnee won't attack her own teammates, Professor, that's for sure."
        gly "It's vague and full of bias, Mr. Manthus. I need a better exposition for this matter."
        hide rhodo with dissolve
        
    elif most_pt == ulm_pt:
        show ulm uni reluctant at midleft with dissolve
        ulm "Professor? If I may speak?"
        ulm uni thinking "I wasn't there when everything's happened, but ... in my opinion, there was high probability of third party interference in this case."
        ulm uni reluctant "As far as I know, Forever Fall is vast and, in the past, there had been cases where the forest was used as hiding places for criminals."
        ulm uni surprised "{size=15}Pardon me, if I said something inaccurate ...{/size}"
        gly default "You've made an accurate point, Mr. Thornman."
        gly "Do you also have a possible explanation as of {i}why{/i} this was happened?"
        ulm "...."
        ulm uni reluctant "I'm sorry, but I don't have anything, Professor."
        gly "I see."
        hide ulm with dissolve
    
    "Professor Goodwitch looked at us one by one. Her glare grows suspicious every time she shift to the next person."
    gly "Are you hiding something from me?"
    show anya uni default at midleft with dissolve
    any "On the other hand, Professor, I can say the exact same thing to you."
    play music cocky fadein 10.0
    any uni smirk "Are {i}you{/i} hiding something from us?"
    ulm "{size=15}ANYA!{/size}"
    ulm "{size=15}WHAT'RE YOU SAYING???{/size}"
    "Both Rhodo and I turn our eyes at Anya, extremely surprised of what she said. Ulm lets out a whimper-like sound, obviously afraid of Professor Goodwitch's anger."
    
    "Professor Goodwitch, however, looks both amused and a bit offended."
    gly "Why do you think so, Ms. Lyre?"
    any uni default "This room."
    any "Despite of your position as the current highest authority in Beacon Academy, students could easily meet you in your office, not here."
    any "Considering the highly restricted access to this room without Professor Scrooge or your permission, I believe this isn't any normal invitation."
    "That's true ..."
    "I couldn't press the button of this floor when we entered the elevator before Anya scanned something from her Scroll on the elevator panel."
    "And once the button to this floor is pressed, the floor number on the panel doesn't show number anymore. It changes into a clock symbol."
    gly thinking "As I've heard, Ms. Lyre, you have a good judgement of your situation."
    
    gly "But I have to warn you about your verbal attitude."
    gly default "Alright, I will make it brief."
    
    gly "I have high concern regarding the accident you've been through during the mission in Forever Fall."
    gly "Especially after I figured out that you lied about your wound, Ms. Lyre."
    gly "It seems to me that you try to hide something from me and after what's happened to team SKYE's leader, I believe I can't just let this pass."
    any uni frown "...."
    any "I apologize, Professor."
    gly "Now, I'd like to hear what do you have to say."
    
    "I swallow and ready myself to tell Professor Goodwitch about Emerald Sustrai."
    show mei uni frown2 at midleft2 behind anya with dissolve
    mei "I--"
    "But Anya makes a gesture to cut what I'm about to say."
    any "Are you suspecting Mei for taking the Golden Apple, Professor?"
    show mei uni surprised with cdissolve
    stop music fadeout 5.0
    "!!!"
    mei "That's--"
    hide mei with dissolve
    
    show rhodo uni shout at midleft2 behind anya with dissolve
    rho "Schnee will never do that!"
    any uni default "Rho, please."
    rho uni frown "But--We know Schnee won't do that!"
    any uni frown "Rhodo. Please."
    hide rhodo with dissolve
    
    "Rhodo still try to say something, but then he backs down. He looks flustered."
    "Ulm, who stands next to him, has locked his gaze to the floor. Horrified with this development."
    any "Professor Goodwitch?"
    gly thinking "Yes. It's just like what you said, Ms. Lyre."
    gly "I suspect Ms. Schnee is involved with the doping case."
    gly "I am very aware that this is a serious accusation. Especially because it's the Heiress of SDC herself involved."
    "That's why Professor Goodwitch asked us to meet her in this room .... She wants to avoid any rumor if someone happen to overhear our conversation if we meet in her office."
    "A rumor about the Heiress of Schnee Dust Company would take no time to leak further outside of Beacon Academy wall."
    
    any "What makes you come to that conclusion, Professor, if I may ask?"
    any uni default "You're one of the most rational lecturer Beacon Academy ever had, so there has to be something more than just discovering that I lied, right?"
    "I could feel cold sensation on my nape, crawling down to my back."
    "This is utterly ridiculous."
    "The shock I felt before gradually turn into some sort of anger toward Professor Goodwitch."
    gly "Well ...."
    gly default "It's probably unseen from the students, but we, teachers at Beacon, have noticed a major changes in your performance in combat after the incident Ms Schnee had."
    gly thinking "We suspect it was a rare case of Semblance degeneration. Professor Scarlatina even mentioned that you experienced a few blackouts in combat training due to excessive usage of your Aura, Ms Schnee."
    
    gly default "However, your performance steadied and grows better during this semester."
    show rhodo uni frown at midleft2 behind anya with dissolve
    rho "That's because she trained really hard to make up for her Semblance error!"
    "I have a mixed feeling when hearing Rhodo's exclamation. First, his choice of word of 'error' is funny. Second, I feel better knowing he defends me."
    any uni sigh "Rho, no interrupting."
    rho uni surprised "Sorry ...."
    hide rhodo with dissolve
    
    any uni default "I'm sorry, Professor. Please continue."
    gly "From the information we gathered regarding the Golden Apple, we found out its exceptional effects: improving Semblance performance in limited time."
    gly "A few even said a regular usage will improve your Semblance permanently."
    any uni frown "You think Mei try to 'fix' her Semblance by consuming it?"
    gly thinking "Yes."
    any "Regularly?"
    gly "I suppose."
    any uni default "Then ... how did she pass the test a few days ago?"
    gly default "...."
    
    hide glynda with dissolve
    hide anya with dissolve
    "I could hear Ulm lets out a relieved sigh behind me."
    "On the other hand, I'm totally unhappy with this."
    "Even if Professor Goodwitch is the current highest authority while Professor Scrooge is away at the moment, it doesn't give her the rights to make an accusation like this."
    "To make it worse, she overlooks a simple fact that invalidate all of her suspicion."
    "Anya must be exaggerating when saying Professor Goodwitch is one of the most rational lecturer."
    "... Or was that a hidden sarcasm? I'm not really sure now ..."
    
    show anya uni default at midleft with dissolve
    any "You seem to have something urgent in your hand, Professor."
    any "Does someone want you to solve the Golden Apple case as soon as possible?"
    show glynda thinking at midright with dissolve
    "I notice a slight change in Professor Goodwitch's expression. It's as if Anya hit a spot with her 'wild guess'."
    show mei uni frown2 at midleft2 behind anya with dissolve
    "But ... I think Anya steps too far. I nudge her, giving a hint to restrain herself."
    
    "Anya turn to me and said in low voice: "
    any "{size=17}Tell her about Emerald Sustrai.{/size}"
    mei "{size=17}Should we?{/size}"
    any "{size=17}She might know something about her.{/size}"
    any "{size=17}A good chance to figure out who is she.{/size}"
    hide anya with dissolve
    play music confusion fadein 10.0
    show mei uni default2 at midleft with dissolve
    
    mei "Have you ever heard the name 'Emerald Sustrai', Professor?"
    "Professor Goodwitch almost couldn't hide her surprise."
    gly default "Yes, I do. What about her?"
    mei "I met her in the tunnel in Forever Fall."
    mei "After the encounter, I met with what seems to me as two hostile security droids."
    mei uni sad "It was--supposed to be--a self-defense against them."
    "Professor Goodwitch nods slowly."
    hide glynda with dissolve
    hide mei with dissolve
    
    "She reaches to the control panel and opens a file."
    "Several windows hover on top of the table, each of them have a close up picture and short biodata of the corresponded person in the picture."
    "And I see it."
    "One of them is Emerald Sustrai."
    gly "Was it her?"
    "Professor Goodwitch maximize the window size by touching it."
    "I recognize her face, her hair and eyes color ... There's no mistake. It's her."
    "The 'Semblance' data in her bio is filled with question mark, followed by an explanation of 'deteorieting target's visual sense; limitation unknown'."
    "I nod, confirming it was indeed her that I met in the tunnel."
    gly thinking "It all makes sense now ..."
    "When Professor Goodwitch returns the window back to its normal size, I see some other profiles have 'DANGEROUS' on top of it."
    
    "One of them is a bluish grey haired man and a jet black haired woman with Grimm mask."
    stop music fadeout 5.0
    "I see another window is opened in the display and it doesn't take me a second to recognize the symbol on it."
    "It's Schnee Dust Company logo."
    "On the top part of the window, I read a mirrored word 'PRIVATE'."
    "Professor Goodwitch must have noticed my attention is drawn to the letter and shut down the entire holographic screen."
    
    show anya uni default at mcenter with dissolve
    any "Since we're already here, Professor, I might as well tell you that you probably want to drop by at Tukson's Book Trade."
    any "It might help you with the Golden Apple issue."
    any uni smile "That's all I have to say."
    any uni default "If we don't have anything more to discuss, we will excuse ourselves."
    any "Thank you very much for your concern regarding our last mission."
    any uni smirk "Good afternoon and have a good day."
    hide anya with dissolve
    hide glynda with dissolve
    
    "Not just me, both Rhodo and Ulm seem to understand Anya tried to pull something from Professor Goodwitch."
    "Her hint about checking on Tukson's was said to tease Professor Goodwitch. Anya didn't leave long enough pause for Professor to inquire more explanation from her about the hint."
    "And ... I don't know whether Rhodo or Ulm think the same with me or not, but I feel like Anya deliberately said 'thank you very much' in order to show her dislike for Professor's early suspicion to us."
    "We almost get into the elevator when Professor Goodwitch calls for us."
    
    gly "You seem to be {i}too interested{/i} with this case, students."
    gly "Are you secretly investigating it behind our back?"
    
    show anya uni default at midleft with dissolve
    show glynda default at midright with dissolve
    any "Everyone might get {i}too interested{/i} by now."
    any "We've met the third years who tried to dig some info from us, so why not others?"
    any "Beacon Academy aims to educate the best negotiator, scout, and rangers in Remnant after all. We keep our eyes and ears open for information."
    any "One thing you need to know, Professor, we're more than willing to help you if you ever need our assistance."
    gly thinking "You haven't answered my question, Ms. Lyre."
    any "Well ...."
    any uni smirk "It's not {i}behind your back{/i}, Professor. You can rest assure."
    hide anya with dissolve
    hide glynda with dissolve
    
    scene black with fade
    "The four of us leave the room, entering the elevator that bring us back to the first floor of the academy."
    show rhodo uni surprised at midleft with dissolve
    rho "I thought we're going to keep the Tukson info for ourselves, you know ..."
    rho "I mean, if Professor thinks we know too much, she's going to tell us to stop investigating."
    show anya uni frown at midright with dissolve
    any "First thing first, I'm sorry for not letting you know what was my intention."
    any "I felt that Professor's suspicion was ... off. She overlooked something so obvious."
    show mei uni angry at mcenter with dissolve
    mei "I'm personally insulted by it."
    mei "She ignores the fact that I'm a role model of Beacon Academy student and that I would {i}never{/i} do such lowly act like what she suggested."
    rho uni default2 "...."
    any uni default "...."
    
    mei uni frown "... What?"
    rho uni frown "I'm not saying anything! Why do you look at me like that?"
    any uni smirk "A 'role model of Beacon Academy student' ... Rrright ...."
    any "Who is that student who is banned from using her Semblance at the school ground after blocking the main hallway with huge ice wall back in the third semester?"
    any "Or is that after two seniors reported that they were punched and flew high over the school wall?"
    mei uni frown2 "... Please forget that I ever said the word 'role model student' and move on ...."
    hide mei with dissolve
    any uni default "Alright. Back to topic."
    any uni frown "I had an impression that Professor is under pressure to solve the Golden Apple case as soon as possible."
    any "And there's the letter from SDC that came a month ago."
    
    hide rhodo with dissolve
    show ulm uni default at midleft with dissolve
    ulm "A month, two weeks, and three days ago."
    ulm uni surprised "Uh ... {size=15}I'm sorry{/size} ...."
    any uni default "I didn't read the title if there's one on the letter, but I thought that, {i}probably{/i}, SDC is the one who put Professor under pressure."
    any "They have the power to do it."
    hide anya with dissolve
    hide ulm with dissolve
    "... Hold on."
    "Roman did tell me about the Golden Apple might be related to a breakout case in RnD division."
    "It makes sense if SDC grows uneasy after there's no visible development from Beacon staff after half and a month."
    
    show anya uni default at midright with dissolve
    any "I leaked the Tukson info to Professor Goodwitch to tease her about how much we know."
    any uni frown "If she tried to get me to say more about where did I get the information, I will stall and offer her the deal."
    show mei uni frown2 at midleft with dissolve
    mei "The deal?"
    any uni default "I want her to authorize our investigation."
    mei "And why is that? Professor doesn't have any reason to involve students in this case."
    mei "Maybe she doesn't know who could be trusted among the students."
    any "She can trust {i}us{/i}. That's what I want to tell her."
    
    show mei uni default2 at mcenter with dissolve
    show rhodo uni frown at midleft with dissolve
    rho "We can do the investigation on our own, right?"
    rho uni surprised "We have more freedom without her auth--oth ... Er ..."
    mei uni frown "Authorization."
    rho uni smile "Yes! That!"
    any uni smirk "Are you sure?"
    any "With her authorization, we might have the access we wouldn't get if we acted alone."
    any "We can have backup from the academy, and--more importantly--we know where to start looking and avoid ourselves from danger."
    
    show mei uni default2 with cdissolve
    rho uni surprised "... Whoa."
    rho "I never thought that way before ...."
    "I have to admit Anya's moves is unpredictable even for me."
    "I agree with her points and reasoning, however ...."
    hide mei with dissolve
    hide rhodo with dissolve
    
    show ulm uni reluctant at midleft with dissolve
    show anya uni default with cdissolve
    ulm "Will Professor Goodwitch give it to us?"
    ulm "She didn't ask further where did you get the information about Tukson."
    ulm "And ... I don't know ...."
    ulm "It sounds like a huge mission ... Can we really handle it?"
    any uni frown "She probably won't ask us to do everything."
    any "She has been handling student missions for years now. Professor will know our capability."
    any uni sigh "Anyway, you're right about will she give it to us or not."
    any "We can lay baits, but will our target bite it?"
    ulm "Maybe not."
    any uni default "Yep. Maybe not. We'll need to wait and see."
    hide anya with dissolve
    hide ulm with dissolve
    
    scene bg walkway with fade
    "We cut our conversation when we arrive at the ground floor."
    play music casual fadein 10.0
    rho "So ... we're free now?"
    show anya uni default at mcenter with dissolve
    any "Yeah. "
    any uni sigh "Oh. Wait."
    any uni default "If anyone ask any of you about where were we and why, you gotta answer:"
    
    any uni frown "'Professor called us regarding our early withdrawal from the mission.'"
    any "'Anya's injury is caused of falling off a cliff, got stuck badly on a tree.'"
    any uni default "{i}Capiche?{/i}"
    rho "Yes, Ma'am."
    ulm "Okay."
    mei "Understood."
    any uni smirk "Great."
    any "I'll visit the infirmary for--hopefully--the last time and then ... somewhere else."
    any "Send me a message if you need me."
    hide anya with dissolve
    
    show ulm uni default at mcenter with dissolve
    ulm "I'll be in the usual place."
    ulm "Library."
    hide ulm with dissolve
    show rhodo uni default2 at mcenter with dissolve
    rho "Well, I ...."
    mei "Gaming in the room?"
    rho uni smile "You know me so well, don't you?"
    
    mei "You're practically the lazy bum of our team, where else would you go?"
    rho uni confused "Can you skip the insulting part every time you talk to me???"
    rho uni default2 "I'm getting used to it, though ...."
    rho uni confused "Naaah, later then."
    hide rhodo with dissolve
    "After everyone else leave, I take out my Scroll to check for any messages."
    "No new message."
    "I walk to the nearest empty class, doesn't exactly have any idea of what to do, but I have to think over something."
    
    scene bg classroom with fade
    "I leave the door open and, almost instinctively, drawn to the blackboard."
    "I take a chalk, drawing the first line."
    
    "What are we going to do if we don't get any authorization from Professor?"
    "Are we going to wait and see just like that? Or are we going to conduct our own investigation?"
    "Mom didn't forbid me from investigating, but ... how far could I get?"
    "I don't like to admit it, but we might not get far without the necessary aid from other people."
    "Even if we do get that far, if we find the culprit, how would we handle it?"
    "Take him or her down by ourselves?"
    
    stop music fadeout 5.0
    "...."
    "Just now, I realize that I struggle to answer the question about what our action would be."
    "The Mei before the incident with Cinder would not hesitate to decide to take the culprit down by herself."
    "The Mei now is shadowed with so many questions and hesitation."
    "I'm not sure whether this is a good thing or a bad thing."
    "I finish drawing a simple snowflake on the blackboard, still not getting any satisfied answer for my own doubts."
    "I flick the chalk to its box and open my Scroll."
    "It might have became a habit for me to text Li whenever I have something in my mind, but never tell her about it."
    
    "I just want to write to her. It always makes me feel better."
    "And Li isn't the kind who would send message first."
    "It makes me worried, especially because she has her own record of being picked on."
    "But I realize that I shouldn't nag her too much. Li would eventually grow up and I couldn't be there all the time for her."
    "When I'm in the middle of writing message to her, a new text message arrives."
    "It's from Roman."
    "What does he have to say this time? It's unusual for him to message me first."
    play music casual fadein 10.0
    
    nvl clear
    r "R: soooo"
    r "R: hows your trip to forever fall?"
    
    "How does Roman know about my mission to Forever Fall???"
    "I didn't tell him, did I?"
    "How in Remnant does Roman know then???"
    
    m "M: HOW"
    m "M: DO"
    m "M: U"
    m "M: KNOW"
    m "M: ABOUT FOREVER FALL?????????"
    r "R: omg peppermint candy"
    r "R: do you need to b so dramatic???"
    r "R: im just asking!"
    m "M: You shouldnt have known ive gone to forever fall!!!!!!"
    m "M: NOW ANSWER ME WERE DID YOU KNOW ABT IT????????"
    
    m "M: *where"
    r "R: omg"
    r "R: CALM DOWN CANDY CANE"
    r "R: IM JUST GUESSING"
    r "R: YOU TEND TO GET URSELF IN THINGS YOU SHOULDNT"
    m "M: You dont need to yell at meeee :((("
    r "R: whos yelling first here?????"
    r "R: so u did go there eh"
    r "R: u dont get yourself in trouble right?"
    
    "I don't buy Roman's explanation about 'just guessing'."
    "Roman is a 'special staff' in SDC for many reasons, one of them is his skill in gathering information."
    "I already told Mom about meeting Emerald in Forever Fall, but would she share it with Roman?"
    "I heard Mom was against recruiting Roman and doesn't fully trust him until now."
    "She refused to fill me with the details, but she dropped a few hints about Roman made a deal with the military for a legit job in Schnee Dust Company."
    "Hmm."
    "Let's see ...."
    
    m "M: What kind of trouble do u mean?"
    r "R: i dunno? Grimm tear off your cloak?"
    r "R: or beowolf chase you around the yard?"
    m "M: If ur trying to provoke me that wont work"
    m "M: What kind of trouble do u mean?"
    r "R: im gonna conclude u got no trouble based on ur reply"
    
    "And then I remember that Mom called me despite of she shouldn't have known that I had returned to Beacon."
    
    m "M: Kindly tell me"
    m "M: Did u tell mom about our chat a few day ago?"
    r "R: why would i?"
    r "R: ceo gonna kill me"
    r "R: i still wanna live for another hundred yrs"
    m "M: Rrrrriighht"
    m "M: Im busy"
    m "M: If u dont have anything more to say im gonna hang up"
    r "R: yea go ahead"
    
    "I'm still curious with how Roman came up with his question and even bothered to send the message first."
    "Maybe I could ask Mom about it."
    "Considering how many times in the past Roman was tasked to watch over me when both of my parents were busy attending an urgent matter, the chance that Mom told Roman to do such thing again isn't zero."
    "I look up her name in my contacts and make a call."
    "..."
    extend "..."
    extend "..."
    "{i}\"The number you are calling is currently out of reach or busy.\"{/i}"
    "{i}\"Please try again in a few moment.\"{/i}"
    
    "Hmm ... maybe Mom is out for a mission or scout?"
    "Never mind. I write and send a short message to her. Mom's reply might arrive later."
    "Oddly enough, my message to Li hasn't been delivered."
    "Is she in an area out of the communication tower reach?"
    "Oh, well ...."
    "I probably should meet with my teammate for now."
    
    
    menu:
        "Meet Anya in \"somewhere else\".":
            jump meetanya
        "Meet Ulm in the library.":
            jump meetulm
        "Meet Rhodo in dorm room.":
            stop music fadeout 2.0
            jump meetrhodo
            
label meetanya:
    $ anya_pt += 3
    scene black with fade
    "Anya's favorite place is the dormitory building's rooftop."
    "There's a small open space that could be accessed from the top floor of the dorm."
    "The door to the place is never locked, allowing students to access it freely."
    "Although, not many do so since the wind up there is quite strong and there's no interesting view to be seen."
    "And in the afternoon, this place is practically flooded by direct sunlight and leave few to no shadow around."
    "But early in the day or at night, this open space is quite pleasant."
    "Anya spends most of her time during that moment to hang out, sometimes take a nap, but, more often, spar against whoever she meets up here."
    "As I've guessed, she is here, on the rooftop."
    "Anya is currently speaking with someone."
    stop music fadeout 5.0
    
    any "I promise you it won't get too dangerous."
    mei "?"
    "Who is she talking to?"
    "From what I could hear so far, she is on the phone."
    "And whoever it is at the other end of the call, the person sounds disagree with what Anya said."
    any "I know ...."
    any "Well, listen ... I'm sorry about that."
    "I stay quiet behind the door and listen."
    "Anya's voice is getting closer and a moment of unsettling pause follows after that."
    any "Hang on ...."
    # blam
    "Without warning, the door is banged from the other side."
    "Surprised, I yelp and move away from the door."
    play sound door
    
    "The door is opened."
    scene bg rooftop with fade
    play music cocky fadein 10.0
    show anya uni shout at mcenter with dissolve
    any "Oh?"
    any "It's you."
    "Anya has ended the phone call. She put her Scroll in her pocket, leaving no clue for me about who did she call."
    any uni smile "Did I surprise you?"
    show anya uni smile at midleft with dissolve
    show mei uni angry at midright with dissolve
    
    mei "What was that for?!"
    mei "Did you do it in purpose??? Knowing I was the one behind the door?"
    any uni default "Nope. I didn't know it's you."
    any uni smirk "I just knew there was someone standing still behind the door, probably listening to my conversation and I gotta give them a surprise. That's all."
    mei uni frown2 "?"
    mei "How did you know there's someone behind the door ...?"
    show anya uni smile with cdissolve
    "Anya points the bottom of the door."
    any "The shadow. Saw it in an old action film I watched sometime ago."
    
    show mei uni surprised with cdissolve
    "An old action film??? For real?"
    show mei uni frown2 with cdissolve
    "Anya was serious about her 'you learn new things every day', I guess."
    "To make things more amazing, she even learns from any kind of resources."
    any uni default "So ... what's up?"
    mei uni default2 "Nothing much."
    "I notice the sling bounding Anya's arm has gone. She doesn't move her right arm as much as usual, but from the looks of it, her condition has improved."
    
    mei uni default "How's your arm? "
    mei uni frown2 "Is it still hurt?"
    any uni smile "Stiff. Not hurt. I'm pretty much good now."
    show mei uni sad with cdissolve
    "When I think about it again, Anya has gone through so many things in the last week."
    "There was unfortunate event with Ulm. "
    "Followed by the unexpected chaos during the mission and her dislocated shoulder because of me."
    "To add them up, Professor Goodwitch caught her lying--which lead to the flawed accusation."
    "... When I think about it again, Professor Goodwitch would perhaps doubt Anya's credibility after that one lie."
     
    show anya uni default with cdissolve
    mei "Are you ... going to be okay?"
    mei "I mean, you've been through a lot lately."
    mei "If I could somehow help you, I would."
    "Anya stares at me for about ten seconds, before she smiles."
    show anya uni smile with cdissolve
    "It isn't her sly crooked smile. This one is more gentle and sincere."
    any "Thanks."
    "She takes a deep breath and sits down, leaning on the railings enclosing the space. I sit next to her."
    stop music fadeout 5.0
    show mei uni default2 with cdissolve
    
    any uni frown "I think I've made a mistake again."
    any "I got carried away when speaking to Professor Goodwitch earlier, I guess."
    mei "Well ... if you want my opinion, then, yes."
    mei uni frown2 "That was a bit ... you know? Walking too near to the edge?"
    any "Yeah. My bad."
    any uni sigh "I'm sorry."
    mei uni default "You always seem interested with anything related to investigation or information."
    mei uni frown2 "Or ... is it just me that see you that way?"
    any uni default "That's true, actually."
    any uni smirk "When I was a kid, I found out that information is something that people willing to pay for."
    any "It's powerful when you know how to use it or to whom you should offer it."
    "I nod in agreement."
    mei uni default "Yes, it is."
    
    show anya uni frown with cdissolve
    "Anya looks reluctant for a moment."
    any "I guess you heard I talked with someone on the phone earlier, right?"
    mei uni frown "Yeah. Before you surprised me with that unnecessary bang on the door."
    any uni smirk "Come on, that's nothing to be upset at."
    mei "I'm still upset."
    mei uni frown2 "Who did you call anyway?"
    any uni default "Oh, right. It was the huntress I often mention to you."
    any uni frown "She scolded me for 'biting more than I can chew'."
    any "She thinks my move to lure Professor to give us an authority for investigation is reckless and unnecessarily put myself in danger."
    any "I shouldn't have done that."
    
    "It's difficult at this point."
    "Part of me agree with what the huntress said about 'biting more than you can chew'. We haven't had any idea what would we face if Professor Goodwitch allow us to do the investigation."
    "On the other side--my curious side that is so eager to get into the bottom of this Golden Apple case--I think Anya did the right thing."
    any "Don't worry too much about it."
    any "It's my mistake. I'll try to fix whatever it caused later."
    mei uni default "I'll help you then."
    any uni shout "... Huh?"
    mei uni smile "You know it's not just you that want to look more about the ... {i}that{/i}."
    mei uni default "Even Ulm Sightjack-ed Saul earlier. He might not seem too excited, but I think Ulm is curious too."
    mei uni smile "So, I'll help you. I don't mind."
    
    "Anya is still looking at me, dumbfounded."
    any "You will help me?"
    hide mei with dissolve
    hide anya with dissolve
    
    "The question reminds me of our first semester, in the first month of our school life."
    "I had hard times accepting the fact that despite of my skill and knowledge, I wasn't the team leader."
    "There were even few times where I almost couldn't resist myself to tell Anya that she wasn't capable to become a leader."
    "However, it wasn't the right thing to do, so I seeked the answer somewhere else."
    "Professor Scarlatina, who was monitoring the students' performance during the initiation, gave me the answer, about Anya showing more potential in coordinating people than me who tends to work alone."
    "Even after that, I didn't fully accept Anya as the leader."
    "With Mom as the leader of team RWBY, to fail becoming a team leader hurt my pride."
    "Anya herself won my heart after that."
    "Instead trying to make us simply accepted her leadership, she asked each of us--personally--the question:"
    "'Will you help me?'"
    
    "I could remember how confused and surprised I were at that time."
    "How could a team leader ask such question? It would make him or her looks weak and incompetent, right?"
    "But Anya didn't give such impression."
    "She's a decent fighter, not exactly the smartest one in our year, but has her own witty quality."
    "And ... she is never hesitated to acknowledge other people's strength. Something I later realized I didn't have because of the privileges I got."
    "My answer for her question remains the same until now."
    
    scene cg anyatalk with fade
    mei "What kind of silly question is that?"
    mei "Of course, I will help you!"
    mei "Do you need me to explain why?"
    mei "First, because you're my partner."
    
    mei "Second, because you're the leader of team AURM."
    mei "And third, why shouldn't I help you?"
    mei "{i}Capiche?{/i}"
    "This time, Anya laughs. It's a good sign, indeed."
    any "Yes, Ma'am."
    any "I'm not going to let you guys down. I promise."
    mei "Guys and {i}gal{/i}."
    
    any "Semantics."
    any "You know exactly what I mean."
    mei "I do, I do."
    
    mei "By the way ... The 'huntress' you called earlier."
    mei "Why you never said her name?"
    mei "No, wait. Do you ever meet her in person?"
    any "Don't worry, I've met her."
    any "As of her name, it was our deal to never mention it whenever I speak of her."
    mei "And why is that?"
    any "She's quite famous. Famous enough until if I slip and say her name and someone from other team hear it, the team will insist to have her contact and ask questions about being hunters."
    
    scene bg rooftop with fade
    show mei uni surprised at midleft with dissolve
    mei "Whoa ... really?"
    mei "You know a person like that?"
    show anya uni smirk at midright with dissolve
    "And there's that sly grin makes a comeback."
    any "You have no idea."
    show mei uni frown with cdissolve
    "I pout and lightly elbow her as a response."
    hide anya with dissolve
    hide mei with dissolve
    scene black with fade
    
    jump endofchoice8
    
label meetulm:
    $ ulm_pt += 3
    scene bg library with fade
    "Both an introvert and a boookworm, Ulm's favorite place is the school's library."
    "I find him behind one of the computers."
    show ulm uni default at mcenter with dissolve
    ulm "Oh, hi, Miss Schnee."
    "I pull the chair next to him and sit."
    hide ulm with dissolve
    "The computer monitor in front of Ulm shows search result about Schnee Dust Company: the company official website, news and articles, sponsored event in Atlas, and so on."
    
    "I think it's safe to assume Ulm is also interested with the letter we accidentally saw at the tower earlier."
    "But at the moment, Ulm's notes on papers stacked near the computer and his notebook get my attention."
    "It's difficult to decipher the scribble--Ulm's handwriting is really, really bad, it's probably because his hand couldn't catch up with the thoughts inside of his head--but I could still read the word 'Semblance' and 'illusion' on the paper."
    
    show mei uni default at midright with dissolve
    mei "What are you doing?"
    show ulm uni surprised at midleft with dissolve
    ulm "Oh ... uhm ... It's a little research."
    "I take a glance at the paper and sticky notes attached on them. In my opinion, those show more than just 'a little research'."
    stop music fadeout 5.0
    ulm uni reluctant "It has been bugging me for a while now. Semblance that can affect human perception of their surroundings ..."
    ulm "There's {i}always{/i} an explanation of how exactly Semblance works and understanding it will be an advantage in combat."
    ulm uni surprised "{size=15}I ... I mean that's what I believe ...{/size}"
    mei "That is true."
    mei "Do you find anything so far?"
    ulm "Rather than 'find anything' ... it's more like I made a guess ... {size=15}I'm sorry ....{/size}"
    
    show mei uni frown2 with cdissolve
    "Really ... what's wrong with Ulm's confidence?"
    "He should stop saying sorry so often."
    "At our current rate, having a guess could be counted as a progress too in unraveling our opponent."
    mei uni default "Care to share your findings with me?"
    ulm "S-sure!"
    play music logic fadein 10.0
    ulm uni default "The first possibility is some kind of 'light-bending'. It's based on how fatamorgana happened."
    show mei uni default2 with cdissolve
    ulm uni thinking "However, reflection and refraction aren't sufficient to create a sight like what you had seen, Miss Schnee."
    "I take a glance at the stacked books near Ulm's notes. There's a book about light-related physics on the stack."
    
    ulm uni default "The second possibility is ... how to put it? I'm not sure how to call it ... It's 'brainwave hack'."
    mei uni surprised "... Sounds bizzare."
    ulm uni thinking "I personally think this is the most feasible hypothesis I can came up with."
    ulm "It's like a reverse version of my Sightjack."
    show mei uni default2 with cdissolve
    ulm "While I 'steal' what another people see through their eyes, this Semblance implant a made-up visual perception on its target."
    ulm uni reluctant "That's why instead of seeing Anya and Rhodo, you saw the droids."
    ulm "The Semblance put 'layers' on what you see."
    mei uni frown2 "That means ... she could make me seeing anything she wants?"
    ulm "I suppose so."
    "Interesting."
    "I wonder if disrupting the scene would affect the 'layer' Emerald put over her target's visual perception?"
    
    ulm uni surprised "{size=15}It's just an assumption, though ...{/size}"
    mei uni default "It would be helpful, however."
    ulm "{size=15}You think so?{/size}"
    mei "Yes."
    "I look at the monitor again, at the various pages about Schnee Dust Company that Ulm opened in the web browser."
    mei "Are you curious about that letter?"
    ulm uni default "What lett--"
    ulm uni surprised "Oh! You mean--"
    ulm uni reluctant "{size=15}Actually ... yes ....{/size}"
    ulm "Professor Goodwitch looked annoyed when found out the window of that letter was still opened. I think it's something that has been bugging her like Anya said."
    ulm uni thinking "And ... based on the date, it's not something related to mission submission for students'."
    ulm "... And the substance test equipment ...."
    
    "Ulm's word keeps getting faint, it's almost inaudible."
    mei uni default2 "Do you think it's related?"
    ulm uni surprised "... I'm sorry? What's related?"
    mei "The apple and SDC."
    ulm uni thinking "I think so ..."
    ulm "Schnee Dust Company does have some problems both from the current director and the previous one ...."
    ulm uni surprised "..."
    stop music fadeout 5.0
    "I could tell Ulm's face gone pale in a milisecond. It's as if he is told that he's sentenced to death."
    ulm uni surprised "I'm sorry! I'm really sorry!"
    ulm "I don't mean to insult your parents!"
    ulm "I'm sorry, I'm sorry!"
    
    "Now he looks in the verge of crying."
    mei uni smile "Hey, it's okay. Really. You didn't insult anyone."
    mei uni default "My parents make a ruthless impression for the public, huh?"
    mei "Believe me, SDC's CEO isn't that cold-blooded like everyone think."
    mei uni default2 "Well ... to be honest."
    mei uni sad "My grandfather, the previous CEO, lead the company in questionable practices."
    mei "Those images stick around until now, even though we have tried to change people's perception about the company."
    show mei uni default2 with cdissolve
    "I take a glance at the opened search tab on the monitor."
    "One of the search result is a news about big event in Atlas which my parents and I attended. I cringe when remembered I usually stay quiet in such occasion."
    
    mei uni default "You probably have noticed how different I act at school and at an event by now ...."
    ulm "{size=15}To tell you the truth ... I once thought Rhodo was going to be jailed because he was rude to you ....{/size}"
    "It takes me about five seconds to fully register what Ulm said just now."
    mei "What--?"
    mei uni smile "Oh my--"
    hide mei with dissolve
    hide ulm with dissolve
    
    scene cg ulmlibrary with fade
    "I couldn't contain my laughter. Ulm whimpers and mumbles 'I'm sorry' for I don't know how many times."
    mei "I'm not going to send anyone to jail, Ulm!"
    mei "Where did you get that idea?"
    ulm "{size=15}I'm so{/size}{size=12}rrrrr{/size}{size=10}yyyyy{/size}...."
    mei "It's okay."
    mei "I'm not omnipotent. I do have limitation."
    ulm "{size=15}Yes ... I'm sorry ....{/size}"
    "I doubt I would get anywhere by trying to assure Ulm there's nothing he need to sorry about, so I change the topic."
    
    mei "Did you find anything so far?"
    ulm "Nothing in particular."
    ulm "I don't know what I should look for. There's no clue."
    ulm "There's no trail whatsoever. This isn't the hunting I usually take part of."
    mei "I see."
    
    scene bg library with fade
    "I already know from Roman that the Golden Apple is based on a formula stolen from SDC's research and development. It would be a good starting point if Ulm want to look more information about it."
    "The problem is I don't remember if the break out made it to the news or not. Even if it is, I doubt the company released more detail than necessary."
    show mei uni frown2 at midleft with dissolve
    mei "I could help you with what might be related to ... what-we're-looking-for."
    "I pause, making sure Ulm understands what I mean."
    "He nods."
    mei uni default2 "I'd say, try to compile any news about theft or break out or accident involving SDC in the last 5 years."
    
    show ulm uni thinking at midright with dissolve
    ulm "In the last 5 years ...."
    mei uni sad "I couldn't guarantee we would find anything useful."
    ulm uni default "I'll do it."
    mei "It might be pointless as I've said ...."
    ulm uni thinking "I know. But we can't tell yet."
    ulm uni smile "Thank you for your help, Miss Schnee."
    "I don't really think my offer is that helpful ...."
    mei uni default "Well ... you're welcome."
    ulm uni default "It's always good to have any starting point or a few hints."
    ulm "And if I find anything about the Semblance, I will let you be the first to know."
    
    "I point at the books Ulm has stacked on the table."
    mei "How about I help you figuring it out?"
    mei uni surprised "Or ... did you finish reading all of them?"
    ulm uni surprised "N-not yet!"
    ulm "I'm good at memorizing things, but not fast-reading."
    mei uni default "Okay then. You could go through the articles while I look something we can use to counter the Semblance."
    ulm uni reluctant "Is it okay with you?"
    mei "?"
    mei "It is. Why?"
    ulm "{size=10}Don't you think people may think there's something between the two of us if they see us together?{/size}"
    "I couldn't grasp what Ulm said. How come he could speak with such a low voice?"
    mei uni frown2 "Sorry? What is it?"
    ulm uni surprised "No--nothing!"
    ulm uni smile "Again, thank you for your help, Miss Schnee!"
    
    hide ulm with dissolve
    hide mei with dissolve
    scene black with fade
    
    jump endofchoice8
    
label meetrhodo:
    $ rhodo_pt += 3
    scene bg dormday with fade
    play music vidgame fadein 10.0
    "A medieval-fantasy-themed tune greets me when I entering the room."
    "Rhodo is sitting in front of a holographic screen set up in the middle of our room, right above the low bookshelf and right in front of the window."
    "The curtains have been pulled shut, allowing the screen to look more vivid and clear."
    "Rhodo is in the middle of a battle; a tactical battle with grids covering the area and the characters' possible movement are colored in green. His brows furrow as he's doing the thinking."
    show rhodo uni frown at midleft with dissolve
    rho "Uh ... what should I do?"
    "I don't want to disturb him, so I sit down quietly and watch him."
    
    show mei uni default2 at midright with dissolve
    mei "Don't blame me if you lose this one battle."
    rho "I won't."
    rho uni confused "I'm not that childish, Schnee."
    rho "Unlike you."
    mei uni frown "Rude."
    show mei uni default2
    show rhodo uni frown
    with cdissolve
    "I watch Rhodo playing for a minute. He has been moving his characters around the arena, avoiding what seems to be the enemies he's supposed to defeat."
    
    mei "What's the objective of this battle?"
    rho "'Defeat All Enemies'."
    mei uni frown2 "You've got one nearby previously, why you moved away instead of attacking it?"
    rho "That enemy will explode and hurting everything around if I hit it."
    rho "I need the archer or the gunslinger to take it down, but they keep trying to surround my swordsman and lancer. Geez ...."
    show mei uni default2 with cdissolve
    "To be honest, I always think Rhodo is more into action hack-and-slash game like 'Blink Eclipse'."
    "This isn't the first time I see him playing this tactical RPG, but I'm still surprised by the determination he put for the game."
    hide mei with dissolve
    hide rhodo with dissolve
    
    scene cg rhodogame with fade
    mei "Is it going to kill your swordsman immediately? The explosion?"
    rho "Uhh, it isn't. But their damage is pretty high."
    mei "Healer and magician?"
    rho "They definitely die with one blow."
    mei "I mean, if you have a healer, you can heal the swordsman or the lancer if they took damage from the enemy."
    rho "So, you think I should just blow them out?"
    rho "I still need to distance them away from my healer, though."
    mei "Blow them, heal, distance, and blow them up again."
    
    rho "Whew, I hope those stupid bombs aren't going to chase my healer after that ...."
    mei "How could this game doesn't have a magician character?"
    rho "It has. I can't add him to the party because of the story. He's off somewhere until I dunno when."
    mei "You actually read the story???"
    rho "Well, Schnee! We've been in the same team for two years!"
    rho "I know very well you think I'm dumb, but I do read!"
    rho "Goddamnit, Schnee ...."
    
    "I have to admit I'm the one to blame for stereotyping Rhodo and rarely seeing his other merits."
    "Rhodo does have his own gift and respectable qualities."
    mei "... They chase your healer ..."
    rho "I know, I know. I kind of expecting it."
    rho "I'm going to lure them to the forest path."
    "There's only a row of grid indicating the forest path. Rhodo moves his healer character to the path and--"
    
    rho "There."
    "--he moves the lancer to stand by on the entrance of the path, blocking the enemies from moving further and reaching the healer."
    mei "That's a good move."
    rho "I know right? Should've done it from the beginning if those goddamn monsters didn't split to chase my swordsman and lancer."
    rho "Lancer has this piercing skill where it'll damage 3 enemies in a straight line in front of her."
    "I don't say anything and watch how Rhodo eliminates the remaining enemies with his strategy. He looks really proud of himself when he clears the level."
    rho "Whew ... Finally. Back to story time."
    
    scene bg dormday with fade
    show mei uni default at midright with dissolve
    mei "You could pull some good strategy if you put your best effort, don't you?"
    show rhodo uni default at midleft with dissolve
    rho "What I really need is time to think."
    rho uni frown "I always start slow. In real combat, it's better if Anya or you or Ulm who think out a strategy. You three can think real fast, unlike me."
    mei uni smile "With proper training, I think you can do the same too."
    rho uni surprised "Yeah? Maybe ...?"
    hide rhodo with dissolve
    hide mei with dissolve
    "I let Rhodo read the conversation on the screen for a while."
    
    "What I could understand from bits and pieces of it, there were two nations at war and the main hero was caught up in the conflicts."
    "The main hero, however, came from another country and didn't want to support either nation. But a group of refugee asked for his help, which he reluctantly agree."
    stop music fadeout 5.0
    
    show rhodo uni default2 at midleft with dissolve
    rho "You know, Schnee? I couldn't read until 8. Or 9."
    show mei uni surprised at midright with dissolve
    mei "What?"
    mei "Didn't it give you trouble at school?"
    rho "I didn't go to school. Not enough Lien to send all boys to school."
    rho "Father chose Xan over me. Well, Xan is smarter, so he was doing good at school."
    rho "Father thought I have dys-what-is-it, but it wasn't the case."
    rho "Once I know how to spell and pronounce, I could read smoothly in no time."
    
    rho uni frown "But the time I took to learn how to spell and pronounce ...."
    rho "*sigh* It's disaster."
    mei uni frown2 "Is it the same with how you learn to fight?"
    rho uni surprised "Now that you mention it ...."
    rho "No."
    rho "I learn how to use pencil faster than Xan, actually ...."
    rho "Huh, wait .... That's odd ...."
    rho "Never thought of that."
    
    rho "I know how to use some weapons even though it was my first time holding it."
    rho "The sword, the shield, the lance, the rifle, and the warhammer."
    rho "{size=17}No, really. This is odd. Using weapon is not inherited, right?{/size}"
    mei uni frown "It is not inherited."
    mei "I know some people is more skillful in motor and physical skill, but you're probably just lazy."
    rho "... What?"
    mei uni default2 "You're doing good in Grimm Studies, but not in another subject."
    rho uni confused "Hey, watch it, Schnee!"
    rho "{i}Grimm's Anatomy: Inside Out Edition{/i} is my only favorite book! I've read it from before I got into Beacon!"
    
    mei uni surprised "How come???"
    rho uni smile "Because it has many pictures in it! "
    rho "And fewer words compared to Remnant History."
    show rhodo uni default
    show mei uni default2
    with cdissolve
    "There's another long conversation in the game after the main hero crossed the map and got into a new city. Again, I stay quiet and let Rhodo concentrates on it."
    rho uni default2 "Will it be like this if we get the approval from Professor?"
    
    mei uni frown2 "...?"
    mei "I'm not sure I understand what do you mean."
    rho surprised "Uh, how to put it ..."
    rho "Like this game? Where we have quests we need to clear?"
    mei "It wouldn't be that simple ...."
    rho uni confused "I know."
    rho uni frown "I mean, we don't just go everywhere and looking for things we're not sure about."
    rho "We know where to go and what to do. Like a series of quests in the game."
    "Oooohhh, now I see what did he mean!"
    
    mei uni default2 "I guess?"
    mei uni default "Are you looking forward to it?"
    rho uni default2 "I really want to do something. Especially because Ulm is already involved in this."
    rho uni default "It's like when one of your friend was blamed for something he or she didn't do and then you found the culprit--the real one--and you smacked them for good."
    show mei uni default2 with cdissolve
    
    "I've found such plot in many action movies. It usually comes around the theme of justice and revenge."
    "I always like the justice part, but when it's combined with revenge ... I'm not really sure."
    rho uni smile "It doesn't change anything. It just feels right and great."
    rho uni default "I did that every now and then when I was a kid, y'know?"
    "Recalling that Rhodo was raised in Vacuo where strength matters, I'm not surprised."
    rho uni confused "... Are you currently thinking how to disagree with me like always?"
    
    mei uni angry "Rude. I am not."
    mei uni frown "I, too, am eager with the prospect of actually do something about the Golden Apple."
    mei uni default "We might have different motivation, but I assume we want the same thing for now."
    rho uni default2 "Uh huh ... Which is rare."
    "I roll my eyes."
    rho uni surprised "Oh, yeah."
    rho uni default2 "Are you okay, by the way?"
    mei uni frown2 "?"
    mei "I'm okay .... Why are you asking?"
    
    rho uni surprised "Uhh ... that letter. The SDC letter."
    rho "I didn't feel good when saw it. Company like SDC won't send letter to schools without any good reason, right?"
    rho "I don't know with you, but I will get anxious if I ever find out if the orphanage or Father send a letter to school. For whatever reason!"
    "I am, indeed, curious about the letter, but it doesn't go as far as making me anxious."
    show mei uni default with cdissolve
    "But I really appreciate the gesture Rhodo shows me and stretch my hand to pat him on the head."
    mei "I'm okay. Don't worry."
    mei "Thank you for asking."
    rho uni surprised "Oh? Sure."
    rho uni confused "... Do you remember that I'm older than you, Schnee?"
    rho "You are not supposed to pat someone's older on the head. That's rude."
    mei uni smile "Because your head is within my reach."
    rho "*sigh*"
    
    rho uni default "Whatever."
    hide rhodo with dissolve
    hide mei with dissolve
    scene black with fade
    
    jump endofchoice8
    
label endofchoice8:
    play music casual fadein 10.0
    "Today is the second day after Professor Goodwitch called us to the headmaster's office."
    "Anya gathers us in the room after our last class (this is one of those days when we only have 2 classes in the morning)."
    
    scene bg dormday with fade
    any "You might want to check this out."
    "Anya shows us her Scroll. On its screen there's an opened e-mail, although its interface looks a little bit different."
    "The word 'CONFIDENTIAL', big and red, across the screen makes me checking on the sender."
    "It's from Professor Goodwitch and the same e-mail is also sent to the principle of Beacon Academy, Professor Scrooge."
    "I hold my breath and quick-read the e-mail."
    "Professor Goodwitch, authorized by Professor Scrooge, has assigned us to a Confidential Mission."
    "There's an attachment which I suppose contains a more detailed brief."
    
    stop music fadeout 5.0
    show anya uni default at midleft with dissolve
    show rhodo uni surprised at midright with dissolve
    rho "This is--"
    rho "Whoa there!"
    rho "Is it what I think it is???"
    rho "She gave it for us to handle???"
    play music discussion fadein 10.0
    any uni smile "Yep."
    any uni frown "I fell asleep when reading the brief last night and came late to the class this morning because of that."
    any uni default "I will read them again, but overall I get the gist of it."
    any "Professor orders us to find out more about how the apple is distributed. At least how it gets into the school."
    
    any "She will help us getting access or 'bending' a few rules as long as we can provide reasonable explanation why we need it."
    any "No need to say we have to carry this mission with discreet, right? You saw the big 'CONFIDENTIAL' mark."
    any "She emphasized--with underline and bold--that we have to avoid involving ourselves further if we ever meet a few persons she listed in the brief. They're considered dangerous and not to be handled by students."
    stop music fadeout 5.0
    hide rhodo with dissolve
    
    show mei uni default2 at midright with dissolve
    mei "But we still need to make sure they're really involved, right?"
    any uni frown "Not necessarily. I've checked a few names and apparently they're most wanted criminals in Remnant."
    mei uni surprised "Most wanted criminals???"
    hide mei with dissolve
    show rhodo uni surprised at midright with dissolve
    rho "Why they involve themselves in this???"
    
    rho uni frown "I mean, they're {i}most wanted{/i} criminals, aren't they supposed to do something more dangerous and cruel than just illegal drugs?"
    any uni default "If black market, trafficking, and illegal goods give you billion Lien, why bother with killing and robbery?"
    rho uni surprised "Uhh ... well ... {size=15}Billion Lien??? For REAL???{/size}"
    any uni smirk "For real, Rho. As real as your pancake creation. Ridiculously unbelievable but it's real."
    hide anya with dissolve
    hide rhodo with dissolve
    
    "The comparison wouldn't win linguistic award, but I like how Anya put it."
    "Rhodo does have the skill in cooking pancake, which I heavily doubt before I ate it myself."
    "I dare to say it's incredible and not even the cake butler from the White Castle could make anything close to it."
    "He's currently experimenting with a variation he described as 'manly pancake' that would make anyone who eat it--both men or women--feel manly after finish eating the pancake."
    "I've heard him mentioning bacon, salami, and steak so far."
    "I have no idea how Rhodo would incorporate steak into pancake, but if he could combine bacon and mushroom to his pancake recipe, maybe he would find a way."
    
    show anya uni default at mcenter with dissolve
    any "Anyway."
    play music discussion fadein 10.0
    any "I've listed some places we can check out to look for starting."
    "Anya shows a notebook to us. There are several places written next to each places, each of them numbered and the numbers are revised several times."
    any uni smile "Hack-proof method, pen and paper."
    any "You already know some of these, I believe."
    
    "There's 'Tukson's Book Trade' and 'administration office' on the list. I know those two, but the rest of them ..."
    show anya uni default at midleft with dissolve
    show ulm uni surprised at midright with dissolve
    ulm "Uh ... Anya?"
    ulm "Aren't those places--how should I say it--not really good ones?"
    any uni smirk "In what manner?"
    any "If you're looking up for some information, they're pretty good choices."
    "Rhodo and I look at each other, not understanding what are Anya and Ulm talking about."
    "I'm from Atlas and Rhodo is from Vacuo, while both Anya and Ulm are from Vale, which means they know more about this city than us."
    hide ulm with dissolve
    
    show mei uni frown2 at midright with dissolve
    mei "Please enlighten me about your discussion."
    mei "Which place do you refer to and why are they 'not really good'?"
    any uni default "This."
    "She points 'Mitsuguma Club' and 'Crow Bar'."
    any "These two places are in the Industrial District. Mitsuguma is near the border to Commercial District and Crow Bar is in the port."
    hide mei with dissolve
    
    show ulm uni reluctant at midright with dissolve
    ulm "{size=15}Industrial District is not the safest place in Vale.{/size}"
    any "In terms of vandalism, theft, and fights, yeah. The crime rate is quite high, or so I heard."
    ulm "{size=15}I don't understand why the police don't do much to secure the district ...{/size}"
    any uni sigh "You know exactly why."
    hide ulm with dissolve
    
    show rhodo uni surprised at midright with dissolve
    rho "Why?"
    any uni default "Bribery."
    "The way Anya talks about it so casually somehow disturbed me."
    "Bribery, high crime rates, unreliable authorities aren't something you suppose to talk about like they're normal things."
    hide rhodo with dissolve
    show mei uni frown2 at midright with dissolve
    mei "Do we have to go there?"
    
    any uni frown "I know you're not going to like it, so I put them in the last options."
    any "If by any chance we have to visit them, we're going together. Don't worry."
    "I'm still worried."
    any uni default "For now, let's focus on Tukson and the administration office."
    show mei uni default2
    any "I will go to Tukson."
    any "I leave the administration office to you, Mei."
    any "Ask around about books delivery. Get the names of the students."
    mei "Alright."
    
    mei uni frown2 "Is that all?"
    any uni frown "I don't send you to Vale because everyone in the city already know your face."
    "Anya reads my mind, apparently. "
    extend "Or she simply knows me that well."
    any "I want to avoid 'a Schnee came to the city and asked around about the Golden Apple'. I think it's not good for the 'discreet' part of our mission."
    "She's right."
    "Bearing the Schnee's family name and having distinctive hair coloration really don't do me a favor in this kind of situation."
    mei default "I understand."
    
    any uni sigh "And there's an urgent matter."
    any uni frown "Professor told me someone had tried to get Saul to go home."
    hide mei with dissolve
    show rhodo uni surprised at midright with dissolve
    rho "Huh? What do you mean?"
    rho "He's guilty for using the doping, but it doesn't mean he can't go home, right?"
    any "We need a written permission approved by appointed teacher whenever we want to leave school and go to the city, remember?"
    any "Team SKYE's appointed teacher is Professor Scrooge."
    any uni default "We've heard that old man is so strict about giving his permission."
    
    show mei uni surprised at mcenter with dissolve
    mei "But Professor Scrooge--"
    any uni frown "Is away at the moment for Vytal Festival meeting in Atlas, yes. He can't approve any kind of permission while outside."
    any "Professor Goodwitch noticed this when one of the staff asked her where to find Professor Scrooge, thinking he already come back."
    any "Whatever is the reason behind this, Professor wants us to figure out where Saul will go tomorrow when arrives in Vale."
    rho uni frown "Why Professor didn't just cancel the permission?"
    
    hide anya with dissolve
    hide mei with dissolve
    show ulm uni reluctant at midleft with dissolve
    ulm "Because Professor Goodwitch can't do that."
    ulm "She's registered as Beacon Academy staff, officially under Professor Scrooge, but not as an {i}official{/i} vice principal."
    ulm "Her authorities are just above normal teacher, but not as much as vice principal or the principal himself."
    ulm "Only the principal or the vice principal can reject an already approved student's leaving permission."
    ulm "Since Professor Scrooge became the new headmaster, the title vice principal is erased from the staff structure."
    
    rho uni confused "I don't know why Professor Goodwitch can assign us with secret mission but can't cancel student leaving permission."
    hide ulm with dissolve
    show mei uni frown at midleft with dissolve
    mei "Change in roles sometimes followed with such things."
    rho uni default2 "Real story, huh?"
    "I nod, personally think that this problem could be avoided."
    "But what could we students do?"
    rho frown "But Professor Scrooge still can do that, right?"
    
    hide mei with dissolve
    show ulm uni thinking at midleft with dissolve
    ulm "He's out of the school network."
    ulm "There are many actions that can't be done if Beacon staff isn't connected to Beacon local network for security purpose."
    ulm reluctant "That's why Professor Goodwitch realized something has gone wrong."
    ulm "Someone among the staffs are probably work with the culprit."
    rho uni surprised "What the--"
    hide rhodo with dissolve
    
    show anya uni sigh at midright with dissolve
    any "Professor Goodwitch can change the leaving date, however. She changed Saul's permission date to tomorrow, stalling him so she can get the brief ready for us."
    any uni frown "He's supposed to leave yesterday."
    
    any uni default "Ulm, can I leave this one on you?"
    ulm uni surprised "Me?"
    any "You don't need to do anything. Just stalk Saul and see wherever he goes. No need to interfere if you don't see him in trouble."
    ulm "But are you sure?"
    any uni sigh "Yes."
    any "Saul is dumber than the deer you usually follow in Vale's outskirt, so I don't think you'll get much trouble with this."
    any uni shout "Uh .... Please don't say that outside of this room ...."
    
    hide anya with dissolve
    hide ulm with dissolve
    show mei uni frown at midleft with dissolve
    mei "That's rude. To describe Saul like that."
    show rhodo uni confused at midright with dissolve
    rho "But he {i}is{/i} dumb."
    mei "And you think you're smarter than him?"
    rho uni shout "Hey! I have textbooks he doesn't even bother to get! And I scored better than him in Weapon and Combat class!"
    hide mei with dissolve
    hide rhodo with dissolve
    
    "Bonus info: Rhodo does score better than Anya in Weapon and Combat class."
    "It's surprising at first when I found out about that, but then there was an ... accident in our room related to ice Dust and an attempt to build a self-made mini-refrigerator."
    "We came up with a conclusion not to trust Anya with handling elemental Dust measurement for anything."
    "If she does, someone need to double-check after her, making sure she's not going to create a mini-apocalypse."
    
    show anya uni sigh at mcenter with dissolve
    any "Don't start again, you two."
    any "And, Mei, quit provoking Rhodo."
    mei "Yes, Ma'am~"
    any uni default "Back to topic. Ulm?"
    show anya uni default at midright with dissolve
    show ulm uni default at midleft with dissolve
    ulm "I think I can do it."
    ulm "I will need to submit a request for leave tomorrow then."
    any uni smirk "Consider it done."
    any uni default "I think Professor Goodwitch already has our handler, Professor Port, to cooperate. They're old cronies after all."
    hide ulm with dissolve
    
    show rhodo uni surprised at midleft with dissolve
    rho "Euh ..."
    rho "Do you have anything for me?"
    rho "I'm not really sure I can help with anything, though."
    any "Do you have anything planned for tomorrow?"
    rho uni default2 "Uh ... not really."
    rho "Jared didn't ask me for sparring or such, so, yeah, I'm free."
    rho uni frown "Should I go with Ulm as backup?"
    any uni frown "No. I don't think that's a good idea."
    any "You're not familiar with the city layout and I doubt you can keep up with Ulm."
    rho uni surprised "That makes sense, yeah ..."
    
    hide anya with dissolve
    show ulm uni surprised at midright with dissolve
    ulm "{size=15}That's exaggerating ... I'm not moving that fast ...{/size}"
    rho uni default "You're doing great with your free-running training! That ain't exaggerating!"
    rho uni smile "You need to work on your stamina, but overall you're doing awesome!"
    ulm "{size=15}T-thank you ...{/size}"
    hide ulm with dissolve
    
    show anya uni default at midright with dissolve
    any "For now, keep your eyes and ears open without getting overly suspicious, okay?"
    rho uni default "Okay."
    any "Oh, right. One more for you, Mei."
    hide rhodo with dissolve
    any "Go meet Professor Scarlatina and ask her for the flash drive."
    
    show mei uni frown2 at midleft with dissolve
    mei "?"
    mei "Alright."
    show mei uni default2
    "For a moment, I thought Anya is going to explain what does Professor Scarlatina have to do with our confidential mission, but Anya doesn't say anything further, except:"
    any uni smile "Professor Scarlatina is our ally. We can trust her."
    any uni default "But you don't need to explain everything to her. Ask for the flash drive and keep your conversation on track."
    mei "I understand."
    hide mei with dissolve
    
    show anya uni default at mcenter with dissolve
    any "Alright. It settles then."
    any "I will take care the permissions for now. Tomorrow, I want to do a quick-review. Please gather in the room and don't go out before we do the review."
    mei "Okay."
    rho "Yes, Ma'am."
    ulm "Understood."
    hide anya with dissolve
    
    stop music fadeout 5.0
    
    scene black with fade
    
    
    "We don't talk much about our assignment for tomorrow."
    "It was so sudden, I'm not even sure whether everyone get nervous like me or not."
    "Yes, I am {i}nervous{/i}."
    "I've tried to dismiss the uneasy feeling I had after Anya gave our first task, but ... it's still there."
    "Is it because there's a warning about the involvement of most wanted criminals?"
    "Does it ... trigger the unconscious fear dwells in me after the incident with Cinder Fall?"
    "... "
    extend "Ugh."
    "It couldn't be like this."
    "I need to be stronger than this. I need to have more faith in myself."
    "... But ..."
    "Having too much faith in myself was exactly what led my own fall ..."
    "Then what should I do?"
    
    scene bg dormnight with fade
    "Later that night, while laying in my bed, I open my Scroll."
    "Still no new message. Neither from Mom nor Li."
    "I realize something just before my eyes close and I fall asleep, but I don't remember it the next morning."
    
    scene bg dormday with fade
    play music casual fadein 10.0
    "We do a really quick briefing before leaving. We review what we need to do today and list a few back up plans."
    "I would say even our so called 'back up plans' doesn't sound like it was carefully planned like what we usually do."
    show anya nor default at mcenter with dissolve
    any "Alright. I think that's enough for now."
    any "Best case, Ulm and I will go back before curfew. Worst case, we'll spend the night in the city."
    any "We will update you if there's a change in plan."
    
    show anya nor default at midleft with dissolve
    show mei nor frown2 at midright with dissolve
    mei "You better do that. Don't make us worry."
    any nor smirk "It's going to be alright."
    any nor default "Okay. Are you ready, Ulm?"
    hide mei with dissolve
    show ulm nor default at midright with dissolve
    ulm "Yes."
    rho "Good luck and be careful you two!"
    ulm nor smile "We will!"
    hide anya with dissolve
    hide ulm with dissolve
    
    "And now it's just me and Rhodo in the room."
    show mei nor default2 at midleft with dissolve
    mei "Still haven't decided where to go and what to do today?"
    mei nor default "You could tag along with me if you want."
    show rhodo nor default2 at midright with dissolve
    rho "Darion texted me this morning, inviting me to play board game."
    mei nor frown "...."
    rho nor confused "... I kinda expect you to snap out at me for playing while everyone else have task in their hand."
    mei "I almost snap."
    
    rho nor surprised "Okay, listen. Don't snap at me."
    rho "Darion told me he invited the rest of team SKYE to join the game. I think it's a chance to dig a thing or two from them."
    rho "They must've known something about Saul that we don't know."
    "When I heard the name 'Darion'--the leader of team DJJT--I immediately become wary."
    mei nor frown2 "You need to be very careful."
    rho nor default "Uhm ... so that I don't lose the game? I will. Although I doubt I can outsmart Darion ...."
    mei nor frown "No, blockhead."
    mei nor frown2 "Did Darion mention anyone else? His twin?"
    rho nor surprised "Huh?"
    rho nor frown "You're right. He didn't say his twin will join the game. They usually together when they held gaming session like this."
    
    mei "Remember, Darion pokes around the school more than Anya does. He's the one who uncover the cheating ring in this school."
    mei "He probably heard about Saul's leaving and decided to find out more from Saul's teammates."
    rho nor confused "Uh ... now I feel intimidated."
    rho "I wanna back out of this, but I know there's a chance to figure out about what happened inside team SKYE."
    mei nor default "Listen."
    mei "As long as you don't blurt out about our investigation, it's going to be okay."
    mei "Try to balance your concentration on the game and paying attention to the other party's conversation."
    rho "Easy for you to say ...."
    mei nor cocky "You're the only one who could do this now, come on."
    mei "Where's that confidence of King of The Castle?"
    
    "Sometimes Rhodo dubbed himself as 'King of The Castle'."
    "It's childish, totally childish. But after watching him for a while, I think it's his way to boost his own confidence and also taunt his opponent."
    rho nor frown "Well!"
    rho nor shout "That punk Jared will mock me if I back away from this game. There's no way I'll let him do that!"
    mei nor smile "That's the spirit!"
    mei nor cocky "Go and crush them!"
    hide rhodo with dissolve
    hide mei with dissolve
    "Honestly, there's a satisfaction when I finally convince Rhodo to do something he was reluctant."
    "Rhodo is quite simple if you know which button you need to push."
    
    "Now, it's time to do my own task."
    "I leave the dormitory and walk toward the main building."
    scene bg walkway with fade
    "So, where should I go first?"
    
    menu:
        "Meet Professor Scarlatina":
            "I decide to meet Professor Scarlatina first."
            jump meetvelvet
        "Go to the Administration Office":
            "I decide to go to the Administration Office first."
            jump adminoffice
    
label meetvelvet:
    $ meet_velvet = True
    "Velvet Scarlatina aka Professor Scarlatina is a combat instructor and the one who responsibles for 'Advance Dust and Weapon' class."
    "Many people mistake her as professor in 'lame classes' like 'Remnant Herbology' or 'International Culture and Sociology' because of her appearance."
    "Professor Scarlatina is a hare Faunus."
    "... I {i}assume{/i} from her animal ears that she is a hare Faunus."
    "To my own surprise, I couldn't understand the difference between 'hare', 'bunny', and 'rabbit'."
    "I would look it up in the dictionary later."
    "Or, should I look it up in the animals encyclopedia?"
    
    scene bg corridor with fade
    "I arrive in front of Professor Scarlatina's office."
    "I knock the door."
    # knocking sfx
    "I could hear an faint answer from inside, permitting me to enter the room."
    #scene bg veloffice with fade
    "The very first thing I notice from this room is the weapon blueprints and schema pasted all over the wall."
    "Big weapons, small weapons, close-ranged weapons, long-ranged weapons ..."
    "Bullet designs, Dust cartridge models, mathematics and physics equations ..."
    "I have to swallow and hold myself not to start fangirling over the amazing design of a big sword that its blade could open up and the core inside it could shoot Dust-powered blast."
    "It looks really-really-really cool!"
    vel "Oh? Miss ... Schnee, I believe?"
    
    scene cg velvet with fade
    mei "Yes, Professor."
    mei "Good afternoon. Am I interrupting something?"
    vel "No, no, no. It's fine, it's fine. I'm not doing something at the moment."
    "I have a strong feeling that Professor is lying."
    "I saw her frantically scribbling something on her drawing pad before she put the pen away and closed her portable computer quickly."
    "She doesn't need to do that if she was doing one of her weapon design, right? Professor print and put her designs all over the room after all."
    vel "Is there anything I could help you with?"
    mei "Well, yes there is."
    mei "Anya told me to get a flash drive from you."
    vel "Ah, I see."
    vel "Hold on, I'll get it for you."
    "Professor Scarlatina goes back to her desk, pulling one of her drawer, and takes a small metallic flash drive."
    vel "How're your parents doing?"
    "I did hear from Mom that Professor Scarlatina was a year above my mother when they were students at Beacon Academy. They know each other."
    
    scene bg classroom with fade
    show mei nor smile at midleft with dissolve
    mei "They're doing great. Thank you very much for your concern."
    "Professor Scarlatina hands the flash drive to me."
    show velvet thinking at midright with dissolve
    vel "Must be hard for Weiss to handle everything happened lately ...."
    mei nor default "She is doing her best to keep everything in order, Professor."
    vel nor default "Knowing Weiss, she sure does, I believe."
    vel "She's a strong-willed person."
    hide mei with dissolve
    hide velvet with dissolve
    
    "I would be more than glad to acknowledge that statement."
    "Both of my parents are stubborn and persistent."
    "Sometimes I wonder how in Remnant they got along in the first place."
    show velvet default at midright with dissolve
    vel "Please send my regards to her."
    show mei nor smile at midleft with dissolve
    mei "Sure, Professor."
    mei "Thank you very much."
    vel "I hope it helps you. The flash drive."
    vel thinking "And ... please be careful."
    mei nor default "We will, Professor."
    # door click ?
    hide mei with dissolve
    hide velvet with dissolve
    
    scene bg corridor with fade
    "I leave Professor Scarlatina's office and close the door behind me."
    "I put the flash drive in my pocket, wondering what is inside it."
    "Professor said, 'I hope it helps you', which lead me to think that the flash drive contains something related to our mission."
    "I still don't know how and why Professor Scarlatina has the information that might help us."
    "Anya sure has a lot of explanation for us, huh?"
    "I'm going to push her to speak up later."
    
    if admin_office:
        jump finishtask
    else:
        "Now, to the Administration Office."
        jump adminoffice
    
label adminoffice:
    $ admin_office = True
    scene bg corridor with fade
    "The Administration Office is located in the furthest part in the main building."
    "This place is basically where students go for administration matters and to retrieve packages."
    "Every packages for students will be kept in the Administration Office for screening before students are allowed to get them."
    "However, Beacon Academy is a school to train hunters. The school doesn't use advance and intricate security check like the one in the immigration posts."
    "They only employ basic security checks, at least to make sure no dangerous or harmful goods get into students hand."
    "Rumor said Beacon Academy applied this security check after someone managed to send a dog in a mail for one of the student."
    "Who in Remnant send a dog??? {i}In a mail{/i}???"
    
    #scene bg admoffice with fade
    "The one and only staff on duty jumps from his chair when he sees me approaching the counter."
    staff "Good afternoon, Miss Schnee!"
    staff "How may I help you today?"
    "I don't get a good first impression about this guy. His excitement doesn't look natural."
    "I would try to think that he simply doesn't expect me to be here when he's on duty."
    show mei nor default at midleft with dissolve
    mei "I want to check is my package somehow mixed up with another student's package or not."
    mei "It's a book. I was told it should be arrived two or three days ago."
    staff "Hmm ... I will check it for you then ..."
    show mei nor frown2 with cdissolve
    "I need {i}the names{/i}, but I know I couldn't just ask it right away."
    "It would raise suspicion from him and that's the last thing I need right now."
    
    show mei nor default2 with cdissolve
    staff "I don't think I see your name here, Miss ..."
    mei "Really?"
    staff "Yes. I've double-checked it."
    mei "Do you think it was sent to someone else by mistake?"
    staff "Sorry?"
    mei nor frown2 "The store put wrong address and sent it to someone else?"
    staff "Um ... well, if that's the case, you probably need to contact the shop, Miss."
    "This is the moment when I'm glad that I befriend an avid reader like Tizana who often goes to the Administration Office just to yell at anyone on duty because her book package is nowhere to be found."
    
    mei nor cocky "I heard from Miss Velsov that her package was given to another student a couple of weeks ago."
    "I'm not bluffing."
    "Tizana almost unleashed her Semblance and destroyed the office because she found her limited edition book was given to another student and the book returned to her with a big juice stain on its pages."
    "She declared the Administration Office as her nemesis for life after that."
    staff "Um ... yeah ..."
    staff "A really ... {i}unfortunate{/i} event."
    mei "Beacon Academy sure isn't the safest workplace around, is it?"
    
    mei nor default "Could I have the list of packages from Tukson's Book Trade instead? I'll go check myself if it was sent to the wrong person."
    staff "I can do that. Not many packages from that place, I can even print the whole deliveries from the beginning of the semester until now in one page."
    show mei nor frown2 with cdissolve
    "Not many?"
    "Hmm ... Perhaps our guess that one of the distribution method for Golden Apple is through fake books isn't quite right ..."
    "But if Saul tried to defend himself when confronted about his calls to the bookstore, this data might come useful."
    show mei nor default2 with cdissolve
    staff "Here. The list."
    mei nor smile "Thank you very much for your help."
    staff "You're welcome."
    hide mei with dissolve
    
    if meet_velvet:
        jump finishtask
    else:
        jump meetvelvet
    
label finishtask:
    scene bg walkway with fade
    
    "Now that I've finished my tasks, I paused for a moment to think what should I do next."
    "I don't want to stay idle while all of my teammates are off somewhere gathering any useful lead."
    
    stop music fadeout 5.0
    
    if bribe_jill:
        unk "Not going to the city?"
        "I look up and find Jillia sits on a tree. She drops herself and hangs upside down."
        show mei nor frown2 at midleft with dissolve
        mei "I don't go to the city too often."
        mei nor default2 "Why are you asking?"
        jil "Jillia saw your friends go."
        mei nor default "Anya and Ulm? Yes. They have something they need to attend to."
        jil "The people from the locker room go to the city every week."
        mei nor frown2 "?"
        "It takes me a few seconds to understand who Jillia refers as 'the people from the locker room'."
        mei "You mean ... the third years?"
        jil "The one who got your one-eyed friend."
        
        play music intense fadein 10.0
        "Now this piece of information picks my curiosity."
        "What are they doing going to the city every week? Is it a family visit? Or something else?"
        "But the more important matter is Anya and Ulm are currently in different place, possibly far enough they wouldn't be able to catch up each other in short time in case something urgent come up."
        "I'm kinda worried if the third years try to flank Ulm once more, not satisfied with their last result with him."
        
        "Hopefully this is just me overthinking about the circumstances."
        "Still ... I better call Anya or Ulm after this."
        mei nor default "You sure pay much attention who's going out to the city, don't you?"
        "Jillia grins."
        jil "Because Jillia can't go to the city alone."
        jil "Jillia watch others. And learn."
        jil "Jillia do a good job. Learning."
        "Her response baffles me so much, I couldn't find a good words to put up as a reply."
        mei nor frown2 "It's ... good for you."
        jil "It's good for Jillia."
        mei nor default "I would see you later then."
        mei "Have a nice day!"
        
        hide mei with dissolve
        
        scene bg corridor with fade
        
        "I open my contact list once I reach the corridor inside."
        
        menu:
            "Call Anya.":
                jump callanya
            "Call Ulm.":
                jump callulm
        
        label callanya:
            $ anya_pt += 1
            "I stop at Anya's number."
            "I trust Anya's split second improvisation and decision making. She would know what to do in this situation."
            any "Anya Lyre. Something's up, Mei?"
            show mei nor frown2 at mcenter with dissolve
            "Even though I want to comment on her habit of providing her name whenever she answers a call, I proceed straight to the point."
            mei "Just now I met Jillia and she told me the third years are also in the city now."
            mei nor sad "The same third years who cornered Ulm a few days ago."
            mei nor frown2 "I'm not a hundred percent sure whether we need to take this into account or not, but ..."
            any "But you've got a gut feeling that we should be careful with them?"
            
            mei nor default2 "Yes."
            any "I'll keep my eyes and ears open then."
            any "By the way, I can use some suggestion from you now."
            mei nor frown2 "What suggestion?"
            any "Apparently, the bookshop is closed today."
            any "I can go assisting Ulm."
            any "Or, I can snoop around Tukson's a little bit more."
            any "I'll tell you what, I can even 'tweak' the back door and go inside."
            stop music fadeout 5.0
            mei nor surprised "!!!"
            mei nor angry "Anya! NO!"
            mei "It's breaking in! You're going to get into trouble!"
            play music cocky fadein 10.0
            any "It'll be quick and nobody will notice."
            
            "At this rate, I'm going to smack Anya on the head for suggesting a law-breaking choice if she was in front of me now."
            mei "Just ... DON'T!"
            mei nor frown2 "Please?"
            "Anya snickers and at that moment, I realize she teased me."
            mei nor angry "Anya! That's not funny!"
            any "Okay, okay. Joke's over."
            any "I'm not gonna intruding, you can have my word."
            any "So, what do you suggest?"
            show mei nor frown2 with cdissolve
            
            menu:
                "Catch up with Ulm.":
                    jump catchulm
                "Wait and see a little longer.":
                    jump waitsee
            
            label catchulm:
                $ ulm_pt += 1
                mei "Do you know where to find Ulm?"
                any "Sure."
                any "I need to tell him about the change in plan before regroup, though."
                any "Contact me again if you find something, okay?"
                mei "Alright."
                mei default "Good luck and be careful."
                
            label waitsee:
                mei "Maybe it's better if you wait and see before leaving."
                mei nor frown "Promise me you wouldn't try to break any law."
                any "I promise, I promise."
                any "I was joking when I told you I could lockpick the door."
                mei "The 'you're going to do it' or 'your ability to do it'?"
                any "The 'I'm going to do it', of course."
                "I make a mental note that Anya doesn't deny her skill in lock picking. Where did she learn it would be the next question to be asked later."
                mei nor default "Oh well ... good luck. And be careful."
                any "Sure."
                any "And thanks for letting me know."
                
            "Anya ends the call."
            stop music fadeout 5.0
            jump endofchoice9
            
        label callulm:
            $ ulm_pt += 1
            "I decide to call Ulm."
            ulm "{size=15}Yes, Miss Schnee?{/size}"
            stop music fadeout 5.0
            "He answers with a very low voice, making me suddenly feel guilty because I forgot that he might be hiding somewhere and the phone call would make unnecessary noise."
            show mei nor frown2 at mcenter with dissolve
            mei "Please just end this call if you're currently hiding or such."
            ulm "No, no, no! I'm fine! Really!"
            ulm "It was because I muted my Scroll and I was in ... say, 'hunting mode'."
            ulm "You don't disturb anything, Miss Schnee, please don't worry."
            
            "Despite of Ulm's attempt to assure me that everything's alright, I still have a little doubt."
            "Better make it quick and clear, then."
            mei nor default2 "Just now, I heard Jillia told me about the third years went to the city."
            mei nor sad "I probably exaggerating about it, but ... at least don't let them get you like the last time."
            ulm "Ah ... alright."
            ulm "{size=15}They really don't have any reason to pick on me, but ...{/size}"
            ulm "I'll be careful, Miss Schnee."
            ulm "Thank you for letting me know."
            
            mei nor frown2 "Please be safe."
            mei "If you must run, don't hesitate to do so."
            ulm "I will."
            ulm "Thank you, Miss Schnee."
            
            "Ulm hangs up."
            jump endofchoice9
            
        label endofchoice9:
            hide mei with dissolve
            "I could only hope that my worries prove to be too much."
            "..."
            "A lot of questions pop up in my head."
            "Am I becoming somewhat paranoid?"
            "Or am I just on the edge because of the sudden assignment from Professor Goodwitch?"
            "I think in my normal condition, I wouldn't make a fuss over unclear information like what I've heard from Jillia."
            "But, again, what is my 'normal condition' now?"
            "Everything has gone wrong ever since that day I crossed Cinder Fall."
            
            "... "
            extend "I hope I don't hold back the others with this."
            "I take a deep breath, trying to calm myself down."
            "Everything's going to be alright, I know."
            
            stop music fadeout 5.0
            
            jump endpt5
            
    else:
        menu:
            "Go to the library.":
                jump golibrary
            "Go to the dining hall.":
                jump godinehall
        
        label golibrary:
            scene bg library with fade
            "I go to the library."
            "My first thought is I would meet the group who play boardgame there and might sit nearby to listen to any interesting info."
            "Or if they still have place for one more player, I could probably join them even though I'm not that fond of playing boardgame."
            "Unfortunately, there is a group currently playing a pen and paper game in the furthest part of the library, but it isn't Darion and the others."
            "They probably play in the dining hall, where during weekend the place turn into some sort of big common room despite of we do have common room in the dorm."
            
            "I look at the computer, thinking of staying here and searching the net instead."
            "And someone familiar slips to one of the seats before I reach the table."
            "It's one of the third years that cornered Ulm earlier."
            "Assuming from the books and the already turned on computer, he has been here for a while and went away for a short break."
            "He doesn't notice me even though I'm standing quite close to him."
            
            show st1 at midleft with dissolve
            st1 "Why did I agree to do this research in the first place ...?"
            st1 "It's not that I can find that damn apple by looking it online ... Geez."
            hide st1 with dissolve
            "I hold my breath."
            "Did I mishear him?"
            "Even though he might recognize me if I sit near him, I think he wouldn't suspect me for anything if I bring a book and occupy myself with the computer."
            "Without much thinking, I walk toward the Semblance section and pick a book about Semblance degeneration."
            "I'm not sure myself why in Remnant I picked that book."
            "Back to the computer table, I sit one chair away from the third year student."
            "If what I read sometime ago was right, nobody would sit between us except all chairs were filled."
            "This distance should be sufficient to listen and watch him."
            
            "..."
            extend "..."
            extend "..."
            "About ten minutes later, it seems frustration and boredom has overtaken him."
            "After leaning back on his chair and staring at the ceiling, he laid his head on the table and didn't move."
            "I think he is sleeping."
            "Working in library during the weekend, for sure, is nowhere near exciting."
            "I don't make any progress with my reading either, but I found some interesting news and articles regarding Semblance in the net."
            "It's said that recently the scientists are researching a method to cure Aura decay and Semblance degeneration."
            "With my Semblance badly deteriorate after Cinder force took it from me and I got it back again, those news make me interested."
            "There are one or two differences between the real Semblance degeneration case and my case, but most of them are the same."
            "It gives me a little hope to fully recover my Semblance."
            
            "Pretty sure my visit in the library wouldn't give me more useful information, I turn off the computer and return the book to the shelf."
            "Just before I leave, I take another glance to the third year."
            "He is up and answer a call, speaking in low voice that I couldn't hear clearly."
            "I wait for another moment, hoping that I could overhear something."
            "Unfortunately, I couldn't get anything from him."
            
            jump endpt5
            
        label godinehall:
            scene bg dininghall with fade
            "I go to the dining hall."
            "During weekend, when there are no classes and at least half of the academy staff in charge of cooking take a day off, there is quite a major change in the dining hall."
            "First, in exchange of the usual buffet serving, students could only pick one from 3-4 menu available."
            "Second, the dining hall has somewhat turns into a bigger version of common room where a lot of students hang out, either they have a casual conversation, work on group homework, or play card or boardgame."
            "The boardgamers usually play in the library, but I've been told that a few variant comes with bigger 'board' which occupy more space. That's why they play in the dining hall."
            "The second reason is there's no restriction to keep their voice low like in the library."
            "I take a look around and find Darion's group on one of the table."
            
            play music reckless fadein 10.0
            "Rhodo slams the table just now and shouts a swearing that echoes through the hall. A member of team SKYE cackles. Darion collects the cards on the table, forcing a bitter smile."
            "What an interesting sight."
            "Darion and his twin are prominent gamers, as far as I know. They rarely lost and when they do, it usually after a tough, intense session."
            "Judging from what I see, it seems one of team SKYE had beaten the others for good."
            
            "I walk toward the vending machines."
            "I'm not used drinking soft drinks like canned soda. My first time having one is when I officially accepted in Beacon."
            "It might be a little exaggerating, but I never saw vending machines as many as the ones in Beacon Academy dining hall."
            "At first, I was tempted to try each of them, but then I had an awful experience when we played Truth or Dare in the last semester."
            "To limit the 'Dare challenge', Anya mixed some canned drinks she got from here into two bottles. So whoever picked 'Dare' that time, he or she had to drink a shot from one of the bottles."
            "There's this one foul drink that should never be existed in the first place named Wasabeer."
            "... That's exactly what Rhodo is currently buying now, next to me."
            
            show mei nor frown2 at midleft with dissolve
            mei "Did you lose?"
            show rhodo nor confused at midright with dissolve
            rho "Yeah."
            rho "The losers will have to drink half can of this."
            "Rhodo slightly shakes the Wasabeer can on his hand."
            rho nor frown "And since I had the lowest point, they told me to buy."
            
            mei nor default "Team SKYE has a formidable opponent for Darion, hm?"
            rho nor surprised "Yeah. It surprised me that he lost."
            mei nor frown2 "{size=17}Did you overhear anything?{/size}"
            rho nor frown "{size=17}As you've said, Darion asked a lot of questions.{/size}"
            rho "{size=17}I don't know if it's relevant or not. His questions were random and not important.{/size}"
            mei "{size=17}Just memorize them and tell us later.{/size}"
            
            "Rhodo collects the last can, trying to bring all five cans by himself."
            mei nor default "Here, let me help you."
            rho nor default "I can do it."
            rho nor surprised "Uh-oh."
            hide rhodo with dissolve
            hide mei with dissolve
            "One Wasabeer falls on the floor."
            
            "I see Darion approaches us when I pick the falling can."
            "He smiles and nods politely at me, before helping Rhodo with the Wasabeers. He doesn't say a thing to me."
            "It's fine with both of us. I get along with the whole team DJJT, but the twins and I tend not to show how close our relationship is."
            "Darion and Tizana are from Atlas and they come from a currently declining noble family."
            "Getting too close to me might invite rumor that span to family affairs. The three of us agree that it would be too much hassle and do our best to avoid it."
            "In another circumstance where both of our team members are present, we mingle and have a chat without any worries."
            
            "I pull the ring on my soda can. It let out a hiss."
            "..."
            "... Why it isn't chilled???"
            "Is the machine broken???"
            "I turn around and see one of team SKYE chokes on his first gulp of Wasabeer. His teammate laughs at him."
            "Rhodo--with mischievous grin on his face--and another teammate grab him down to his seat and urge him to drink more."
            "I couldn't help not chuckle at them before leaving."
            stop music fadeout 5.0
            
            jump endpt5
            
    

        
label endpt5:
    scene black with fade
    call chapter("End of Part 5", "Grey Area") from _call_chapter_8
    
    #centered "End of Part 5 - Grey Area"
    
    jump part6
    
return