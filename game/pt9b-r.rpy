# PART 9: CERBERUS #

label part9BR:
    
    "I couldn't sleep too well."
    "The weather was cold, keeping me awake every now and then and making it difficult to fall asleep again."
    "And because of that, I think I had a nightmare."
    "Even though I don't remember what it was about when I woke up."
    
    "I finally get up and wear my cloak when I see there's enough light from the outside."
    scene bg island with fade
    "The sun hasn't risen yet. The sky looks grey, but clear."
    "Rhodo sits near the entrance door. His eyes are closed and he nods off."
    "When I thought he has been sleeping outside for the entire night, Rhodo opens his eyes."
    
    show rhodo nor surprised at midright with dissolve
    rho "... Huh?"
    rho "Schnee? You awake?"
    show mei nor frown2 at midleft with dissolve
    mei "It's morning and I doubt I could continue sleeping."
    mei "Don't say you spent the night outside ...."
    
    rho nor default "Maybe for some hours."
    "He yawns."
    rho nor frown "Damnit ... so sleepy ...."
    mei nor surprised "Did you stay awake since last night???"
    mei nor angry "And you told me you're going to wake me up if you're too tired to keep watch!"
    
    rho nor shout "Well, I wasn't that tired. And I slept for one hour every two or three hour."
    rho nor frown "I'm gonna be fine."
    show mei nor frown with cdissolve
    "Rhodo yawns again."
    "He couldn't be having 'enough sleep' like he said before."
    
    rho nor surprised "... I'm gonna wash my face a bit."
    mei "Yes, go ahead."
    hide rhodo with dissolve
    hide mei with dissolve
    "Really. What about taking turn stay on guard we talked about last night?"
    "Rhodo is wounded too. He should have more rest than what he described earlier."
    
    "Rhodo looks a bit better when he returns. Only {i}a bit{/i}."
    "I could see dark circles under his eyes and he seems out of focus, stares blankly at something faraway."
    "Oh, dear ... this wouldn't do."
    show mei nor frown at midleft with dissolve
    mei "Blockhead. Thirty minutes sleep. Now."
    
    show rhodo nor surprised at midright with dissolve
    rho "Huh?"
    rho "No, no, no. We should--"
    mei nor angry "I said, get some sleep. {i}Now.{/i}"
    mei "You're not in any shape to even defend yourself from a Creeper. We're not going anywhere until you get some rest."
    
    rho nor frown "But--"
    mei "Thirty minutes."
    "Rhodo grumbles."
    rho nor confused "Alright, alright ...."
    hide rhodo with dissolve
    hide mei with dissolve
    
    "I hear him said 'bossy' when walking inside the control post."
    "I glare at him and after making sure Rhodo shut his eyes, I check on my Scroll."
    "There are several messages from Anya and ..."
    "... from Mom."
    
    "Mom would make a fuss if she found out I got stranded in an unknown place. It's totally understandable."
    "But that means she would also find out that I went against her words about investigating the Golden Apple."
    "If I were alone, it's all my responsibility. But as part of a team, it would be {i}our{/i} responsibility, the four of us."
    "Hoping Mom wouldn't be suspicious for my late response, I write back to her."
    
    nvl clear
    m "M: Whoa"
    m "M: Sry for the late reply"
    m "M: I sleep early lst nite"
    
    "I almost type about being a bit tired, but then stop."
    "Let's not make her worried and call around this time."
    
    m "M: Test's almost here"
    m "M: Btw. What's up mom?"
    
    "I wait for a moment."
    "The messages are already sent, but there's no sign that Mom see and read it."
    "At least I already responded her."
    "Now let's see what did Anya send."
    
    "I open her messages and find map data attached on one of them."
    "It's a map of this island, said to be from Professor Goodwitch herself."
    "... Huh?"
    "That means someone had already known about this island and even had its map when the facility was fully functioning."
    "There's a mark on the map, a spot where we have to go next. An airship would meet and pick us there."
    
    "I wonder how much of these buildings, landmarks, and path are still here?"
    "The airship would arrive--approximately--two hours from now."
    "Besides the problem with our route, there's also that giant Grimm."
    "It did fall down and didn't return to attack us during the night, but I think it's still alive."
    
    mei "*sigh*"
    "Let's plan the possible routes for now then."
    "It took me some time to locate our current position, but after that, things are getting easier."
    "I also want to work on some plan for various cases that might happen to us when we're walking to the rendezvous point ...."
    "But my mind draws blank."
    
    "I have no idea what would we find in all of these routes, let alone any way to utilize them to our advantages."
    "How is the environment, the structures, and else ...."
    "I take a deep breath and pinch the bridge of my nose."
    "Calm down, Mei, calm down ...."
    
    "Now is not the right time to get nervous and anxious ...."
    "Everything's going to be alright .... Be sure of it."
    #alarm sound
    "An alarm beeping noise distracts me."
    "It's Rhodo's alarm clock from his Scroll."
    
    "With a groan, Rhodo wakes up. He raise and walks out while stretching his arms."
    show rhodo nor confused at midleft with dissolve
    rho "Mornin'."
    show mei nor frown2 at midright with dissolve
    mei "Morning."
    "Maybe it's a surprising fact, but Rhodo is used to wake up on alarm clock."
    
    "At first, I, just like other students in our year, thought Rhodo is a sleepyhead type, who regularly oversleeps and failed to wake up on alarm noise."
    rho nor default "What's with the gloom face, Schnee?"
    rho nor surprised "Did something happen when I was sleeping? Something really bad?"
    mei "Anya sent us the map of this island and I've been drawing several routes to our rendezvous point."
    
    rho nor smile "That's great! So they sent people to get us out, eh?"
    rho nor default "All we need to do is walking there, right? And with map, we're not going to get lost."
    show mei nor frown with cdissolve
    "I roll my eyes, feeling a bit upset."
    "One reason why I keep calling Rhodo 'blockhead' is because he has a simple, too straightforward thoughts."
    "He is often failed to prevent or avoid bad consequences that he could foresee before."
    
    mei "First, the map might be not accurate with the current condition of this place."
    mei "We could still get lost in our way."
    rho "You said you drew some routes. There are more than one, right?"
    mei nor frown2 "Well ... yes."
    mei "However, second, we still have Grimms as our concern. There would be some in our way to rendezvous point and don't forget the three-headed Grimm is still alive somewhere in this island."
    
    rho nor frown "Then we fight them!"
    mei nor sad "And if we lost the battle ...?"
    "Rhodo stares at me. Rather than surprised or realizing his mistakes, Rhodo seems more ... confused?"
    "Which part of my explanation isn't clear enough for him?"
    
    "But before I ask it, Rhodo speaks first:"
    rho nor surprised "Schnee, why're you so pessimistic?"
    mei nor angry "I'm not pessimistic! I tried to plan something that has the lowest chance to fail!"
    mei "We could not afford to fail!"
    
    "Rhodo steps back. He raises both hands in a defensive gesture, as if he try to shield himself from invisible attack."
    rho "Whoa there, Schnee ...."
    rho "I know we can't fail this one. I know."
    rho "I don't mean to underestimate our situation or anything, really."
    
    rho "If you get it the other way, I'm sorry."
    mei nor sad "I ...."
    hide mei with dissolve
    hide rhodo with dissolve
    "I grit my teeth, slowly understand why I lashed out at Rhodo."
    "I'm ... {i}afraid{/i}."
    
    "It feels like I have a heavy burden on my shoulder."
    "We need to get out of here, safe and sound. Back to Vale."
    "We couldn not afford to fail. This one has no room to fail."
    "But I couldn't plan everything accordingly. Too much uncertain things."
    
    "What if ...."
    "What if it ended up like ...."
    "... Like my last meeting with Cinder Falls? When she took my Semblance and left me helpless without it?"
    "I couldn't let that happen, could I? Not with my teammate involved ...."
    
    show rhodo nor surprised at midleft with dissolve
    rho "Schnee?"
    "Rhodo gently touches my elbow, taking me back to the present."
    "I rub the tears from my eyes. I didn't even realize when did I start crying without voice."
    rho "Do you think you can walk?"
    "That's a ridiculous question. Why does he even ask?"
    
    "I nod."
    rho nor default "Great."
    rho "Let's start by walking one of your routes. Which one do you pick?"
    show mei nor frown2 at midright with dissolve
    mei "But ... I haven't--"
    
    mei nor sad "I couldn't--I couldn't think about what to do in our worst case scenario for each routes."
    rho nor default2 "You said you {i}can walk{/i} not you {i}can think{/i}."
    rho "Father always said to me to do what I can first. You'll learn to do more later."
    hide rhodo with dissolve
    hide mei with dissolve
    "That's ...."
    
    "I clear my throat and rub my eyes again."
    "That's a very good advice."
    "I nod at Rhodo again and show him the map, telling him which route I think the best for now."
    "We walk in silent, keeping ourselves wary for any incoming attack."
    
    show rhodo nor confused at midright with dissolve
    rho "I really hope we could get something to eat when we're back later ...."
    rho "I'm hungry."
    show mei nor frown2 at midleft with dissolve
    mei "Me too."
    rho nor frown "I should've splitted the protein bar into two, not ate all of it last night."
    "I wouldn't comment on that. I, too, ate the whole bar last night."
    
    rho "If only we can eat Grimm meat ...."
    mei nor frown "That sounds as wrong as suggesting us eating snake."
    rho nor default2 "Snake meat is edible, Schnee. It's common thing at home. At Vacuo. We even eat bats. If you can catch them, though."
    mei nor surprised "It's not your daily food, isn't it?"
    
    rho nor default "Eh, no. Emergency foods. Camping foods. Things you eat when you're trying to reserve your supplies."
    show mei nor frown2
    rho nor surprised "... Wait. If only I know how to make proper traps to catch them, I should've made ones for our dinner last night."
    hide rhodo with dissolve
    hide mei with dissolve
    "Traps ...?"
    "That might be an option to slow down the giant Grimm if it attacked us again."
    "But we don't know much about its behavior or how it thinks."
    
    "Setting up a trap or two without having the knowledge of our target would be useless."
    "I stop and check on the map."
    "We are on the right way. As long as we don't get into a dead end and backtrack, we would arrive in our destination in no time."
    "Or if we don't get attacked by Grimms."
    "... I really couldn't help not to think about every bad things that might happen to us."
    
    show rhodo nor default at midright with dissolve
    rho "Hey, Schnee?"
    show mei nor frown2 at midleft with dissolve
    mei "?"
    rho "Do you still have your cookies?"
    rho nor surprised "You probably want to eat them. You're getting gloomy again."
    hide rhodo with dissolve
    hide mei with dissolve
    "Even though I like cookies, they aren't something that would ease my mind."
    
    "Distracting me from the problems, yes. Solving the problems? No."
    "However, I know Rhodo tried to be considerate and I appreciate it."
    "Rhodo stops walking, turning around toward the sea stretches on our left. He pulls out his Scroll and takes a picture."
    
    show mei nor frown at midleft with dissolve
    mei "I'm not going to comment about this is not the right time to do that."
    show rhodo nor smile at midright with dissolve
    rho "Documentation."
    mei "Rrrright?"
    hide rhodo with dissolve
    hide mei with dissolve
    "I have to admit the scenery is quite nice."
    "No ships or boats. No tall buildings. No people."
    "No Gri--"
    
    #Grimm low growl
    "What is that?"
    "Both Rhodo and I turn around. Weapon's ready."
    "Nothing behind us."
    "That growl was really, really close. Where did it come from?"
    "...."
    "{i}Above{/i} us?"
    
    "When I look up, I catch a glimpse of something big has just left the cliff."
    "{i}What was that?{/i}"
    "The three-headed Grimm or another giant Grimm?"
    "We don't need another giant Grimm now."
    
    show rhodo arm frown at midright with dissolve
    rho "I don't like this."
    rho "Let's be quick. I don't want to get caught by the Grimms here."
    show mei arm frown2 at midleft with dissolve
    mei "Agree."
    mei "Do we need to change our plan? Take another route?"
    rho arm default2 "Nah, let's keep walking for now. We'll change course if we really have to do that."
    
    mei "Alright."
    hide rhodo with dissolve
    hide mei with dissolve
    "I keep Alpenaster unsheathed and load it with Dust, just in case."
    "Rhodo walks faster, cautiously watches our surroundings every now and then."
    "We encounter small Grimms along the way, which we defeated without much effort. In normal occassion, they're weak ones that are not even close to be a warm-up."
    "But now? I'm nervous and almost unleashed too much power."
    
    show rhodo nor default2 at midright with dissolve
    rho "Schnee?"
    show mei nor default2 at midleft with dissolve
    mei "... Yes?"
    rho "You okay?"
    show mei nor frown2 with dissolve
    "I take a deep breath."
    mei "Yes, I think I'm okay."
    
    rho nor surprised "That Grimm, the three-headed giant dog one, I mean."
    rho "I don't remember seeing it in the books. Or Professor's lecture. Do you know anything about it?"
    mei "Not really ...."
    mei "I once heard about mutated Grimms and parasitic Grimms that could fuse two or more Grimms into one creature."
    rho "Parasitic? The kind that attach themselves on objects or living creatures?"
    mei "Yes. They usually small and don't have the best mobility, but that's exactly why they need a host. They're vulnerable."
    
    rho "Huh ... so, that's perhaps the case with the Big Bad Doggie."
    "'Big Bad Doggie' ...."
    show mei nor default with cdissolve
    "I snort and couldn't help not smiling at the ridiculous title."
    rho nor confused "What?"
    rho "It's exactly what it is! A Big Bad Doggie!"
    mei nor smile "I know, I know."
    mei "But the name sounds funny, not scary at all."
    rho nor default "Well, that's exactly why!"
    rho "You gotta nicknamed scary things with weird names. It's gonna make them less scary whenever you talk about it."
    
    hide rhodo with dissolve
    hide mei with dissolve
    
    "The conversation lights up my mood a little."
    "Unfortunately, today isn't destined to be the day where I could feel happy more than five minutes."
    "We stumble on a dead end."
    "It's not entirely dead end. There's a pipeline network on our right, but I didn't mark it down as an alternative route on the map."
    "The ground where pipelines stretch like a maze is lower than our current position."
    "It looks as if whoever built this facility had removed a huge chunk if the island and built pipelines in them."
    "There is space to walk between those big pipes and I could see a ladder at the other end."
    "I check the map again."
    
    "It would be a detour if we go through the pipe."
    "On another note, we wouldn't stray that far from our destination, so I guess it's an acceptable route."
    "If we keep our current pace, we might be able to arrive before the airship arrive."
    "I jump down first. Rhodo follows right behind me."
    "At the moment, I realize how unusual for both of us to go together without much words like this."
    
    "Well, I think this is the {i}first{/i} time."
    "I stop upon hearing a muffled noise from a bit far away from us."
    "I think something big has fallen somewhere on these pipelines."
    "Or ... no. Not 'fall'."
    "What follows after that is a series of footsteps and it's getting closer."
    
    "Both Rhodo and I freeze in our place."
    show rhodo nor surprised at mcenter with dissolve
    rho "Schnee? I think we should turn ar--"
    hide rhodo with dissolve
    "Before I could voice my agreement with Rhodo, fierce growl come closer from behind us."
    "No less than a dozen of Beowolves are heading toward us."
    "But it's not the last thing we should worry about."
    
    "The three-headed Grimm arrives, looming above us."
    "It stands on the pipelines, too big to jump to the ground, but it doesn't mean it couldn't attack us."
    "I duck just at the right moment. The Grimm's jaw snapped close several inches from my head."
    show rhodo arm shout at midright with dissolve
    rho "Schnee!"
    rho "I leave the Beowolves for you!"
    show mei arm surprised at midleft with dissolve
    mei "Wait--!"
    mei "What do you--"
    hide rhodo with qdissolve
    hide mei with qdissolve
    
    "Without answering my question, Rhodo climbs the nearby pipe."
    "I hear him shouting at the giant Grimm."
    "The creature seems distracted by Rhodo and slowly moves away."
    "I do worry about Rhodo, whether he could handle the monster alone or not."
    "However, the Beowolves pack had surrounded me."
    
    
    "One of them lunges at me."
    "I try to counter its attack while dodging, but only manages to scratch it a little."
    "Another one comes for me. Not having enough time to cast my Glyph, I drive Alpenaster deep into its chest, and kick it."
    "I turn around, ignite the Dust in Alpenaster before I see my target, and unleash a series of icicle."
    "The icicles find their target in two Beowolves, killing them immediately. One of the icicle wounded another Beowolf."
    
    mei "!!!"
    "Something pulled my cloak from behind. Before I get to see who or what it was, I see a quick movement from the corner of my eyes."
    "I move away and raise my arms on time. A set of claws flashed near my head, scratching me deep enough. I'm lucky I still have protection from my Aura."
    "The Beowolf, which still grabbing my cloak, try to claw me again. I quickly unclasp my cloak and take a few steps away while casting another Glyph."
    "The knight's gauntlet covers my arm. I grab one unlucky Beowolf nearby and throw it to the ones gathered around."
    "I smash the one that has my cloak and punch another behind it. With my free hand, I take my cloak from the ground."
    
    "However ... instead of withdrawing, the number of Beowolves around me seems to be increasing."
    "With this many, I suspect there's an Alpha Beowolf leads them. If it is true, then the most effective way to end this battle is by killing the Alpha."
    "I don't see any Alpha Beowolf, that's the problem."
    "In order to delay incoming Grimms, I dispel the knight's gauntlet and cast another Glyph."
    "An ice wall forms, trapping several Beowolves inside it and blocking the rest of them behind the tall blockade. Beowolves could climb, but they're not the best climber and I'm proud to say that my ice wall is slippery."
    
    "There are three Beowolves left around me. They look reluctant at first, but then one of them decides to attack first."
    "Being smaller in size, I easily dodge its attack and counter immediately. Alpenaster tip digs deep into the creature's chest."
    "I pull my dagger and step back, creating enough distance to anticipate another attack. The wounded Beowolf collapses."
    "The other two whines and slowly withdraws."
    
    unk "Schnee! Hey!"
    "Rhodo lands next to me. He doesn't hurt, as far as I see, only need to catch his breath."
    show rhodo arm frown at midright with dissolve
    rho "We gotta hurry before Doggie escapes and tears this place apart."
    show mei arm surprised at midleft with dissolve
    mei "Escapes???"
    mei arm frown2 "What happened?"
    rho "I managed to trap it. Bending a few pipes around."
    mei arm default2 "Alright. Clear enough."
    mei "Let's go."
    hide rhodo with dissolve
    hide mei with dissolve
    
    "That explains why Rhodo is out of breath. These pipes are quite big. Even though Rhodo has enough Aura to use his Semblance, he has to spend a lot to bend something of this size."
    "I lead the way toward the stairs, going as quickly as I could while not leaving Rhodo too far behind."
    show mei nor frown2 at midleft with dissolve
    mei "How confident are you with the current situation?"
    mei "I mean, how long do you think until the giant creature escapes?"
    show rhodo nor surprised at midright with dissolve
    rho "I'm not Ulm with his accuracy, how could I know?"
    rho nor frown "The pipes are rusty in some parts. I kinda expect it to escape anytime soon."
    hide rhodo with dissolve
    hide mei with dissolve
    
    "That's ... bad."
    "If the giant Grimm manages to get itself out and then chases us, it might attack the airship too."
    "Our safest option is killing it for good. The next question would be how are we going to do it without depleting our Aura?"
    "Both Rhodo and I aren't in our prime condition. Exhausted, hungry, and Rhodo is wounded."
    "We reach the stairs and start climbing."
    
    "Around halfway through the stairs, I see an odd-looking, twisted pipes on far left side."
    "An angry figure behind the pipes growls and try to force its way out, clawing and hitting the metal surrounding it."
    "It doesn't look good, just as Rhodo suggested. The creature might escape its makeshift captivity anytime now."
    "While I climbing the stairs, I try to recall anything related to this kind of Grimm creature."

    "We've talked about the possibility of this creature being a chimera-type, with parasitic Grimm involved."
    "If that's the case ... like what I've read sometime ago, we need to find the parasitic Grimm and kill it rather than cutting all heads like we usually do with King Taijitsu."
    
    show mei nor frown2 at mcenter with dissolve
    mei "Rho? Did you see anything that looked like a core or a smaller parasitic Grimm on it?"
    rho "On Doggie? Hmm ...."
    rho "I'm not sure, really. We move a lot earlier and I try not to let it bite my head off."
    rho "Talking about head, don't you think we can kill it by destroying each of Doggie's heads?"
    rho "Like, King Taijitsu?"
     
    mei "That's one possibility. Another possibility I just thought of is Doggie is a mutated kind, either caused by another Grimm or something else."
    mei "Its crystal-like spikes suggest a mutation. That means ... we might have an opponent with the ability to regrow its head in a flash."
    mei "With mutation, anything is possible. And unpredictable."
    rho "...."
    rho "What a monstrosity ...."
    mei "All Grimm creatures are monstrosity."
    
    rho "This one is beyond normal monstrosity."
    show mei nor frown with cdissolve
    "... What's 'normal monstrosity', exactly?"
    mei nor frown2 "This one is an abomination."
    rho "Oh, I like that word."
    hide mei with dissolve
    "We almost reach the top of the stairs when a loud crashing noise echoes from below."
    "I don't need to turn around to see it's the three-headed Grimm."
    
    show rhodo arm surprised at midleft with dissolve
    rho "Schnee? We need a plan. Now."
    show mei arm angry at midright with dissolve
    mei "I know! I know!"
    hide mei with dissolve
    hide rhodo with dissolve
    "I take a quick glance at my surroundings, looking for anything we could use."
    "There are a lot of containers and what look like cages. All of them aren't aligned properly and some are flipped upside down."
    "There's also a crane. A good spot if we're going to launch an ambush or place sharpshooters."
    "At a time like this, I do really wish I chose a bigger weapon like Mom did."
    
    "A High Caliber Sniper Scythe like Crescent Rose, Mom's weapon, is the best when it comes to big Grimms."
    "Or something canon-like. Highly destructive. Or blasting weapon."
    "... Okay, alright. Focus. Mei, focus. Back to topic."
    "A giant Grimm. Quick, has three heads that allow it to have more view of its surroundings. Could see and anticipating most of our attacks."
    "But what if it couldn't see incoming attack?"
    
    "And ... canon-like? Highly destructive?"
    show mei arm frown2 at midright with dissolve
    mei "Rhodo, you're left-handed, right?"
    show rhodo arm default at midleft with dissolve
    rho "Yeah."
    mei "Do you remember your experiment with Ulm? The one about using your Semblance to accelerate your spear-throwing?"
    rho arm default2 "Oh, that? Sky Piercer."
    mei arm frown2 "Ulm gave that nickname?"
    rho arm confused "No, Anya did. She said it's unofficial, though. We haven't tested or used it."
    
    mei arm cocky "We'll make it official today. {i}Right now{/i}."
    rho arm surprised "What do you--OH!"
    rho arm smile "You have a plan! Okay, I'm all ears."
    mei arm default2 "I'm going to lure the Grimm here, letting it chase me around a little, and make it stuck its heads into one of those containers."
    
    "I hear the Grimm footsteps. It finally realizes where we are, I suppose. That means I don't need to lure it here."
    mei arm angry "You stay away from its sight, get ready for Sky Piercer when Doggie sticks its head inside the container. Attack it from its blind spot."
    mei "Do consider the position of the container. I don't want to be skewered. And don't miss the target."
    mei "Got it?"
    rho arm frown "Got it!"
    hide rhodo with qdissolve
    hide mei with qdissolve
    
    "Considering its size, the giant Grimm wouldn't take long to reach this place."
    "I cast a summon Glyph, getting ready to greet the three-headed monster with a little surprise."
    "Doggie climbs with ease. Its giant stature looms several feet away from me."
    "Before the creature could fully survey its surroundings and see Rhodo--whereever he is now--I unleash a flock of small Nevermores toward it."
    
    "It's a trick I learned from Aunt Winter in which we call forth a flock of small Nevermores to distract the opponent."
    "Despite of huge size differences between the summoned Nevermores and the three-headed Grimm, the smaller creatures do their job well. For a while, the bigger Grimm is occupied with small, fierce birds that circle and attack it."
    "However, having three heads mean this Grimm could process way more visual information than ordinary, one-headed Grimm."
    "The middle head fixes its sight on me, realizing that I'm still on the same place as before and Doggie decides its time to ignore the birds and attack the real target: me."
    
    "This wouldn't be the first time I'm up against a bigger opponent. Schnee Dust Company has a good training ground and a few ... facilities on it. Without doubt, it provides the best training for Huntsmen."
    "But, the best doesn't mean it has something like this three-headed Grimm. We have various {i}size{/i} but not enough {i}variety{/i}, apparently."
    "I dodge the incoming attack. Really, really close."
    "The Grimm slashes again. This time, I leap forward and roll, stopping under the Grimm's belly."
    
    "Seeing a wide opening, I immediately cast another Glyph, creating a massive ice structure."
    "The ice trapped a little part of the Grimm and stabbed him in several places."
    "I quickly move from below the three-headed Grimm. It roars and thrashes its own body to free itself from the ice."
    "Even though I created a big ice with sharp and pointy edges, it wasn't supposed to stop the creature completely."
    "It's only to stop his movement and annoy it, so it wouldn't be distracted and looking for Rhodo--whom I don't know where he is now."
    
    "I already run when Doggie finally manages to free itself. Unfortunately, I haven't put enough distance by now."
    "With a single leap, the Grimm closes in. I turn around just in time to cast a protection Glyph, at least big enough to protect myself."
    "The Grimm hit the Glyph. Quite hard." with hpunch
    "While the middle head needs a few seconds to recover from the hit, the left and right heads are alright and the creature makes its next move toward me."
    
    "Apparently, rendering this creature unconscious would require me to get all three of its heads."
    "This Grimm is an interesting material for further study, if anyone could capture it alive."
    "I manage to draw another safe distance from the Grimm."
    "It's time for a cat and mouse game."
    
    "I run toward a couple of containers, taking a sharp turn to the left, into the space between two containers."
    "It's a narrow space, big enough for me, but definitely too small for the Grimm."
    "It doesn't stop it from chasing me, actually."
    "Followed by a deafening roar, the Grimm chase me."
    
    "It smashes the containers aside, easily tosses them away to make a clear path."
    "I hate to say that I didn't expect the Grimm to be this ... furious."
    "I cast a Glyph to help me reach the upper side of nearby container."
    "Before I move to the next one, the Grimm knocks the container I'm on."
    
    mei "!!!" with hpunch
    "The container flips over. I jump again, almost got stuck under the metal storage."
    "Not giving me a chance to breathe, the Grimm climbs on the container and leaps at me."
    "I barely dodge that one."
    
    "The three-headed Grimm crashes onto a stack of metal cages. One of those cage lands less than a foot from where I stand."
    "I ... doubt I would get any luckier than this and scram."
    "I enter the very first opened container I find."
    "Doggie, following me closely behind, extends its claw to reach me. It couldn't."
    "It growls and snarls. Upset. {i}Really{/i} upset."
    
    "Once again, Doggie try to reach me. I stand on the furthest end of the container and Doggie's claw isn't long enough to even touch me."
    show mei arm cocky at mcenter with dissolve
    mei "Need some help, big dog?"
    hide mei with dissolve
    "... Really not the time to be cocky, Mei ...."
    "But I can't help not to tease the creature."
    
    "And then it happened."
    "The three-headed Grimm pulls its claw out and pushes its head further."
    "The container tilts forward."
    mei "Wha--?!"
    
    "The Grimm lifts the container with its head inside, trying to make me slide right into its mouth."
    "I look for anything to hold onto, but end up not find any. There's no way except casting another Glyph below me."
    "Before I finish casting the Glyph, I hear some sort of high-pitched sound."
    "I don't know what sound was it. The only thing I know is the container is thrown away violently with me still inside."
    "I curl into a ball and protect my head with both arms."
    
    "It takes quite some time before everything calmed down."
    "I don't hear the Grimm. The container had stopped and stays in one place."
    "I'm still intact. My back and shoulders are hurt after hitting the container wall multiple times."
    "Pretty much alive. Great."
    
    rho "Schnee!"
    "Rhodo kneels next to me, checking on my condition."
    "Sure enough, he's responsible for the tumbling container."
    show rhodo nor surprised at midright with dissolve
    rho "Are you ... okay?"
    
    rho "I'm really sorry about, err ... the disturbance."
    show mei nor frown at midleft with dissolve
    mei "You mean 'inconvenience'."
    rho nor smile "Yes! That's the word!"
    rho nor surprised "I got panicked when seeing the goddamn Grimm lifted this thing up. So, yeah, aim and shoot."
    
    show mei nor default2
    "Of course he panicked. I didn't expect the Grimm to act that way too."
    "I straighten myself and rub my shoulder."
    mei nor frown2 "The Grimm ... what happened to it?"
    rho "I hit it with Sky Piercer. Right on its side. And ... you know ...."
    "Rhodo takes a deep breath."
    
    rho nor frown "I never knew Leonidas can do such damage. It pierces through the Grimm, leaving behind a massive hole. And there was {i}fire{/i}. I think small explosion too. And the Grimm--that {i}big{/i} Grimm--was thrown off like a ragdoll."
    rho nor surprised "Doggie exploded, by the way. Bursting into green ashes."
    mei "So I guess I was lucky to be inside the container when it happened?"
    rho "Eh, you can put it that way. Yeah."
    
    mei "You look ... shock with what has happened."
    rho nor shout "Well! Of course!"
    rho nor surprised "Ulm never told me it could do so much destruction!"
    rho nor shout "More importantly! He didn't tell me that I might lose my spear because it flies so far away!"
    "Oh, so that's the real reason ...."
    
    "Even though I think Rhodo overdid it with his Semblance, which caused his spear travels in an extreme speed and power, I could sympathize with him."
    "I sit down and pat his shoulder, the one which isn't wounded."
    rho nor surprised "... Are you trying to cheer me up?"
    mei nor smile "Yeah."
    mei nor smile "And thank you."
    
    rho "Oh?"
    rho nor default "Heh."
    rho nor smile "You're welcome."
    hide rhodo with dissolve
    hide mei with dissolve
    
    scene black with fade
    "We catch the airship in time."
    "Exhausted (and famished), I sleep almost immediately the moment the airship takes off from the island."
    "I remember thinking about what happened to the Huntsman who led the attacks at the port."
    "And what about Emerald?"
    "Does Anya or Ulm manage to get anything?"
    
    "I couldn't think clearly and just glad that we're alive to see another day."
    
    
    call chapter("End of Part 9", "Cerberus") from _call_chapter_15
    centered "End of Demo Version 0.8\nThank you for playing!"
    return
    