# PART 8 : STRANDED #

label part8BU:
    
    "I walk around the boat, restless like an animal in a cage, when I notice Ulm is busy scribbling in his notebook."
    "Curious, I try peeking at what he is doing."
    "Ulm has written down numbers and equations on his notebook. There's also a drawing with nodes, with one of the nodes has 'Vale Port' written next to it."
    "However, he looks confused and kind of upset with his own work, so I don't say anything."
    "Ulm doesn't even realize that I peek on him."
    
    "The boat slows down upon nearing on an island."
    "I don't have the slightest idea where are we now. We arrive in the middle of nowhere."
    "So far, we don't see anybody here."
    
    scene bg island with fade
    
    show ulm nor reluctant at midright with dissolve
    ulm "According to my estimation, we aren't that far from Vale .... But there's still no signal."
    ulm "Could it be ... signal jammers?"
    show mei nor frown2 at midleft with dissolve
    mei "Weak signal in mine."
    mei "Should I call Anya or Rhodo?"
    ulm nor surprised "Y-yes, please!"
    ulm nor default "We should at least tell Anya that we're ... safe ...."
    
    mei nor default "Good idea."
    ulm "I will ... watch our surrounding ...."
    hide ulm with dissolve
    show mei nor default2 with cdissolve
    "I don't need to wait for too long. Anya picks up almost immediately, as if she has been waiting for the call."
    any "Mei! Are you alright?"
    show mei nor default with cdissolve
    "I take it as a good sign when I hear Rhodo said my name loudly in the background."
    "At least I could assume both Anya and Rhodo are in a safe place."
    
    mei nor frown2 "I am. Even though I couldn't tell where am I now except that I'm in an island ...."
    mei "And we barely have any signal for our Scroll now. I doubt you could pick our location at the moment."
    any "When you said 'we', that means you and Ulm, right?"
    mei nor default "Yes. Ulm is here with me."
    
    any "That's a relief."
    show mei nor default2 with cdissolve
    any "For now, try to get to know the island you are on. And, if possible, find somewhere you can get better signal so we can pick your location."
    any "Keep us updated with your situation whenever you have the chance."
    mei "Understood."
    
    any "There's something we need to take care over here. Don't waste your Scroll battery, okay?"
    mei nor frown "You don't need to remind me for such thing. It's trivial."
    any "Keep your Aura usage in check too."
    mei "Yes, Ma'am~"
    show mei nor default with cdissolve
    "I could picture Anya is grinning when I call her 'Ma'am'."
    any "I'll be waiting for the good news."
    
    show mei nor default2 with cdissolve
    "As the call ends, I wonder if we would have 'good news' during our stay in this mysterious island."
    show ulm nor default at midright with dissolve
    "Ulm looks at me, clearly waiting for further instruction."
    mei nor frown2 "Are we somewhere you know?"
    ulm nor surprised "{size=15}I'm sorry ... but no.{/size}"
    ulm "{size=15}I don't know where we are.{/size}"
    ulm "{size=15}I'm sorry ....{/size}"
    
    mei nor default "I saw you calculating something when we were on the boat previously. What was it?"
    ulm nor default "Oh, that ...."
    ulm "{size=15}I found there are coordinates inputted into the boat's auto-pilot system.{/size}"
    mei nor frown "If you could speak louder, please?"
    ulm nor surprised "{size=15}Ah, yes. I'm sorry.{/size}"
    ulm "I found there are coordinates in the boat's auto-pilot system."
    ulm nor default "But then ... I realized the coordinates are to make sure the boat traveled in a misleading course."
    
    ulm "I tried to figure out our destination based on these coordinates and the boat's speed and direction."
    mei nor frown2 "Direction? How? Compass from our Scroll?"
    ulm nor surprised "Uh, no ...."
    ulm nor reluctant "{size=15}I forgot our Scroll could be used as a compass too ....{/size}"
    ulm nor default "The sun's position on the sky."
    mei nor surprised "... You sure have many practical skills with nature, don't you?"
    
    ulm nor default "{size=15}I ... used to go hunting with minimum equipment with my uncles ....{/size}"
    ulm "I got the coordinates in the end. An approximation, at least. But ...."
    show mei nor frown2 with cdissolve
    ulm nor reluctant "{size=15}But I don't remember seeing any kind of island on the map ....{/size}"
    hide ulm with dissolve
    hide mei with dissolve
    "Ulm memorizes map???"
    "Even though I know Ulm's ability to memorize so many things people don't usually memorize, it still surprises me to learn about this."
    "We could easily obtain a printed map or view one on our Scroll whenever we're connected to CTC network."
    "... Which we are not capable to do any at the moment, so Ulm and the map he has learned would be a priceless resource now."
    
    show mei nor frown2 at midleft with dissolve
    mei "An uncharted island, huh?"
    show ulm nor default at midright with dissolve
    ulm "M-maybe ...?"
    ulm nor thinking "There are burn marks on the rocks around here that aren't from campfire or anything similar to it."
    ulm "{size=15}Maybe explosions ...?{/size}"
    ulm "So ... I think someone once visited this island ...."
    mei nor default2 "Do you mind if we explore this island to find out more about it?"
    mei nor frown2 "We still need to find someplace with better network to send our location."
    ulm nor surprised "S-sure!"
    hide ulm with dissolve
    hide mei with dissolve
    
    "I could not help noticing that Ulm is nervous."
    "In normal situation, during a mission or combat training, Ulm and I are rarely paired."
    "Just like when we were in the port, Ulm is more effective when he acts as a scout, moving separately from us."
    "In a situation when Ulm got discovered by our opponents and being targeted, one of us should go to assist him, providing cover or distraction."
    "Of course, I have voiced my disagreement with this arrangement! I could see the ineffectiveness. It is so glaring."
    "It is usually either Rhodo or Anya who takes the role to assist Ulm. Rhodo knows a few defensive strategies. Anya knows how to create chaos and distraction."
    "... Now that I think about it, each of them has their own weakness."
    "Rhodo isn't too agile and he isn't the type who could quickly adjust to unpredictable situation."
    "Anya's Aura is abnormally weak compared to the three of us. It only takes a few hits to shatter her Aura defense, leaving her vulnerable."
    "If I were to be paired with Ulm .... Theoretically speaking, I have both the capabilities to provide cover and to create distraction."
    "But why do I never be paired with Ulm? Considering all the pros and cons, I am practically the best choice to assist him."
    "Is it because of my disagreement?"

    scene bg island with fade
    "We find what looks like an entrance gate after quite a walk. It is a chain link gate, a bit rusty, but it still holds on."
    "Beyond the gate, there's a complex consists of several plain buildings. The pipes, chimneys, and signage on each buildings aren't unusual for me: it reminds me of SDC facilities back in Mantle."
    show ulm nor surprised at midright with dissolve
    ulm "W-what is this?"
    show mei nor surprised at midleft with dissolve
    mei "I think ... this is a factory. Sort of."
    mei nor frown2 "Wait. No. Not a factory."
    mei "This is a mining facility. But what did it mine? Dust?"
    ulm "{size=15}If it's a Dust mining, won't SDC already know about--{/size}"
    "Ulm takes a nervous glance at me. I pretend not the hear anything."
    "He is correct, actually. Schnee Dust Company has its own extensive information network that wouldn't miss information about any potential Dust mining spot."
    
    show ulm nor thinking with cdissolve
    "Ulm presses his palm on his right eye. Circular symbol glows faintly in front of his left eye."
    ulm "We haven't encounter anything so far, but I'd like to make sure .... Just in case."
    mei nor default "That's a good idea."
    ulm nor surprised "{size=12}Thank you ....{/size}"
    "It doesn't take long for Ulm to finish surveying using his Semblance."
    ulm nor thinking "Nothing except Grimms. Most of them are Creepers and Beowolves."
    
    mei nor default "As long as we watch our back, we're going to be fine, then."
    ulm nor surprised "Oh, and I think I have an idea who owns this place."
    ulm "I saw this symbol."
    show mei nor default2 with cdissolve
    "Ulm draws the symbol on his notebook and shows it to me."
    mei nor frown2 "?"
    mei "I'm afraid I don't know whose or what symbol is it ...."
    ulm nor thinking "It's Merlot Industry symbol. They've long gone since the incident which makes Mountain Glenn deserted like now."
    ulm "Their base is in Mountain Glenn."
    
    mei nor surprised "But the incident in Mountain Glenn took place more than 20 years ago."
    mei "Did they manage to save their assets and continue their business here?"
    ulm nor reluctant "Without anyone knowing it? That's ... suspicious."
    show mei nor frown2 with cdissolve
    "Good point."
    "If someone build a facility in a remote island, far from civilization, and the existence of the facility itself wasn't publicized, high chance they want to keep it a secret from anyone."
    mei "Whoever set that boat know about this place .... How?"
    
    ulm nor surprised "{size=15}And 'why' ....{/size}"
    ulm "{size=15}Why did they lure you into the boat and send you here?{/size}"
    show mei nor surprised
    show ulm nor thinking
    with cdissolve
    "Right .... I didn't consider it before."
    show mei nor frown2 with cdissolve
    "Let's say Emerald is the one who planned this, I think she wants to exclude me from the team."
    "She couldn't take the risk playing the same trick like in Forever Fall."
    ulm nor default "I ... have a theory. About why did they send you here."
    
    ulm nor thinking "They already know we're after them. Separating our team will delay us from discovering their hideout."
    ulm "When we return to Vale later, I don't think we can find the Golden Apple stash even though we've gotten some information about its whereabouts."
    show mei nor sad with cdissolve
    "And whose fault is that? {i}Me{/i}."
    "It's my fault if what Ulm said is true and we failed to catch the culprit behind the Golden Apple affair."
    ulm nor surprised "Miss ... Schnee?"
    ulm "{size=15}Please don't blame yourself. I should have been more watchful and noticed Emerald before you did ....{/size}"
    
    mei "Why are you blaming yourself ...?"
    ulm "Uh ... umm ...."
    ulm "{size=15}I ... uh ....{/size}"
    ulm "{size=15}I'm sorry ....{/size}"
    "To be honest, I don't know what to say to Ulm."
    "He has been like this since the very first time the four of us was announced as team AURM. A bit better now, actually."
    "But, still ...."
    mei "Come one, let's look around a bit more."
    mei "We still need to find someplace where we can get better signal to send our location."
    ulm "Y-yes!"
    
    scene bg island with fade
    "We walk in silence. Ulm is following me a few steps behind."
    "After a while, we find what looks like a communication tower. The structure looks rather old, with rust on its exposed surface, but I see the transmission antennas and receiver disc still on its place."
    "I check on my Scroll and see the signal bar is full."
    mei "I'll send our position and call Anya."
    mei "Please watch our surroundings while I do so."
    ulm "A-alright."
    
    "I have to wait a few seconds before Anya answers the call."
    "It's longer than the usual, making me thinking she is currently occupied by something."
    show mei nor frown2 at midleft with dissolve
    mei "Is it the right time or should I call you later?"
    any "Huh? No. Why do you think of calling again later?"
    mei "It took you long enough to answer. I thought you're in the middle of something."
    any "Oh, right. Sorry about that."
    
    any "Things are getting a little interesting here."
    "The way Anya puts her words raises my suspicion."
    mei "What did you do?"
    mei nor frown "I hope you're not into something dangerous."
    any "Don't worry. Everything's okay. We managed to get some information from the guy we captured."
    mei nor surprised "How did you do that? He finally gave up and told you everything?"
    
    any "Long story short, we agreed on something and he told us what we want to know."
    show mei nor frown2 with cdissolve
    "I'm curious with the detail, really."
    "Anya keeps surprising me with her knowledge and methods. Some of them are questionable, indeed, but they're all ... {i}interesting{/i}."
    mei "I've sent you our coordinates. Have you checked it?"
    any "I saw it. I'll forward it to Professor Goodwitch after this."
    any "Anyway, bad news, a storm is approaching Vale right now."
    any "Based on how many flights and ferry services are canceled, I guess this storm is quite bad."
    any "I doubt Professor can send any airship now. You gotta wait until .... Not sure. Maybe tomorrow morning."
    
    mei "Don't we have any other choices?"
    any "Sorry."
    mei nor default "Oh well. We're going to be alright."
    mei "It's not like it would be the first time I spend the night outdoor."
    any "A kindly reminder that you're stranded in someplace we don't know, unlike our last mission to Forever Fall."
    mei nor frown2 "I know, I know."
    mei "We would manage. I promise."
    any "Glad to hear that."
    
    any "I'll cut this conversation short. You'll need to reserve your Scroll battery."
    any "Anything else to report?"
    mei nor default2 "Nothing at the moment."
    any "Keep us updated whenever you can and be careful."
    mei "Understood."
    hide mei with dissolve
    
    "I hear a faint whine from a bit further from our current position."
    "When I turn around to see the source of the sound, I see Ulm putting another bolt on his crossbow."
    "There's a black smoke from a disappearing Grimm, almost certainly it was killed by Ulm's shot."
    show mei nor smile at midleft with dissolve
    mei "Accurate shot as always, Ulm."
    show ulm arm surprised at midright with dissolve
    ulm "O-oh!"
    ulm arm smile "Um ... thank you."
    hide ulm with dissolve
    hide mei with dissolve
    "Ulm uses his Semblance again, checking our surroundings."
    "He lowers down his crossbow and stands up, indicating there's no immediate danger around."
    
    "I feel like I should say something to Ulm aside from praising him for his accuracy."
    "But my mind draws a blank."
    "I only heard Ulm said something when there are the four of us. He doesn't say much either, only correcting Rhodo, answering various things Anya asked him ...."
    "...."
    "... Maybe I'm overthinking this. Let's just focus on our current situation here."



    # show mei nor sad at midleft with dissolve
    # mei "I apologize for earlier."
    # mei "I didn't mean to snap out like that. I got carried away."
    # mei "I'm really sorry."
    # "For a moment, Ulm looks struggling with what to say."
    # show ulm nor reluctant at midright with dissolve
    # ulm "I'm ... uhm .... Actually ...."
    # ulm "{size=15}Rhodo did say to me sometime ago that it's annoying to keep hearing me apologizing.{/size}"
    # ulm "{size=15}I try not to, but I've been doing it for too long ....{/size}"
    
    # mei nor frown2 "I agree with Rhodo."
    # mei "I know, it takes some time before you could get used to it."
    # ulm nor surprised "I'm--"
    # "Ulm cuts his sentence midway. His expression is an odd mix of embarrassment, awkward, and confusion."
    # mei nor default "It takes time, like I've said."
    # mei nor default2 "Now, while I was calling Anya, I guess you did another little survey with your Semblance?"


    show mei nor default2 at midleft with dissolve
    mei "Do you find anything of particular?"
    mei "I guess you did a little survey with your Semblance?"
    show ulm nor thinking at midright with dissolve
    ulm nor thinking "Uhm ... yes ...."
    ulm "There's something that bothered me ...."
    ulm nor reluctant "I Sightjack-ed an unidentified Grimm and saw some abnormal ones."
    
    ulm "The abnormal ones had a crystal-like spikes on its back, not the usual bone-like spikes."
    ulm "Some of them have entirely different appearance than any documented Grimms."
    ulm nor thinking "My best guess, they're mutated Grimms. But even it doesn't explain their crystal spike. Known mutated Grimms don't look anything like them."
    show mei nor frown2 with cdissolve
    ulm "As for the unidentified Grimm, I can say based on its sight angle, it's ... {i}big{/i}."
    mei "How big?"
    ulm nor default "At least its head level is the same as Goliath's."
    "Oh, that's {i}gigantic{/i}. My definition for 'big' stops at a normal-sized Deathstalker."
    
    ulm "And I think it has more than one head."
    mei nor surprised "W-what?!"
    ulm nor thinking "I got into several different sights and saw a glimpse of another head on the side, while the environment around was the same. Different angle, but the same."
    ulm "This Grimm might be chimaera-type, I think."
    hide ulm with dissolve
    hide mei with dissolve
    "Chimaera-type Grimm is a term we use to describe several Grimm creatures which are 'fused' together."
    "This phenomenon is caused by a possession-type Grimm: smaller, parasitic type which uses object or living beings as its host."
    "What possession-type Grimm does to its host--especially the one that uses other Grimms--is unpredictable and, more than often, bizzare."
    
    "Not only making the poor hosts stronger, the posession-type makes them even more horrible."
    "I don't like the idea of meeting a chimaera-type now."
    show mei nor frown2 at midleft with dissolve
    mei "Do you know where it was?"
    mei "Could we somehow avoid any encounter with it?"
    show ulm nor surprised at midright with dissolve
    ulm "The problem is, Miss Schnee, I don't know this place's layout."
    ulm "I can describe what I've seen through the creature's sight, but I don't know its exact position and where we have to go to avoid it."
    
    "Ulm's right. At this rate, the Grimm might find us first before we could even think of a way to avoid it."
    mei nor default2 "Not the brightest idea I could think of, but do you think we could take shelter on the boat which brought us here?"
    ulm nor default "That might work."
    mei nor default "Let's go then."
    hide mei with dissolve
    hide ulm with dissolve
    "Now knowing there's danger lurking in this island, we move more slowly and carefully."
    "It takes us quite long, even though we remember which way we came before."
    
    "But ... before we find the boat again, there's an explosion."
    "Ulm looks pale."
    show ulm nor surprised at midleft with dissolve
    ulm "I think ... I think it's from where the boat is ...."
    show mei nor surprised at midright with dissolve
    mei "Well, {i}there are{/i} explosives in there."
    mei "I'd like to make sure what was that explosion."
    mei "We would be really lucky if it wasn't the boat."
    hide mei with dissolve
    hide ulm with dissolve
    
    "Ulm nods and follows me closely."
    "The explosion isn't that big. When we arrive, we find the boat is sinking. There's a visible damage on its hull."
    "Whoever set up the explosives, I think they want us to be alive as long as possible, but want to make sure we couldn't tinker with their boat and using it to return to Vale."
    "It needs quite extensive knowledge to carry such task. There must be someone else in the group who is a machine expert."
    
    show ulm nor surprised at mcenter with dissolve
    ulm "M-Miss Schnee ...."
    ulm "We need to get out of here .... N-now!"
    hide ulm with dissolve
    "Ulm signals me to keep quiet and points somewhere further."
    "He's scared of something."
    "Not asking more question for now, I follow him."
    "And right after that, I hear a heavy sound of big creature landing on the ground."
    
    "Is that the chimaera-type???"
    "We already ran quite far and I couldn't catch a glimpse of the creature from our current position."
    "This starts to feel like a really dangerous Hide and Seek game."
    "If that creature decides to track us, we have no choice but to at least fight it back."
    "How long could we avoid it?"
    
    "And there's no guarantee the creature wouldn't attack the airship sent to get us next morning."
    "... Oh, I seem to forget one other important matter."
    show mei nor frown2 at midleft with dissolve
    mei "Ulm, Anya said about incoming storm."
    mei "Do you have any idea what should we do for the night? Help wouldn't be coming until tomorrow morning."
    show ulm nor thinking at midright with dissolve
    ulm "Uhh ... obviously, we'll need shelter."
    ulm nor reluctant "{size=15}How can we explore this island while avoiding the giant Grimm?{/size}"
    
    mei "Let's proceed with caution."
    mei "Don't waste your Aura by keep using your Semblance to check the Grimms whereabouts."
    mei "If it finds us, then we fight it just enough to keep it at bay and allow us to escape."
    show ulm nor thinking with cdissolve
    "For a moment, Ulm is silent, lost in thought."
    ulm "It might work."
    ulm "{size=15}But we still need to change our plan if something unpredictable happened ....{/size}"
    
    mei nor frown "Oh, be more confident about changing plans, Ulm."
    mei "We've done so many 'improvisation' in many occasions. Blame our team leader for that."
    mei nor cocky "We're going to be ready."
    show ulm nor smile with cdissolve
    "Ulm smiles for the first time today."
    ulm "{size=15}Has anyone ever told you that your positivity is contagious?{/size}"
    mei nor frown2 "?"
    mei "Come again?"
    
    ulm nor surprised "N-no! It's nothing! I'm sorry!"
    ulm "We better move then."
    hide ulm with dissolve
    hide mei with dissolve
    "We take another route this time."
    "There are a few Grimm creatures attacking us along the way, but we defeat them easily."
    "No sign of the giant Grimm Ulm described previously. I start to hope that Ulm made a mistake or at least the Grimm has left the island."
    "Leaving the island has very low chance to happen, I know. Ulm didn't say anything regarding this giant Grimm's ability to swim."
    
    "The factory complex is--surprisingly--vast and runs deep into rock structures which dominated this island."
    "We leave the interior for later, if necessary. We agree there would be more risk and danger if we enter the factory in the cave-like part."
    show ulm nor thinking at midright with dissolve
    ulm "If I'm not mistaken, my grandpa used to work for Merlot Industry."
    show mei nor default at midleft with dissolve
    mei "As a scientist, I assume?"
    ulm nor default "I heard he's one of the youngest and the brightest researcher back then."
    ulm nor reluctant "I don't know my grandpa very well. He already passed away when I was born."
    mei nor sad "My condolences."
    ulm "Thank you."
    
    ulm nor thinking "He wasn't in good term with his kids. My father described him as a workaholic. One of my uncle said he was eccentric. Another uncle said Grandpa was crazy."
    mei nor frown2 "The last description is very rude, if I might say so."
    ulm nor surprised "{size=15}Uncle Pine is a bit rude, yes.{/size}"
    ulm nor reluctant "{size=15}But ... he did something unforgiveable. No wonder if Uncle is angry to him.{/size}"
    hide ulm with dissolve
    hide mei with dissolve
    "'Unforgiveable'?"
    "Well, there's always one or two black sheep in the family."
    
    "Sometimes they're overly eccentric ones. Or have a really bad attitude one could only wonder where do they get that from."
    "Sometimes ... they're killer and thief."
    "A loud caw startles me."
    "A couple of black birds fly away from an abandoned water tower."
    "... If it's about {i}her{/i}, rather than 'black sheep', 'black bird' would be more suitable."
    ulm "Uh ...."
    "Ulm stops walking. I hold my breath , seeing what makes Ulm stop."
    
    "There are several giant footprints on the ground. I recognize its feature as a canine-like creature."
    "But this is way too big. Even for a Grimm, this is {i}too big{/i}."
    show ulm nor surprised at mcenter with dissolve
    ulm "Oh no ...."
    ulm "We walked right into its lair ...."
    hide ulm with dissolve
    "Further ahead of us, where the ground isn't covered with metal sheet that acts as platform around this place, there's a hole."
    "Rather than a simple 'hole', it wouldn't be exaggerating to call this as a cave, big enough to house the giant creature walks this island."
    
    mei "We should turn around and find another way."
    "My feeling, however, isn't positive about our chance to leave without encountering the Grimm."
    "Before we take more than ten steps, there's a small earthquake."
    "No, correction. It's not earthquake. It's {i}footsteps{/i}."
    "Ulm's face turns pale. He mumbles something and looks really frightened, but he unlocks his crossbow and puts a bolt on it."
    show mei nor frown2 at midleft with dissolve
    show ulm arm surprised at midright with dissolve
    mei "Ulm? Think you could find us a way out without it noticing us?"
    ulm "I'll try."
    hide mei with dissolve
    hide ulm with dissolve
    
    "I unsheathe Alpenaster and follows Ulm's lead."
    "Summoning the knight and using it to fight the giant Grimm is the only thing I could think about at the moment."
    "I don't have anything in mind regarding the Grimm's physical appearance beside its size."
    "Apparently, I don't have to wait for too long to find out the answer."
    scene cg cerberus with fade
    "It's the most enormous Grimm I've seen in my life."
    "It walks on four, with visible canine features, giving me ideas that this chimaera-type is a fusion of Beowolves."
    "It has three heads, each of them turn toward us after they sniff the ground."
    "Ulm grabs my wrist, stopping me from making any movement."

    scene bg island with fade
    show ulm arm default at midright with dissolve
    ulm "Don't move, Miss Schnee."
    ulm "Not all Grimm immediately attacks. Besides, it has been here for quite a long time, without any interaction with human."
    show mei arm frown2 at midleft with dissolve
    mei "Doesn't that mean it would consider us as a threat?"
    ulm "Not always. If we provoke first, it'll definitely see us a threat, but not always if we wait."
    hide ulm with dissolve
    hide mei with dissolve
    "Some Beowolves and Creepers start gathering around the gigantic Grimm. It's common for smaller Grimm creatures to follow bigger ones. They could get themselves easy prey after the bigger Grimm initiating an attack."
    
    "Every single nerves in my body flinch when I see the three-headed Grimm walks closer to us."
    "It doesn't look like it would attack us."
    "Maybe Ulm is right and this Grimm is pretty much ... docile?"
    "I grit my teeth, struggling to overcome my instinct to attack before being attacked."
    "Ulm doesn't look any better, actually."
    "He might have said the possibility of a non-aggresive Grimm, but proving it takes a lot of emotional endurance."
    
    "The Grimm practically stands in front of us now. It is no further than 20 feet away and I could feel its warm breath."
    "Ulm slowly releases my hand. He locks his eyes with the Grimm."
    "Everything seems ... alright."
    "But that's exactly the time when I must not let my guard down."
    "The Grimm suddenly swings its claw."
    "I push Ulm down. The claw swooshes above us--"
    "--but not without hitting my shoulder."
    
    mei "!!!" with hpunch
    "Fortunately, the attack didn't deplete my Aura and left me defenseless."
    "I cast a Glyph, as big as possible, below the creature."
    "Next to me, Ulm shoots his crossbow. The bolt lodges in the giant Grimm's eye and explodes."
    "The Grimm is taken aback by that attack, giving me enough time to finish the Glyph."
    "Spiky ice chunk burst from the Glyph. It traps the giant Grimm's feet and fully freezes some smaller Grimms that decided to attack us."
    show mei arm angry at mcenter with dissolve
    mei "It wouldn't hold long! Run!"
    hide mei with qdissolve
    
    "We run as quick as we could, only stop to decide which way we should take."
    "Without knowing this island, we don't have any idea how to stop the Grimm from chasing us."
    "We have near zero chance to escape the giant Grimm once it chases us."
    "But then, we arrive at pathways built over a large pit."
    "I couldn't see its bottom when I try to take a look. Maybe it's deep enough to be called an abyss."
    show ulm arm surprised at midleft with dissolve
    ulm "If we can lure the Grimm so it'll fall down there ...."
    
    show mei arm frown2 at midright with dissolve
    mei "Good idea."
    ulm arm reluctant "{size=15}But I don't have enough data to determine the success rate of this plan!{/size}"
    mei arm cocky "Then, let's make it a gambling."
    hide ulm with dissolve
    hide mei with dissolve
    "I'm quite surprised with my own words."
    "I always prefer a well planned and calculated strategy rather than a impulsive and uncertain one."
    "... My mother, however, is impulsive when she has to come up with strategy. She isn't the most organized person in the family."
    "I think that part of her finally get into me."
    
    show mei arm frown2 at midright with dissolve
    mei "How strong do you think this pathways are?"
    mei "Could it withhold the giant Grimm's weight?"
    show ulm arm thinking at midleft with dissolve
    ulm "It will collapse once the three-headed Grimm try to step on it, I'm almost certain about it."
    ulm "Will definitely collapse if it jumps and lands on it."
    show ulm arm surprised
    show mei arm surprised
    with cdissolve
    "The ground shakes. The Grimm is getting near."
    mei "Then, let's cross over and let it chase us over these bridges."
    hide ulm with dissolve
    hide mei with dissolve
    
    "Ulm nods and doesn't mention any flaws we might have in this reckless plan."
    "I hate to admit it, but I'm sure if we discussed it further, we could point out at least a dozen of how this plan could fail."
    "The three-headed Grimm arrives. We reach the platform in the middle of the pit by now."
    show mei arm frown2 at midright with dissolve
    mei "Could it reach us here with a single leap?"
    show ulm arm default at midleft with dissolve
    ulm "Definitely."
    mei arm cocky "Great."
    ulm "Yes, that's right--wait."
    ulm arm surprised "Did you just say 'great'?"
    ulm "I'm sorry, Miss Schnee, but, it isn't the time to admire Grimm creatures!"
    
    mei arm frown "I ... I have a plan! Not admiring it!"
    mei "Some kind of a plan."
    ulm "Oh .... {size=15}Yes, I'm sorry.{/size}"
    hide ulm with dissolve
    hide mei with dissolve
    "I don't even need to wait. The Grimm jumps toward us almost immediately."
    "Quickly, before the Grimm lands, I cast another Glyph."
    "Mid-air, the Grimm wouldn't be able to dodge any incoming attacks. This is a good chance to eliminate it."
    "The knight's huge sword appears from the Glyph before me. I swing it toward the Grimm, try to cut at least one of its three heads."
    "However, it manages to block the attack with its claw, even though it leaves a deep cut."
    "I channel more Aura, force the sword to cut deeper, but to no avail."
    
    "The Grimm lands in front of us. At the same time, the sword shatters and I feel something rippling through my body."
    "Oh, no ...."
    "I overuse my Aura. It would take some time before it fully recovered."
    "But before the Grimm launches another attack, Ulm shoots first."
    "I catch a glimpse of his crossbow bolt and realize there's something attached on it."
    "Before I recognize the extra attachment, Ulm pushes me down."
    show ulm arm surprised at mcenter with dissolve
    ulm "I-I'm sorry, Miss Schnee!"
    hide ulm with dissolve
    
    "There is a blast, right above us, then a gush of hot wind and then an angry roar."
    "A series of broken and bent metal noise follows. I could hear furious scratches too."
    "Next to me, Ulm reloads another bolt into his crossbow. From the yellow mark on the bolt, I know it's loaded with electricity Dust."
    "On the other end of the platform, the three-headed Grimm claws the bridge, try to pull itself up. The blast was strong enough to hurt it badly and destroy part of the metal platform."
    "Ulm's bolt find its target between the left and the middle head, despite of the Grimm moving so much."
    "Jolt of electricity stuns the creature, for a very short moment stops its deliberate struggle to climb."
    "Even though it's just less than five seconds, the electric shock is enough to make the Grimm falls down to the bottom of the pit."
    
    show mei arm surprised at midright with dissolve
    mei "That's ... brilliant."
    "I let out a nervous laugh, relieved that we're so lucky we're still alive."
    show ulm arm surprised at midleft with dissolve
    ulm "It's because you already hurt it, Miss Schnee."
    ulm arm thinking "Your attack also interrupted its leap. It stopped and rethought its next action, giving me enough time to finish a simple modification on my bolt."
    mei arm default "I saw it. You attached something on the bolt. What was it?"
    ulm arm surprised "A ... prototype."
    
    ulm arm reluctant "Recently, Rhodo and I talked about simple explosives made of fire Dust which should be useful when we need to take down giant-sized Grimm."
    ulm "We haven't experimented further. I forgot to store the prototype after telling Rhodo about it this morning."
    mei arm surprised "...."
    mei arm frown2 "So ... we're really lucky with the Grimm."
    ulm arm default "Yes."

    "I look at the sky and see dark clouds have gathered. Somewhere far away, thunder strikes."
    ulm arm default "Storm is coming."
    mei "Yeah. Time to find a shelter for the night."
    hide mei with dissolve
    hide ulm with dissolve

    "The smaller Grimms have retreated after seeing the three-headed Grimm fell down into the pit, but we both proceed cautiously."
    "We find a small building that looks like a control post, on the other side of the cliff."
    
    scene bg outpostnight with fade
    "However, when we enter the place, we don't find any machine inside."
    "It's either the machine had been dismantled or this isn't a control panel room in the first place."
    "Maybe it's a security post?"
    "I don't really bother to inspect the place more closely."
    
    "Tired, I sit in the corner and close my eyes."
    "Even though I doubt it at first, it doesn't take long for me to drift into sleep."
    
    scene black with fade
    call chapter("End of Part 8", "Stranded") from _call_chapter_13
    centered "End of Demo Version 0.8\nThank you for playing!"
    
    return