init python:
    
    def summon_callback(drags, drop):
        
        if not drop:
            return
        
        store.glyph = drop.drag_name
        
        return True
        
    def other_callback(drags, drop):
        if not drop:
            return
        
        store.glyph = drop.drag_name
        
        return True

screen glyph_first:
    draggroup:
         # Orb
        drag:
            drag_name "Orb"
            child "images/orb.png"
            droppable False
            dragged other_callback
            xpos 640 ypos 600
        
        # Summon
        drag:
            drag_name "Platform"
            child "images/glyph.png"
            draggable False
            xpos 640 ypos 25
        
screen glyph_summon_small:
    draggroup:
        # Orb
        drag:
            drag_name "Orb"
            child "images/orb.png"
            droppable False
            dragged summon_callback
            xpos 640 ypos 600
        
        # Summon
        drag:
            drag_name "Boarbatusk"
            child "images/glyph.png"
            draggable False
            xpos 200 ypos 200
            
        drag:
            drag_name "Nevermore"
            child "images/glyph.png"
            draggable False
            xpos 550 ypos 200


screen glyph_summon_big:
    draggroup:
        # Orb
        drag:
            drag_name "Orb"
            child "images/orb.png"
            droppable False
            dragged summon_callback
            xpos 640 ypos 600
        
        # Summon
        drag:
            drag_name "Knight"
            child "images/glyph.png"
            draggable False
            xpos 640 ypos 25